# * ********************************************************************************************************* *
# *
# * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017-2018 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

cmake_minimum_required(VERSION 3.6)

project(<? @var project.name ?>)

set(BUILD_TYPE <? @var build.type ?>)
set(PLATFORM_TYPE <? @var build.platform ?>)
set(TOOLCHAIN_TYPE <? @var project.target.toolchain ?>)

if (BUILD_TYPE STREQUAL "dev")
    set(CMAKE_BUILD_TYPE Debug)
    add_definitions(-DDEV)
else ()
    set(CMAKE_BUILD_TYPE Release)
endif ()

# Only generate Debug and Release configuration types which corresponds to WUI build system.
set(CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE STRING "" FORCE)

set(COVERAGE_FLAG "")
if (DEFINED COVERAGE)
    set(COVERAGE_FLAG "--coverage")
endif ()

macro(PARSE_ARGS args)
    set(ARGS_ITEMS "")
    set(ARGS_SWITCHES "")
    set(ARGS_VALUES "")

    string(REPLACE "," ";" repArgs "${args}")
    if (PARSE_ARGS_VERBOSE)
        message("arg_replaced: ${repArgs}")
    endif ()

    foreach (item ${repArgs})
        string(REGEX MATCH "(-[a-zA-z]+)(=(.*))?$" output ${item})
        if (output)
            if (CMAKE_MATCH_3)
                if (PARSE_ARGS_VERBOSE)
                    message("arg_match_val: ${CMAKE_MATCH_1} with ${CMAKE_MATCH_3}")
                endif ()
                list(APPEND ARGS_VALUES ${CMAKE_MATCH_1}=${CMAKE_MATCH_3})
            else (CMAKE_MATCH_3)
                if (PARSE_ARGS_VERBOSE)
                    message("arg_match_switch: ${CMAKE_MATCH_1}")
                endif ()
                list(APPEND ARGS_SWITCHES ${CMAKE_MATCH_1})
            endif ()
        else (output)
            if (PARSE_ARGS_VERBOSE)
                message("arg_match_item: ${item}")
            endif ()
            list(APPEND ARGS_ITEMS ${item})
        endif ()
    endforeach ()
endmacro()

# Shared configuration of CMAKE
if (NOT CMAKE_CXX_STANDARD)
    set(CMAKE_CXX_STANDARD 17)
endif ()
set(BUILD_SHARED_LIBS OFF)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build/target)

if (EXISTS ${CMAKE_SOURCE_DIR}/build/compiled)
    include_directories(${CMAKE_SOURCE_DIR}/build/compiled)
endif ()

# Find each source files of current project
FILE(GLOB_RECURSE SOURCE_FILES ${CMAKE_SOURCE_DIR}/source/cpp/*.cpp)
list(REMOVE_ITEM SOURCE_FILES "${CMAKE_SOURCE_DIR}/source/cpp/ApplicationMain.cpp")
FILE(GLOB_RECURSE TEST_FILES ${CMAKE_SOURCE_DIR}/test/unit/cpp/*.cpp)

if (WIN32)
    set(CMAKE_SHARED_LIBRARY_PREFIX "")
else ()
    set(CMAKE_POSITION_INDEPENDENT_CODE ON)
endif ()

set(TEST_TARGET_NAME ${PROJECT_NAME}.lib)
if (PLATFORM_TYPE STREQUAL "shared" OR PLATFORM_TYPE STREQUAL "static")
    set(TEST_TARGET_NAME <? @var project.target.name ?>)
    if (PLATFORM_TYPE STREQUAL "shared")
        FILE(GLOB_RECURSE DEF_FILES ${CMAKE_SOURCE_DIR}/build/compiled/resource/configs/*.def)

        if (WIN32)
            add_library(<? @var project.target.name ?>
                    SHARED
                    ${CMAKE_SOURCE_DIR}/build/compiled/version.rc
                    ${CMAKE_SOURCE_DIR}/source/cpp/reference.hpp
                    ${DEF_FILES}
                    ${SOURCE_FILES}
                    ${EMBEDDED_RESOURCES})

            set_target_properties(<? @var project.target.name ?>
                    PROPERTIES
                    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/bin")
        else ()
            add_library(<? @var project.target.name ?>
                    SHARED
                    ${CMAKE_SOURCE_DIR}/source/cpp/reference.hpp
                    ${DEF_FILES}
                    ${SOURCE_FILES}
                    ${EMBEDDED_RESOURCES})

            set_target_properties(<? @var project.target.name ?>
                    PROPERTIES
                    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/bin")
        endif ()

        FILE(GLOB_RECURSE C_SOURCE_FILES ${CMAKE_SOURCE_DIR}/source/cpp/*.c ${CMAKE_SOURCE_DIR}/source/cpp/*.h)

        if (C_SOURCE_FILES)
            add_library(${PROJECT_NAME}.lib
                    STATIC
                    ${C_SOURCE_FILES})
            set(TEST_TARGET_NAME ${PROJECT_NAME}.lib)
            set_target_properties(${PROJECT_NAME}.lib
                    PROPERTIES
                    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/lib"
                    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/lib")

            add_dependencies(<? @var project.target.name ?> ${PROJECT_NAME}.lib)
        else ()
            message(FATAL_ERROR "Shared library without pure C interface is not currently supported.")
        endif ()
    else ()
        add_library(<? @var project.target.name ?>
                STATIC
                ${CMAKE_SOURCE_DIR}/source/cpp/reference.hpp
                ${SOURCE_FILES}
                ${EMBEDDED_RESOURCES})

        set_target_properties(<? @var project.target.name ?>
                PROPERTIES
                ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/lib"
                LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/lib")
    endif ()
else ()
    if (WIN32)
        add_executable(<? @var project.target.name ?>
                ${CMAKE_SOURCE_DIR}/source/cpp/reference.hpp
                ${SOURCE_FILES} ${CMAKE_SOURCE_DIR}/build/compiled/version.rc
                ${CMAKE_SOURCE_DIR}/build/compiled/reflectionData.hpp
                ${CMAKE_SOURCE_DIR}/build/compiled/source/cpp/ApplicationMain.cpp
                ${EMBEDDED_RESOURCES})
    else ()
        add_executable(<? @var project.target.name ?>
                ${CMAKE_SOURCE_DIR}/source/cpp/reference.hpp
                ${SOURCE_FILES}
                ${CMAKE_SOURCE_DIR}/build/compiled/reflectionData.hpp
                ${CMAKE_SOURCE_DIR}/build/compiled/source/cpp/ApplicationMain.cpp
                ${EMBEDDED_RESOURCES})
    endif ()
    add_library(${PROJECT_NAME}.lib
            ${CMAKE_SOURCE_DIR}/source/cpp/reference.hpp
            ${SOURCE_FILES}
            ${CMAKE_SOURCE_DIR}/build/compiled/reflectionData.hpp)
endif ()

add_library(internal-unit-test-runner
        SHARED
        ${CMAKE_SOURCE_DIR}/bin/resource/scripts/UnitTestRunner.hpp
        ${CMAKE_SOURCE_DIR}/bin/resource/scripts/UnitTestRunner.cpp
        ${CMAKE_SOURCE_DIR}/test/unit/cpp/reference.hpp
        ${TEST_FILES})

if (WIN32)
    set_target_properties(internal-unit-test-runner
            PROPERTIES
            ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/lib"
            LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/lib"
            RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/bin")
else ()
    set_target_properties(internal-unit-test-runner
            PROPERTIES
            ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/lib"
            LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build/compiled/bin")
endif ()

add_executable(UnitTestRunner
        ${CMAKE_SOURCE_DIR}/bin/resource/scripts/UnitTestLoader.cpp)

add_dependencies(UnitTestRunner internal-unit-test-runner)

# Add gtest framework includes
target_include_directories(internal-unit-test-runner
        BEFORE PUBLIC "<? @var properties.externalModules ?>/gtest/googletest/include"
        BEFORE PUBLIC "<? @var properties.externalModules ?>/gtest/googlemock/include")

if (WIN32)
    message(STATUS "Detected platform: WIN\n")
    add_definitions(-DWIN_PLATFORM)

    set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/build_cache/win)

    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static")

    if (TOOLCHAIN_TYPE STREQUAL "gcc")
        # Set general GCC Compiler/Linker flags
        set(CMAKE_C_FLAGS_DEBUG "-O0 -g ${COVERAGE_FLAG} ")
        set(CMAKE_C_FLAGS_RELEASE "-O2 -fdata-sections -ffunction-sections -DNDEBUG ")
        set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g ${COVERAGE_FLAG} -Wsign-compare -Wparentheses ")
        set(CMAKE_CXX_FLAGS_RELEASE "-O2 -fdata-sections -ffunction-sections -DNDEBUG -Wsign-compare -Wparentheses ")
        set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${COVERAGE_FLAG} ")
        set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-Wl,-O1 -Wl,--as-needed -Wl,--gc-sections ")

        # Add gtest framework libs
        if (PLATFORM_TYPE STREQUAL "shared")
            target_link_libraries(internal-unit-test-runner
                    ${TEST_TARGET_NAME}
                    <? @var project.target.name ?>
                    "<? @var properties.externalModules ?>/gtest/build_gcc/googlemock/gtest/libgtest.a"
                    "<? @var properties.externalModules ?>/gtest/build_gcc/googlemock/libgmock.a")
        else ()
            target_link_libraries(internal-unit-test-runner
                    ${TEST_TARGET_NAME}
                    "<? @var properties.externalModules ?>/gtest/build_gcc/googlemock/gtest/libgtest.a"
                    "<? @var properties.externalModules ?>/gtest/build_gcc/googlemock/libgmock.a")
        endif ()
    else ()
        # Set general MSVC Compiler/Linker flags
        set(CMAKE_C_FLAGS "/DWIN64 /D_WINDOWS /W3 ")
        set(CMAKE_C_FLAGS_DEBUG "/MTd /Od /RTC1 /D_DEBUG /ZI ")
        set(CMAKE_C_FLAGS_RELEASE "/MT /Ox /D_NDEBUG /ZI ")
        set(CMAKE_CXX_FLAGS "/DWIN64 /D_WINDOWS /W3 /GR /EHsc ")
        set(CMAKE_CXX_FLAGS_DEBUG "/MTd /Od /RTC1 /D_DEBUG /ZI ")
        set(CMAKE_CXX_FLAGS_RELEASE "/MT /Ox /D_NDEBUG /ZI ")
        set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS} /NODEFAULTLIB:LIBCMT /NODEFAULTLIB:LIBCPMT ")
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS} /NODEFAULTLIB:LIBCMT /NODEFAULTLIB:LIBCPMT ")

        # Add gtest framework libs
        target_link_libraries(UnitTestRunner
                ${TEST_TARGET_NAME}
                <? @var properties.externalModules ?>/gtest/build_msvc/googlemock/gtest/gtestd.lib
                <? @var properties.externalModules ?>/gtest/build_msvc/googlemock/gmockd.lib)
    endif ()

elseif (UNIX AND NOT APPLE)
    message(STATUS "Detected platform: LINUX\n")
    add_definitions(-DLINUX_PLATFORM)

    set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/build_cache/linux)

    # Set general Compiler/Linker flags
    set(CMAKE_C_FLAGS_DEBUG "-O0 -g ${COVERAGE_FLAG} ")
    set(CMAKE_C_FLAGS_RELEASE "-O2 -fdata-sections -ffunction-sections -DNDEBUG ")
    set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g ${COVERAGE_FLAG} -Wsign-compare -Wparentheses ")
    set(CMAKE_CXX_FLAGS_RELEASE "-O2 -fdata-sections -ffunction-sections -DNDEBUG -Wsign-compare -Wparentheses ")
    set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${COVERAGE_FLAG} ")
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-Wl,-O1 -Wl,--as-needed -Wl,--gc-sections ")

    target_link_libraries(UnitTestRunner dl pthread)

    # Add gtest framework libs
    set(ADITIONAL_TEST_TARGET)
    if (PLATFORM_TYPE STREQUAL "shared")
        set(ADITIONAL_TEST_TARGET <? @var project.target.name ?>)
    endif ()

    if (TOOLCHAIN_TYPE STREQUAL "gcc")
        target_link_libraries(internal-unit-test-runner
                ${TEST_TARGET_NAME}
                ${ADITIONAL_TEST_TARGET}
                <? @var properties.externalModules ?>/gtest/build_gcc/googlemock/gtest/libgtest.a
                <? @var properties.externalModules ?>/gtest/build_gcc/googlemock/libgmock.a)
    else ()
        target_link_libraries(internal-unit-test-runner
                ${TEST_TARGET_NAME}
                ${ADITIONAL_TEST_TARGET}
                <? @var properties.externalModules ?>/gtest/build_arm/googlemock/gtest/libgtest.a
                <? @var properties.externalModules ?>/gtest/build_arm/googlemock/libgmock.a)
    endif ()

elseif (APPLE)
    message(STATUS "Detected platform: MAC\n")
    add_definitions(-DMAC_PLATFORM)

    set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/build_cache/mac)

    # Set general Compiler/Linker flags
    set(CMAKE_C_FLAGS_DEBUG "-O0 -g ${COVERAGE_FLAG} ")
    set(CMAKE_C_FLAGS_RELEASE "-O2 -fdata-sections -ffunction-sections -DNDEBUG ")
    set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g ${COVERAGE_FLAG} -Wsign-compare -Wparentheses ")
    set(CMAKE_CXX_FLAGS_RELEASE "-O2 -fdata-sections -ffunction-sections -DNDEBUG -Wsign-compare -Wparentheses ")
    set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${COVERAGE_FLAG} ")
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-Wl,-O1 -Wl,--as-needed -Wl,--gc-sections ")

    # Add gtest framework libs
    if (TOOLCHAIN_TYPE STREQUAL "gcc")
        target_link_libraries(internal-unit-test-runner
                ${TEST_TARGET_NAME}
                <? @var properties.externalModules ?>/gtest/build_gcc/googlemock/gtest/libgtest.a
                <? @var properties.externalModules ?>/gtest/build_gcc/googlemock/libgmock.a)
     elseif (TOOLCHAIN_TYPE STREQUAL "clang")
        target_link_libraries(internal-unit-test-runner
                ${TEST_TARGET_NAME}
                <? @var properties.externalModules ?>/gtest/build_clang/googlemock/gtest/libgtest.a
                <? @var properties.externalModules ?>/gtest/build_clang/googlemock/libgmock.a)
     endif()
endif ()

# Add common dependencies if exist
<? @forEach templates.cmake.commonDependency in properties.cppDependencies ?>
# Add WUI libraries if exist
<? @forEach templates.cmake.wuiDependency in properties.wuiDependencies ?>

# Rise application callbacks if exist
if (COMMAND APPLICATION_MAIN_CALLBACK)
    message("Starting custom application configuration callback...")
    APPLICATION_MAIN_CALLBACK(<? @var project.target.name ?>)
    message("Custom application configuration callback finished.")
endif ()

if (COMMAND APPLICATION_TEST_CALLBACK)
    message("Starting custom test configuration callback...")
    APPLICATION_TEST_CALLBACK(internal-unit-test-runner ${TEST_TARGET_NAME})
    message("Custom test configuration callback finished.")
endif ()

if (COMMAND APPLICATION_WUI_DEPENDENCIES_CALLBACK)
    message("Starting custom wui dependencies configuration callback...")
    APPLICATION_WUI_DEPENDENCIES_CALLBACK(${PROJECT_NAME}_wui_dependencies.lib)
    message("Custom WUI dependencies callback finished.")
endif ()

# Print project config
message(STATUS "*** CMAKE CONFIGURATION SETTINGS ***")
message(STATUS "Generator:                      ${CMAKE_GENERATOR}")
message(STATUS "Platform:                       ${CMAKE_SYSTEM_NAME}")
message(STATUS "Architecture:                   ${PROJECT_ARCH}")
message(STATUS "Build type:                     ${CMAKE_BUILD_TYPE}")
message(STATUS "Build platform type:            ${PLATFORM_TYPE}")
message(STATUS "Build toolchain type:           ${TOOLCHAIN_TYPE}")
message(STATUS "Binary root:                    ${CMAKE_BINARY_DIR}")
message(STATUS "Compile C defines:              ${CMAKE_C_FLAGS}")
message(STATUS "Compile C defines (DEBUG):      ${CMAKE_C_FLAGS_DEBUG}")
message(STATUS "Compile C defines (RELEASE):    ${CMAKE_C_FLAGS_RELEASE}")
message(STATUS "Compile C++ defines:            ${CMAKE_CXX_FLAGS}")
message(STATUS "Compile C++ defines (DEBUG):    ${CMAKE_CXX_FLAGS_DEBUG}")
message(STATUS "Compile C++ defines (RELEASE):  ${CMAKE_CXX_FLAGS_RELEASE}")
message(STATUS "Exe link flags:                 ${CMAKE_EXE_LINKER_FLAGS}")
message(STATUS "Exe link flags (DEBUG):         ${CMAKE_EXE_LINKER_FLAGS_DEBUG}")
message(STATUS "Exe link flags (RELEASE):       ${CMAKE_EXE_LINKER_FLAGS_RELEASE}")
