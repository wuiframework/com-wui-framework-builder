# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017-2018 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(BOOST_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running BOOST_CONFIGURE with (\n\tapp_target: ${app_target},\n\ttest_target: ${test_target},\n\tattributes: ${attributes}\n).")

    PARSE_ARGS("${attributes}")

    set(BOOST_INCLUDE_DIRS )
    set(BOOST_LIBRARIES )

    if (UNIX)
        if (TOOLCHAIN_TYPE STREQUAL "arm")
            set(BOOST_LIBRARYDIR ${path}/stage/gcc-arm/lib)
         elseif (TOOLCHAIN_TYPE STREQUAL "clang")
            set(BOOST_LIBRARYDIR ${path}/stage/clang/lib)
        else ()
            set(BOOST_LIBRARYDIR ${path}/stage/gcc-7/lib)
        endif ()
    else ()
        set(BOOST_LIBRARYDIR ${path}/stage/lib)
        set(BOOST_ROOT ${path})
    endif ()

    foreach (item ${ARGS_VALUES})
        if (${item} MATCHES "-libs=(.*)$")
            if(IS_ABSOLUTE ${CMAKE_MATCH_1})
                set(BOOST_LIBRARYDIR ${CMAKE_MATCH_1})
            elseif(IS_ABSOLUTE ${CMAKE_MATCH_1})
                set(BOOST_LIBRARYDIR "${path}/${CMAKE_MATCH_1}")
            endif()

            message("found attribute -libs: ${CMAKE_MATCH_1}")
            message("combined path: ${BOOST_LIBRARYDIR}")
        endif ()
    endforeach ()

    set(Boost_COMPONENTS ${ARGS_ITEMS})
    set(Boost_USE_STATIC_LIBS ON)
    set(Boost_USE_MULTITHREADED ON)
    set(BOOST_ROOT ${path})
    if (Boost_COMPONENTS)
        find_package(Boost ${version} REQUIRED COMPONENTS ${Boost_COMPONENTS})
    else ()
        find_package(Boost ${version})
    endif ()

    if (Boost_FOUND)
        set(BOOST_INCLUDE_DIRS ${Boost_INCLUDE_DIR})
        set(BOOST_LIBRARIES ${Boost_LIBRARIES})

        target_include_directories(${app_target} SYSTEM PUBLIC ${BOOST_INCLUDE_DIRS})
        target_include_directories(${test_target} SYSTEM PUBLIC ${BOOST_INCLUDE_DIRS})
        target_include_directories(${test_lib_target} SYSTEM PUBLIC ${BOOST_INCLUDE_DIRS})

        target_link_libraries(${app_target} ${BOOST_LIBRARIES})
        target_link_libraries(${test_target} ${BOOST_LIBRARIES})
        target_link_libraries(${test_lib_target} ${BOOST_LIBRARIES})
    endif ()

endmacro()
