/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BaseHttpResolver = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver;
    import ThreadPool = Com.Wui.Framework.Commons.Events.ThreadPool;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HttpResolver = Com.Wui.Framework.Commons.HttpProcessor.HttpResolver;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import HttpRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs;
    import BaseOutputHandler = Com.Wui.Framework.Commons.IOApi.Handlers.BaseOutputHandler;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import LogLevel = Com.Wui.Framework.Commons.Enums.LogLevel;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;
    import Counters = Com.Wui.Framework.Commons.Utils.Counters;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;
    import HttpManager = Com.Wui.Framework.Commons.HttpProcessor.HttpManager;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export abstract class UnitTestRunner extends BaseObject {
        private static windowCache : MockWindow;
        private static loader : any;
        protected driver : any;
        protected by : any;
        protected vbox : IVBoxManager;
        private readonly unitRootPath : string;
        private readonly mockServerLocation : string;
        private methodFilter : string[];
        private readonly environment : any;
        private defaultTimeout : number = 10000; // ms
        private currentTimeout : number;
        private isFailing : boolean;
        private testName : string;
        private passingCount : number;
        private failingCount : number;
        private asyncHandler : any;
        private assertErrors : Error[];

        constructor() {
            super();

            if (isBrowserRun) {
                this.driver = chromeBuilder.build();
                this.driver.manage().window().maximize();
                this.by = selenium.By;
            }
            this.vbox = vbox;

            this.prepareAssert();

            this.methodFilter = [];

            if (!ObjectValidator.IsSet((<any>EnvironmentArgs).prototype.Load)) {
                this.environment = new (<any>EnvironmentArgs)(
                    "com-wui-framework-builder", "2.0.0", new Date().toTimeString(), false,
                    "Com.Wui", true);
                if (ObjectValidator.IsSet((<any>Com.Wui.Framework).Localhost)) {
                    this.environment.appName = "UnitTest";
                }
            } else {
                this.environment = {
                    name      : "com-wui-framework-builder",
                    version   : "2.0.0",
                    build     : {
                        time: new Date().toTimeString(),
                        type: "dev"
                    },
                    namespaces: ["Com.Wui"],
                    target    : {
                        name   : "UnitTest",
                        metrics: {
                            enabled : false,
                            location: ""
                        }
                    },
                    domain    : {
                        location: "localhost"
                    }
                };
            }

            this.initLoader();
            this.currentTimeout = this.defaultTimeout;
            this.isFailing = false;
            this.passingCount = 0;
            this.failingCount = 0;
            this.asyncHandler = null;
            this.assertErrors = [];
        }

        public Process($done : () => void) : void {
            this.resolver($done);
        }

        protected timeoutLimit($value? : number) : number {
            return this.currentTimeout = Property.Integer(this.currentTimeout, $value);
        }

        protected before() : void | IUnitTestRunnerPromise {
            // override this method for ability to execute code BEFORE run of ALL tests in current UnitTest
            return;
        }

        protected setUp() : void | IUnitTestRunnerPromise {
            // override this method for ability to execute code BEFORE EACH test in current UnitTest
            return;
        }

        protected tearDown() : void | IUnitTestRunnerPromise {
            // override this method for ability to execute code AFTER EACH test in current UnitTest
            return;
        }

        protected after() : void | IUnitTestRunnerPromise {
            // override this method for ability to execute code AFTER run of ALL tests in current UnitTest
            return;
        }

        protected setMethodFilter(...$names : string[]) : void {
            $names.forEach(($name : string) : void => {
                this.methodFilter.push(StringUtils.ToLowerCase($name));
            });
        }

        protected stripInstrumentation($input : string) : string {
            return ($input + "")
                .replace(/(cov_.*\+\+;)/gm, "")
                .replace(/\(cov_.*?\+\+, (.*?)(\))?\)/g, ($match : string, $value : string, $postfix : string) : string => {
                    return $value + (!ObjectValidator.IsEmptyOrNull($postfix) ? $postfix : "");
                });
        }

        protected resetCounters() : void {
            Counters.Clear();
            (<any>BaseOutputHandler).handlerIterator = 0;
        }

        protected registerElement($id : string, $tagName? : string) : void {
            if (!ObjectValidator.IsSet($tagName)) {
                $tagName = "div";
            }
            let element = document.createElement($tagName);
            element.id = $id;
            document.body.appendChild(element);
        }

        protected reload() : void {
            if (!ObjectValidator.IsEmptyOrNull(UnitTestRunner.loader.getInstance())) {
                this.getHttpManager().RefreshWithoutReload();
            }
        }

        protected setUrl($value : string) : void {
            window.location.href = $value;
            jsdom.reconfigure({url: $value});
            this.reload();
        }

        protected setUserAgent($browserTypeOrValue : BrowserType | string, $modern : boolean = true) : void {
            let value : string = UnitTestRunner.windowCache.navigator.userAgent;
            switch ($browserTypeOrValue) {
            case BrowserType.INTERNET_EXPLORER:
                if ($modern) {
                    value =
                        "Mozilla/4.0 (.NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                        "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E; rv:11.0)";
                } else {
                    value =
                        "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; " +
                        "Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                        "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E)";
                }
                break;
            case BrowserType.FIREFOX:
                value = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0";
                break;
            case BrowserType.GOOGLE_CHROME:
                value =
                    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                    "Chrome/52.0.2743.116 Safari/537.36";
                break;
            case BrowserType.OPERA:
                value =
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                    "Chrome/52.0.2743.116 Safari/537.36 OPR/39.0.2256.71";
                break;
            case BrowserType.SAFARI:
                value = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2";
                break;
            default:
                if (!ObjectValidator.IsEmptyOrNull($browserTypeOrValue)) {
                    value = <string>$browserTypeOrValue;
                }
                break;
            }
            this.forSetUserAgent(value);
            this.reload();
        }

        protected setCookieItem($key : string, $value : string) : void {
            document.cookie = $key + "=" + $value + ";";
            this.reload();
        }

        protected setStorageItem($key : string, $value : string) : void {
            localStorage.setItem($key, $value);
            this.reload();
        }

        protected getAbsoluteRoot() : string {
            return this.unitRootPath;
        }

        protected getMockServerLocation() : string {
            return this.mockServerLocation;
        }

        protected initLoader($className? : any) : void {
            if (ObjectValidator.IsEmptyOrNull($className)) {
                if (ObjectValidator.IsEmptyOrNull(UnitTestRunner.loader)) {
                    let loaderClassName = "<? @var project.loaderClass ?>";
                    if (ObjectValidator.IsEmptyOrNull(loaderClassName)) {
                        loaderClassName = "Com.Wui.Framework.Commons.Loader";
                    }
                    let classObject = window;
                    loaderClassName.split(".").forEach(($part : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull(classObject[$part])) {
                            classObject = classObject[$part];
                        }
                    });
                    UnitTestRunner.loader = classObject;
                }
            } else {
                UnitTestRunner.loader = $className;
            }
        }

        protected initSendBox() : void {
            this.setUrl(UnitTestRunner.windowCache.location.href);
            (<MockWindow>window).location.headers = UnitTestRunner.windowCache.location.headers;
            this.setUserAgent(BrowserType.FIREFOX);

            this.registerElement("Content");
            UnitTestRunner.loader.Load(this.environment);
            ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST);
            if ((<any>Com.Wui.Framework).Gui) {
                (<any>Com.Wui.Framework).Gui.Utils.StaticPageContentManager.Clear(true);
            }
            document.cookie = "";
            localStorage.clear();
            document.documentElement.innerHTML = "";
            this.registerElement("Content");
            Echo.Init("Content", true);

            (<any>ThreadPool).events = new EventsManager();

            delete (<any>LogIt).output;
            delete (<any>LogIt).level;
            delete (<any>LogIt).owner;
            LogIt.Init(LogLevel.ALL);
            console.log = ($message : string) : void => {
                if (!StringUtils.ContainsIgnoreCase($message, "__ASYNC_ASSERT_FAIL__")) {
                    console.info($message);
                    if (StringUtils.StartsWith($message, "ERROR:")) {
                        throw new Error(this.testName + "\n" + $message);
                    }
                }
            };
            if ((<any>Com.Wui.Framework).Localhost) {
                process.env.NODE_PATH = process.cwd() + "/dependencies/nodejs/build/node_modules";
            }
        }

        protected resolver($done : () => void) : void {
            if (!ObjectValidator.IsSet(UnitTestRunner.windowCache)) {
                UnitTestRunner.windowCache = <MockWindow>{
                    location : {
                        hash    : window.location.hash,
                        headers : (<MockWindow>window).location.headers,
                        host    : window.location.host,
                        hostname: window.location.hostname,
                        href    : window.location.href,
                        origin  : window.location.origin,
                        pathname: window.location.pathname,
                        port    : window.location.port,
                        protocol: window.location.protocol,
                        search  : window.location.search
                    },
                    navigator: {
                        cookieEnabled: window.navigator.cookieEnabled,
                        language     : window.navigator.language,
                        onLine       : window.navigator.onLine,
                        platform     : window.navigator.platform,
                        userAgent    : window.navigator.userAgent
                    }
                };
                this.initSendBox();
            }

            const tests : string[] = [];
            let startTime : number;
            let beforeTest : string;
            let integrityTestReport : string = "";
            let timeoutId : number = null;

            const stopTimeout : any = () : void => {
                if (timeoutId !== null) {
                    clearTimeout(timeoutId);
                }
            };
            const createTimeout : any = ($handler : any) : void => {
                stopTimeout();
                if (this.currentTimeout > -1) {
                    timeoutId = <any>setTimeout(() : void => {
                        this.isFailing = true;
                        __unitTestRunner.report.assert(this.testName,
                            new Error("Test case/suite has reached timeout (" + this.currentTimeout + " ms)"));
                        stopTimeout();
                        $handler();
                    }, this.currentTimeout);
                }
            };
            const handlePromiseCall : any = ($promise : any, $handler : () => void) : void => {
                this.asyncHandler = null;
                try {
                    const promiseInterface : any = $promise.apply(this, []);
                    if (ObjectValidator.IsFunction(promiseInterface)) {
                        this.asyncHandler = () : void => {
                            stopTimeout();
                            $handler();
                        };
                        createTimeout($handler);
                        promiseInterface.apply(this, [this.asyncHandler]);
                    } else {
                        $handler();
                    }
                } catch (ex) {
                    if (!this.isFailing) {
                        this.isFailing = false;
                        __unitTestRunner.report.error(this.testName, ex);
                    }
                    if (ObjectValidator.IsEmptyOrNull(this.asyncHandler)) {
                        $handler();
                    }
                }
            };
            const integrityCheck : any = () : string => {
                let guiObjectManagerCRC : string = "";
                let bodyContentCRC : string = "";
                if (ObjectValidator.IsSet((<any>this.getHttpResolver()).getGuiRegister)) {
                    guiObjectManagerCRC =
                        "    GuiObjectManager CRC: " + StringUtils.getCrc((<any>this.getHttpResolver()).getGuiRegister().ToString("", false)) + "\n";
                }
                if (!ObjectValidator.IsSet((<any>Com.Wui.Framework).Localhost)) {
                    bodyContentCRC =
                        "    Body content CRC: " + StringUtils.getCrc(document.documentElement.innerHTML) + "\n";
                }

                return "\n" +
                    "    EventsManager CRC: " + StringUtils.getCrc(this.getEventsManager().ToString("", false)) + "\n" +
                    "    ExceptionsManager CRC: " + StringUtils.getCrc(ExceptionsManager.getAll().ToString("", false)) + "\n" +
                    "    HttpManager CRC: " + StringUtils.getCrc(this.getHttpManager().getResolversCollection().ToString("", false)) + "\n" +
                    "    Environment CRC: " + StringUtils.getCrc(this.getEnvironmentArgs().ToString("", false)) + "\n" +
                    bodyContentCRC +
                    guiObjectManagerCRC;
            };
            const reportError : any = () : void => {
                this.assertErrors.forEach(($ex : Error) : void => {
                    __unitTestRunner.report.assert(this.testName, $ex);
                });
                this.assertErrors = [];
                __unitTestRunner.report.fail(this.testName, new Date().getTime() - startTime);
            };
            const getNextTest : any = ($index : number, $callback : () => void) : void => {
                this.isFailing = false;
                this.assertErrors = [];
                if ($index < tests.length) {
                    try {
                        const method : string = tests[$index];
                        const testName : string = StringUtils.Remove(method, "test", "__Ignore");
                        startTime = new Date().getTime();
                        this.testName = this.getClassName() + " - " + testName;

                        if (this.methodFilter.length === 0 && StringUtils.StartsWith(method, "__Ignore")
                            || this.methodFilter.length !== 0 && (
                                this.methodFilter.indexOf(StringUtils.ToLowerCase(testName)) === -1 &&
                                this.methodFilter.indexOf(StringUtils.ToLowerCase("test" + testName)) === -1 &&
                                this.methodFilter.indexOf(StringUtils.ToLowerCase(method)) === -1)) {
                            __unitTestRunner.report.skip(this.testName);
                            getNextTest($index + 1, $callback);
                        } else {
                            const tearDownHandler : any = () : void => {
                                if (!this.isFailing) {
                                    this.isFailing = false;
                                    this.passingCount++;
                                    __unitTestRunner.report.pass(this.testName, new Date().getTime() - startTime);
                                } else {
                                    this.failingCount++;
                                    reportError();
                                }
                                __unitTestRunner.events.onTestCaseEnd(this.testName);
                                const afterTest : string = integrityCheck();
                                if (beforeTest !== afterTest) {
                                    integrityTestReport +=
                                        "\x1b[31m" +
                                        "WARNING: Integrity test failure at: " + this.testName + "\n" +
                                        "Expected: " + beforeTest + "\n" +
                                        "Actual: " + afterTest +
                                        "\x1b[0m";
                                }
                                getNextTest($index + 1, $callback);
                            };

                            const testCaseHandler : any = () : void => {
                                this.currentTimeout = this.defaultTimeout;
                                handlePromiseCall(this.tearDown, tearDownHandler);
                            };

                            const setUpHandler : any = () : void => {
                                __unitTestRunner.events.onTestCaseStart(this.testName);
                                handlePromiseCall(this[method], testCaseHandler);
                            };

                            beforeTest = integrityCheck();
                            this.initSendBox();
                            handlePromiseCall(this.setUp, setUpHandler);
                        }
                    } catch (ex) {
                        if (!this.isFailing) {
                            this.isFailing = false;
                            __unitTestRunner.report.error(this.testName, ex);
                        }
                        getNextTest($index + 1, $callback);
                    }
                } else {
                    if (this.assertErrors.length > 0) {
                        reportError();
                    }
                    $callback();
                }
            };
            const beforeHandler : any = () : void => {
                let index : number;
                const methods : string[] = this.getMethods();
                for (index = 0; index < methods.length; index++) {
                    if (StringUtils.Contains(StringUtils.ToLowerCase(methods[index]), "test")) {
                        tests.push(methods[index]);
                    }
                }
                __unitTestRunner.events.onSuiteStart();

                if (tests.length > 0) {
                    if (this.assertErrors.length > 0) {
                        reportError();
                    }
                    getNextTest(0, () : void => {
                        const afterHandler : any = () : void => {
                            if (!ObjectValidator.IsEmptyOrNull(integrityTestReport)) {
                                console.error(integrityTestReport);
                            }
                            __unitTestRunner.events.onSuiteEnd(this.passingCount, this.failingCount);
                            $done();
                        };

                        handlePromiseCall(this.after, afterHandler);
                    });
                } else {
                    console.log("Suite \"" + this.getClassName() + "\" skipped: no test methods found");
                    $done();
                }
            };

            this.testName = "Suite " + this.getClassName();
            handlePromiseCall(this.before, beforeHandler);
        }

        protected getEnvironmentArgs() : EnvironmentArgs {
            return UnitTestRunner.loader.getInstance().getEnvironmentArgs();
        }

        protected getHttpResolver() : HttpResolver {
            return UnitTestRunner.loader.getInstance().getHttpResolver();
        }

        protected getHttpManager() : HttpManager {
            return UnitTestRunner.loader.getInstance().getHttpManager();
        }

        protected getRequest() : HttpRequestParser {
            return UnitTestRunner.loader.getInstance().getHttpManager().getRequest();
        }

        protected getEventsManager() : EventsManager {
            return UnitTestRunner.loader.getInstance().getHttpResolver().getEvents();
        }

        private forSetUserAgent($value : string) : void {
            (<any>window.navigator).__defineGetter__("userAgent", function () {
                return $value;
            });
        }

        private prepareAssert() : void {
            const resolveAssert : any = ($validator : () => void) : void => {
                try {
                    $validator();
                    this.isFailing = false;
                    this.assertErrors = [];
                    __unitTestRunner.events.onAssert(true);
                } catch (ex) {
                    this.isFailing = true;
                    this.assertErrors.push(ex);
                    __unitTestRunner.events.onAssert(false);
                    if (!ObjectValidator.IsEmptyOrNull(this.asyncHandler)) {
                        this.asyncHandler();
                        throw new Error("__ASYNC_ASSERT_FAIL__");
                    } else {
                        throw ex;
                    }
                }
            };

            assert.ok = ($value : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    __unitTestRunner.assert.ok($value, $message);
                });
            };

            assert.equal = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                        $message += ": ";
                    } else {
                        $message = "";
                    }
                    try {
                        __unitTestRunner.assert.equal($actual, $expected, $message);
                    } catch (ex) {
                        throw new Error(
                            "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be\n" +
                            JSON.stringify($expected) + "\n"
                        );
                    }
                });
            };

            assert.notEqual = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                        $message += ": ";
                    } else {
                        $message = "";
                    }
                    try {
                        __unitTestRunner.assert.notEqual($actual, $expected, $message);
                    } catch (ex) {
                        throw new Error(
                            "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be NOT\n" +
                            JSON.stringify($expected) + "\n"
                        );
                    }
                });
            };

            assert.deepEqual = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                        $message += ": ";
                    } else {
                        $message = "";
                    }
                    try {
                        __unitTestRunner.assert.deepEqual($actual, $expected, $message);
                    } catch (ex) {
                        throw new Error(
                            "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be\n" +
                            JSON.stringify($expected) + "\n"
                        );
                    }
                });
            };

            assert.notDeepEqual = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                        $message += ": ";
                    } else {
                        $message = "";
                    }
                    try {
                        __unitTestRunner.assert.notDeepEqual($actual, $expected, $message);
                    } catch (ex) {
                        throw new Error(
                            "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be NOT\n" +
                            JSON.stringify($expected) + "\n"
                        );
                    }
                });
            };

            assert.throws = ($block : any, $error? : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    let passed : boolean = true;
                    ExceptionsManager.Clear();
                    try {
                        $block();
                    } catch (ex) {
                        passed = false;
                        if (!ExceptionsManager.IsNativeException(ex) && StringUtils.Contains(ex.message, ".ExRethrow")) {
                            ex.message = ExceptionsManager.getLast().Message();
                            ExceptionsManager.Clear();
                        }
                        if (ObjectValidator.IsString($error)) {
                            $error = new RegExp($error);
                        }
                        if (ObjectValidator.IsSet($error) && $error.exec(ex.message) === null) {
                            if (!ObjectValidator.IsEmptyOrNull($message)) {
                                $message += ": ";
                            } else {
                                $message = "";
                            }
                            throw Error("AssertFailed: " + $message + "\"" + ex.message + "\" " +
                                "expected to be in match with regEx: \"" + $error.toString() + "\"");
                        }
                    }
                    if (passed) {
                        throw Error("AssertFailed: Assertion expected throwing an error.");
                    }
                });
            };

            assert.doesHandleException = ($block : any, $error? : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    let passed : boolean = true;
                    let wuiError : string = "";
                    const consoleLog : any = console.log;
                    console.log = ($data : string) : void => {
                        if (StringUtils.StartsWith($data, "ERROR:")) {
                            wuiError = ExceptionsManager.getLast().Message();
                            ExceptionsManager.Clear();
                        }
                    };
                    ExceptionsManager.Clear();
                    try {
                        $block();
                    } catch (ex) {
                        passed = false;
                    }
                    console.log = consoleLog;
                    if (passed) {
                        if (ObjectValidator.IsString($error)) {
                            $error = new RegExp($error);
                        }
                        if (ObjectValidator.IsSet($error) && $error.exec(wuiError) === null) {
                            if (!ObjectValidator.IsEmptyOrNull($message)) {
                                $message += ": ";
                            } else {
                                $message = "";
                            }
                            throw Error("AssertFailed: " + $message + "\"" + wuiError + "\" " +
                                "expected to be in match with regEx: \"" + $error.toString() + "\"");
                        }
                    } else {
                        throw Error("AssertFailed: Assertion expected handling an error.");
                    }
                });
            };

            assert.onRedirect = ($setUp : () => void, $resolve : ($eventArgs : AsyncRequestEventArgs) => void, $done : () => void,
                                 $urlBase? : string) : void => {
                resolveAssert(() : void => {
                    console.log = ($message : string) : void => {
                        console.info($message);
                    };
                    this.getHttpResolver().CreateRequest($urlBase);
                    this.reload();
                    this.getEventsManager().Clear(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST);
                    this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST,
                        ($eventArgs : AsyncRequestEventArgs) : void => {
                            try {
                                $resolve($eventArgs);
                            } catch (ex) {
                                throw ex;
                            }
                            $done();
                        });
                    $setUp();
                });
            };

            assert.doesNotThrow = ($block : any, $error? : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    ExceptionsManager.Clear();
                    try {
                        $block();
                    } catch (ex) {
                        if (!ExceptionsManager.IsNativeException(ex) && StringUtils.Contains(ex.message, ".ExRethrow")) {
                            ex.message = ExceptionsManager.getLast().Message();
                            ExceptionsManager.Clear();
                        }
                        if (ObjectValidator.IsString($error)) {
                            $error = new RegExp($error);
                        }
                        if (!ObjectValidator.IsSet($error) || ObjectValidator.IsSet($error) && $error.exec(ex.message) !== null) {
                            if (!ObjectValidator.IsEmptyOrNull($message)) {
                                $message += ": ";
                            } else {
                                $message = "";
                            }
                            $message = this.testName + "\n" +
                                "AssertFailed: Assertion expected not throwing an error. " + $message + ex.message;
                            throw Error($message);
                        }
                    }
                });
            };

            __unitTestRunner.assert.patternEqual = ($actual : any, $expected : any, $message? : string) : void => {
                if (!ObjectValidator.IsEmptyOrNull($message)) {
                    $message += ": ";
                } else {
                    $message = "";
                }
                try {
                    __unitTestRunner.assert.ok(StringUtils.PatternMatched($expected, $actual));
                } catch (ex) {
                    throw new Error(
                        "AssertionError: " + $message + "\n" + JSON.stringify($actual, null, 2) + "\nexpected to be\n" +
                        JSON.stringify($expected, null, 2) + "\n"
                    );
                }
            };

            assert.patternEqual = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    __unitTestRunner.assert.patternEqual($actual, $expected, $message);
                });
            };

            assert.resolveEqual = ($className : any, $expected : string, $eventArgs? : HttpRequestEventArgs | AsyncRequestEventArgs,
                                   $message? : string) : BaseHttpResolver => {
                if (ObjectValidator.IsEmptyOrNull($eventArgs)) {
                    $eventArgs = new HttpRequestEventArgs(this.getRequest().getScriptPath());
                }
                let resolver : BaseHttpResolver = new $className();
                resolver.RequestArgs($eventArgs);
                resolveAssert(() : void => {
                    this.setUserAgent(BrowserType.FIREFOX);
                    Echo.ClearAll();
                    resolver.Process();
                    let actual : string = document.documentElement.innerHTML;
                    if (ObjectValidator.IsEmptyOrNull(actual) ||
                        actual === "<head></head><body></body>" ||
                        actual === "<head></head><body><div id=\"Content\"></div></body>") {
                        actual = Echo.getStream();
                    }

                    this.initSendBox();
                    if (StringUtils.Contains($expected, "*")) {
                        __unitTestRunner.assert.patternEqual(actual, $expected, $message);
                    } else {
                        __unitTestRunner.assert.equal(actual, $expected, $message);
                    }
                });
                return resolver;
            };

            assert.onGuiComplete = ($instance : any, $resolve : () => void, $done : () => void, $owner? : any) : void => {
                resolveAssert(() : void => {
                    if (ObjectValidator.IsSet($owner)) {
                        $instance.InstanceOwner($owner);
                    } else {
                        $instance.InstanceOwner({});
                    }
                    (<any>Com.Wui.Framework).Gui.Utils.StaticPageContentManager.BodyAppend($instance.Draw());
                    (<any>Com.Wui.Framework).Gui.Utils.StaticPageContentManager.Draw();

                    $instance.getEvents().setOnComplete(() : void => {
                        $resolve();
                        this.initSendBox();
                        $done();
                    });
                    $instance.Visible(true);
                });
            };

            let validatorName : string;
            for (validatorName in __unitTestRunner.assert) {
                if (__unitTestRunner.assert.hasOwnProperty(validatorName) && !assert.hasOwnProperty(validatorName)) {
                    assert[validatorName] = (...$args : any[]) : void => {
                        resolveAssert(() : void => {
                            __unitTestRunner.assert.apply(this, $args);
                        });
                    };
                }
            }
        }
    }

    export type IUnitTestRunnerPromise = ($done : () => void) => void;
}
