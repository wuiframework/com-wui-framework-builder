/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef INTERNAL_UNITTESTRUNNER_HPP_
#define INTERNAL_UNITTESTRUNNER_HPP_

#ifdef WIN_PLATFORM
#define API_EXPORT __declspec(dllexport)
#define API_CALL __cdecl
#else
#define API_EXPORT
#define API_CALL
#endif

namespace Internal {
    class IUnitTestRunner {
     public:
        virtual int API_CALL RunUnitTests(int argc, char **argv) = 0;
    };

    class API_EXPORT UnitTestRunner : public IUnitTestRunner {
     public:
        int API_CALL RunUnitTests(int argc, char **argv);
    };
}

#endif  // INTERNAL_UNITTESTRUNNER_HPP_
