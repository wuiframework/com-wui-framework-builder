package Com.Wui.Framework;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class UnitTestRunner extends Runner {
    private Class<?> testClass;
    private Map<String, Method> methods;
    private Method before;
    private Method after;
    private Method setUp;
    private Method tearDown;

    public UnitTestRunner(Class clazz) throws IllegalArgumentException {
        testClass = clazz;
        methods = new HashMap<>();
        initTestMethods();
        filterSelectedMethods();
    }

    private void initTestMethods() throws IllegalArgumentException {
        Method[] declaredMethods = testClass.getDeclaredMethods();
        for (Method method : declaredMethods) {
            String methodName = method.getName();
            if (methodName.contains("test") || methodName.startsWith("__Ignore")) {
                methods.put(method.getName(), checkMethodValidity(method));
            } else {
                switch (method.getName()) {
                    case "before":
                        before = checkMethodValidity(method);
                        break;
                    case "after":
                        after = checkMethodValidity(method);
                        break;
                    case "setUp":
                        setUp = checkMethodValidity(method);
                        break;
                    case "tearDown":
                        tearDown = checkMethodValidity(method);
                        break;
                }
            }
        }
    }

    private Method checkMethodValidity(Method $method) throws IllegalArgumentException {
        if ($method.getParameterTypes().length != 0) {
            throw new IllegalArgumentException("Method " + $method + " must not contain parameters.");
        }
        return $method;
    }

    // enables running single test directly from idea
    private void filterSelectedMethods() {
        Properties properties = System.getProperties();
        String testCommand = properties.getProperty("sun.java.command");
        final Map<String, Method> selectedMethods = new HashMap<>();
        if (testCommand != null && testCommand.contains("JUnit")) {
            Arrays.asList(testCommand.split(",")).forEach((String $substr) -> {
                if (!$substr.contains(" ") && methods.containsKey($substr)) {
                    selectedMethods.put($substr, methods.get($substr));
                }
            });
        }
        if (!selectedMethods.isEmpty()) {
            methods = selectedMethods;
        }
    }

    @Override
    public Description getDescription() {
        return Description.createSuiteDescription(this.testClass.getName());
    }

    @Override
    public void run(RunNotifier notifier) {
        try {
            Object object = testClass.getConstructor().newInstance();
            if (setUp != null) {
                setUp.invoke(object);
            }
            methods.forEach((String $name, Method $method) -> {
                Description spec = Description.createTestDescription($method.getClass(), $method.getName());
                if ($name.contains("__Ignore")) {
                    notifier.fireTestIgnored(spec);
                } else {
                    notifier.fireTestStarted(spec);
                    try {
                        if (before != null) {
                            before.invoke(object);
                        }
                        $method.invoke(object);
                        if (after != null) {
                            after.invoke(object);
                        }
                        notifier.fireTestFinished(spec);
                    } catch (InvocationTargetException e) {
                        notifier.fireTestFailure(new Failure(spec, e.getTargetException()));
                    } catch (Exception e) {
                        notifier.fireTestFailure(new Failure(spec, e));
                    }
                }
            });
            if (tearDown != null) {
                tearDown.invoke(object);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
