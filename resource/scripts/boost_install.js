/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Com.Wui.Framework.Builder.Loader;
const LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
const ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
const EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();
const properties = Loader.getInstance().getAppProperties();

let cwd;
let boostComponents = [];
let boostLibsPath = "stage";

const processLibPath = filesystem.getTempPath() + "/com-wui-framework-builder/boost_ext/process_5_0";
const zlibPath = filesystem.NormalizePath(properties.externalModules + "/zlib", true);

function installProcessExtension($callback) {
    if (!filesystem.Exists(cwd + "/boost/process.hpp")) {
        const installExtension = () => {
            filesystem.Copy(processLibPath, cwd, ($status) => {
                if ($status) {
                    $callback();
                } else {
                    LogIt.Error("Installation of Boost process extension has failed.");
                }
            });
        };
        if (filesystem.Exists(processLibPath)) {
            installExtension();
        } else {
            filesystem.Download("http://www.highscore.de/boost/process0.5/process.zip", ($headers, $tmpPath) => {
                filesystem.Unpack($tmpPath, {output: processLibPath, override: false}, () => {
                    filesystem.Delete($tmpPath);
                    installExtension();
                });
            });
        }
    } else {
        $callback();
    }
}

function installZlib($callback) {
    if (!filesystem.Exists(zlibPath)) {
        filesystem.Download("http://www.zlib.net/fossils/zlib-1.2.8.tar.gz", ($headers, $tmpPath) => {
            filesystem.Unpack($tmpPath, {output: zlibPath, override: false}, () => {
                filesystem.Delete($tmpPath);
                $callback();
            });
        });
    } else {
        $callback();
    }
}

function installBoostBase($toolset, $callback) {
    let buildFlagsGlobal = ["link=static", "cxxflags=-fPIC", "toolset=" + $toolset];
    if (EnvironmentHelper.Is64bit()) {
        buildFlagsGlobal.push("address-model=64");
    }
    buildFlagsGlobal.push("-j" + EnvironmentHelper.getCores());
    if (EnvironmentHelper.IsLinux() || EnvironmentHelper.IsMac()) {
        buildFlagsGlobal.push("threading=multi", "--stagedir=" + boostLibsPath + "/" + $toolset);
    } else if (boostLibsPath !== "stage") {
        buildFlagsGlobal.push("--stagedir=" + boostLibsPath);
    }

    let buildFlags = [];
    buildFlags = buildFlags.concat(buildFlagsGlobal);
    boostComponents.forEach(($item) => {
        buildFlags.push("--with-" + $item);
    });

    let cmd = "b2";
    if (EnvironmentHelper.IsWindows()) {
        cmd += ".exe";
    } else {
        cmd = "./" + cmd;
    }
    terminal.Spawn(cmd, buildFlags, {
        cwd,
        env: process.env
    }, function ($exitCode) {
        if ($exitCode !== 0) {
            LogIt.Warning("Boost build install has failed with code: " + $exitCode + ". Trying to compile zlib...");
        }

        installZlib(() => {
            buildFlags = buildFlagsGlobal.concat(["--with-iostreams", "-sNO_ZLIB=0", "-sZLIB_SOURCE=\"" + zlibPath + "\""]);
            terminal.Spawn(cmd, buildFlags, {
                cwd,
                env: process.env
            }, ($exitCode) => {
                if ($exitCode !== 0) {
                    LogIt.Error("Boost iostreams build with Zlib has failed.");
                }
                $callback();
            });
        });
    });
}

function installBoostLibraries($toolset, $callback) {
    if (EnvironmentHelper.IsLinux() || EnvironmentHelper.IsMac()) {
        const userConfig = cwd + "/tools/build/src/user-config.jam";
        if (!filesystem.Exists(userConfig)) {
            filesystem.Write(userConfig, "using gcc : 7 : gcc ;\n");

            if (EnvironmentHelper.IsLinux()) {
                filesystem.Write(userConfig, "using gcc : arm : aarch64-linux-gnu-gcc-7 ;", true);
            } else if (EnvironmentHelper.IsMac()) {
                filesystem.Write(userConfig, "using clang : 4 : " + properties.externalModules + "/clang/c++ ;", true);
            }
        }
    }
    if (Loader.getInstance().getProjectConfig().dependencies["boost"].version === "1.61.0") {
        LogIt.Warning("Boost package in version 1.61.0 is deprecated. Please use new version.");
        installProcessExtension(() => {
            installBoostBase($toolset, $callback);
        });
    } else {
        installBoostBase($toolset, $callback);
    }
}

function runInstall($callback) {
    if (EnvironmentHelper.IsLinux()) {
        installBoostLibraries("gcc-7", () => {
            installBoostLibraries("gcc-arm", $callback);
        });
    } else if (EnvironmentHelper.IsMac()) {
        installBoostLibraries("gcc-7", () => {
            installBoostLibraries("clang", $callback);
        });
    } else {
        installBoostLibraries("gcc", $callback);
    }
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    let buildAll = false;
    $args.forEach(($arg) => {
        if ($arg.startsWith("-libs=")) {
            boostLibsPath = $arg.replace("-libs=", "");
        } else if ($arg.startsWith("-all")) {
            buildAll = true;
        } else if (boostComponents.indexOf($arg) === -1) {
            boostComponents.push($arg);
        }
    });

    if (buildAll) {
        boostComponents = []; // clear to build all components
    }

    let b2path = $cwd + "/b2";
    if (EnvironmentHelper.IsWindows()) {
        b2path += ".exe";
    }

    if (filesystem.Expand(b2path).length === 0) {
        let cmd = "bootstrap";
        if (EnvironmentHelper.IsWindows()) {
            cmd += ".bat";
        } else {
            cmd = "./" + cmd + ".sh";
        }
        terminal.Spawn(cmd, [(EnvironmentHelper.IsWindows() ? "gcc" : "")], $cwd, function ($exitCode) {
            if ($exitCode !== 0) {
                LogIt.Error("Boost build prepare has failed.");
            } else {
                runInstall($done);
            }
        });
    } else {
        runInstall($done);
    }
};
