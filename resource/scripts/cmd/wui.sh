#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017-2018 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sourceFile="${BASH_SOURCE[0]}"
while [ -h "$sourceFile" ]; do
    sourceLink="$(readlink "$sourceFile")"
    if [[ $sourceLink == /* ]]; then
        sourceFile="$sourceLink"
    else
        sourceDir="$( dirname "$sourceFile" )"
        sourceFile="$sourceDir/$sourceLink"
    fi
done
binBase="$( cd -P "$( dirname "$sourceFile" )" && pwd )"

wuiExecutablePath=$binBase/../<? @var project.target.name ?>
if [ -x $wuiExecutablePath ]; then
    $wuiExecutablePath $@
else
    if [[ -z $NODEJSRE_SKIP_LOADER ]]; then
		export NODE_PATH=$binBase/../../../dependencies/nodejs/build/node_modules
		$binBase/../../../dependencies/nodejs/node $binBase/../resource/javascript/loader.min.js $@
	else
		export NODE_PATH="${NODEJSRE_NODE_PATH:-${binBase}/../../../dependencies/nodejs/build/node_modules}"
		$binBase/../../../dependencies/nodejs/node $@
	fi
fi
