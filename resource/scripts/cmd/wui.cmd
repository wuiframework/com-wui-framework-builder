@setlocal enabledelayedexpansion
@echo off
REM * ********************************************************************************************************* *
REM *
REM * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
REM * Copyright (c) 2017-2018 NXP
REM *
REM * SPDX-License-Identifier: BSD-3-Clause
REM * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
REM * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
REM *
REM * ********************************************************************************************************* *

set "wuiExecutablePath=%~dp0..\<? @var project.target.name ?>.exe"
IF EXIST !wuiExecutablePath! (
    !wuiExecutablePath! %*
) ELSE (
    set NODE_PATH=%~dp0..\..\..\dependencies\nodejs\build\node_modules
    IF DEFINED NODEJSRE_NODE_PATH (
        set NODE_PATH=%NODEJSRE_NODE_PATH%
    )
    IF DEFINED NODEJSRE_SKIP_LOADER (
        %~dp0..\..\..\dependencies\nodejs\node.exe %*
    ) ELSE (
        %~dp0..\..\..\dependencies\nodejs\node.exe %~dp0..\resource\javascript\loader.min.js %*
    )
)
