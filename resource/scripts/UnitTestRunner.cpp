/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "UnitTestRunner.hpp"
#include "../../../test/unit/cpp/reference.hpp"

namespace Internal {
    int API_CALL UnitTestRunner::RunUnitTests(int argc, char **argv) {
#ifdef COM_WUI_FRAMEWORK_XCPPCOMMONS_SOURCEFILESMAP_HPP_
        Com::Wui::Framework::XCppCommons::EnvironmentArgs::getInstance().Load();
        Com::Wui::Framework::XCppCommons::Utils::LogIt::Init(Com::Wui::Framework::XCppCommons::Enums::LogLevel::ERROR,
                                                         Com::Wui::Framework::XCppCommons::IOApi::IOHandlerFactory::getHandler(
                                                                 Com::Wui::Framework::XCppCommons::Enums::IOHandlerType::OUTPUT_FILE));
#endif

        testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    }

}

// CreateInstance should be published as extern "C" function to produce undecorated entry
extern "C" {
API_EXPORT Internal::UnitTestRunner *API_CALL CreateInstance() {
    return new Internal::UnitTestRunner();
}
}
