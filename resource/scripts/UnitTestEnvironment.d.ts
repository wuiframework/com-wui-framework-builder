/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import BaseHttpResolver = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver;

declare interface IAssert {
    fail($actual : any, $expected : any, $message : string, $operator : string) : void;

    assert($value : any, $message : string) : void;

    ok($value : any, $message? : string) : void;

    equal($actual : any, $expected : any, $message? : string) : void;

    notEqual($actual : any, $expected : any, $message? : string) : void;

    deepEqual($actual : any, $expected : any, $message? : string) : void;

    notDeepEqual($actual : any, $expected : any, $message? : string) : void;

    strictEqual($actual : any, $expected : any, $message? : string) : void;

    notStrictEqual($actual : any, $expected : any, $message? : string) : void;

    throws($block : any, $error? : any, $message? : string) : void;

    doesNotThrow($block : any, $error? : any, $message? : string) : void;

    ifError($value : any) : void;

    onRedirect($setUp : () => void, $resolve : ($eventArgs : any) => void, $done : () => void, $urlBase? : string) : void;

    resolveEqual($className : any, $expected : string, $eventArgs? : any, $message? : string) : BaseHttpResolver;

    patternEqual($actual : any, $expected : any, $message? : string) : void;

    doesHandleException($block : any, $error? : any, $message? : string) : void;

    onGuiComplete($instance : any, $resolve : () => void, $done : () => void, $owner? : any) : void;
}

declare interface IVBoxSession {
    Copy($sourcePath : string, $destinationPath : string) : IVBoxPromise;

    Execute($filePath : string) : IVBoxPromise;
}

declare interface IVBoxPromise {
    Then($callback : ($status : boolean, $session? : IVBoxSession) => void) : void;
}

declare interface IVBoxManager {
    Start($machineNameOrId : string, $user? : string, $pass? : string) : IVBoxPromise;
}

declare let assert : IAssert;
declare let isBrowserRun : boolean;
declare let selenium : any;
declare let chromeBuilder : any;
declare let jsdom : any;
declare let vbox : IVBoxManager;
declare let builder : any;

declare const __unitTestRunner : any;

declare let NodeFile : {
    prototype : File;
    new(optionsOrPath : string) : File;
};

interface MockLocation extends Location {
    headers : string;
}

interface MockWindow extends Window {
    location : MockLocation;
}
