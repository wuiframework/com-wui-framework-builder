package Com.Wui.Framework;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

// works only without tycho
public class UnitTestListener extends RunListener {

    @Deprecated
    public void testRunStarted(Description description) throws Exception {
    }

    @Deprecated
    public void testRunFinished(Result result) throws Exception {
    }

    public void testStarted(Description description) throws Exception {
        System.out.println("-------------------------------------------------------");
        System.out.println("  Started test: " + description.getMethodName());
        System.out.println("-------------------------------------------------------");
    }

    public void testFinished(Description description) throws Exception {
        System.out.println("-------------------------------------------------------");
        System.out.println("  Finished test: " + description.getMethodName());
        System.out.println("-------------------------------------------------------");
    }

    public void testFailure(Failure failure) throws Exception {
        System.out.println("----- Test Failed: " + failure.getDescription().getMethodName() + " -----");
    }

    public void testAssumptionFailure(Failure failure) {
        System.out.println("----- Assertion Failed: " + failure.getDescription().getMethodName() + " -----");
    }

    public void testIgnored(Description description) throws Exception {
        System.out.println("----- Ignored: " + description.getMethodName() + " -----");
    }
}
