# /* ********************************************************************************************************* *
# *
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* */

import copy
import fnmatch
import os
import re
import string

import sys

import subprocess

import time

import datetime

from pydoc import html

import itertools

from optparse import OptionParser

import sys
import os

__version__ = "1.0.0"

exclude_line_flag = "_EXCL_"
exclude_line_pattern = re.compile('([GL]COVR?)_EXCL_(LINE|START|STOP)')

c_style_comment_pattern = re.compile('/\*.*?\*/')
cpp_style_comment_pattern = re.compile('//.*?$')

medium_coverage = 75.0
high_coverage = 90.0
low_color = "LightPink"
medium_color = "#FFFF55"
high_color = "LightGreen"
covered_color = "LightGreen"
uncovered_color = "LightPink"
takenBranch_color = "Green"
notTakenBranch_color = "Red"

# Definitions
non_code_mapper = dict.fromkeys(ord(i) for i in '}{')


def is_non_code(code):
    if sys.version_info < (3, 0):
        code = code.strip().translate(None, '}{')
    else:
        code = code.strip().translate(non_code_mapper)
    return len(code) == 0 or code.startswith("//") or code == 'else'


def find_files(directory, pattern):
    found_files = []
    for root, dirs, dir_files in os.walk(directory):
        for basename in dir_files:
            if fnmatch.fnmatch(basename, pattern):
                found_files.append(os.path.join(root, basename))
    return found_files


def search_dir(search_paths, pattern):
    found_files = []
    for _dir in search_paths:
        if os.path.exists(_dir):
            found_files.extend(find_files(_dir, pattern))
    return found_files


def get_data_files(search_paths):
    found = []
    # filter out data filenames
    for fnd in search_dir(search_paths, "*.gcda"):
        _dir, _file = os.path.split(fnd)
        if "UnitTestLoader" not in _file:
            found.append(fnd)
    return found


def get_gcov_files(search_paths):
    found = search_dir(search_paths, "*.gcov")
    return found


def mkdir_recursive(path):
    sub_path = os.path.dirname(path)
    if not os.path.exists(sub_path):
        mkdir_recursive(sub_path)
    if not os.path.exists(path):
        os.mkdir(path)


def print_summary(covdata):
    lines_total = 0
    lines_covered = 0
    branches_total = 0
    branches_covered = 0

    keys = list(covdata.keys())

    for key in keys:
        (t, n, txt) = covdata[key].coverage(False)
        lines_total += t
        lines_covered += n

        (t, n, txt) = covdata[key].coverage(True)
        branches_total += t
        branches_covered += n

    percent = lines_total and (100.0 * lines_covered / lines_total)
    percent_branches = branches_total and (100.0 * branches_covered / branches_total)

    lines_out = "lines: %0.1f%% (%s out of %s)\n" % (
        percent, lines_covered, lines_total
    )
    branches_out = "branches: %0.1f%% (%s out of %s)\n" % (
        percent_branches, branches_covered, branches_total
    )

    sys.stdout.write("\nCoverage summary:\n")
    sys.stdout.write(" - \t" + lines_out)
    sys.stdout.write(" - \t" + branches_out)


#
# CSS declarations for the HTML output
#
css = string.Template('''
    body
    {
      color: #000000;
      background-color: #FFFFFF;
    }

    /* Link formats: use maroon w/underlines */
    a:link
    {
      color: navy;
      text-decoration: underline;
    }
    a:visited
    {
      color: maroon;
      text-decoration: underline;
    }
    a:active
    {
      color: navy;
      text-decoration: underline;
    }

    /*** TD formats ***/
    td
    {
      font-family: sans-serif;
    }
    td.title
    {
      text-align: center;
      padding-bottom: 10px;
      font-size: 20pt;
      font-weight: bold;
    }

    /* TD Header Information */
    td.headerName
    {
      text-align: right;
      color: black;
      padding-right: 6px;
      font-weight: bold;
      vertical-align: top;
      white-space: nowrap;
    }
    td.headerValue
    {
      text-align: left;
      color: blue;
      font-weight: bold;
      white-space: nowrap;
    }
    td.headerTableEntry
    {
      text-align: right;
      color: black;
      font-weight: bold;
      white-space: nowrap;
      padding-left: 12px;
      padding-right: 4px;
      background-color: LightBlue;
    }
    td.headerValueLeg
    {
      text-align: left;
      color: black;
      font-size: 80%;
      white-space: nowrap;
      padding-left: 10px;
      padding-right: 10px;
      padding-top: 2px;
    }

    /* Color of horizontal ruler */
    td.hr
    {
      background-color: navy;
      height:3px;
    }
    /* Footer format */
    td.footer
    {
      text-align: center;
      padding-top: 3px;
      font-family: sans-serif;
    }

    /* Coverage Table */

    td.coverTableHead
    {
      text-align: center;
      color: white;
      background-color: SteelBlue;
      font-family: sans-serif;
      font-size: 120%;
      white-space: nowrap;
      padding-left: 4px;
      padding-right: 4px;
    }
    td.coverFile
    {
      text-align: left;
      padding-left: 10px;
      padding-right: 20px;
      color: black;
      background-color: LightBlue;
      font-family: monospace;
      font-weight: bold;
      font-size: 110%;
    }
    td.coverBar
    {
      padding-left: 10px;
      padding-right: 10px;
      background-color: LightBlue;
    }
    td.coverBarOutline
    {
      background-color: white;
    }
    td.coverValue
    {
      padding-top: 2px;
      text-align: right;
      padding-left: 10px;
      padding-right: 10px;
      font-family: sans-serif;
      white-space: nowrap;
      font-weight: bold;
    }

    /* Link Details */
    a.detail:link
    {
      color: #B8D0FF;
      font-size:80%;
    }
    a.detail:visited
    {
      color: #B8D0FF;
      font-size:80%;
    }
    a.detail:active
    {
      color: #FFFFFF;
      font-size:80%;
    }

    .graphcont{
        color:#000;
        font-weight:700;
        float:left
    }

    .graph{
        float:left;
        background-color: white;
        position:relative;
        width:280px;
        padding:0
    }

    .graph .bar{
        display:block;
        position:relative;
        border:black 1px solid;
        text-align:center;
        color:#fff;
        height:10px;
        font-family:Arial,Helvetica,sans-serif;
        font-size:12px;
        line-height:1.9em
    }

    .graph .bar span{
        position:absolute;
        left:1em
    }

    td.coveredLine,
    span.coveredLine
    {
        background-color: ${covered_color}!important;
    }

    td.uncoveredLine,
    span.uncoveredLine
    {
        background-color: ${uncovered_color}!important;
    }

    .linebranch, .linecount
    {
        border-right: 1px gray solid;
        background-color: lightgray;
    }

    span.takenBranch
    {
        color: ${takenBranch_color}!important;
        cursor: help;
    }

    span.notTakenBranch
    {
        color: ${notTakenBranch_color}!important;
        cursor: help;
    }

    .src
    {
        padding-left: 12px;
    }

    .srcHeader,
    span.takenBranch,
    span.notTakenBranch
    {
        font-family: monospace;
        font-weight: bold;
    }

    pre
    {
        height : 15px;
        margin-top: 0;
        margin-bottom: 0;
    }

    .lineno
    {
        background-color: #EFE383;
        border-right: 1px solid #BBB15F;
    }
''')

#
# A string template for the root HTML output
#
root_page = string.Template('''
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=${ENC}"/>
  <title>${HEAD}</title>
  <style media="screen" type="text/css">
  ${CSS}
  </style>
</head>

<body>

  <table width="100%" border=0 cellspacing=0 cellpadding=0>
    <tr><td class="title">GCC Code Coverage Report</td></tr>
    <tr><td class="hr"></td></tr>

    <tr>
      <td width="100%">
        <table cellpadding=1 border=0 width="100%">
          <tr>
            <td width="10%" class="headerName">Directory:</td>
            <td width="35%" class="headerValue">${DIRECTORY}</td>
            <td width="5%"></td>
            <td width="15%"></td>
            <td width="10%" class="headerValue" style="text-align:right;">Exec</td>
            <td width="10%" class="headerValue" style="text-align:right;">Total</td>
            <td width="15%" class="headerValue" style="text-align:right;">Coverage</td>
          </tr>
          <tr>
            <td class="headerName">Date:</td>
            <td class="headerValue">${DATE}</td>
            <td></td>
            <td class="headerName">Lines:</td>
            <td class="headerTableEntry">${LINES_EXEC}</td>
            <td class="headerTableEntry">${LINES_TOTAL}</td>
            <td class="headerTableEntry" style="background-color:${LINES_COLOR}">${LINES_COVERAGE} %</td>
          </tr>
          <tr>
            <td class="headerName">Legend:</td>
            <td class="headerValueLeg">
              <span style="background-color:${low_color}">low: &lt; ${COVERAGE_MED} %</span>
              <span style="background-color:${medium_color}">medium: &gt;= ${COVERAGE_MED} %</span>
              <span style="background-color:${high_color}">high: &gt;= ${COVERAGE_HIGH} %</span>
            </td>
            <td></td>
            <td class="headerName">Branches:</td>
            <td class="headerTableEntry">${BRANCHES_EXEC}</td>
            <td class="headerTableEntry">${BRANCHES_TOTAL}</td>
            <td class="headerTableEntry" style="background-color:${BRANCHES_COLOR}">${BRANCHES_COVERAGE} %</td>
          </tr>
        </table>
      </td>
    </tr>

    <tr><td class="hr"></td></tr>
  </table>

  <center>
  <table width="80%" cellpadding=1 cellspacing=1 border=0>
    <tr>
      <td width="44%"><br></td>
      <td width="8%"></td>
      <td width="8%"></td>
      <td width="8%"></td>
      <td width="8%"></td>
      <td width="8%"></td>
    </tr>
    <tr>
      <td class="coverTableHead">File</td>
      <td class="coverTableHead" colspan=3>Lines</td>
      <td class="coverTableHead" colspan=2>Branches</td>
    </tr>

    ${ROWS}

    <tr>
      <td width="44%"><br></td>
      <td width="8%"></td>
      <td width="8%"></td>
      <td width="8%"></td>
      <td width="8%"></td>
      <td width="8%"></td>
    </tr>
  </table>
  </center>

  <table width="100%" border=0 cellspacing=0 cellpadding=0>
    <tr><td class="hr"><td></tr>
    <tr><td class="footer">Generated by: <a href="http://gcovr.com">GCOVR (Version ${VERSION})</a></td></tr>
  </table>
  <br>

</body>

</html>
''')

#
# A string template for the source file HTML output
#
source_page = string.Template('''
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=${ENC}"/>
  <title>${HEAD}</title>
  <style media="screen" type="text/css">
  ${CSS}
  </style>
</head>

<body>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td class="title">GCC Code Coverage Report</td></tr>
    <tr><td class="hr"></td></tr>

    <tr>
      <td width="100%">
        <table cellpadding="1" border="0" width="100%">
          <tr>
            <td width="10%" class="headerName">Directory:</td>
            <td width="35%" class="headerValue">${DIRECTORY}</td>
            <td width="5%"></td>
            <td width="15%"></td>
            <td width="10%" class="headerValue" style="text-align:right;">Exec</td>
            <td width="10%" class="headerValue" style="text-align:right;">Total</td>
            <td width="15%" class="headerValue" style="text-align:right;">Coverage</td>
          </tr>
          <tr>
            <td class="headerName">File:</td>
            <td class="headerValue">${FILENAME}</td>
            <td></td>
            <td class="headerName">Lines:</td>
            <td class="headerTableEntry">${LINES_EXEC}</td>
            <td class="headerTableEntry">${LINES_TOTAL}</td>
            <td class="headerTableEntry" style="background-color:${LINES_COLOR}">${LINES_COVERAGE} %</td>
          </tr>
          <tr>
            <td class="headerName">Date:</td>
            <td class="headerValue">${DATE}</td>
            <td></td>
            <td class="headerName">Branches:</td>
            <td class="headerTableEntry">${BRANCHES_EXEC}</td>
            <td class="headerTableEntry">${BRANCHES_TOTAL}</td>
            <td class="headerTableEntry" style="background-color:${BRANCHES_COLOR}">${BRANCHES_COVERAGE} %</td>
          </tr>
        </table>
      </td>
    </tr>

    <tr><td class="hr"></td></tr>
  </table>

  <br>
  <table cellspacing="0" cellpadding="1">
    <tr>
      <td width="5%" align="right" class="srcHeader">Line</td>
      <td width="5%" align="right" class="srcHeader">Branch</td>
      <td width="5%" align="right" class="srcHeader">Exec</td>
      <td width="75%" align="left" class="srcHeader src">Source</td>
    </tr>

    ${ROWS}

  </table>
  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td class="hr"><td></tr>
    <tr><td class="footer">Generated by: <a href="http://gcovr.com">GCOVR (Version ${VERSION})</a></td></tr>
  </table>
  <br>

</body>

</html>
''')


def calculate_coverage(covered, total):
    return 0.0 if total == 0 else round(100.0 * covered / total, 1)


def coverage_to_color(coverage):
    if coverage < medium_coverage:
        return low_color
    elif coverage < high_coverage:
        return medium_color
    else:
        return high_color


def common_path(files):
    if len(files) == 1:
        return os.path.join(os.path.relpath(os.path.split(files[0])[0]), '')

    _common_path = os.path.realpath(files[0])
    _common_dirs = _common_path.split(os.path.sep)

    for f in files[1:]:
        path = os.path.realpath(f)
        dirs = path.split(os.path.sep)
        common = []
        for a, b in itertools.izip(dirs, _common_dirs):
            if a == b:
                common.append(a)
            elif common:
                _common_dirs = common
                break
            else:
                return ''

    return os.path.join(os.path.relpath(os.path.sep.join(_common_dirs)), '')


#
# Produce an HTML report
#
def print_html_report(_cov_data, rep_root, _config):
    def _num_uncovered(_key):
        (_total, _covered, _percent) = _cov_data[_key].coverage(False)
        return _total - _covered

    def _percent_uncovered(_key):
        (_total, _covered, _percent) = _cov_data[_key].coverage(False)
        if _covered:
            return -1.0 * _covered / _total
        else:
            return _total or 1e6

    def _alpha(_key):
        return _key

    data = {
        'HEAD': "Head",
        'VERSION': __version__,
        'TIME': str(int(time.time())),
        'DATE': datetime.datetime.now().strftime("%Y-%m-%d / %H:%M:%S"),
        'ROWS': [],
        'ENC': 'UTF-8',
        'low_color': low_color,
        'medium_color': medium_color,
        'high_color': high_color,
        'COVERAGE_MED': medium_coverage,
        'COVERAGE_HIGH': high_coverage,
        'CSS': css.substitute(
            low_color=low_color, medium_color=medium_color, high_color=high_color,
            covered_color=covered_color, uncovered_color=uncovered_color,
            takenBranch_color=takenBranch_color, notTakenBranch_color=notTakenBranch_color
        ),
        'DIRECTORY': ''
    }

    branch_total = 0
    branch_covered = 0
    for key in _cov_data.keys():
        (total, covered, percent) = _cov_data[key].coverage(True)
        branch_total += total
        branch_covered += covered
    data['BRANCHES_EXEC'] = str(branch_covered)
    data['BRANCHES_TOTAL'] = str(branch_total)
    coverage = calculate_coverage(branch_covered, branch_total)
    data['BRANCHES_COVERAGE'] = str(coverage)
    data['BRANCHES_COLOR'] = coverage_to_color(coverage)

    line_total = 0
    line_covered = 0
    for key in _cov_data.keys():
        (total, covered, percent) = _cov_data[key].coverage(False)
        line_total += total
        line_covered += covered
    data['LINES_EXEC'] = str(line_covered)
    data['LINES_TOTAL'] = str(line_total)
    coverage = calculate_coverage(line_covered, line_total)
    data['LINES_COVERAGE'] = str(coverage)
    data['LINES_COLOR'] = coverage_to_color(coverage)

    # Generate the coverage output (on a per-package basis)
    files = []
    dirs = []
    filtered_file_name = ''
    keys = list(_cov_data.keys())
    keys.sort(
        key=_num_uncovered or _percent_uncovered or _alpha, reverse=True
    )

    for f in keys:
        cdata = _cov_data[f]
        filtered_file_name = f
        files.append(filtered_file_name)
        dirs.append(os.path.dirname(filtered_file_name) + os.sep)
        cdata.filename = filtered_file_name
        cdata.sourcefile = '{0}.html'.format(os.path.split(filtered_file_name)[1])

    # Define the common root directory, which may differ from options.root
    # when source files share a common prefix.
    if len(files) > 1:
        common_dir = common_path(files)
        if common_dir != '':
            data['DIRECTORY'] = common_dir
    else:
        dir_, file_ = os.path.split(filtered_file_name)
        if dir_ != '':
            data['DIRECTORY'] = dir_ + os.sep

    for f in keys:
        cdata = _cov_data[f]
        class_lines = 0
        class_hits = 0
        class_branches = 0
        class_branch_hits = 0
        for line in sorted(cdata.all_lines):
            hits = cdata.covered.get(line, 0)
            class_lines += 1
            if hits > 0:
                class_hits += 1
            branches = cdata.branches.get(line)
            if branches is None:
                pass
            else:
                b_hits = 0
                for v in branches.values():
                    if v > 0:
                        b_hits += 1
                class_branch_hits += b_hits
                class_branches += len(branches)

        lines_covered = 100.0 if class_lines == 0 else 100.0 * class_hits / class_lines
        branches_covered = 100.0 if class_branches == 0 else 100.0 * class_branch_hits / class_branches

        data['ROWS'].append(html_row(
            True, cdata.namespace, cdata.sourcefile,
            directory=data['DIRECTORY'],
            filename=os.path.relpath(os.path.realpath(cdata.filename), data['DIRECTORY']),
            LinesExec=class_hits,
            LinesTotal=class_lines,
            LinesCoverage=lines_covered,
            BranchesExec=class_branch_hits,
            BranchesTotal=class_branches,
            BranchesCoverage=branches_covered
        ))
    data['ROWS'] = '\n'.join(data['ROWS'])

    if data['DIRECTORY'] == '':
        data['DIRECTORY'] = "."

    html_string = root_page.substitute(**data)

    if rep_root is None:
        sys.stdout.write(html_string + '\n')
    else:
        _output = open(os.path.join(rep_root, "index.html"), 'w')
        _output.write(html_string + '\n')
        _output.close()

    #
    # Generate an HTML file for every source file
    #
    for f in keys:
        cdata = _cov_data[f]

        data['FILENAME'] = cdata.filename
        data['ROWS'] = ''

        branch_total, branch_covered, tmp = cdata.coverage(True)
        data['BRANCHES_EXEC'] = str(branch_covered)
        data['BRANCHES_TOTAL'] = str(branch_total)
        coverage = calculate_coverage(branch_covered, branch_total)
        data['BRANCHES_COVERAGE'] = str(coverage)
        data['BRANCHES_COLOR'] = coverage_to_color(coverage)

        line_total, line_covered, tmp = cdata.coverage(False)
        data['LINES_EXEC'] = str(line_covered)
        data['LINES_TOTAL'] = str(line_total)
        coverage = calculate_coverage(line_covered, line_total)
        data['LINES_COVERAGE'] = str(coverage)
        data['LINES_COLOR'] = coverage_to_color(coverage)

        data['ROWS'] = []

        _input = open(data['FILENAME'], 'r')
        ctr = 1
        for line in _input:
            data['ROWS'].append(
                source_row(ctr, line.rstrip(), cdata)
            )
            ctr += 1
        _input.close()

        data['ROWS'] = '\n'.join(data['ROWS'])

        html_string = source_page.substitute(**data)

        _sub_dir_path = os.path.join(_config.rep_dir, cdata.namespace)
        if not os.path.exists(_sub_dir_path):
            mkdir_recursive(_sub_dir_path)
        _path = os.path.join(_sub_dir_path, cdata.sourcefile)
        _output = open(_path, 'w')
        _output.write(html_string + '\n')
        _output.close()


def source_row(line_no, source, cdata):
    row_str = string.Template('''
    <tr>
    <td align="right" class="lineno"><pre>${lineno}</pre></td>
    <td align="right" class="linebranch">${linebranch}</td>
    <td align="right" class="linecount ${covclass}"><pre>${linecount}</pre></td>
    <td align="left" class="src ${covclass}"><pre>${source}</pre></td>
    </tr>''')
    kwargs = {
        'lineno': str(line_no)
    }
    if line_no in cdata.covered:
        kwargs['covclass'] = 'coveredLine'
        kwargs['linebranch'] = ''
        # If line has branches them show them with ticks or crosses
        if line_no in cdata.branches.keys():
            branches = cdata.branches.get(line_no)
            branch_counter = 0
            for branch in branches:
                if branches[branch] > 0:
                    kwargs['linebranch'] += '<span class="takenBranch" title="Branch ' + str(branch) + ' taken ' + str(
                        branches[branch]) + ' times">&check;</span>'
                else:
                    kwargs['linebranch'] += '<span class="notTakenBranch" title="Branch ' + str(branch) + ' not taken">&cross;</span>'
                branch_counter += 1
                # Wrap at 4 branches to avoid too wide column
                if (branch_counter > 0) and ((branch_counter % 4) == 0):
                    kwargs['linebranch'] += '<br/>'
        kwargs['linecount'] = str(cdata.covered.get(line_no, 0))
    elif line_no in cdata.uncovered:
        kwargs['covclass'] = 'uncoveredLine'
        kwargs['linebranch'] = ''
        kwargs['linecount'] = ''
    else:
        kwargs['covclass'] = ''
        kwargs['linebranch'] = ''
        kwargs['linecount'] = ''
    kwargs['source'] = html.escape(source)
    return row_str.substitute(**kwargs)


#
# Generate the table row for a single file
#
n_rows = 0


def html_row(details, _rel_to_root, sourcefile, **kwargs):
    sourcefile = os.path.basename(sourcefile)
    row_str = string.Template('''
    <tr>
      <td class="coverFile" ${altstyle}>${filename}</td>
      <td class="coverBar" align="center" ${altstyle}>
        <table border=0 cellspacing=0 cellpadding=1><tr><td class="coverBarOutline">
                <div class="graph"><strong class="bar" style="width:${LinesCoverage}%; ${BarBorder}background-color:${LinesBar}"></strong></div>
                </td></tr></table>
      </td>
      <td class="CoverValue" style="font-weight:bold; background-color:${LinesColor};">${LinesCoverage}&nbsp;%</td>
      <td class="CoverValue" style="font-weight:bold; background-color:${LinesColor};">${LinesExec} / ${LinesTotal}</td>
      <td class="CoverValue" style="background-color:${BranchesColor};">${BranchesCoverage}&nbsp;%</td>
      <td class="CoverValue" style="background-color:${BranchesColor};">${BranchesExec} / ${BranchesTotal}</td>
    </tr>
''')
    global n_rows
    n_rows += 1
    if n_rows % 2 == 0:
        kwargs['altstyle'] = 'style="background-color:LightSteelBlue"'
    else:
        kwargs['altstyle'] = ''
    if details:
        kwargs['filename'] = '<a href="%s">%s</a>' % (
            _rel_to_root + os.path.sep + sourcefile, kwargs['filename']
        )
    kwargs['LinesCoverage'] = round(kwargs['LinesCoverage'], 1)
    # Disable the border if the bar is too short to see the color
    if kwargs['LinesCoverage'] < 1e-7:
        kwargs['BarBorder'] = "border:white; "
    else:
        kwargs['BarBorder'] = ""
    if kwargs['LinesCoverage'] < medium_coverage:
        kwargs['LinesColor'] = low_color
        kwargs['LinesBar'] = 'red'
    elif kwargs['LinesCoverage'] < high_coverage:
        kwargs['LinesColor'] = medium_color
        kwargs['LinesBar'] = 'yellow'
    else:
        kwargs['LinesColor'] = high_color
        kwargs['LinesBar'] = 'green'

    kwargs['BranchesCoverage'] = round(kwargs['BranchesCoverage'], 1)
    if kwargs['BranchesCoverage'] < medium_coverage:
        kwargs['BranchesColor'] = low_color
        kwargs['BranchesBar'] = 'red'
    elif kwargs['BranchesCoverage'] < high_coverage:
        kwargs['BranchesColor'] = medium_color
        kwargs['BranchesBar'] = 'yellow'
    else:
        kwargs['BranchesColor'] = high_color
        kwargs['BranchesBar'] = 'green'

    return row_str.substitute(**kwargs)


class CoverageData(object):
    def __init__(
            self, fname, namespace, uncovered, uncovered_exceptional, covered, branches,
            noncode):
        self.fname = fname
        self.namespace = namespace
        # Shallow copies are cheap & "safe" because the caller will
        # throw away their copies of covered & uncovered after calling
        # us exactly *once*
        self.uncovered = copy.copy(uncovered)
        self.uncovered_exceptional = copy.copy(uncovered_exceptional)
        self.covered = copy.copy(covered)
        self.noncode = copy.copy(noncode)
        # But, a deep copy is required here
        self.all_lines = copy.deepcopy(uncovered)
        self.all_lines.update(uncovered_exceptional)
        self.all_lines.update(covered.keys())
        self.branches = copy.deepcopy(branches)

    def update(
            self, namespace, uncovered, uncovered_exceptional, covered, branches,
            noncode):
        self.namespace = namespace
        self.all_lines.update(uncovered)
        self.all_lines.update(uncovered_exceptional)
        self.all_lines.update(covered.keys())
        self.uncovered.update(uncovered)
        self.uncovered_exceptional.update(uncovered_exceptional)
        self.noncode.intersection_update(noncode)
        for k in covered.keys():
            self.covered[k] = self.covered.get(k, 0) + covered[k]
        for k in branches.keys():
            for b in branches[k]:
                d = self.branches.setdefault(k, {})
                d[b] = d.get(b, 0) + branches[k][b]
        self.uncovered.difference_update(self.covered.keys())
        self.uncovered_exceptional.difference_update(self.covered.keys())

    def uncovered_str(self, exceptional, use_branch):
        if use_branch:
            #
            # Don't do any aggregation on branch results
            #
            tmp = []
            for line in self.branches.keys():
                for branch in self.branches[line]:
                    if self.branches[line][branch] == 0:
                        tmp.append(line)
                        break
            tmp.sort()
            return ",".join([str(x) for x in tmp]) or ""

        if exceptional:
            tmp = list(self.uncovered_exceptional)
        else:
            tmp = list(self.uncovered)
        if len(tmp) == 0:
            return ""

        #
        # Walk through the uncovered lines in sorted order.
        # Find blocks of consecutive uncovered lines, and return
        # a string with that information.
        #
        tmp.sort()
        first = None
        last = None
        ranges = []
        for item in tmp:
            if last is None:
                first = item
                last = item
            elif item == (last + 1):
                last = item
            else:
                #
                # Should we include noncode lines in the range of lines
                # to be covered???  This simplifies the ranges summary, but it
                # provides a counterintuitive listing.
                #
                # if len(self.noncode.intersection(range(last+1,item))) \
                #       == item - last - 1:
                #    last = item
                #    continue
                #
                if first == last:
                    ranges.append(str(first))
                else:
                    ranges.append(str(first) + "-" + str(last))
                first = item
                last = item
        if first == last:
            ranges.append(str(first))
        else:
            ranges.append(str(first) + "-" + str(last))
        return ",".join(ranges)

    def coverage(self, use_branch):
        if use_branch:
            total = 0
            cover = 0
            for line in self.branches.keys():
                for branch in self.branches[line].keys():
                    total += 1
                    cover += self.branches[line][branch] > 0 and 1 or 0
        else:
            total = len(self.all_lines)
            cover = len(self.covered)

        percent = total and str(int(100.0 * cover / total)) or "--"
        return total, cover, percent

    def summary(self, use_branch):
        tmp = self.fname  # root_filter.sub('', self.fname)
        if not self.fname.endswith(tmp):
            # Do no truncation if the filter does not start matching at
            # the beginning of the string
            tmp = self.fname
        tmp = tmp.ljust(40)
        if len(tmp) > 40:
            tmp = tmp + "\n" + " " * 40

        (total, cover, percent) = self.coverage(use_branch)
        uncovered_lines = self.uncovered_str(False, use_branch)
        if not use_branch:
            t = self.uncovered_str(True, use_branch)
            if len(t):
                uncovered_lines += " [* " + t + "]"
        return (total, cover,
                tmp + str(total).rjust(8) + str(cover).rjust(8) +
                percent.rjust(6) + "%   " + uncovered_lines)


def process_gcov(_cov_data, file_path, _config):
    working_back = os.getcwd()
    f_dir, f_name = os.path.split(file_path)
    os.chdir(f_dir)

    _cmd = ["gcov", "--branch-counts", "--branch-probabilities", file_path]
    env = dict(os.environ)
    env['LC_ALL'] = 'en_US'

    subprocess.Popen(_cmd, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    # out, err = subprocess.Popen(cmd, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    # out = out.decode('utf-8')
    # err = err.decode('utf-8')

    os.chdir(working_back)

    # find all gcov_s and filter them according to source path
    gcov_s = get_gcov_files(_config.data_dirs)

    for gfile in gcov_s:
        if os.path.exists(gfile):
            lines = tuple(open(gfile, 'r'))
            line = lines[0]
            file_ref = re.search(r":Source:(.*$)", line, re.M).group(1)

            # check if referenced file exists in source path
            contains = False
            if _config.source_dir in os.path.normpath(file_ref):
                contains = True

            if contains:
                # get namespace->folder structure
                if _config.out_dir is not None and os.path.exists(_config.out_dir):
                    _dir, _file = os.path.split(file_ref)
                    g_dir, g_file = os.path.split(gfile)

                    ns = os.path.relpath(_dir, _config.source_dir)
                    dir_destination = os.path.join(_config.out_dir, ns)
                    if not os.path.exists(dir_destination):
                        mkdir_recursive(dir_destination)

                    moveto = os.path.join(dir_destination, g_file)
                    if os.path.exists(moveto) and os.path.isfile(moveto):
                        os.remove(moveto)
                    os.rename(gfile, moveto)
                    sys.stdout.write(
                        "Copying file \"%s\" to \"%s.\"\n"
                        % (g_file, moveto,))

                    # #######################################################################################################
                    # generate data report
                    _input = open(moveto, "r")
                    line = _input.readline()
                    segments = line.split(':', 3)
                    if len(segments) != 4 or not segments[2].lower().strip().endswith('source'):
                        raise RuntimeError(
                            'Fatal error parsing gcov file, line1: \n\t"%s"' % line.rsplit()
                        )

                    # parse gcov
                    excluding = []
                    non_code = set()
                    uncovered = set()
                    uncovered_exceptional = set()
                    covered = {}
                    branches = {}
                    line_no = 0
                    last_code_line = ""
                    last_code_line_no = 0
                    last_code_line_excluded = False
                    for line in _input:
                        segments = line.split(":", 2)
                        tmp = segments[0].strip()
                        if len(segments) > 1:
                            try:
                                line_no = int(segments[1].strip())
                            except ValueError:
                                pass  # keep previous line number!

                        if exclude_line_flag in line:
                            excl_line = False
                            for header, flag in exclude_line_pattern.findall(line):
                                if flag == 'START':
                                    excluding.append((header, line_no))
                                elif flag == 'STOP':
                                    if excluding:
                                        _header, _line = excluding.pop()
                                        if _header != header:
                                            sys.stderr.write(
                                                "(WARNING) %s_EXCL_START found on line %s "
                                                "was terminated by %s_EXCL_STOP on line %s, "
                                                "when processing %s\n"
                                                % (_header, _line, header, line_no, g_file)
                                            )
                                    else:
                                        sys.stderr.write(
                                            "(WARNING) mismatched coverage exclusion flags.\n"
                                            "\t%s_EXCL_STOP found on line %s without "
                                            "corresponding %s_EXCL_START, when processing %s\n"
                                            % (header, line_no, header, g_file)
                                        )
                                elif flag == 'LINE':
                                    excl_line = True
                            if excl_line:
                                excluding.append(False)

                        is_code_statement = False
                        if tmp[0] == '-' or (excluding and tmp[0] in "#=0123456789"):
                            is_code_statement = True
                            # code = segments[2].strip()
                            # remember certain non-executed lines
                            if excluding or is_non_code(segments[2]):
                                non_code.add(line_no)
                        elif tmp[0] == '#':
                            is_code_statement = True
                            if is_non_code(segments[2]):
                                non_code.add(line_no)
                            else:
                                uncovered.add(line_no)
                        elif tmp[0] == '=':
                            is_code_statement = True
                            uncovered_exceptional.add(line_no)
                        elif tmp[0] in "0123456789":
                            is_code_statement = True
                            covered[line_no] = int(segments[0].strip())
                        elif tmp.startswith('branch'):
                            exclude_branch = False
                            exclude_reason = ''
                            if line_no == last_code_line_no:
                                if last_code_line_excluded:
                                    exclude_branch = True
                                    exclude_reason = "marked with exclude pattern"
                                else:
                                    code = last_code_line
                                    code = re.sub(cpp_style_comment_pattern, '', code)
                                    code = re.sub(c_style_comment_pattern, '', code)
                                    code = code.strip()
                                    code_no_space = code.replace(' ', '')
                                    exclude_branch = code in ['', '{', '}'] or code_no_space == '{}'
                                    exclude_reason = "detected as compiler-generated code"

                            if exclude_branch:
                                sys.stdout.write(
                                    "Excluding unreachable branch on line %d "
                                    "in file %s (%s).\n"
                                    % (line_no, g_file, exclude_reason)
                                )
                            else:
                                fields = line.split()
                                try:
                                    count = int(fields[3])
                                except ValueError:
                                    count = 0

                                branches.setdefault(line_no, {})[int(fields[1])] = count
                        elif tmp.startswith('call'):
                            pass
                        elif tmp.startswith('function'):
                            pass
                        elif tmp[0] == 'f':
                            pass
                            # if first_record:
                            # first_record=False
                            # uncovered.add(prev)
                            # if prev in uncovered:
                            # tokens=re.split('[ \t]+',tmp)
                            # if tokens[3] != "0":
                            # uncovered.remove(prev)
                            # prev = int(segments[1].strip())
                            # first_record=True
                        else:
                            sys.stderr.write(
                                "(WARNING) Unrecognized GCOV output: '%s'\n"
                                "\tThis is indicitive of a gcov output parse error.\n"
                                "\tPlease report this to the gcovr developers." % tmp
                            )

                        if is_code_statement:
                            last_code_line = "".join(segments[2:])
                            last_code_line_no = line_no
                            last_code_line_excluded = False
                            if excluding:
                                last_code_line_excluded = True

                        if excluding and not excluding[-1]:
                            excluding.pop()

                    if file_ref not in _cov_data:
                        _cov_data[file_ref] = CoverageData(
                            file_ref, ns, uncovered, uncovered_exceptional, covered, branches, non_code
                        )
                    else:
                        _cov_data[file_ref].update(
                            ns, uncovered, uncovered_exceptional, covered, branches, non_code
                        )

                    _input.close()

                    # #############################################################################

                else:
                    sys.stderr.write("")
            else:
                os.remove(gfile)


class Config(object):
    def __init__(self, source_dir='', out_dir='', rep_dir='', data_dirs=''):
        self.source_dir = source_dir
        self.out_dir = out_dir
        self.rep_dir = rep_dir
        self.data_dirs = data_dirs


def main(args=None):
    parser = OptionParser()
    parser.add_option(
        "--version",
        help="Print the version number and exit.",
        action="store_true",
        dest="version",
        default=False,
    )

    parser.add_option(
        "-s", "--source",
        help="Set directory to project source files (.hpp, .cpp). "
             "This directory will be used as filter for gcov results and "
             "as root directory for output directory sub-folders structure. "
             "(i.e. <source>/xx/yy/zz) -> <output>/xx/yy/zz",
        metavar="DIRECTORY",
        action="store",
        dest="source_dir",
        default=None
    )

    parser.add_option(
        "-d", "--data-directory",
        help="Specify the directory that contains .gcda of .gcov data files. "
             "Sub-directories will be also included in search. "
             "Multiple directories could be split by semicolon \";\".",
        metavar="DIRECTORY",
        action="append",
        dest="data_dirs",
        default=[]
    )

    parser.add_option(
        "-o", "--output",
        help="Specify the directory where .gcov files will be stored. "
             "In that directory will be created directory structure according "
             "to file corresponding source file location relative to <source> directory.",
        metavar="DIRECTORY",
        action="store",
        dest="out_dir",
        default=None
    )

    parser.add_option(
        "-r", "--report-directory",
        help="Specify the directory where HTML reports will be stored. Note "
             "that main file \"index.html\" will be stored directly in that directory "
             "and sub-reports for each source files in corresponding location according to "
             "source file location relative to <source> directory",
        metavar="DIRECTORY",
        action="store",
        dest="rep_dir",
        default=None
    )

    parser.usage = "gcov-reporter [options]"
    parser.description = "A utility to run gcov and generate coverage report."

    # process options
    options, _args = parser.parse_args(args)

    if options.version:
        sys.stdout.write(
            "gcov-reporter %s\n"
            "\n"
            "Copyright (c) 2016 Freescale Semiconductor, Inc.\n"
            "Copyright (c) 2017 NXP\n"
            % (__version__,)
        )
        sys.exit(0)

    if options.rep_dir is None and options.out_dir is None and not options.data_dirs and not options.source_dirs:
        parser.print_help()
        sys.exit(0)

    if options.source_dir is None:
        sys.stderr.write(
            "(ERROR) -s, --source must be specified."
        )
        sys.exit(0)
    else:
        if not os.path.exists(options.source_dir):
            sys.stderr.write(
                "(ERROR) directory specified by -s, --source\n"
                "\"%s\" is not exist."
                % (options.source_dir,)
            )
            sys.exit(0)
    source_dir = options.source_dir

    if not options.data_dirs:
        sys.stderr.write(
            "(ERROR) -d, --data-directory must be specified."
        )
        sys.exit(0)
    else:
        for itm in options.data_dirs:
            if not os.path.exists(itm):
                sys.stderr.write(
                    "(ERROR) directory specified by -d, --data-directory\n"
                    "\"%s\" is not exist."
                    % (itm,)
                )
                sys.exit(0)
    data_dirs = options.data_dirs

    if options.out_dir is None:
        sys.stdout.write(
            "-o, --output was not specified. <Current directory>/output will be used."
        )
        out_dir = os.path.join(os.getcwd(), "output")
    else:
        if not os.path.exists(options.out_dir):
            sys.stdout.write(
                "Specified path \"%s\" is not exist. default path ./output will be used."
                % (options.out_dir,)
            )
            out_dir = os.path.join(os.getcwd(), "output")
        else:
            out_dir = options.out_dir

    if options.rep_dir is None:
        sys.stdout.write(
            "-r, --report-directory was not specified. <Current directory>/report will be used instead."
        )
        rep_dir = os.path.join(os.getcwd(), "report")
    else:
        if not os.path.exists(options.rep_dir):
            sys.stdout.write(
                "Specified path \"%s\" is not exist. Default path ./report will be used."
                % (options.rep_dir,)
            )
            rep_dir = os.path.join(os.getcwd(), "report")
        else:
            rep_dir = options.rep_dir

    config = Config(source_dir, out_dir, rep_dir, data_dirs)

    data_files = get_data_files(config.data_dirs)

    cov_data = {}
    for df in data_files:
        process_gcov(cov_data, df, config)

    print_summary(cov_data)
    print_html_report(cov_data, config.rep_dir, config)


if __name__ == '__main__':
    main()
