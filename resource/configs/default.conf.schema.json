{
  "id": "package.conf.jsonp",
  "definitions": {
    "IProjectAuthor": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "email": {
          "type": "string"
        }
      }
    },
    "IProjectSplashScreenLabel": {
      "type": "object",
      "properties": {
        "text": {
          "type": "string"
        },
        "x": {
          "type": "integer"
        },
        "y": {
          "type": "integer"
        },
        "width": {
          "type": "integer",
          "minimum": 0
        },
        "height": {
          "type": "integer",
          "minimum": 0
        },
        "justify": {
          "type": "string"
        },
        "visible": {
          "type": "boolean"
        },
        "font": {
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            },
            "size": {
              "type": "integer",
              "minimum": 0
            },
            "bold": {
              "type": "boolean"
            },
            "color": {
              "type": "string"
            }
          }
        }
      }
    },
    "IProjectDependencyScriptConfig": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "path": {
          "type": "string"
        },
        "ignore-parent-attribute": {
          "type": "boolean"
        },
        "attributes": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "array",
              "items": {
                "anyOf": [
                  {
                    "type": "string"
                  },
                  {
                    "$ref": "#/definitions/IProjectDependencyScriptConfig"
                  }
                ]
              }
            }
          ]
        }
      }
    },
    "IProjectTargetResourceFiles": {
      "type": "object",
      "properties": {
        "cwd": {
          "type": "string"
        },
        "src": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          ]
        },
        "dest": {
          "type": "string"
        }
      }
    },
    "IProjectTarget": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "companyName": {
          "type": "string"
        },
        "copyright": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "icon": {
          "type": "string"
        },
        "remotePort": {
          "anyOf": [
            {
              "type": "integer",
              "minimum": -1,
              "maximum": -1
            },
            {
              "type": "integer",
              "minimum": 0
            }
          ]
        },
        "width": {
          "type": "integer",
          "minimum": 0
        },
        "height": {
          "type": "integer",
          "minimum": 0
        },
        "maximized": {
          "type": "boolean"
        },
        "resources": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "input": {
                "type": "string"
              },
              "files": {
                "type": "array",
                "items": {
                  "$ref": "#/definitions/IProjectTargetResourceFiles"
                }
              },
              "embed": {
                "type": "boolean"
              },
              "output": {
                "type": "string"
              },
              "copy": {
                "type": "boolean"
              },
              "skip-replace": {
                "type": "boolean"
              },
              "resx": {
                "type": "boolean"
              },
              "postBuildCopy": {
                "type": "boolean"
              },
              "keepLinks": {
                "type": "boolean"
              },
              "platforms": {
                "anyOf": [
                  {
                    "type": "string"
                  },
                  {
                    "type": "array",
                    "items": {
                      "type": "string"
                    }
                  }
                ]
              },
              "toolchain": {
                "anyOf": [
                  {
                    "type": "string"
                  },
                  {
                    "type": "array",
                    "items": {
                      "type": "string"
                    }
                  }
                ]
              }
            }
          }
        },
        "platforms": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "object",
              "properties": {
                "type": {
                  "type": "string"
                },
                "os": {
                  "type": "string"
                },
                "arch": {
                  "type": "string"
                },
                "toolchain": {
                  "type": "string"
                }
              }
            },
            {
              "type": "array",
              "items": {
                "anyOf": [
                  {
                    "type": "string"
                  },
                  {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "os": {
                        "type": "string"
                      },
                      "arch": {
                        "type": "string"
                      },
                      "toolchain": {
                        "type": "string"
                      }
                    }
                  }
                ]
              }
            }
          ]
        },
        "elevate": {
          "type": "boolean"
        },
        "singletonApp": {
          "type": "boolean"
        },
        "startPage": {
          "type": "string"
        },
        "codeSigning": {
          "type": "object",
          "properties": {
            "enabled": {
              "type": "boolean"
            },
            "certPath": {
              "type": "string"
            },
            "serverConfig": {
              "type": "string"
            },
            "serverLocation": {
              "type": "string"
            },
            "password": {
              "type": "string"
            }
          }
        },
        "upx": {
          "type": "object",
          "properties": {
            "enabled": {
              "type": "boolean"
            },
            "options": {
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          }
        },
        "splashScreen": {
          "default": 1,
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "object",
              "properties": {
                "url": {
                  "type": "string"
                },
                "width": {
                  "type": "integer",
                  "minimum": 0
                },
                "height": {
                  "type": "integer",
                  "minimum": 0
                },
                "background": {
                  "type": "string"
                },
                "installPath": {
                  "type": "string"
                },
                "executable": {
                  "type": "string"
                },
                "executableArgs": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "localization": {
                  "type": "string"
                },
                "userControls": {
                  "type": "object",
                  "properties": {
                    "label": {
                      "$ref": "#/definitions/IProjectSplashScreenLabel"
                    },
                    "labelStatic": {
                      "$ref": "#/definitions/IProjectSplashScreenLabel"
                    },
                    "progress": {
                      "type": "object",
                      "properties": {
                        "x": {
                          "type": "integer",
                          "minimum": 0
                        },
                        "y": {
                          "type": "integer",
                          "minimum": 0
                        },
                        "width": {
                          "type": "integer",
                          "minimum": 0
                        },
                        "height": {
                          "type": "integer",
                          "minimum": 0
                        },
                        "marquee": {
                          "type": "boolean"
                        },
                        "visible": {
                          "type": "boolean"
                        },
                        "trayProgress": {
                          "type": "boolean"
                        },
                        "background": {
                          "type": "string"
                        },
                        "foreground": {
                          "type": "string"
                        }
                      }
                    }
                  }
                },
                "resources": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "location": {
                        "type": "string"
                      },
                      "copyOnly": {
                        "type": "boolean"
                      },
                      "type": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      }
                    }
                  }
                }
              }
            }
          ]
        },
        "toolchain": {
          "type": "string"
        },
        "toolchainSettings": {
          "type": "object",
          "properties": {
            "cwd": {
              "type": "string"
            },
            "version": {
              "type": "string"
            }
          }
        },
        "skipped": {
          "type": "boolean"
        },
        "configs": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "wuiModulesType": {
          "type": "string"
        },
        "requestPatterns": {
          "default": 1,
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "array"
            }
          ]
        },
        "hubLocation": {
          "type": "string"
        },
        "embedFeatures": {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      }
    },
    "IDeployServer": {
      "properties": {
        "prod": {
          "type": "string"
        },
        "dev": {
          "type": "string"
        },
        "eap": {
          "type": "string"
        }
      }
    },
    "IDeployUpdates": {
      "properties": {
        "name": {
          "type": "string"
        },
        "skipped": {
          "type": "boolean"
        },
        "register": {
          "type": "string"
        },
        "location": {
          "type": "object",
          "properties": {
            "path": {
              "$ref": "#/definitions/IDeployServer"
            },
            "projectName": {
              "type": "string"
            },
            "releaseName": {
              "type": "string"
            },
            "platform": {
              "type": "string"
            },
            "version": {
              "type": "string"
            }
          }
        },
        "agents": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "hub": {
                "type": "string"
              },
              "name": {
                "type": "string"
              },
              "skipped": {
                "type": "boolean"
              },
              "profile": {
                "type": "string"
              }
            }
          }
        },
        "app": {
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            },
            "path": {
              "type": "string"
            },
            "config": {
              "type": "string"
            },
            "args": {
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          }
        }
      }
    }
  },
  "type": "object",
  "properties": {
    "extends": {
      "type": "string"
    },
    "schema": {
      "type": "string"
    },
    "builder": {
      "type": "object",
      "properties": {
        "location": {
          "type": "string"
        },
        "version": {
          "type": "string"
        },
        "port": {
          "type": "integer"
        }
      }
    },
    "name": {
      "type": "string"
    },
    "version": {
      "type": "string"
    },
    "namespaces": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "description": {
      "type": "string"
    },
    "license": {
      "type": "string"
    },
    "homepage": {
      "type": "string"
    },
    "author": {
      "$ref": "#/definitions/IProjectAuthor"
    },
    "contributors": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/IProjectAuthor"
      }
    },
    "private": {
      "type": "boolean"
    },
    "repository": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string"
        },
        "url": {
          "type": "string"
        }
      }
    },
    "loaderClass": {
      "type": "string"
    },
    "target": {
      "$ref": "#/definitions/IProjectTarget"
    },
    "deploy": {
      "type": "object",
      "properties": {
        "server": {
          "$ref": "#/definitions/IDeployServer"
        },
        "agentsHub": {
          "type": "string"
        },
        "chunkSize": {
          "type": "integer",
          "minimum": 0
        },
        "updates": {
          "type": "object",
          "additionalProperties": {
            "$ref": "#/definitions/IDeployUpdates"
          }
        }
      }
    },
    "releases": {
      "type": "object",
      "additionalProperties": {
        "$ref": "#/definitions/IProjectTarget"
      }
    },
    "docs": {
      "type": "object",
      "properties": {
        "systemName": {
          "type": "string"
        },
        "copyright": {
          "type": "string"
        }
      }
    },
    "dependencies": {
      "type": "object",
      "additionalProperties": {
        "anyOf": [
          {
            "type": "string"
          },
          {
            "type": "object",
            "properties": {
              "version": {
                "type": "string"
              },
              "name": {
                "type": "string"
              },
              "groupId": {
                "type": "string"
              },
              "location": {
                "anyOf": [
                  {
                    "type": "string"
                  },
                  {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "path": {
                        "anyOf": [
                          {
                            "type": "string"
                          },
                          {
                            "type": "object",
                            "properties": {
                              "win": {
                                "anyOf": [
                                  {
                                    "type": "string"
                                  },
                                  {
                                    "type": "object",
                                    "properties": {
                                      "x86": {
                                        "type": "string"
                                      },
                                      "x64": {
                                        "type": "string"
                                      }
                                    }
                                  }
                                ]
                              },
                              "linux": {
                                "type": "string"
                              },
                              "imx": {
                                "type": "string"
                              },
                              "mac": {
                                "type": "string"
                              }
                            }
                          }
                        ]
                      },
                      "branch": {
                        "type": "string"
                      },
                      "releaseName": {
                        "type": "string"
                      },
                      "platform": {
                        "type": "string"
                      }
                    }
                  }
                ]
              },
              "configure-script": {
                "$ref": "#/definitions/IProjectDependencyScriptConfig"
              },
              "install-script": {
                "$ref": "#/definitions/IProjectDependencyScriptConfig"
              },
              "platforms": {
                "anyOf": [
                  {
                    "type": "string"
                  },
                  {
                    "type": "array",
                    "items": {
                      "type": "string"
                    }
                  }
                ]
              }
            }
          }
        ]
      }
    }
  }
}
