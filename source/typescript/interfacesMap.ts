/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    export let IBuilderHubTaskBasePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnMessage",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    export let IBuilderHubTaskPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnError"
            ]);
        }();
}

namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    export let IBuilderServerPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnMessage",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    export let IDockerBasePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    export let IDockerPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnError"
            ]);
        }();
}

namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    export let IDockerProgressPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnMessage"
            ]);
        }();
}

namespace Com.Wui.Framework.Builder.Utils {
    "use strict";
    export let IVBoxSession : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Copy",
                "Execute"
            ]);
        }();
}

namespace Com.Wui.Framework.Builder.Utils {
    "use strict";
    export let IVBoxPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    export let IBuilderHubRegisterPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnTaskRequest",
                "OnMessage"
            ], Com.Wui.Framework.Services.Connectors.IAcknowledgePromise);
        }();
}

namespace Com.Wui.Framework.Builder.Interfaces {
    "use strict";
    export let IBaseTask : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Name",
                "Process",
                "getDependenciesTree",
                "setEnvironment",
                "setOwner"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

/* tslint:enable */
