/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.WebServiceApi.Clients {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import IBuilderServerCredentials = Com.Wui.Framework.Builder.Interfaces.IBuilderServerCredentials;
    import OwnCloudFile = Com.Wui.Framework.Builder.Primitives.OwnCloudFile;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class OwnCloudClient extends BaseObject {
        private configuration : IBuilderServerCredentials;
        private capabilities : any;
        private version : string;
        private isLogged : boolean;
        private authToken : string;
        private reconnectCounter : number;
        private readonly maxReconnection : number;

        constructor($configuration : IBuilderServerCredentials) {
            super();
            this.configuration = $configuration;
            this.isLogged = false;
            this.reconnectCounter = 0;
            this.maxReconnection = 5;
        }

        public IsAuthorized($callback : ($status : boolean) => void) {
            this.logIn($callback);
        }

        public List($path : string, $depth : string, $callback : ($items : OwnCloudFile[]) => void) : void {
            this.davRequest("PROPFIND", $path, {
                depth: $depth
            }, "", ($elements : any) : void => {
                const items : OwnCloudFile[] = [];
                if (!ObjectValidator.IsBoolean($elements)) {
                    if (!ObjectValidator.IsArray($elements)) {
                        $elements = [$elements];
                    }
                    $elements.forEach(($element : any) : void => {
                        let name : string = $element["d:href"];
                        name = StringUtils.Substring(name, StringUtils.IndexOf(name, "/webdav/") + 8);
                        if (!StringUtils.StartsWith(name, "/")) {
                            name = "/" + name;
                        }
                        name = ObjectDecoder.Utf8(ObjectEncoder.Utf8(ObjectDecoder.Url(name)));
                        items.push(new OwnCloudFile(name, $element["d:propstat"]["d:prop"]));
                    });
                }
                $callback(items);
            });
        }

        public FileInfo($path : string, $callback : ($info : OwnCloudFile) => void) : void {
            this.List($path, "0", ($items : OwnCloudFile[]) : void => {
                $callback($items.length === 1 ? $items[0] : null);
            });
        }

        public Exists($path : string, $callback : ($status : boolean) => void) : void {
            this.FileInfo($path, ($info : OwnCloudFile) : void => {
                $callback(!ObjectValidator.IsEmptyOrNull($info));
            });
        }

        public getFile($path : string, $localPath : string, $callback : ($status : boolean) => void) : void {
            const fs : Types.NodeJS.fs = require("fs");
            const path : Types.NodeJS.path = require("path");
            this.logIn(() : void => {
                if (!fs.existsSync(path.dirname($localPath))) {
                    fs.mkdirSync(path.dirname($localPath));
                }
                this.httpRequest({
                    headers    : {
                        "content-type": "application/octet-stream"
                    },
                    url        : this.pathToUrl($path),
                    writeStream: $localPath
                }, $callback);
            });
        }

        public CreateFolder($path : string, $callback : ($status : boolean) => void) : void {
            this.davRequest("MKCOL", $path, null, "", $callback);
        }

        public Delete($path : string, $callback : ($status : boolean) => void) : void {
            this.davRequest("DELETE", $path, null, "", $callback);
        }

        public PutFile($path : string, $localPath : string, $callback : ($status : boolean) => void) : void {
            const fs : Types.NodeJS.fs = require("fs");
            this.logIn(() : void => {
                const info : any = fs.statSync($localPath);
                this.httpRequest({
                    headers   : {
                        "content-length": info.size,
                        "x-oc-mtime"    : info.mtime
                    },
                    method    : "PUT",
                    readStream: $localPath,
                    url       : this.pathToUrl($path)
                }, $callback);
            });
        }

        private logIn($callback : ($status : boolean) => void) : void {
            if (!this.isLogged) {
                const parser : any = require("xml2js").parseString;
                this.authToken = "Basic " + Buffer.from(this.configuration.user + ":" + this.configuration.pass).toString("base64");
                this.httpRequest({
                    headers: {
                        "ocs-apirequest": true
                    },
                    url    : this.configuration.url + "/ocs/v1.php/cloud/capabilities"
                }, ($headers : string, $body : string) : void => {
                    parser($body, {explicitArray: false}, ($error : Error, $data : any) : void => {
                        if ($error !== null) {
                            LogIt.Error(this.getClassName(), $error);
                        }
                        const body : any = $data.ocs.data;
                        this.capabilities = body.capabilities;
                        this.version = body.version.string + "-" + body.version.edition;
                        this.isLogged = true;
                        $callback(true);
                    });
                });
            } else {
                $callback(true);
            }
        }

        private pathToUrl($path : string) : string {
            if (ObjectValidator.IsEmptyOrNull($path)) {
                return "/";
            }
            if (!StringUtils.StartsWith($path, "/")) {
                $path = "/" + $path;
            }
            if (!StringUtils.EndsWith($path, "/")) {
                $path += "/";
            }
            $path = ObjectEncoder.Utf8($path);
            $path = ObjectEncoder.Url($path);
            $path = StringUtils.Replace($path, "%2F", "/");
            return this.configuration.url + "/remote.php/webdav" + ObjectEncoder.Utf8($path);
        }

        private davRequest($method : string, $path : string, $headers : any, $body : string,
                           $callback : ($response : any) => void) : void {
            this.logIn(() : void => {
                this.httpRequest({
                    body   : $body,
                    headers: $headers,
                    method : $method,
                    url    : this.pathToUrl($path)
                }, ($statusOrHeaders : boolean | string, $body : string) : void => {
                    if (ObjectValidator.IsBoolean($statusOrHeaders)) {
                        $callback($statusOrHeaders);
                    } else {
                        const parser : any = require("xml2js").parseString;
                        parser($body, {explicitArray: false}, ($error : Error, $data : any) : void => {
                            if ($error !== null) {
                                LogIt.Error(this.getClassName(), $error);
                            }
                            $callback($data["d:multistatus"]["d:response"]);
                        });
                    }
                });
            });
        }

        private httpRequest($options : any, $callback? : ($statusOrHeaders : boolean | string, $body? : string | Buffer) => void) : void {
            const url : Types.NodeJS.url = require("url");
            const http : Types.NodeJS.http = require("http");
            const https : Types.NodeJS.https = require("https");
            const fs : Types.NodeJS.fs = require("fs");
            const options : any = {
                body   : "",
                headers: {
                    authorization: this.authToken
                },
                method : "GET",
                url    : ""
            };

            const mergeOptions : any = ($source : any, $target : any) : void => {
                let property : string;
                for (property in $source) {
                    if ($source.hasOwnProperty(property) && !ObjectValidator.IsEmptyOrNull($source[property])) {
                        if (ObjectValidator.IsObject($source[property]) && $target.hasOwnProperty(property)) {
                            mergeOptions($source[property], $target[property]);
                        } else {
                            $target[property] = $source[property];
                        }
                    }
                }
            };
            mergeOptions($options, options);

            const urlDetails : any = url.parse(options.url);
            if (urlDetails.protocol === "http:" || urlDetails.protocol === "https:") {
                let client : any = http;
                if (urlDetails.protocol === "https:") {
                    client = https;
                }
                const onError : any = ($message : string) : void => {
                    this.reconnectCounter = 0;
                    LogIt.Warning($message);
                    $callback(false, $message);
                };
                urlDetails.method = options.method;
                urlDetails.headers = options.headers;
                const httpRequest : any = client.request(urlDetails, ($httpResponse : any) : void => {
                    switch ($httpResponse.statusCode) {
                    case 200:
                    case 207:
                        const length : number = StringUtils.ToInteger($httpResponse.headers["content-length"]);
                        const isChunked : boolean = !ObjectValidator.IsEmptyOrNull($httpResponse.headers["transfer-encoding"]) &&
                            StringUtils.ContainsIgnoreCase($httpResponse.headers["transfer-encoding"], "chunked");
                        let currentLength : number = 0;
                        try {
                            let fsStream : any = null;
                            let streamBuffers : any[] = [];
                            if (!ObjectValidator.IsEmptyOrNull(options.writeStream)) {
                                fsStream = fs.createWriteStream(options.writeStream);
                                $httpResponse.pipe(fsStream);
                            }
                            let counter : number = 0;
                            $httpResponse.on("data", ($chunk : string) : void => {
                                currentLength += $chunk.length;
                                if (ObjectValidator.IsEmptyOrNull(options.writeStream)) {
                                    streamBuffers.push($chunk);
                                }
                                if (counter > 5 && counter < 100) {
                                    Echo.Print(".");
                                } else if (counter > 100) {
                                    if (!isNaN(length)) {
                                        Echo.Println(" (" + Math.floor(100 / length * currentLength) + "%)");
                                    } else {
                                        Echo.Println("");
                                    }
                                    counter = 0;
                                }
                                counter++;
                            });

                            let completed : boolean = false;
                            const onComplete : any = () : void => {
                                if (!completed) {
                                    if (counter > 5) {
                                        Echo.Println(" (100%)");
                                    }
                                    this.reconnectCounter = 0;
                                    completed = true;
                                    $callback($httpResponse.headers, Buffer.concat(streamBuffers));
                                }
                            };
                            let connectionClosed : boolean = false;
                            $httpResponse.on("close", () : void => {
                                if (currentLength !== length && !isChunked) {
                                    connectionClosed = true;
                                    streamBuffers = [];
                                    if (this.reconnectCounter < this.maxReconnection) {
                                        LogIt.Warning("Reconnecting ...");
                                        httpRequest.end();
                                        this.httpRequest(options, $callback);
                                    } else {
                                        LogIt.Error("Connection has been lost.");
                                    }
                                    this.reconnectCounter++;
                                } else {
                                    onComplete();
                                }
                            });
                            if (ObjectValidator.IsEmptyOrNull(fsStream)) {
                                $httpResponse.on("end", () : void => {
                                    if (!connectionClosed) {
                                        onComplete();
                                    }
                                });
                            } else {
                                fsStream.on("finish", () : void => {
                                    if (!connectionClosed) {
                                        onComplete();
                                    }
                                });
                            }
                        } catch (ex) {
                            LogIt.Error(ex);
                        }
                        break;
                    case 201:
                    case 204:
                        this.reconnectCounter = 0;
                        $callback(true);
                        break;
                    case 301:
                    case 302:
                        options.url = $httpResponse.headers.location;
                        this.httpRequest(options, $callback);
                        break;
                    case 401:
                        onError("Unauthorized operation");
                        break;
                    case 404:
                        onError("Element not found on cloud storage");
                        break;
                    case 405:
                        onError("Operation not allowed");
                        break;
                    default:
                        onError("Bad response status code: " + $httpResponse.statusCode);
                        break;
                    }
                });

                httpRequest.on("error", ($ex : Error) : void => {
                    if (this.reconnectCounter < this.maxReconnection) {
                        LogIt.Warning($ex.message);
                        this.httpRequest(options, $callback);
                    } else {
                        LogIt.Error($ex.message);
                    }
                    this.reconnectCounter++;
                });

                if (ObjectValidator.IsEmptyOrNull(options.readStream)) {
                    if (options.body !== "") {
                        httpRequest.write(options.body);
                    }
                    httpRequest.end();
                } else {
                    let counter : number = 0;
                    const length : number = StringUtils.ToInteger(options.headers["content-length"]);
                    let currentLength : number = 0;
                    fs.createReadStream(options.readStream)
                        .on("data", ($chunk : string) : void => {
                            currentLength += StringUtils.Length($chunk);
                            if (counter > 5 && counter < 100) {
                                Echo.Print(".");
                            } else if (counter > 100) {
                                if (!isNaN(length)) {
                                    Echo.Println(" (" + Math.floor(100 / length * currentLength) + "%)");
                                } else {
                                    Echo.Println("");
                                }
                                counter = 0;
                            }
                            counter++;
                        })
                        .on("end", () : void => {
                            if (counter > 5) {
                                Echo.Println(" (100%)");
                            }
                        })
                        .pipe(httpRequest);
                }
            } else {
                LogIt.Error("Selected protocol \"" + urlDetails.protocol + "\" is not supported." +
                    " Please use http://, https:// or file:// protocol instead.");
            }
        }
    }
}
