/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Structures {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import CliTaskType = Com.Wui.Framework.Builder.Enums.CliTaskType;
    import LogSeverity = Com.Wui.Framework.Commons.Enums.LogSeverity;

    export class ProgramArgs extends Com.Wui.Framework.Localhost.Structures.ProgramArgs {
        private tasks : string[];
        private isHubClient : boolean;
        private isServiceClient : boolean;
        private isBuild : boolean;
        private isTest : boolean;
        private readonly options : string[];
        private port : number;
        private isForce : boolean;
        private readonly internalOptions : ProgramArgsOptions;
        private startAgent : boolean;
        private agentName : string;
        private isExternalModuleTask : boolean;

        constructor() {
            super();
            this.tasks = [];
            this.options = [];
            this.isHubClient = false;
            this.isServiceClient = false;
            this.isBuild = false;
            this.isTest = false;
            this.port = -1;
            this.isForce = false;
            this.internalOptions = {
                /* tslint:disable: object-literal-sort-keys object-literal-shorthand */
                noServer        : false,
                file            : "",
                to              : "",
                newVersion      : "",
                description     : "",
                forwardArgs     : [],
                noSudo          : false,
                noTarget        : false,
                selfextract     : false,
                selfupdate      : false,
                project         : "",
                isServiceTask   : false,
                isAgentTask     : false,
                isForwardTask   : false,
                withDependencies: false,
                logLevel        : LogSeverity.HIGH,
                withTest        : false
                /* tslint:enable */
            };
            this.startAgent = false;
            this.agentName = "";
            this.isExternalModuleTask = false;
        }

        public getTasks() : string[] {
            return this.tasks;
        }

        public IsServiceClient($value? : boolean) : boolean {
            return this.isServiceClient = Property.Boolean(this.isServiceClient, $value);
        }

        public IsHubClient($value? : boolean) : boolean {
            return this.isHubClient = Property.Boolean(this.isHubClient, $value);
        }

        public Port($value? : number) : number {
            return this.port = Property.PositiveInteger(this.port, $value, 1);
        }

        public IsForce($value? : boolean) : boolean {
            return this.isForce = Property.Boolean(this.isForce, $value);
        }

        public getOptions() : ProgramArgsOptions {
            return this.internalOptions;
        }

        public StartAgent($value? : boolean) : boolean {
            return this.startAgent = Property.Boolean(this.startAgent, $value);
        }

        public AgentName($value? : string) : string {
            return this.agentName = Property.String(this.agentName, $value);
        }

        public IsForwardedTask() : boolean {
            return !ObjectValidator.IsEmptyOrNull(process.env.IS_WUI_PROXY_TASK) && <any>process.env.IS_WUI_PROXY_TASK === 1 ||
                this.internalOptions.isServiceTask || this.internalOptions.isAgentTask || this.internalOptions.isForwardTask;
        }

        public IsServiceTask() : boolean {
            return this.internalOptions.isServiceTask;
        }

        public IsAgentTask() : boolean {
            return this.internalOptions.isAgentTask;
        }

        public IsExternalModuleTask() : boolean {
            return this.isExternalModuleTask;
        }

        public PrintHelp() : void {
            let body : string = "";
            let question : string = "";
            if (this.isBuild) {
                body =
                    "Usage: build <type> [--with-test]                                            \n" +
                    "                                                                             \n" +
                    "where <type> is one of:                                                      \n" +
                    "   prod, eap, dev, help                                                      \n" +
                    "                                                                             \n" +
                    "Tasks description:                                                           \n" +
                    "   help                Involved overview                                     \n" +
                    "   dev                 Build development type of release                     \n" +
                    "   dev --with-test     Build development type of release with testing        \n" +
                    "   eap                 Build EAP type of release                             \n" +
                    "                         all tests are skipped                               \n" +
                    "                         all development code is removed                     \n" +
                    "                         code optimize is turned on                          \n" +
                    "   prod                Build production type of release                      \n" +
                    "                         all tests are performed                             \n";
                question = "Hit Enter for exit or specify build type and args";
            } else if (this.isTest) {
                body =
                    "Usage: test <type>                                                           \n" +
                    "                                                                             \n" +
                    "where <type> is one of:                                                      \n" +
                    "   all, unit, coverage, selenium, lint                                       \n" +
                    "                                                                             \n" +
                    "Tasks description:                                                           \n" +
                    "   help                Involved overview                                     \n" +
                    "   all                 Run all implemented tests                             \n" +
                    "   unit                Run only unit tests                                   \n" +
                    "   unit <filePath>     Run only single unit test with desired file path      \n" +
                    "                         from the root test folder                           \n" +
                    "   coverage            Run only code coverage test                           \n" +
                    "   selenium            Run only integral testing provided by selenium        \n" +
                    "   lint                Run only lint tests                                   \n";
                question = "Hit Enter for exit or specify test type";
            } else {
                body =
                    "Basic options:                                                               \n" +
                    "   -h [ --help ]      Prints application help description.                   \n" +
                    "   -v [ --version ]   Prints application version message.                    \n" +
                    "   -p [--path]        Print current WUI Connector location.                  \n" +
                    "   --base=<path>      Specify project base path.                             \n" +
                    "                                                                             \n" +
                    "Mode options:                                                                \n" +
                    "   service [stop]     Start or stop WUI Builder service                      \n" +
                    "   agent              Connect WUI Builder service to WUI Hub                 \n" +
                    "   --agent-name       Specify name for agent instance                        \n" +
                    "                                                                             \n" +
                    "Tasks options:                                                               \n" +
                    "   update             Update current WUI Builder instance                    \n" +
                    "   install            Prepare project dependencies and settings              \n" +
                    "   build              Build project with desired type and runtime env.       \n" +
                    "   test               Run test of source code or linting                     \n" +
                    "   run                Build and run development release as plugin            \n" +
                    "   docs               Provide automatically generated documentation          \n" +
                    "   clean              Clean up all files generated by builder tasks          \n" +
                    "   <task_name>        Execute specific builder task.                         \n" +
                    "                                                                             \n" +
                    "Other options:                                                               \n" +
                    "   --force            Force execute task.                                    \n" +
                    "   --service          Specify if task should be forwarded to service.        \n" +
                    "   --port=<value>     Specify port for service mode.                         \n" +
                    "   --hub              Specify if task should be forwarded to WUI Hub.        \n" +
                    "   -- <cmd> <args>    Forward args directly to WUI Builder toolchain.        \n";
            }
            this.getHelp("" +
                "WUI Framework Builder                                                        \n" +
                "   - build tool based on Node.js                                             \n" +
                "                                                                             \n" +
                "   copyright:         Copyright (c) 2014-2016 Freescale Semiconductor, Inc., \n" +
                "                      Copyright (c) 2017-2019 NXP                            \n" +
                "   author:            Jakub Cieslar, jakub.cieslar@nxp.com                   \n" +
                "   license:           BSD-3-Clause License distributed with this material    \n",
                body, "",
                question
            );
        }

        public IsBaseTask() : boolean {
            return this.IsVersion() || this.IsPath() || this.IsHelp();
        }

        public Normalize($args : string[]) : string[] {
            return $args;
        }

        protected processArgs($args : ArrayList<string>) : void {
            $args.foreach(($value : string, $key : string) : void => {
                if (!this.isExternalModuleTask) {
                    if (!this.processArg($key, $value)) {
                        Echo.Printf("Unrecognized program option: {0}", $key);
                    }
                } else {
                    this.options.push($key);
                }
            });
            if (this.isExternalModuleTask) {
                this.internalOptions.forwardArgs = [].concat(this.options);
            } else if (this.isServiceClient || this.isHubClient) {
                this.internalOptions.forwardArgs = [].concat(this.tasks.concat(this.options));
            } else {
                if (this.isBuild || this.isTest) {
                    this.IsHelp($args.Length() === 1 && this.options.length === 0);
                    let buildTask : string = "";
                    let isUnit : boolean = false;
                    this.options.forEach(($option : string) : void => {
                        if (this.isBuild) {
                            switch ($option) {
                            case "prod":
                            case "eap":
                            case "beta":
                            case "alpha":
                            case "dev":
                                buildTask = $option;
                                break;
                            case "--with-test":
                                this.internalOptions.withTest = true;
                                break;
                            case "--skip-test":
                                this.internalOptions.withTest = false;
                                break;
                            default:
                                process.stderr.write("" +
                                    "ERROR:\n" +
                                    "   \"" + $option + "\" is not any of supported build type.");
                                setTimeout(() : void => {
                                    Loader.getInstance().Exit(-1);
                                }, 3000);
                                break;
                            }
                        } else {
                            switch ($option) {
                            case "all":
                            case "selenium":
                            case "lint":
                                this.tasks.push("test:" + $option);
                                break;
                            case "unit":
                                this.tasks.push("run-unit-test");
                                isUnit = true;
                                break;
                            case "coverage":
                                this.tasks.push("run-coverage-test");
                                isUnit = true;
                                break;
                            default:
                                if (isUnit && ObjectValidator.IsEmptyOrNull(this.internalOptions.file)) {
                                    this.internalOptions.file = $option;
                                } else {
                                    process.stderr.write("" +
                                        "ERROR:\n" +
                                        "   \"" + $option + "\" is not any of supported test type.");
                                    setTimeout(() : void => {
                                        Loader.getInstance().Exit(-1);
                                    }, 3000);
                                }
                                break;
                            }
                        }
                    });
                    if (!ObjectValidator.IsEmptyOrNull(buildTask)) {
                        this.tasks.push("build:" + buildTask);
                    }
                } else if (this.options.length !== 0) {
                    this.tasks = this.tasks.concat(this.options);
                    if (this.IsServiceClient() || this.IsHubClient()) {
                        this.internalOptions.forwardArgs = [].concat(this.tasks);
                        this.internalOptions.forwardArgs.shift();
                        this.tasks = [this.tasks[0]];
                    }
                }
            }
        }

        protected processArg($key : string, $value : string) : boolean {
            switch ($key) {
            case "service":
                this.StartServer(true);
                this.internalOptions.noTarget = true;
                break;
            case "--port":
                this.Port(StringUtils.ToInteger($value));
                break;
            case "stop":
                this.StartServer(false);
                this.StopServer(true);
                this.internalOptions.noTarget = true;
                break;
            case "agent":
                this.StartAgent(true);
                this.internalOptions.noTarget = true;
                break;
            case "--agent-name":
                this.AgentName($value);
                break;
            case "--service":
                this.IsServiceClient(true);
                break;
            case "--hub":
                this.IsHubClient(true);
                break;
            case "--base":
                this.TargetBase($value);
                break;
            case "install":
            case "run":
            case "docs":
                this.tasks.push($key);
                break;
            case "clean":
                this.tasks.push("project-cleanup:all");
                break;
            case "build":
                this.isBuild = true;
                if (this.IsServiceClient() || this.IsHubClient()) {
                    this.tasks.push($key);
                }
                break;
            case "test":
                this.isTest = true;
                if (this.IsServiceClient() || this.IsHubClient()) {
                    this.tasks.push($key);
                }
                break;
            case "task":
                this.tasks.push($value);
                break;
            case "grunt":
                if (this.IsServiceClient() || this.IsHubClient()) {
                    this.tasks.push($key);
                }
                break;
            case CliTaskType.ALPHA:
            case CliTaskType.BETA:
            case CliTaskType.EAP:
            case CliTaskType.PROD:
            case CliTaskType.DEV:
            case CliTaskType.DEV_SKIP_TEST:
                if ($key === CliTaskType.ALPHA || $key === CliTaskType.BETA) {
                    $key = CliTaskType.EAP;
                }
                if (!this.isBuild) {
                    this.tasks.push("build:" + $key);
                } else {
                    this.options.push($key);
                }
                break;
            case CliTaskType.REBUILD:
            case CliTaskType.REBUILD_DEV:
            case CliTaskType.REBUILD_DEV_SKIP_TEST:
                this.tasks.push("build-machine:" + $key);
                break;
            case "--with-test":
                this.internalOptions.withTest = true;
                break;
            case "--force":
                this.IsForce(true);
                break;
            case "update":
                this.internalOptions.selfupdate = true;
                this.internalOptions.noTarget = true;
                break;
            case "--":
                this.options.splice(0, this.options.length);
                this.isExternalModuleTask = true;
                this.internalOptions.noTarget = true;
                break;
            default:
                if (!this.processOtherOptions($key, $value) && !super.processArg($key, $value)) {
                    this.options.push($key);
                }
                break;
            }
            return true;
        }

        private processOtherOptions($key : string, $value : string) : boolean {
            switch ($key) {
            case "--no-server":
                this.internalOptions.noServer = true;
                break;
            case "--file":
                this.internalOptions.file = $value;
                break;
            case "--to":
                this.internalOptions.to = $value;
                break;
            case "--new-version":
                this.internalOptions.newVersion = $value;
                break;
            case "--description":
                this.internalOptions.description = $value;
                break;
            case "--no-sudo":
                this.internalOptions.noSudo = true;
                break;
            case "--no-target":
                this.internalOptions.noTarget = true;
                break;
            case "--selfextract":
                this.internalOptions.noTarget = true;
                this.internalOptions.selfextract = true;
                break;
            case "--project":
                this.internalOptions.project = $value;
                break;
            case "--service-task":
                this.internalOptions.isServiceTask = true;
                this.internalOptions.noServer = true;
                break;
            case "--agent-task":
                this.internalOptions.isAgentTask = true;
                this.internalOptions.noServer = true;
                break;
            case "--forward-task":
                this.internalOptions.isForwardTask = true;
                break;
            case "--with-dependencies":
                this.internalOptions.withDependencies = true;
                break;
            case "--log-level":
                this.internalOptions.logLevel = StringUtils.ToInteger($value);
                break;
            default:
                return false;
            }
            return true;
        }
    }

    export abstract class ProgramArgsOptions {
        public noServer : boolean;
        public file : string;
        public to : string;
        public newVersion : string;
        public description : string;
        public forwardArgs : string[];
        public noSudo : boolean;
        public noTarget : boolean;
        public selfextract : boolean;
        public selfupdate : boolean;
        public project : string;
        public isServiceTask : boolean;
        public isAgentTask : boolean;
        public isForwardTask : boolean;
        public withDependencies : boolean;
        public logLevel : number;
        public withTest : boolean;
    }
}
