/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Structures {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ProductType = Com.Wui.Framework.Builder.Enums.ProductType;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;
    import ArchType = Com.Wui.Framework.Builder.Enums.ArchType;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class BuildProductArgs extends BaseObject {
        private type : ProductType;
        private os : OSType;
        private arch : ArchType;
        private toolchain : ToolchainType;

        constructor() {
            super();
            this.setDefaults();
        }

        public Parse($value : string | IBuildPlatform, $toolchain? : string) : void {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.setDefaults();
                if (!ObjectValidator.IsEmptyOrNull($toolchain)) {
                    LogIt.Warning("Toolchain should be specified as part of platform definition object.");
                }
                if (ObjectValidator.IsString($value)) {
                    $value = StringUtils.ToLowerCase(<string>$value);
                    if (!ObjectValidator.IsEmptyOrNull($toolchain)) {
                        $toolchain = StringUtils.ToLowerCase($toolchain);
                        this.Toolchain($toolchain);
                    }
                    if (StringUtils.StartsWith(<string>$value, "lib")) {
                        $value = StringUtils.Remove(<string>$value, "-" + ProductType.STATIC);
                        if (StringUtils.Contains(<string>$value, "-" + ProductType.SHARED)) {
                            $value = StringUtils.Remove(<string>$value, "-" + ProductType.SHARED);
                            this.Type(ProductType.SHARED);
                        }
                    }

                    const splitters : number = StringUtils.OccurrenceCount(<string>$value, "-");
                    if (splitters === 0) {
                        if (StringUtils.Contains(<string>$value, "32")) {
                            LogIt.Warning(" Conversion to 64-bit architecture has been done, " +
                                "because specification of 32-bit architecture has been deprecated for inline platform values. " +
                                "Use platform object instead if 32-bit architecture is required.");
                            $value = StringUtils.Remove(<string>$value, "32");
                        }
                        $value = StringUtils.Remove(<string>$value, "64");

                        switch ($value) {
                        case "web":
                            if (this.Toolchain() === ToolchainType.JDK) {
                                this.Type(ProductType.SHARED);
                            }
                            break;

                        case OSType.WIN:
                            this.Type(ProductType.APP);
                            if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                                this.Toolchain(ToolchainType.CHROMIUM_RE);
                            }
                            break;
                        case OSType.LINUX:
                            this.Type(ProductType.APP);
                            this.OS(OSType.LINUX);
                            if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                                this.Toolchain(ToolchainType.GCC);
                            }
                            break;
                        case OSType.MAC:
                            this.Type(ProductType.APP);
                            this.OS(OSType.MAC);
                            break;
                        case ToolchainType.ARM:
                            if (ObjectValidator.IsEmptyOrNull($toolchain) || $toolchain !== ToolchainType.NODEJS) {
                                this.OS(OSType.LINUX);
                                this.Toolchain(ToolchainType.ARM);
                            } else {
                                this.Type(ProductType.APP);
                                this.OS(OSType.IMX);
                                this.Toolchain(ToolchainType.NODEJS);
                            }
                            break;

                        case "lib":
                        case "libwin":
                            if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                                this.Toolchain(ToolchainType.GCC);
                            }
                            break;
                        case "liblinux":
                            this.OS(OSType.LINUX);
                            if (this.Toolchain() !== ToolchainType.ARM) {
                                this.Toolchain(ToolchainType.GCC);
                            }
                            break;
                        case "libmac":
                            this.OS(OSType.MAC);
                            break;

                        case ToolchainType.ECLIPSE:
                            if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                                this.Type(ProductType.PLUGIN);
                                this.Toolchain(ToolchainType.ECLIPSE);
                            } else if ($toolchain === ToolchainType.JDK) {
                                this.Type(ProductType.SHARED);
                                this.Toolchain(ToolchainType.ECLIPSE);
                            }
                            break;
                        case ToolchainType.IDEA:
                            if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                                this.Type(ProductType.PLUGIN);
                                this.Toolchain(ToolchainType.IDEA);
                            } else if ($toolchain === ToolchainType.JDK) {
                                this.Type(ProductType.SHARED);
                                this.Toolchain(ToolchainType.IDEA);
                            }
                            break;

                        case OSType.ANDROIND:
                            this.Type(ProductType.APP);
                            this.OS(OSType.ANDROIND);
                            this.Toolchain(ToolchainType.PHONEGAP);
                            break;
                        case OSType.WINPHONE:
                            this.Type(ProductType.APP);
                            this.OS(OSType.WINPHONE);
                            this.Toolchain(ToolchainType.PHONEGAP);
                            break;

                        case ToolchainType.NODEJS:
                            this.Type(ProductType.SHARED);
                            this.Toolchain(ToolchainType.NODEJS);
                            break;
                        case ProductType.INSTALLER:
                            this.Type(ProductType.INSTALLER);
                            break;

                        default:
                            LogIt.Debug("Unrecognized build argument option: {0}", $value);
                            break;
                        }
                    } else if (splitters === 2) {
                        const parts : string[] = StringUtils.Split(<string>$value, "-");
                        this.Type(parts[0]);
                        this.OS(StringUtils.Remove(parts[1], "32", "64"));
                        this.Arch(StringUtils.Contains(parts[1], "32") ? "x86" : "x64");
                        this.Toolchain(parts[2]);
                    } else {
                        LogIt.Error("Parsing of \"" + $value + "\" has failed, " +
                            "because build platform must have 0 or 2 \"-\" delimiters.");
                    }
                } else {
                    const platformObject : IBuildPlatform = <IBuildPlatform>$value;
                    this.Type(platformObject.type);
                    this.Arch(platformObject.arch);
                    this.Toolchain(platformObject.toolchain);
                    if (this.Toolchain() === ToolchainType.CHROMIUM_RE) {
                        this.Type(ProductType.APP);
                    } else if (this.Toolchain() === ToolchainType.ARM) {
                        this.OS(OSType.LINUX);
                    } else if (this.Toolchain() === ToolchainType.APT_GET) {
                        this.Type(ProductType.PACKAGE);
                        this.OS(OSType.LINUX);
                    } else {
                        this.OS((<IBuildPlatform>$value).os);
                    }
                }
            }
        }

        public Type($value? : ProductType) : ProductType {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                $value = StringUtils.ToLowerCase($value.toString());
            }
            return this.type = Property.EnumType(this.type, $value, ProductType, ProductType.STATIC);
        }

        public OS($value? : OSType) : OSType {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                $value = StringUtils.ToLowerCase($value.toString());
            }
            return this.os = Property.EnumType(this.os, $value, OSType, OSType.WIN);
        }

        public Arch($value? : ArchType) : ArchType {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                $value = StringUtils.ToLowerCase($value.toString());
                if ($value === "32-bit" || $value === "32bit") {
                    $value = ArchType.X86;
                } else if ($value === "64-bit" || $value === "64bit") {
                    $value = ArchType.X64;
                }
            }
            return this.arch = Property.EnumType(this.arch, $value, ArchType, ArchType.X64);
        }

        public Toolchain($value? : ToolchainType) : ToolchainType {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                $value = StringUtils.ToLowerCase($value.toString());
                $value = StringUtils.Remove($value.toString(), "-");
            }
            return this.toolchain = Property.EnumType(this.toolchain, $value, ToolchainType, ToolchainType.NONE);
        }

        public Value() : string {
            const arch : string = this.os === OSType.WIN && this.arch === ArchType.X86 ? "32" : "";
            return this.type + "-" + this.os + arch + "-" + this.toolchain;
        }

        public toString() : string {
            return this.Value();
        }

        private setDefaults() : void {
            this.type = ProductType.STATIC;
            this.os = OSType.WIN;
            this.arch = ArchType.X64;
            this.toolchain = ToolchainType.NONE;
        }
    }

    export abstract class IBuildPlatform {
        public os? : string;
        public type? : string;
        public arch? : string;
        public toolchain? : string;
    }
}
