/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Structures {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;
    import IBuildArgs = Com.Wui.Framework.Builder.Interfaces.IBuildArgs;

    export class TaskEnvironment extends Com.Wui.Framework.Localhost.Structures.ProgramArgs {
        private project : IProject;
        private properties : IProperties;
        private programArgs : ProgramArgs;
        private buildArgs : IBuildArgs;

        public Project($value? : IProject) : IProject {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.project = $value;
            }
            return this.project;
        }

        public Properties($value? : IProperties) : IProperties {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.properties = $value;
            }
            return this.properties;
        }

        public ProgramArgs($value? : ProgramArgs) : ProgramArgs {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.programArgs = $value;
            }
            return this.programArgs;
        }

        public BuildArgs($value? : IBuildArgs) : IBuildArgs {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.buildArgs = $value;
            }
            return this.buildArgs;
        }
    }
}
