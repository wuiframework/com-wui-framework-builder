/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder {
    "use strict";
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import IProjectDependencyScriptConfig = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyScriptConfig;
    import IProjectDependency = Com.Wui.Framework.Builder.Interfaces.IProjectDependency;
    import IProjectDependencyLocation = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocation;
    import IProjectTargetResources = Com.Wui.Framework.Builder.Interfaces.IProjectTargetResources;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import CliTaskType = Com.Wui.Framework.Builder.Enums.CliTaskType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IProjectTargetResourceFiles = Com.Wui.Framework.Builder.Interfaces.IProjectTargetResourceFiles;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;
    import IAppConfiguration = Com.Wui.Framework.Builder.Interfaces.IAppConfiguration;
    import Resources = Com.Wui.Framework.Builder.DAO.Resources;
    import IProjectDependencyLocationPath = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocationPath;
    import IProjectDependencyLocationArch = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocationArch;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;
    import IBuildArgs = Com.Wui.Framework.Builder.Interfaces.IBuildArgs;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;
    import ITestConfig = Com.Wui.Framework.Builder.Interfaces.ITestConfig;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;
    import DeployServerType = Com.Wui.Framework.Builder.Enums.DeployServerType;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;
    import JsonUtils = Com.Wui.Framework.Commons.Utils.JsonUtils;

    export class EnvironmentArgs extends Com.Wui.Framework.Localhost.EnvironmentArgs {
        private project : IProject;
        private properties : IProperties;
        private buildArgs : IBuildArgs;
        private testConfig : ITestConfig;
        private fileSystem : FileSystemHandler;

        public getProjectConfig() : IAppConfiguration {
            return <IAppConfiguration>super.getProjectConfig();
        }

        public getTargetConfig() : IProject {
            return this.project;
        }

        public getAppProperties() : IProperties {
            return this.properties;
        }

        public getBuildArgs() : IBuildArgs {
            return this.buildArgs;
        }

        public getTestConfig() : ITestConfig {
            return this.testConfig;
        }

        public getRealProjectPath($path : string) : string {
            if ($path !== "..") {
                $path = StringUtils.Replace($path, "\\", "/");
                const workspace : any = /\[WORKSPACE_(.*?)\]\//gm.exec($path);
                const appConfig : IAppConfiguration = this.getProjectConfig();
                if (workspace !== null && workspace.length === 2 &&
                    appConfig.workspaces.hasOwnProperty(workspace[1])) {
                    $path = StringUtils.Replace($path,
                        "[WORKSPACE_" + workspace[1] + "]/", appConfig.workspaces[workspace[1]]);
                } else {
                    const cloudspace : any = /\[CLOUDBASE_(.*?)\]\//gm.exec($path);
                    if (cloudspace !== null && cloudspace.length === 2) {
                        let syncBase : string = appConfig.ownCloud.syncBase;
                        if (ObjectValidator.IsEmptyOrNull(syncBase)) {
                            LogIt.Error("Unable to synchronize cloud folder due to missing " +
                                "ownCloud.syncBase property at builder configuration.");
                        }
                        if (!StringUtils.EndsWith(syncBase, "/")) {
                            syncBase += "/";
                        }
                        syncBase += cloudspace[1] + "/";
                        if (!this.fileSystem.Exists(syncBase)) {
                            this.fileSystem.CreateDirectory(syncBase);
                        }
                        $path = StringUtils.Replace($path, "[CLOUDBASE_" + cloudspace[1] + "]/", syncBase);
                    }
                }
            }
            return $path;
        }

        public UpdateStats() : void {
            PersistenceFactory.getPersistence(this.getClassName()).Variable("Stats", this.properties.stats);
        }

        public ResolveDependencyPath($path : string | IProjectDependencyLocationPath) : string {
            let path : string;
            if (ObjectValidator.IsString($path)) {
                path = <string>$path;
            } else {
                let pathWithArch : string | IProjectDependencyLocationArch = "";
                if (EnvironmentHelper.IsWindows()) {
                    pathWithArch = (<IProjectDependencyLocationPath>$path).win;
                } else if (EnvironmentHelper.IsMac()) {
                    pathWithArch = (<IProjectDependencyLocationPath>$path).mac;
                } else {
                    pathWithArch = (<IProjectDependencyLocationPath>$path).linux;
                }
                if (!ObjectValidator.IsEmptyOrNull(pathWithArch)) {
                    if (ObjectValidator.IsString(pathWithArch)) {
                        path = <string>pathWithArch;
                    } else {
                        if (EnvironmentHelper.Is64bit()) {
                            path = (<IProjectDependencyLocationArch>pathWithArch).x64;
                        } else {
                            path = (<IProjectDependencyLocationArch>pathWithArch).x86;
                        }
                    }
                }
                if (ObjectValidator.IsEmptyOrNull($path)) {
                    LogIt.Error("Unmet dependency path based on builder platform and architecture. " +
                        "Specify platform specific paths for win, linux and mac as location.path attribute. " +
                        "Optionally can be specified x86 or x64 architecture.");
                }
            }
            return path;
        }

        public LoadTargetEnvironment() : void {
            const url : Types.NodeJS.url = require("url");
            const path : Types.NodeJS.path = require("path");
            const stripJsonComments : any = require("strip-json-comments");
            const ZSchema : any = require("z-schema");
            const loader : Loader = Loader.getInstance();
            this.fileSystem = loader.getFileSystemHandler();

            this.project = <any>{
                /* tslint:disable: object-literal-sort-keys */
                builder    : {
                    location: "host",
                    version : "",
                    port    : -1
                },
                version    : "",
                namespaces : ["Com.Wui"],
                loaderClass: "",
                target     : {
                    name           : "",
                    companyName    : "NXP Semiconductors",
                    copyright      : "Copyright (c) 2014-2016 Freescale Semiconductor, Inc., Copyright (c) 2017-2019 NXP",
                    description    : "",
                    icon           : "resource/graphics/icon.ico",
                    remotePort     : -1,
                    width          : 1036,
                    height         : 780,
                    maximized      : false,
                    resources      : [],
                    platforms      : ["web"],
                    elevate        : false,
                    singletonApp   : false,
                    startPage      : "",
                    codeSigning    : {
                        enabled     : false,
                        certPath    : "",
                        serverConfig: ""
                    },
                    upx            : {
                        enabled: false,
                        options: [
                            "--compress-resources=0",
                            "--compress-icons=0",
                            "--compress-exports=0",
                            "--strip-relocs=0",
                            "--best"
                        ]
                    },
                    splashScreen   : {
                        width         : 900,
                        height        : 600,
                        background    : "#ffffff",
                        installPath   : "",
                        executable    : "",
                        executableArgs: [],
                        userControls  : {
                            label      : {
                                text   : "",
                                x      : 0,
                                y      : 0,
                                width  : 200,
                                height : 35,
                                justify: "left",
                                visible: true,
                                font   : {
                                    name : "Courier New",
                                    size : 14,
                                    bold : false,
                                    color: "#ff0000"
                                }
                            },
                            labelStatic: {
                                text   : "",
                                x      : 0,
                                y      : 0,
                                width  : 200,
                                height : 35,
                                justify: "left",
                                visible: true,
                                font   : {
                                    name : "Courier New",
                                    size : 14,
                                    bold : false,
                                    color: "#0050ff"
                                }
                            },
                            progress   : {
                                background  : "#ffff00",
                                x           : 0,
                                y           : 50,
                                width       : 400,
                                height      : 20,
                                marquee     : false,
                                visible     : true,
                                trayProgress: false,
                                foreground  : "#ff00ff"
                            }
                        },
                        resources     : []
                    },
                    toolchain      : "",
                    configs        : [],
                    wuiModulesType : DeployServerType.PROD,
                    requestPatterns: [],
                    embedFeatures  : []
                },
                deploy     : {
                    server   : {
                        prod: "",
                        dev : "",
                        eap : ""
                    },
                    chunkSize: 1024 * 1024 * 1.5 // 1.5 MB
                },
                releases   : {},
                docs       : {
                    systemName: "",
                    copyright : ""
                }
                /* tslint:enable */
            };
            this.project.docs.copyright = this.project.target.copyright;

            const appConfig : IAppConfiguration = this.getProjectConfig();
            if (ObjectValidator.IsEmptyOrNull(appConfig.workspaces)) {
                appConfig.workspaces = <any>{};
            }
            let workspace : string;
            for (workspace in appConfig.workspaces) {
                if (appConfig.workspaces.hasOwnProperty(workspace)) {
                    let path : string = appConfig.workspaces[workspace];
                    if (!ObjectValidator.IsEmptyOrNull(path)) {
                        path = StringUtils.Replace(path, "\\", "/");
                        if (!StringUtils.EndsWith(path, "/")) {
                            path += "/";
                        }
                    } else {
                        path = "";
                    }
                    appConfig.workspaces[workspace] = path;
                }
            }
            const args : ProgramArgs = loader.getProgramArgs();

            const binBase : string = args.BinBase();
            let targetBase : string = args.TargetBase(Loader.getInstance().getFileSystemHandler().NormalizePath(
                this.getRealProjectPath(args.TargetBase())));
            if (targetBase === args.BinBase()) {
                targetBase = args.TargetBase(Loader.getInstance().getFileSystemHandler().NormalizePath(process.cwd()));
            }
            loader.setAnonymizer({
                targetBase
            });

            const sourceRoot : string = "/source/typescript/";
            const sources : string[] = this.fileSystem.Expand([
                targetBase + sourceRoot + "*/*", targetBase + "/dependencies/*" + sourceRoot + "*/*"
            ]);
            sources.forEach(($path : string) : void => {
                const namespace = StringUtils.Replace(
                    StringUtils.Substring($path, StringUtils.IndexOf($path, sourceRoot) + StringUtils.Length(sourceRoot)),
                    "/", ".");
                if (this.project.namespaces.indexOf(namespace) === -1) {
                    this.project.namespaces.push(namespace);
                }
            });

            this.project.deploy.server = appConfig.deployServer;
            if (!args.getOptions().noTarget) {
                LogIt.Info("Processing WUI Builder task at: \"" + targetBase + "\"");
                process.cwd = () : string => {
                    return targetBase;
                };
            } else {
                LogIt.Info("Processing WUI Builder task without target.");
                Resources.Extend(this.project, appConfig);
            }
            const projectConfPath : string = targetBase + "/package.conf.json";
            if (!this.fileSystem.Exists(projectConfPath)) {
                if (!args.getOptions().noTarget) {
                    LogIt.Error("Target folder \"" + targetBase + "\" has not been recognized as WUI Project root");
                }
            } else {
                Resources.Extend(this.project, JSON.parse(this.fileSystem.Read(projectConfPath).toString()));
            }
            if (this.project.hasOwnProperty("dependencies")) {
                const fail : any = ($key : string) : void => {
                    LogIt.Error("It is not allowed to specify local path of \"" + $key + "\" in package.conf.json file, " +
                        "but configuration can be overridden in private.conf.json file.");
                };
                const checkScript : any = ($name : string, $source : any, $scriptName : string) : void => {
                    if ($source.hasOwnProperty($scriptName)) {
                        const script : IProjectDependencyScriptConfig = $source[$scriptName];
                        if (script.hasOwnProperty("path")) {
                            const scriptPath : string = script.path;
                            if (path.isAbsolute(scriptPath)) {
                                fail($name + ":" + $scriptName + ":path");
                            }
                        }
                    }
                };
                Object.keys(this.project.dependencies).forEach(($name : string) : void => {
                    const source : IProjectDependency = this.project.dependencies[$name];
                    if (typeof source !== "object") {
                        if ((<any>source).startsWith("!")) {
                            LogIt.Error("It is not allowed to force bypass dependency branches tree check. " +
                                "This feature is allowed only in private.conf.json file.");
                        } else if ((<any>source).startsWith("dir+")) {
                            LogIt.Error("It is not allowed to specify local repo as dependency by package.conf.json file, " +
                                "but configuration of dependencies with remote repo can be overridden by private.conf.json file.");
                        }
                    } else if (typeof source === "object") {
                        if (source.hasOwnProperty("location")) {
                            const location : IProjectDependencyLocation = <IProjectDependencyLocation>source.location;
                            if (location.hasOwnProperty("type") && location.type === "dir") {
                                fail($name + ":location");
                            }

                            if (location.hasOwnProperty("path")) {
                                const path : string = this.ResolveDependencyPath((<IProjectDependencyLocation>location).path);
                                if (url.parse(path).protocol === "file:") {
                                    fail($name + ":location:path");
                                }
                            }
                        }

                        checkScript($name, source, "configure-script");
                        checkScript($name, source, "install-script");
                    }
                });
            }

            let name : string;
            for (name in this.project.target.resources) {
                if (this.project.target.resources.hasOwnProperty(name)) {
                    const resource : IProjectTargetResources = this.project.target.resources[name];
                    let hasAbsolutePath : boolean = false;
                    if (resource.hasOwnProperty("input") && path.isAbsolute(resource.input)) {
                        hasAbsolutePath = true;
                    }
                    if (!hasAbsolutePath && resource.hasOwnProperty("files")) {
                        resource.files.forEach(($file : IProjectTargetResourceFiles) : void => {
                            if (!hasAbsolutePath) {
                                if ($file.hasOwnProperty("cwd") && path.isAbsolute($file.cwd)) {
                                    hasAbsolutePath = true;
                                } else {
                                    if (ObjectValidator.IsString($file.src)) {
                                        hasAbsolutePath = path.isAbsolute(<string>$file.src);
                                    } else {
                                        (<string[]>$file.src).forEach(($input : string) : void => {
                                            if (!hasAbsolutePath) {
                                                hasAbsolutePath = path.isAbsolute($input);
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                    if (hasAbsolutePath) {
                        LogIt.Error(
                            "It is not allowed to specify local resource input path for target:resources[" + name + "] " +
                            "in package.conf.json file, but configuration can be overridden in private.conf.json file.");
                    }
                }
            }

            const wuiPatternExists : any = ($pattern : string) : boolean => {
                let result : boolean = false;
                if (this.fileSystem.Expand(targetBase + "/" + $pattern).length === 0) {
                    const filter : string[] = [targetBase + "/dependencies/*/package.conf.json"];
                    OSType.getProperties().forEach(($value : string) : void => {
                        filter.push(targetBase + "/dependencies/*/" + OSType[$value] + "/package.conf.json");
                    });
                    const dependencies : string[] = this.fileSystem.Expand(filter);
                    let index : number;
                    for (index = 0; index < dependencies.length; index++) {
                        const dependencyPath : string = path.dirname(dependencies[index]);
                        if (this.fileSystem.Expand(dependencyPath + "/" + $pattern).length > 0) {
                            result = true;
                            break;
                        }
                    }
                } else {
                    result = true;
                }
                return result;
            };

            this.properties = <any>{
                /* tslint:disable: object-literal-sort-keys */
                binBase,
                binBaseUnix             : StringUtils.Replace(binBase, "\\", "/"),
                projectBase             : targetBase,
                externalModules         : args.AppDataPath() + "/external_modules",
                sources                 : "source/**",
                resources               : "resource",
                dependencies            : "dependencies/*/source/**",
                javaDependencies        : [],
                javaProjectVersion      : "",
                mavenResourcePaths      : [],
                mavenArtifacts          : [],
                mavenRepositories       : [],
                wuiMavenModules         : [],
                dependencyMavenArtifacts: [],
                cppDependencies         : [],
                cppIncludes             : "",
                cppLibraries            : "",
                wuiDependencies         : [],
                mobileExtensions        : {
                    android : ".apk",
                    ios     : ".ipa",
                    winphone: ".appx"
                },
                dependenciesResources   : "dependencies/*/resource",
                tmp                     : "build/compiled/source",
                dest                    : "build/target",
                compiled                : "build/compiled",
                packageName             : "",
                loaderPath              : "resource/javascript/loader.min.js",
                nodejsPort              : 35666,
                licenseText             : "/* Copyright (c) 2014-2016 Freescale Semiconductor, Inc., Copyright (c) 2017-2019 NXP. " +
                    "The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution " +
                    "or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText. */",
                stats                   : {
                    buildMinutes: 0
                },
                projectHas              : {
                    TypeScript   : {
                        Source: () : boolean => {
                            const result : boolean = wuiPatternExists("source/typescript/**/*.ts");
                            this.properties.projectHas.TypeScript.Source = () : boolean => {
                                return result;
                            };
                            return result;
                        },
                        Tests : () : boolean => {
                            const result : boolean = wuiPatternExists("test/unit/typescript/**/*.ts");
                            this.properties.projectHas.TypeScript.Tests = () : boolean => {
                                return result;
                            };
                            return result;
                        }
                    },
                    Cpp          : {
                        Source: () : boolean => {
                            const result : boolean = wuiPatternExists("source/cpp/**/*.cpp");
                            this.properties.projectHas.Cpp.Source = () : boolean => {
                                return result;
                            };
                            return result;
                        },
                        Tests : () : boolean => {
                            const result : boolean = wuiPatternExists("test/unit/cpp/**/*.cpp");
                            this.properties.projectHas.Cpp.Tests = () : boolean => {
                                return result;
                            };
                            return result;
                        }
                    },
                    Java         : {
                        Source: () : boolean => {
                            const result : boolean = wuiPatternExists("source/java/**/*.java");
                            this.properties.projectHas.Java.Source = () : boolean => {
                                return result;
                            };
                            return result;
                        },
                        Tests : () : boolean => {
                            const result : boolean = wuiPatternExists("test/unit/java/**/*.java");
                            this.properties.projectHas.Java.Tests = () : boolean => {
                                return result;
                            };
                            return result;
                        }
                    },
                    SASS         : () : boolean => {
                        const result : boolean = wuiPatternExists("resource/sass/**/*.scss");
                        this.properties.projectHas.SASS = () : boolean => {
                            return result;
                        };
                        return result;
                    },
                    Resources    : () : boolean => {
                        const result : boolean = wuiPatternExists("resource/**/*.*");
                        this.properties.projectHas.Resources = () : boolean => {
                            return result;
                        };
                        return result;
                    },
                    SeleniumTests: () : boolean => {
                        const result : boolean = wuiPatternExists("test/selenium/**/*.ts");
                        this.properties.projectHas.SeleniumTests = () : boolean => {
                            return result;
                        };
                        return result;
                    },
                    WuiContent($pattern : string) : boolean {
                        return wuiPatternExists($pattern);
                    }
                },
                buildCheckRegister      : [],
                serverConfig            : JsonUtils.Clone(this.project)
                /* tslint:enable */
            };

            if (ObjectValidator.IsEmptyOrNull(appConfig.eclipseBase)) {
                appConfig.eclipseBase = args.AppDataPath() + "/external_modules/eclipse";
            }
            if (ObjectValidator.IsEmptyOrNull(appConfig.ideaBase)) {
                appConfig.ideaBase = args.AppDataPath() + "/external_modules/idea";
            }
            if (ObjectValidator.IsEmptyOrNull(appConfig.vcvarsPath)) {
                appConfig.vcvarsPath = "C:/Program Files (x86)/Microsoft Visual Studio 14.0/VC";
            }

            this.testConfig = {
                environment: {
                    cwd : "",
                    path: ""
                }
            };

            let testConfigSubDir : string = "";
            if (EnvironmentHelper.IsWindows()) {
                testConfigSubDir = "Win";
            } else if (EnvironmentHelper.IsLinux()) {
                testConfigSubDir = "Linux";
            } else if (EnvironmentHelper.IsMac()) {
                testConfigSubDir = "Mac";
            }
            const testConfigs : string[] = this.fileSystem.Expand(
                this.properties.projectBase + "/test/resource/config/**/" + testConfigSubDir + "/test.config.json"
            );
            if (testConfigs.length === 1) {
                Resources.Extend(this.testConfig, JSON.parse(this.fileSystem.Read(testConfigs[0]).toString()));
            }

            if (this.fileSystem.Exists(targetBase + "/private.conf.json")) {
                let privateConfData : string = stripJsonComments(this.fileSystem.Read(targetBase + "/private.conf.json").toString());
                const symbols : any = appConfig.symbols;
                if (ObjectValidator.IsSet(symbols) && ObjectValidator.IsObject(symbols)) {
                    for (const item in symbols) {
                        if (symbols.hasOwnProperty(item)) {
                            privateConfData = StringUtils.Replace(privateConfData, "<? " + item + " ?>", symbols[item]);
                        }
                    }
                }
                privateConfData = StringReplace.Content(privateConfData, StringReplaceType.VARIABLES);

                const privateConf : IProject = JSON.parse(privateConfData);
                if (privateConf.hasOwnProperty("dependencies") && !this.project.hasOwnProperty("dependencies")) {
                    LogIt.Error("Dependencies can not be overridden by private.conf.json, " +
                        "because they are not specified by package.conf.json file.");
                }
                Resources.Extend(this.project, privateConf);
            }

            this.project = JSON.parse(StringReplace.Content(JSON.stringify(this.project), StringReplaceType.VARIABLES));

            if (!args.getOptions().noTarget) {
                const validator : any = new ZSchema({
                    assumeAdditional: true,
                    noExtraKeywords : true
                });
                ZSchema.registerFormat("IProjectDependency", () : boolean => {
                    return true;
                });
                const schemaConfig : any = JSON.parse(this.fileSystem.Read(
                    binBase + "/resource/configs/default.conf.schema.json").toString());
                let projectSchemaPath : string = targetBase + "/resource/configs/package.conf.schema.json";
                if (!ObjectValidator.IsEmptyOrNull(this.project.schema)) {
                    projectSchemaPath = this.project.schema;
                    if (!StringUtils.StartsWith(projectSchemaPath, "/")) {
                        projectSchemaPath = "/" + projectSchemaPath;
                    }
                    projectSchemaPath = targetBase + projectSchemaPath;
                }
                if (this.fileSystem.Exists(projectSchemaPath)) {
                    Resources.Extend(schemaConfig, JSON.parse(this.fileSystem.Read(projectSchemaPath).toString()));
                }
                validator.validate(this.project, schemaConfig);
                const configErrors : any[] = validator.getLastErrors();
                if (!ObjectValidator.IsEmptyOrNull(configErrors)) {
                    LogIt.Warning(JSON.stringify(configErrors[0], null, 2));
                    LogIt.Error("Invalid project configuration has been detected.");
                } else {
                    LogIt.Info("Configuration has been loaded without issues.");
                }
                this.project.schema = schemaConfig;
            }

            if (ObjectValidator.IsEmptyOrNull(this.project.target.name)) {
                this.project.target.name = this.project.name;
            }
            if (ObjectValidator.IsEmptyOrNull(this.project.target.description)) {
                this.project.target.description = this.project.description;
            }
            if (ObjectValidator.IsEmptyOrNull(this.project.target.codeSigning.certPath)) {
                this.project.target.codeSigning.certPath = binBase + "/resource/data/Com/Wui/Framework/Builder/CodeSign/SPC.pfx";
            }

            if (ObjectValidator.IsEmptyOrNull(this.project.docs.systemName)) {
                this.project.docs.systemName = this.project.target.name;
            }

            this.properties.javaProjectVersion = StringUtils.Replace(this.project.version, "-", ".");
            this.properties.packageName = this.project.name + "-" + StringUtils.Replace(this.project.version, ".", "-");

            if (this.project.hasOwnProperty("license") && this.project.license !== "") {
                this.properties.licenseText = "/* " + this.project.license + " */";
            }
            const stats : any = PersistenceFactory.getPersistence(this.getClassName()).Variable("Stats");
            if (!ObjectValidator.IsEmptyOrNull(stats)) {
                this.properties.stats = stats;
            }
            const allowedReleases : any = {};
            const allowedReleaseNames : string[] = [];
            const skippedReleases : string[] = [];
            let releaseName : string;
            for (releaseName in this.project.releases) {
                if (this.project.releases.hasOwnProperty(releaseName)) {
                    if (this.project.releases[releaseName].hasOwnProperty("skipped")) {
                        if (!this.project.releases[releaseName].skipped) {
                            allowedReleases[releaseName] = this.project.releases[releaseName];
                            allowedReleaseNames.push(releaseName);
                        } else {
                            skippedReleases.push(releaseName);
                        }
                    } else {
                        allowedReleases[releaseName] = this.project.releases[releaseName];
                        allowedReleaseNames.push(releaseName);
                    }
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(skippedReleases)) {
                LogIt.Warning("Some releases will be skipped: " + skippedReleases.join(", "));
            }

            this.project.releases = allowedReleases;
            if (allowedReleaseNames.length === 1) {
                Resources.Extend(this.project.target, allowedReleases[allowedReleaseNames[0]]);
            }

            if (EnvironmentHelper.IsWindows()) {
                process.env.PATH = loader.getTerminal()
                    .NormalizeEnvironmentPath(appConfig.vcvarsPath + ";");
            }

            if (!ObjectValidator.IsEmptyOrNull(appConfig.httpProxy)) {
                let http : string = StringUtils.Remove(appConfig.httpProxy, "http://", "https://");
                if (!StringUtils.StartsWith(http, "http://")) {
                    http = "http://" + http;
                }
                if (!StringUtils.EndsWith(http, "/")) {
                    http += "/";
                }
                const proxy : Types.NodeJS.Modules.url.UrlWithStringQuery = require("url").parse(http);
                const spawnSync : Types.NodeJS.Modules.child_process.spawnSync = require("child_process").spawnSync;
                const retVal : Types.NodeJS.Modules.child_process.SpawnSyncReturns<Buffer> = spawnSync(
                    "ping " + proxy.hostname + " -n 1", {
                        shell      : true,
                        windowsHide: true
                    });

                if (retVal.status === 0) {
                    process.env.HTTP_PROXY = http;
                    process.env.http_proxy = http;
                    process.env.HTTPS_PROXY = http;
                    process.env.https_proxy = http;

                    if (!StringUtils.ContainsIgnoreCase(retVal.stdout.toString(), "success")) {
                        LogIt.Info("Using proxy: " + http);
                    }
                } else {
                    appConfig.httpProxy = "";
                    LogIt.Info(">>"[ColorType.RED] + " Unable to use proxy settings: " + retVal.stdout + retVal.stderr);
                }
            }

            if (!ObjectValidator.IsEmptyOrNull(this.project.target.wuiModulesType) &&
                !DeployServerType.Contains(this.project.target.wuiModulesType)) {
                LogIt.Error("Unsupported WUI Modules type \"" + this.project.target.wuiModulesType + "\", " +
                    "please use one of: prod, dev or eap");
            }

            this.buildArgs = {
                isRebuild  : false,
                isTest     : false,
                platform   : "web",
                product    : null,
                releaseName: this.project.name,
                time       : new Date(),
                timestamp  : 0,
                type       : CliTaskType.PROD
            };
            this.UpdateProperties();
        }

        public UpdateProperties() : void {
            const serverConfig : any = this.properties.serverConfig;
            serverConfig.build = this.buildArgs;
            serverConfig.schema = "";
            serverConfig.releases = {};
            serverConfig.dependencies = {};
            serverConfig.repository = {};
            if (ObjectValidator.IsSet(serverConfig.noReport)) {
                serverConfig.noReport = false;
                serverConfig.eclipseBase = "";
                serverConfig.ideaBase = "";
            }
            serverConfig.target = JsonUtils.Clone(this.project.target);
            serverConfig.target.appDataPath = "";
            serverConfig.target.codeSigning = {};
            serverConfig.target.resources = [];
            serverConfig.loaderClass = this.project.loaderClass;

            this.properties.clientConfig = {
                /* tslint:disable: object-literal-sort-keys */
                name        : serverConfig.name,
                version     : serverConfig.version,
                description : serverConfig.description,
                license     : serverConfig.license,
                homepage    : serverConfig.homepage,
                author      : serverConfig.author,
                contributors: serverConfig.contributors,
                loaderClass : serverConfig.loaderClass,
                target      : {
                    name       : serverConfig.target.name,
                    companyName: serverConfig.target.companyName,
                    copyright  : serverConfig.target.copyright,
                    description: serverConfig.target.description,
                    appDataPath: ""
                },
                namespaces  : serverConfig.namespaces,
                build       : this.buildArgs,
                packageName : this.properties.packageName
                /* tslint:enable */
            };
            this.project.target.embedFeatures.forEach(($feature : string) : void => {
                let source : any = {
                    project: JsonUtils.Clone(this.project)
                };
                const namespaces : string[] = StringUtils.Split($feature, ".");
                let found : boolean = false;
                for (const item of namespaces) {
                    if (source.hasOwnProperty(item)) {
                        source = source[item];
                        found = true;
                    } else {
                        found = false;
                    }
                }
                if (found) {
                    const target : any = {project: {}};
                    let index : number;
                    let item : any = target;
                    for (index = 0; index < namespaces.length; index++) {
                        if (index < namespaces.length - 1) {
                            item = item[namespaces[index]] = {};
                        } else {
                            item[namespaces[index]] = source;
                        }
                    }
                    JsonUtils.Extend(this.properties.clientConfig, target.project);
                }
            });
        }
    }
}
