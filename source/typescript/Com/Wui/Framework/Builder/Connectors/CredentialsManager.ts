/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    import CredentialType = Com.Wui.Framework.Builder.Enums.CredentialType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class CredentialsManager extends BaseObject {
        private static serviceName : string = "WUIBuilder";

        public static setLinuxRoot($password : string, $callback? : ($status : boolean) => void) : void {
            CredentialsManager.setPassword(CredentialType.LINUX, $password, $callback);
        }

        public static getLinuxRoot($callback : ($value : string) => void) : void {
            CredentialsManager.getPassword(CredentialType.LINUX, $callback);
        }

        public static setPhonegapToken($password : string, $callback? : ($status : boolean) => void) : void {
            CredentialsManager.setPassword(CredentialType.PHONEGAP, $password, $callback);
        }

        public static getPhonegapToken($callback : ($value : string) => void) : void {
            CredentialsManager.getPassword(CredentialType.PHONEGAP, $callback);
        }

        public static setJIRAPass($userName : string, $password : string, $callback? : ($status : boolean) => void) : void {
            CredentialsManager.setPassword($userName, $password, $callback);
        }

        public static getJIRAPass($userName : string, $callback : ($value : string) => void) : void {
            CredentialsManager.getPassword($userName, $callback);
        }

        private static setPassword($type : CredentialType, $value : string, $callback? : ($status : boolean) => void) : void {
            const keytar : any = require("keytar");
            if (ObjectValidator.IsString($value)) {
                try {
                    keytar.setPassword(CredentialsManager.serviceName, $type, $value)
                        .then(() : void => {
                            $callback(true);
                        })
                        .catch((error) => {
                            LogIt.Warning(error);
                            if (ObjectValidator.IsSet($callback)) {
                                $callback(false);
                            }
                        });
                } catch (ex) {
                    LogIt.Warning(ex.stack);
                    if (ObjectValidator.IsSet($callback)) {
                        $callback(false);
                    }
                }
            } else if (ObjectValidator.IsSet($callback)) {
                $callback(false);
            }
        }

        private static getPassword($type : CredentialType, $callback : ($value : string) => void) : void {
            const keytar : any = require("keytar");
            try {
                keytar.getPassword(CredentialsManager.serviceName, $type)
                    .then(($value : string) : void => {
                        if ($value === null) {
                            $value = "";
                        }
                        $callback($value);
                    })
                    .catch((error) => {
                        LogIt.Warning(error);
                        if (ObjectValidator.IsSet($callback)) {
                            $callback("");
                        }
                    });
            } catch (ex) {
                LogIt.Warning(ex.stack);
                $callback("");
            }
        }
    }
}
