/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import VirtualBoxManager = Com.Wui.Framework.Builder.Utils.VirtualBoxManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IVBoxSession = Com.Wui.Framework.Builder.Utils.IVBoxSession;
    import VirtualBoxSessionHandler = Com.Wui.Framework.Builder.Utils.VirtualBoxSessionHandler;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;

    export class VirtualBoxHandler extends BaseObject {
        private static sessions : ArrayList<IVBoxSession>;

        public static Start($machine : string, $user : string, $pass : string,
                            $callback : (($status : boolean, $sessionId : string) => void) | IResponse) : void {
            if (!ObjectValidator.IsSet(VirtualBoxHandler.sessions)) {
                VirtualBoxHandler.sessions = new ArrayList<IVBoxSession>();
            }
            VirtualBoxManager.getInstanceSingleton()
                .Start($machine, $user, $pass)
                .Then(($status : boolean, $session : VirtualBoxSessionHandler) : void => {
                    const sessionId : string = $session.getId();
                    VirtualBoxHandler.sessions.Add($session, sessionId);
                    ResponseFactory.getResponse($callback).Send($status, sessionId);
                });
        }

        public static Copy($sessionId : string, $sourcePath : string, $destinationPath : string,
                           $callback : (($status : boolean) => void) | IResponse) : void {
            VirtualBoxHandler.getSession($sessionId, ($session : IVBoxSession) : void => {
                $session.Copy($sourcePath, $destinationPath).Then(($status : boolean) : void => {
                    ResponseFactory.getResponse($callback).Send($status);
                });
            }, $callback);
        }

        public static Execute($sessionId : string, $filePath : string, $callback : (($status : boolean) => void) | IResponse) : void {
            VirtualBoxHandler.getSession($sessionId, ($session : IVBoxSession) : void => {
                $session.Execute($filePath).Then(($status : boolean) : void => {
                    ResponseFactory.getResponse($callback).Send($status);
                });
            }, $callback);
        }

        private static getSession($sessionId : string, $onFound : ($session : IVBoxSession) => void, $errorResponse : any) : void {
            if (VirtualBoxHandler.sessions.KeyExists($sessionId)) {
                $onFound(VirtualBoxHandler.sessions.getItem($sessionId));
            } else {
                ResponseFactory.getResponse($errorResponse).Send(false);
            }
        }
    }
}
