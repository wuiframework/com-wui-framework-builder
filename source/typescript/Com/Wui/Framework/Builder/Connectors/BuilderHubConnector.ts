/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import WebServiceConfiguration = Com.Wui.Framework.Commons.WebServiceApi.WebServiceConfiguration;
    import IAcknowledgePromise = Com.Wui.Framework.Services.Connectors.IAcknowledgePromise;
    import AgentsRegisterConnector = Com.Wui.Framework.Services.Connectors.AgentsRegisterConnector;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;

    export class BuilderHubConnector extends AgentsRegisterConnector {

        public RegisterAgent($capabilities : IWuiBuilderAgent) : IBuilderHubRegisterPromise {
            const callbacks : any = {
                onComplete($success : boolean) : any {
                    // declare default callback
                },
                onError: null,
                onMessage($data : any, $taskId? : string) : any {
                    // declare default callback
                },
                onTaskRequest($args : string[], $taskId? : string) : any {
                    // declare default callback
                }
            };
            LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
                "RegisterAgent", $capabilities)
                .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                    this.errorHandler(callbacks.onError, $error, $args);
                })
                .Then(($result : any) : void => {
                    if (ObjectValidator.IsSet($result.type)) {
                        if ($result.type === EventType.ON_CHANGE) {
                            if ($result.data.type === "taskRequest") {
                                callbacks.onTaskRequest($result.data.data, $result.data.taskId);
                            } else {
                                callbacks.onMessage($result.data.data, $result.data.taskId);
                            }
                        }
                    } else if (ObjectValidator.IsBoolean($result)) {
                        callbacks.onComplete($result);
                    }
                });
            const promise : IBuilderHubRegisterPromise = {
                OnError      : ($callback : ($error : ErrorEventArgs) => void) : IAcknowledgePromise => {
                    callbacks.onError = $callback;
                    return promise;
                },
                OnMessage    : ($callback : ($data : any, $taskId? : string) => void) : IAcknowledgePromise => {
                    callbacks.onMessage = $callback;
                    return promise;
                },
                OnTaskRequest: ($callback : ($args : string[], $taskId? : string) => void) : IBuilderHubRegisterPromise => {
                    callbacks.onTaskRequest = $callback;
                    return promise;
                },
                Then         : ($callback : ($success : boolean) => void) : void => {
                    callbacks.onComplete = $callback;
                }
            };
            return promise;
        }

        public RunTask($task : IWuiBuilderTask) : IBuilderHubTaskPromise {
            const callbacks : any = {
                onError($message : string) : any {
                    // declare default callback
                },
                onComplete($success : boolean, $builderId? : string, $taskId? : string) : any {
                    // declare default callback
                },
                onMessage($data : any, $taskId? : string) : any {
                    // declare default callback
                }
            };
            LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
                "RunTask", $task)
                .Then(($result : any) : void => {
                    if (ObjectValidator.IsSet($result.type)) {
                        if ($result.type === EventType.ON_CHANGE) {
                            callbacks.onMessage($result.data, $result.data.taskId);
                        } else if ($result.type === EventType.ON_COMPLETE && !$result.data.status) {
                            callbacks.onError($result.data.message);
                        }
                    } else if (ObjectValidator.IsObject($result) && ObjectValidator.IsSet($result.builderId)) {
                        callbacks.onComplete(true, $result.builderId, $result.taskId);
                    }
                });
            return {
                OnError: ($callback : ($message : string) => void) : IBuilderHubTaskBasePromise => {
                    callbacks.onError = $callback;
                    const subPromise : IBuilderHubTaskBasePromise = {
                        OnMessage: ($callback : ($data : any, $taskId? : string) => void) : IBuilderHubTaskBasePromise => {
                            callbacks.onMessage = $callback;
                            return subPromise;
                        },
                        Then     : ($callback : ($success : boolean, $builderId? : string, $taskId? : string) => void) : void => {
                            callbacks.onComplete = $callback;
                        }
                    };
                    return subPromise;
                }
            };
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_HUB_CONNECTOR] = "Com.Wui.Framework.Hub.Utils.WuiBuilderHub";
            return namespaces;
        }

        protected init($serverConfigurationSource? : string | WebServiceConfiguration, $clientType? : WebServiceClientType) : void {
            if (ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
                $serverConfigurationSource = Loader.getInstance().getAppConfiguration().hub.url + "/connector.config.jsonp";
            }
            super.init($serverConfigurationSource, $clientType);
        }
    }

    export abstract class IWuiBuilderAgent {
        public name : string;
        public platform : string;
        public domain : string;
        public version : string;
    }

    export abstract class IWuiBuilderTask {
        public args : string[];
        public platforms : string[];
        public agent : IWuiBuilderAgent;
    }

    export interface IBuilderHubRegisterPromise extends Com.Wui.Framework.Services.Connectors.IAcknowledgePromise {
        OnTaskRequest($callback : ($args : string[], $taskId? : string) => void) : IBuilderHubRegisterPromise;

        OnMessage($callback : ($data : any, $taskId? : string) => void) : IAcknowledgePromise;
    }

    export interface IBuilderHubTaskBasePromise {
        OnMessage($callback : ($data : any, $taskId? : string) => void) : IBuilderHubTaskBasePromise;

        Then($callback : ($success : boolean, $builderId? : string, $taskId? : string) => void) : void;
    }

    export interface IBuilderHubTaskPromise {
        OnError($callback : ($message : string) => void) : IBuilderHubTaskBasePromise;
    }
}
