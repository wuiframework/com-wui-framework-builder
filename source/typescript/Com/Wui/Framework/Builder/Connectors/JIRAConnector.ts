/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import IBuilderServerCredentials = Com.Wui.Framework.Builder.Interfaces.IBuilderServerCredentials;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class JIRAConnector extends BaseObject {
        private connector : any;
        private name : string;

        constructor($name : string, $credentials : IBuilderServerCredentials) {
            super();
            this.name = $name;
            const JiraApi : any = require("jira-client");
            LogIt.Debug("Connecting to {0} at {1} as {2}", $name, $credentials.url, $credentials.user);
            const uri : Types.NodeJS.Modules.url.UrlWithStringQuery = require("url").parse($credentials.url);
            this.connector = new JiraApi({
                host     : uri.host,
                password : $credentials.pass,
                protocol : uri.protocol,
                strictSSL: false,
                username : $credentials.user
            });
        }

        public getName() : string {
            return this.name;
        }

        public getIssue($issueKey : string, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.findIssue($issueKey), $callback);
        }

        public findRapidView($projectName : string, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.findRapidView($projectName), $callback);
        }

        public getBacklog($id : number, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.getBacklogForRapidView($id), $callback);
        }

        public getLastSprint($rapidView : number, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.getLastSprintForRapidView($rapidView), ($data : any) : void => {
                $callback($data.name);
            });
        }

        public getIssues($rapidViewId : string, $sprintId : string, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.getSprintIssues($rapidViewId, $sprintId), $callback);
        }

        public getProject($project : string, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.getProject($project), $callback);
        }

        public getUser($userName : string, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.searchUsers({
                includeInactive: true,
                username       : $userName
            }), ($data : any) : void => {
                $callback($data[0]);
            });
        }

        public getFields($callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.listFields(), $callback);
        }

        public Search($jql : string, $callback : ($data : any) => void, $startIndex : number = 0) : void {
            let issues : any = [];
            const getNextResult : any = ($startIndex : number, $callback : any) : void => {
                this.handleConnection(this.connector.searchJira($jql, {
                    maxResults: 500,
                    startAt   : $startIndex
                }), ($data : any) : void => {
                    issues = issues.concat($data.issues);
                    LogIt.Debug("Searching at {0} in range [{1}:{2}] - received {3}",
                        this.getName(), $startIndex + "", ($startIndex + $data.maxResults) + "", $data.issues.length + "");
                    if ($data.total > $startIndex + $data.maxResults) {
                        getNextResult($startIndex + $data.maxResults, $callback);
                    } else {
                        $callback(issues);
                    }
                });
            };
            getNextResult($startIndex, $callback);
        }

        public CreateIssue($data : any, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.addNewIssue({
                fields: $data
            }), $callback);
        }

        public UpdateIssue($issueKey : string, $fields : any, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.updateIssue($issueKey, $fields), $callback);
        }

        public DoIssueTransition($issueId : string, $transitionId : string, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.transitionIssue($issueId, {
                transition: {
                    id: $transitionId
                }
            }), $callback);
        }

        public getTransitions($issueId : string, $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.listTransitions($issueId), ($data : any) : void => {
                const transitions : any[] = [];
                $data.transitions.forEach(($transaction : any) : void => {
                    transitions.push({
                        id  : $transaction.id,
                        name: $transaction.to.statusCategory.name,
                        text: $transaction.to.name
                    });
                });
                $callback(transitions);
            });
        }

        public UpdateComment($issueKey : string, $commentId : any, $comment : string, $options : any,
                             $callback : ($data : any) => void) : void {
            this.handleConnection(this.connector.updateComment($issueKey, $commentId, $comment, $options), $callback);
        }

        private handleConnection($method : any, $onResponse : any) : void {
            $method
                .then($onResponse)
                .catch(($error : Error) : void => {
                    LogIt.Error(this.getClassName(), $error);
                });
        }
    }
}
