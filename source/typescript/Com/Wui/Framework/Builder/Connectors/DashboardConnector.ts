/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import JsonUtils = Com.Wui.Framework.Commons.Utils.JsonUtils;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class DashboardConnector extends BaseConnector {

        @Extern()
        public static getSchema($callback : IResponse) : void {
            this.getInstance<DashboardConnector>().getSchema($callback);
        }

        @Extern()
        public static getProjectConfig($callback : IResponse) : void {
            const config : IProject = JsonUtils.DeepClone(this.getInstance<DashboardConnector>().getProjectConfig(), false);
            config.schema = "";
            ResponseFactory.getResponse($callback).Send(config);
        }

        @Extern()
        public static SaveProjectConfig($config : IProject, $callback : IResponse) : void {
            this.pipe<DashboardConnector>($callback).SaveProjectConfig($config);
        }

        public getSchema($callback : (($schema : any) => void) | IResponse) : void {
            const response : IResponse = ResponseFactory.getResponse($callback);
            const parser : any = require("json-schema-ref-parser");
            try {
                parser
                    .dereference(JsonUtils.DeepClone(this.getProjectConfig().schema))
                    .then(($schema : any) : void => {
                        response.Send(JsonUtils.DeepClone($schema, false));
                    })
                    .catch(($err : Error) : void => {
                        response.OnError($err);
                    });
            } catch (ex) {
                response.OnError(ex);
            }
        }

        public getProjectConfig() : IProject {
            return Loader.getInstance().getProjectConfig();
        }

        public SaveProjectConfig($config : IProject) : boolean {
            return Loader.getInstance().getFileSystemHandler().Write(
                Loader.getInstance().getAppProperties().projectBase + "/.wui/Project.json",
                JSON.stringify($config, null, 2));
        }
    }
}
