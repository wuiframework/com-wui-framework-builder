/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import WebServiceConfiguration = Com.Wui.Framework.Commons.WebServiceApi.WebServiceConfiguration;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IWuiServiceConfig = Com.Wui.Framework.Builder.Interfaces.IWuiServiceConfig;
    import BuilderServer = Com.Wui.Framework.Builder.Tasks.Servers.BuilderServer;

    export class BuilderServiceConnector extends BaseConnector {
        private readonly config : IWuiServiceConfig;

        constructor($port? : number, $address? : string) {
            super(true);
            this.config = Loader.getInstance().getAppConfiguration().service;
            if (ObjectValidator.IsEmptyOrNull(this.config)) {
                this.config = <any>{};
            }
            if (!ObjectValidator.IsEmptyOrNull($port) && $port > 0) {
                this.config.port = $port;
            }
            if (!ObjectValidator.IsEmptyOrNull($address) && $address !== "host" && $address !== "service") {
                this.config.address = $address;
            }
            if (ObjectValidator.IsEmptyOrNull(this.config.port)) {
                this.config.port = 3666;
            }
            if (ObjectValidator.IsEmptyOrNull(this.config.address)) {
                this.config.address = Loader.getInstance().getHttpManager().getRequest().getServerIP();
            }
            this.init();
        }

        public RunTask($args : string[]) : IBuilderServerPromise {
            return this.invoke.apply(this, ["TaskProxy"].concat($args));
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.WEB_SOCKETS;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WEB_SOCKETS] = BuilderServer.ClassName();
            return namespaces;
        }

        protected init() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.config)) {
                const serverConfigurationSource : WebServiceConfiguration = new WebServiceConfiguration();
                serverConfigurationSource.ServerAddress(this.config.address);
                serverConfigurationSource.ServerPort(this.config.port);
                serverConfigurationSource.ServerBase("/wuihost");
                super.init(serverConfigurationSource, WebServiceClientType.WEB_SOCKETS);
            }
        }
    }

    export interface IBuilderServerPromise {
        OnMessage($callback : ($data : any, $taskId? : string) => void) : IBuilderServerPromise;

        Then($callback : ($success : boolean, $builderId? : string, $taskId? : string) => void) : void;
    }
}
