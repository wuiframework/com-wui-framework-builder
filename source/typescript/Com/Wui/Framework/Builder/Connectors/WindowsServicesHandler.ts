/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";
    import WindowsServiceStatus = Com.Wui.Framework.Builder.Enums.WindowsServiceStatus;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class WindowsServicesHandler extends BaseConnector {

        @Extern()
        public static getStatus($name : string, $callback? : (($value : WindowsServiceStatus) => void) | IResponse) : void {
            const onComplete : any = ($value : WindowsServiceStatus) : void => {
                ResponseFactory.getResponse($callback).Send($value);
            };
            Loader.getInstance().getTerminal().Spawn("sc", ["query", "\"" + $name + "\""], null,
                ($exitCode : number, $std : string[]) : void => {
                    const stdOut : string = $std[0] + $std[1];
                    if ($exitCode === 0) {
                        if (stdOut.indexOf(WindowsServiceStatus.RUNNING) !== -1) {
                            onComplete(WindowsServiceStatus.RUNNING);
                        } else if (stdOut.indexOf(WindowsServiceStatus.STOPPED) !== -1) {
                            onComplete(WindowsServiceStatus.STOPPED);
                        } else if (stdOut.indexOf(WindowsServiceStatus.START_PENDING) !== -1) {
                            onComplete(WindowsServiceStatus.START_PENDING);
                        } else if (stdOut.indexOf(WindowsServiceStatus.STOP_PENDING) !== -1) {
                            onComplete(WindowsServiceStatus.STOP_PENDING);
                        }
                    } else if ($exitCode === 1060 || stdOut.indexOf(WindowsServiceStatus.DOES_NOT_EXIST) !== -1) {
                        onComplete(WindowsServiceStatus.DOES_NOT_EXIST);
                    } else if ($exitCode === 2 || stdOut.indexOf(WindowsServiceStatus.NOT_STARTED) !== -1) {
                        onComplete(WindowsServiceStatus.NOT_STARTED);
                    } else {
                        onComplete(WindowsServiceStatus.UNDEFINED);
                    }
                });
        }

        @Extern()
        public static Stop($name : string, $callback? : (($status : boolean) => void) | IResponse) : void {
            Loader.getInstance().getTerminal().Spawn("sc", ["stop", "\"" + $name + "\""], null, ($exitCode : number) : void => {
                if ($exitCode === 0) {
                    const getStatus : any = () : void => {
                        WindowsServicesHandler.getStatus($name, ($status : WindowsServiceStatus) : void => {
                            if ($status === WindowsServiceStatus.STOP_PENDING) {
                                setTimeout(getStatus, 500);
                            } else {
                                ResponseFactory.getResponse($callback).Send(true);
                            }
                        });
                    };
                    getStatus();
                } else if ($exitCode === 1062 || $exitCode === 2) {
                    ResponseFactory.getResponse($callback).Send(true);
                } else {
                    ResponseFactory.getResponse($callback).Send(false);
                }
            });
        }

        @Extern()
        public static Start($name : string, $callback? : (($status : boolean) => void) | IResponse) : void {
            Loader.getInstance().getTerminal().Spawn("sc", ["start", "\"" + $name + "\""], null, ($exitCode : number) : void => {
                if ($exitCode === 0) {
                    const getStatus : any = () : void => {
                        WindowsServicesHandler.getStatus($name, ($status : WindowsServiceStatus) : void => {
                            if ($status === WindowsServiceStatus.START_PENDING) {
                                setTimeout(getStatus, 500);
                            } else {
                                ResponseFactory.getResponse($callback).Send(true);
                            }
                        });
                    };
                    getStatus();
                } else if ($exitCode === 1060) {
                    ResponseFactory.getResponse($callback).Send(true);
                } else {
                    ResponseFactory.getResponse($callback).Send(false);
                }
            });
        }

        @Extern()
        public static Delete($name : string, $callback? : (($status : boolean) => void) | IResponse) : void {
            Loader.getInstance().getTerminal().Spawn("sc", ["delete", "\"" + $name + "\""], null, ($exitCode : number) : void => {
                if ($exitCode === 0) {
                    const getStatus : any = () : void => {
                        WindowsServicesHandler.getStatus($name, ($status : WindowsServiceStatus) : void => {
                            if ($status === WindowsServiceStatus.DOES_NOT_EXIST) {
                                ResponseFactory.getResponse($callback).Send(true);
                            } else {
                                setTimeout(getStatus, 500);
                            }
                        });
                    };
                    getStatus();
                } else if ($exitCode === 1060) {
                    ResponseFactory.getResponse($callback).Send(true);
                } else {
                    ResponseFactory.getResponse($callback).Send(false);
                }
            });
        }

        @Extern()
        public static Add($name : string, $binPath : string, $args : string[] = [], $autoStart : boolean = true,
                          $callback? : (($status : boolean) => void) | IResponse) : void {
            const args : string[] = ["create", "\"" + $name + "\"", "binPath=", "\"\\\"" + $binPath + "\\\" " + $args.join(" ") + "\""];
            if ($autoStart) {
                args.push("DisplayName=", "\"" + $name + "\"");
            }
            if ($autoStart) {
                args.push("start=", "auto");
            }
            WindowsServicesHandler.getStatus($name, ($status : WindowsServiceStatus) : void => {
                if ($status === WindowsServiceStatus.DOES_NOT_EXIST) {
                    Loader.getInstance().getTerminal().Spawn("sc", args, null, ($exitCode : number) : void => {
                        if ($exitCode === 0) {
                            const getStatus : any = () : void => {
                                WindowsServicesHandler.getStatus($name, ($status : WindowsServiceStatus) : void => {
                                    if ($status === WindowsServiceStatus.DOES_NOT_EXIST) {
                                        ResponseFactory.getResponse($callback).Send(true);
                                    } else {
                                        setTimeout(getStatus, 500);
                                    }
                                });
                            };
                            getStatus();
                        } else if ($exitCode === 1060) {
                            ResponseFactory.getResponse($callback).Send(true);
                        } else {
                            ResponseFactory.getResponse($callback).Send(false);
                        }
                    });
                } else {
                    ResponseFactory.getResponse($callback).Send(true);
                }
            });
        }
    }
}
