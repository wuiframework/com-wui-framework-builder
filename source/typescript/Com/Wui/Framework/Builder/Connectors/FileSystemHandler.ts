/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";

    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import ArchiveOptions = Com.Wui.Framework.Services.Connectors.ArchiveOptions;
    import IFileSystemItemProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemItemProtocol;
    import ShortcutOptions = Com.Wui.Framework.Services.Connectors.ShortcutOptions;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class FileSystemHandler extends Com.Wui.Framework.Connector.Connectors.FileSystemHandler {

        @Extern()
        public Exists($path : string) : boolean {
            return super.Exists($path);
        }

        @Extern()
        public IsEmpty($path : string) : boolean {
            return super.IsEmpty($path);
        }

        @Extern()
        public IsFile($path : string) : boolean {
            return super.IsFile($path);
        }

        @Extern()
        public IsDirectory($path : string) : boolean {
            return super.IsDirectory($path);
        }

        @Extern()
        public IsSymbolicLink($path : string) : boolean {
            return super.IsSymbolicLink($path);
        }

        @Extern()
        public getTempPath() : string {
            return super.getTempPath();
        }

        @Extern()
        public Expand($pattern : string | string[]) : string[] {
            return super.Expand($pattern);
        }

        @Extern()
        public CreateDirectory($path : string) : boolean {
            return super.CreateDirectory($path);
        }

        @Extern()
        public Rename($oldPath : string, $newPath : string) : boolean {
            return super.Rename($oldPath, $newPath);
        }

        @Extern()
        public Read($path : string) : string | Buffer {
            return super.Read($path);
        }

        @Extern()
        public Write($path : string, $data : any, $append : boolean = false) : boolean {
            return super.Write($path, $data, $append);
        }

        @Extern()
        public Copy($sourcePath : string, $destinationPath : string, $callback? : (($status : boolean) => void) | IResponse) : void {
            super.Copy($sourcePath, $destinationPath, $callback);
        }

        @Extern()
        public Download($urlOrOptions : string | FileSystemDownloadOptions,
                        $callback? : (($headers : string, $bodyOrPath : string) => void) | IResponse) : void {
            super.Download($urlOrOptions, $callback);
        }

        @Extern()
        public AbortDownload($id : number) : boolean {
            return super.AbortDownload($id);
        }

        @Extern()
        public Delete($path : string,
                      $callback? : (($status : boolean) => void) | IResponse) : boolean {
            return super.Delete($path, $callback);
        }

        @Extern()
        public Pack($path : string | string[], $options? : ArchiveOptions,
                    $callback? : (($tmpPath : string) => void) | IResponse) : void {
            super.Pack($path, $options, $callback);
        }

        @Extern()
        public Unpack($path : string, $options? : ArchiveOptions,
                      $callback? : (($tmpPath : string) => void) | IResponse) : void {
            super.Unpack($path, $options, $callback);
        }

        @Extern()
        public NormalizePath($source : string, $osSeparator : boolean = false) : string {
            return super.NormalizePath($source, $osSeparator);
        }

        @Extern()
        public getPathMap($path : string,
                          $callback : (($map : IFileSystemItemProtocol[]) => void) | IResponse) : void {
            super.getPathMap($path, $callback);
        }

        @Extern()
        public getDirectoryContent($path : string) : IFileSystemItemProtocol[] {
            return super.getDirectoryContent($path);
        }

        @Extern()
        public CreateShortcut($source : string, $destination : string, $options : ShortcutOptions,
                              $callback : (($status : boolean) => void) | IResponse) : void {
            super.CreateShortcut($source, $destination, $options, $callback);
        }

        @Extern()
        public ReadStream($path : string) : string {
            return super.ReadStream($path);
        }

        @Extern()
        public WriteStream($path : string, $data : string, $append : boolean = false) : boolean {
            return super.WriteStream($path, $data, $append);
        }
    }
}
