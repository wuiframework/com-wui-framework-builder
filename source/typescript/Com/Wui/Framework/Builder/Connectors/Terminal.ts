/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";

    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import TerminalOptions = Com.Wui.Framework.Services.Connectors.TerminalOptions;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class Terminal extends Com.Wui.Framework.Connector.Connectors.Terminal {

        @Extern()
        public Execute($cmd : string, $args : string[], $cwdOrOptions : string | TerminalOptions,
                       $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
            return super.Execute($cmd, $args, $cwdOrOptions, $callback);
        }

        @Extern()
        public Spawn($cmd : string, $args : string[], $cwdOrOptions : string | TerminalOptions,
                     $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
            return super.Spawn($cmd, $args, $cwdOrOptions, $callback);
        }

        @Extern()
        public Open($path : string, $callback? : (($exitCode : number) => void) | IResponse) : number {
            return super.Open($path, $callback);
        }

        @Extern()
        public Elevate($cmd : string, $args : string[], $cwd : string,
                       $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
            return super.Elevate($cmd, $args, $cwd, $callback);
        }

        @Extern()
        public Kill($path : string, $callback? : (($status : boolean) => void) | IResponse) : void {
            super.Kill($path, $callback);
        }
    }
}
