/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";

    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import FFIOptions = Com.Wui.Framework.Services.Connectors.FFIOptions;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class FFIProxy extends Com.Wui.Framework.Connector.Connectors.FFIProxy {

        @Extern()
        public Load($fileOrOptions : string | FFIOptions,
                    $callback? : (($status : boolean, $libraryPath : string) => void) | IResponse) : void {
            super.Load($fileOrOptions, $callback);
        }

        @Extern(false)
        public InvokeMethod($name : string, $returnType : any, ...$args) : void {
            super.InvokeMethod($name, $returnType, ...$args);
        }

        @Extern()
        public Release() : void {
            super.Release();
        }
    }
}
