/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Connectors {
    "use strict";

    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class Network extends Com.Wui.Framework.Connector.Connectors.Network {

        @Extern()
        public getFreePort($callback : (($value : number) => void) | IResponse) : void {
            super.getFreePort($callback);
        }
    }
}
