/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.HttpProcessor {
    "use strict";
    import IAppConfiguration = Com.Wui.Framework.Builder.Interfaces.IAppConfiguration;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;
    import Network = Com.Wui.Framework.Builder.Connectors.Network;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class HttpServer extends Com.Wui.Framework.Localhost.HttpProcessor.HttpServer {

        public Start() : void {
            let port : number = Loader.getInstance().getAppConfiguration().servicePort;
            const args : ProgramArgs = Loader.getInstance().getProgramArgs();
            if (args.Port() > 0) {
                port = args.Port();
            }
            if (ObjectValidator.IsEmptyOrNull(port) || port < 0) {
                port = 0;
            }
            const networking : Network = new Network();
            networking.IsPortFree(port, ($status : boolean) : void => {
                if ($status) {
                    this.createServer(port, false, true);
                } else {
                    LogIt.Error("Service port \"" + port + "\" is already in use.\n" +
                        "    Please use --port option or kill process using required port.");
                }
            });
        }

        public StartConnector($isService : boolean = true) : void {
            const args : ProgramArgs = Loader.getInstance().getProgramArgs();
            const networking : Network = new Network();
            networking.getFreePort(($port : number) : void => {
                if ($isService) {
                    const logPath : string = args.TargetBase() + "/log/server/connector.log";
                    Loader.getInstance().getFileSystemHandler().Delete(logPath);
                    Loader.getInstance().setLogPath(logPath);
                }
                Loader.getInstance().getFileSystemHandler()
                    .CreateDirectory(args.TargetBase() + "/resource/data/Com/Wui/Framework/Builder");
                Loader.getInstance().getFileSystemHandler()
                    .CreateDirectory(args.TargetBase() + "/build/target/resource/data/Com/Wui/Framework/Builder");
                this.createServer($port, false, true, [
                    args.TargetBase() + "/resource/data/Com/Wui/Framework/Builder",
                    args.TargetBase() + "/build/target/resource/data/Com/Wui/Framework/Builder"
                ]);
            });
        }

        protected getAppConfiguration() : IAppConfiguration {
            return <IAppConfiguration>super.getAppConfiguration();
        }
    }
}
