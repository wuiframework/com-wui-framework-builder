/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.HttpProcessor {
    "use strict";

    export class HttpResolver extends Com.Wui.Framework.Localhost.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            const indexClass : any = Com.Wui.Framework.Builder.Index;
            resolvers["/"] = indexClass;
            resolvers["/index"] = indexClass;
            resolvers["/index.html"] = indexClass;
            resolvers["/web/"] = indexClass;

            if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
                return resolvers;
            }

            resolvers["/liveContent/"] = Com.Wui.Framework.Connector.HttpProcessor.Resolvers.LiveContentResolver;

            return resolvers;
        }
    }
}
