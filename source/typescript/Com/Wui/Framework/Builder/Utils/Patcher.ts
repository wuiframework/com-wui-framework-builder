/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    /**
     * Patcher class provides helpful features to modify file content with automatic backup of original one.
     */
    export class Patcher extends BaseObject {
        private static singleton : Patcher;
        private readonly cleanupList : any[];

        /**
         * @return {Patcher} Returns patcher singleton.
         */
        public static getInstance() : Patcher {
            if (ObjectValidator.IsEmptyOrNull(Patcher.singleton)) {
                Patcher.singleton = new Patcher();
            }
            return Patcher.singleton;
        }

        /**
         * Singleton instance could be constructed only internally.
         */
        private constructor() {
            super();
            this.cleanupList = [];
        }

        /**
         * Replace $key found in $file by $replacement if found or if extra condition is true.
         * @param {string} $file Specify file to be patch applied for.
         * @param {string} $key Specify key which should be replaced.
         * @param {string} $replacement Specify value to replace $key.
         * @param {()=>void} $callback Callback will be called after patching finished.
         * @param {($data:string)=>boolean} $condition Return false in condition handler to skip patch.
         */
        public Patch($file : string, $key : string, $replacement : string, $callback : () => void,
                     $condition? : ($data : string) => boolean) : void {
            if (ObjectValidator.IsEmptyOrNull($condition)) {
                $condition = ($data : string) => {
                    return true;
                };
            }
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (fileSystem.Exists($file)) {
                const data = fileSystem.Read($file).toString();
                if ($condition(data)) {
                    if (fileSystem.Write($file, StringUtils.Replace(data, $key, $replacement))) {
                        this.addCleanupAction($file, ($callback) => {
                                fileSystem.Write($file, data, false);
                                LogIt.Info("Rollback of patched file \"" + $file + "\" done.");
                                $callback();
                            }
                        );

                        LogIt.Info("Patch applied for key: \"" + $key + "\" in file: \"" + $file + "\"");
                        $callback();
                    } else {
                        this.Rollback(() : void => {
                            LogIt.Error("Can not apply patch for key: \"" + $key + "\" in file: \"" + $file + "\"");
                        });
                    }
                } else {
                    $callback();
                }
            } else {
                $callback();
            }
        }

        /**
         * Replace $key found in all of $files by $replacement.
         * @param {string[]} $files Specify list of files to be patch applied for.
         * @param {string} $key Specify key which should be replaced.
         * @param {string} $replacement Specify value to replace $key.
         * @param {()=>void} $callback Callback will be called after patching finished.
         */
        public PatchMultiFiles($files : string[], $key : string, $replacement : string, $callback : () => void) : void {
            if (!ObjectValidator.IsEmptyOrNull($files)) {
                let index : number = 0;
                const process : any = () : void => {
                    if (index < $files.length) {
                        this.Patch($files[index++], $key, $replacement, process);
                    } else {
                        $callback();
                    }
                };

                process();
            } else {
                $callback();
            }
        }

        /**
         * Replace all of keys in $keyMap for all of files in $files
         * @param {string|string[]} $files Specify single path or list of paths.
         * @param {any} $keyMap Specify JSON object { "<key>" : "<replacement>" }.
         * @param {()=>void} $callback Callback will be called after patching finished.
         */
        public PatchMultiKeys($files : string | string[], $keyMap : any, $callback : () => void) : void {
            if (!ObjectValidator.IsEmptyOrNull($keyMap)) {
                let index : number = 0;
                const $keys : string[] = [];
                for (const item in $keyMap) {
                    if ($keyMap.hasOwnProperty(item)) {
                        $keys.push(item);
                    }
                }

                const process : any = () : void => {
                    if (index < $keys.length) {
                        this.PatchMultiFiles((ObjectValidator.IsString($files) ? [<string>$files] : <string[]>$files),
                            $keys[index], $keyMap[$keys[index]], () : void => {
                                index++;
                                process();
                            });
                    } else {
                        $callback();
                    }
                };

                process();
            } else {
                $callback();
            }
        }

        /**
         * Rollback all of patches applied by Patcher singleton instance.
         * @param {()=>void} $callback Callback will be called after rollback finished.
         */
        public Rollback($callback : () => void) : void {
            let index : number = 0;

            const clean : any = ($callback : () => void) : void => {
                if (index < this.cleanupList.length) {
                    this.cleanupList[index++].action(() => {
                        clean($callback);
                    });
                } else {
                    $callback();
                }
            };

            clean($callback);
        }

        protected addCleanupAction($file : string, $action : ($callback : () => void) => void) {
            let cleanupActionFound = false;
            for (const item in this.cleanupList) {
                if (this.cleanupList.hasOwnProperty(item)) {
                    if (this.cleanupList[item].file === $file) {
                        cleanupActionFound = true;
                        break;
                    }
                }
            }
            if (!cleanupActionFound) {
                this.cleanupList.unshift({
                    action: $action,
                    file  : $file
                });
            }
        }
    }
}
