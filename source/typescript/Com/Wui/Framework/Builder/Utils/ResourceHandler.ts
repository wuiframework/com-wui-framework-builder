/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ResourceHandler extends BaseObject {

        public static GenerateVersionRc($executablePath : string, $targetName : string) : string {
            const EOL : string = require("os").EOL;
            const project : IProject = Loader.getInstance().getProjectConfig();

            if (EnvironmentHelper.IsWindows()) {
                const versions : string[] = project.version.split("-")[0].split(".");

                let rcFileTemplate : string = "" +
                    "1 VERSIONINFO" + EOL +
                    "FILEVERSION     " + versions[0] + "," + versions[1] + "," + versions[2] + ",0" + EOL +
                    "PRODUCTVERSION  " + versions[0] + "," + versions[1] + "," + versions[2] + ",0" + EOL +
                    "BEGIN" + EOL +
                    "   BLOCK \"StringFileInfo\"" + EOL +
                    "   BEGIN" + EOL +
                    "       BLOCK \"040904E4\"" + EOL +
                    "       BEGIN" + EOL +
                    "           VALUE \"ProductName\", \"" + $targetName + "\"" + EOL +
                    "           VALUE \"ProductVersion\", \"" + project.version + "\"" + EOL +
                    "           VALUE \"CompanyName\", \"" + project.target.companyName + "\"" + EOL +
                    "           VALUE \"FileDescription\", \"" + project.target.description + "\"" + EOL +
                    "           VALUE \"LegalCopyright\", \"" + project.target.copyright + "\"" + EOL +
                    "           VALUE \"InternalName\", \"" + project.name + "\"" + EOL +
                    "           VALUE \"FileVersion\", \"" + project.version + "\"" + EOL +
                    "           VALUE \"OriginalFilename\", \"" + project.name + ".exe\"" + EOL;

                if (!ObjectValidator.IsEmptyOrNull($executablePath)) {
                    $executablePath = Loader.getInstance().getFileSystemHandler().NormalizePath($executablePath);
                    const oldVersionInfo : any = ResourceHandler.getVersionInfo($executablePath);
                    if (oldVersionInfo !== null && oldVersionInfo.hasOwnProperty("BaseProjectName")) {
                        rcFileTemplate += "" +
                            "           VALUE \"BaseProjectName\", \"" + oldVersionInfo.BaseProjectName + "\"" + EOL;
                    }
                }

                rcFileTemplate += "" +
                    "       END" + EOL +
                    "   END" + EOL + EOL +
                    "   BLOCK \"VarFileInfo\"" + EOL +
                    "   BEGIN" + EOL +
                    "       VALUE \"Translation\", 0x409, 1252" + EOL +
                    "   END" + EOL +
                    "END";

                return rcFileTemplate;
            }
            return "";
        }

        public static ModifyVersionInfo($filePath : string, $targetName : string, $callback : () => void) : void {
            $filePath = Loader.getInstance().getFileSystemHandler().NormalizePath($filePath);
            const properties : IProperties = Loader.getInstance().getAppProperties();

            if (EnvironmentHelper.IsWindows()) {
                const rcFilePath : string = Loader.getInstance().getFileSystemHandler().NormalizePath(
                    properties.projectBase + "/" + properties.compiled + "/version.rc");

                if (Loader.getInstance().getFileSystemHandler()
                    .Write(rcFilePath, ResourceHandler.GenerateVersionRc($filePath, $targetName))) {
                    ResourceHandler.Compile(rcFilePath, () : void => {
                        ResourceHandler.EmbedResource($filePath, rcFilePath + ".res", () : void => {
                            LogIt.Info("Resources has been updated " + $filePath);
                            $callback();
                        });
                    });
                } else {
                    LogIt.Error("Failed to create version.rc file.");
                }
            } else {
                LogIt.Info("Version info update has been skipped: supported only on Windows platform");
                $callback();
            }
        }

        public static ModifyIcon($filePath : string, $iconPath : string, $callback : () => void, $resIndex : number = 0) : void {
            if (EnvironmentHelper.IsWindows()) {
                ResourceHandler.EmbedFile($filePath, "ICONGROUP", $resIndex.toString(), $iconPath, () : void => {
                    LogIt.Info("Icon has been updated " + $filePath);
                    $callback();
                });
            } else {
                LogIt.Info("Icon update has been skipped: supported only on Windows platform");
                $callback();
            }
        }

        public static ModifyManifest($filePath : string, $callback : () => void) : void {
            const EOL : string = require("os").EOL;
            const project : IProject = Loader.getInstance().getProjectConfig();
            const properties : IProperties = Loader.getInstance().getAppProperties();

            if (EnvironmentHelper.IsWindows()) {
                const rcFilePath : string = Loader.getInstance().getFileSystemHandler().NormalizePath(
                    properties.projectBase + "/" + properties.compiled + "/manifest.rc");
                const level : string = project.target.elevate ? "requireAdministrator" : "asInvoker";

                const rcFileTemplate : string =
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + EOL +
                    "<assembly xmlns=\"urn:schemas-microsoft-com:asm.v1\" manifestVersion=\"1.0\">" + EOL +
                    "  <trustInfo xmlns=\"urn:schemas-microsoft-com:asm.v3\">" + EOL +
                    "    <security>" + EOL +
                    "      <requestedPrivileges>" + EOL +
                    "        <requestedExecutionLevel level=\"" + level + "\" uiAccess=\"false\"/>" + EOL +
                    "      </requestedPrivileges>" + EOL +
                    "    </security>" + EOL +
                    "  </trustInfo>" + EOL +
                    "  <compatibility xmlns=\"urn:schemas-microsoft-com:compatibility.v1\">" + EOL +
                    "    <application>" + EOL +
                    "      <!--The ID below indicates application support for Windows Vista -->" + EOL +
                    "      <supportedOS Id=\"{e2011457-1546-43c5-a5fe-008deee3d3f0}\"/>" + EOL +
                    "      <!--The ID below indicates application support for Windows 7 -->" + EOL +
                    "      <supportedOS Id=\"{35138b9a-5d96-4fbd-8e2d-a2440225f93a}\"/>" + EOL +
                    "      <!--The ID below indicates application support for Windows 8 -->" + EOL +
                    "      <supportedOS Id=\"{4a2f28e3-53b9-4441-ba9c-d69d4a4a6e38}\"/>" + EOL +
                    "      <!--The ID below indicates application support for Windows 8.1 -->" + EOL +
                    "      <supportedOS Id=\"{1f676c76-80e1-4239-95bb-83d0f6d0da78}\"/>" + EOL +
                    "      <!--The ID below indicates application support for Windows 10 -->" + EOL +
                    "      <supportedOS Id=\"{8e0f7a12-bfb3-4fe8-b9a5-48fd50a15a9a}\"/>" + EOL +
                    "    </application>" + EOL +
                    "  </compatibility>" + EOL +
                    "</assembly>" + EOL;
                Loader.getInstance().getFileSystemHandler().Write(rcFilePath, rcFileTemplate);

                ResourceHandler.Compile(rcFilePath, () : void => {
                    ResourceHandler.EmbedFile($filePath, "MANIFEST", "1", rcFilePath + ".res",
                        () : void => {
                            LogIt.Info("Manifest has been updated " + $filePath);
                            $callback();
                        });
                });
            } else {
                LogIt.Info("Manifest update has been skipped: supported only on Windows platform");
                $callback();
            }
        }

        public static CreateDesktopEntryTemplate($targetName : string, $iconPath : string) : void {
            const EOL : string = require("os").EOL;
            const project : IProject = Loader.getInstance().getProjectConfig();
            const properties : IProperties = Loader.getInstance().getAppProperties();

            const template : string = "" +
                "[Desktop Entry]" + EOL +
                "Type=Application" + EOL +
                "Name=" + $targetName + EOL +
                "Icon={appRoot}/" + $iconPath + EOL +
                "Exec={appRoot}/" + $targetName + EOL +
                "Comment=" + project.target.description + EOL +
                "";
            Loader.getInstance().getFileSystemHandler()
                .Write(properties.projectBase + "/build/target/" + project.target.name + ".desktop", template);
        }

        public static EmbedFile($filePath : string, $type : string, $name : string, $dataFile : string, $callback : () => void) : void {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            $filePath = fileSystem.NormalizePath($filePath);
            const path : Types.NodeJS.path = require("path");
            const properties : IProperties = Loader.getInstance().getAppProperties();

            if (fileSystem.Exists($filePath) && fileSystem.Exists($dataFile)) {
                if (fileSystem.IsFile($filePath) && fileSystem.IsFile($dataFile)) {
                    if (!ObjectValidator.IsEmptyOrNull($type) && !ObjectValidator.IsEmptyOrNull($name)) {
                        if (EnvironmentHelper.IsWindows()) {
                            const run : any = ($dataTmpPath : string, $handle : () => void) => {
                                const command : string[] = [
                                    "-open", "\"" + $filePath + "\"",
                                    "-save", "\"" + $filePath + "\"",
                                    "-action", "addoverwrite",
                                    "-res", "\"" + fileSystem.NormalizePath($dataTmpPath) + "\"",
                                    "-mask", $type + "," + $name + ","
                                ];
                                this.executeResourceHacker(command, $handle);
                            };
                            if (path.extname($dataFile).toLocaleLowerCase() === ".exe") {
                                let tmpDataFile : string = (properties.projectBase + "/" + properties.compiled + "/" +
                                    path.basename($dataFile) + "_" + Math.random().toString());
                                tmpDataFile = tmpDataFile.replace(/\./g, "_") + ".dump";
                                fileSystem.Copy($dataFile, tmpDataFile, ($status : boolean) => {
                                    if ($status) {
                                        run(tmpDataFile, () => {
                                            fileSystem.Delete(tmpDataFile);
                                            $callback();
                                        });
                                    }
                                });
                            } else {
                                run($dataFile, $callback);
                            }
                        } else {
                            const fullSectionName : string = "." + $type + "/" + $name;
                            Loader.getInstance().getTerminal().Execute("objcopy", [
                                "--add-section " + fullSectionName + "=" + StringUtils.Replace($dataFile, "$", "\\$"),
                                "--set-section-flags " + fullSectionName + "=noload,readonly",
                                $filePath
                            ], null, ($exitCode : number, $std : string[]) : void => {
                                if ($exitCode === 0) {
                                    $callback();
                                } else {
                                    LogIt.Error("Unable to update resource.\n" + $std[0] + "\n" + $std[1]);
                                }
                            });
                        }
                    } else {
                        LogIt.Warning("Resource type (=" + $type + ") and name(=" + $name + ") must be specified.");
                        $callback();
                    }
                } else {
                    LogIt.Error("EmbedFile method do not support directories embedding.");
                }
            } else {
                LogIt.Error("Requested file to embed (" + $filePath + ") or " +
                    "file to be embedded (" + $dataFile + ") does not exists.");
            }
        }

        public static EmbedResource($filePath : string, $resourceFile : string, $callback : () => void) : void {
            if (EnvironmentHelper.IsWindows()) {
                const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                if (fileSystem.Exists($filePath) && fileSystem.Exists($resourceFile)) {
                    if (fileSystem.IsFile($filePath) && fileSystem.IsFile($resourceFile)) {
                        const command : string[] = [
                            "-open", "\"" + $filePath + "\"",
                            "-save", "\"" + $filePath + "\"",
                            "-action", "addoverwrite",
                            "-res", "\"" + Loader.getInstance().getFileSystemHandler().NormalizePath($resourceFile) + "\""
                        ];
                        this.executeResourceHacker(command, $callback);
                    } else {
                        LogIt.Error("EmbedResource method do not support directories embedding.");
                    }
                } else {
                    LogIt.Error("Requested file to embed or resource file must exists.");
                }
            } else {
                LogIt.Info("Resource embed has been skipped: supported only on Windows platform");
                $callback();
            }
        }

        public static Compile($filePath : string, $callback : () => void) : void {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (fileSystem.Exists($filePath)) {
                if (fileSystem.IsFile($filePath)) {
                    const command : string[] = [
                        "-open", "\"" + $filePath + "\"",
                        "-save", "\"" + $filePath + ".res\"",
                        "-action", "compile"
                    ];
                    this.executeResourceHacker(command, $callback);
                } else {
                    LogIt.Error("EmbedFile method do not support directories embedding.");
                }
            } else {
                LogIt.Error("Requested file to be compiled not exists.");
            }
        }

        public static getVersionInfo($filePath : string) : any {
            const readVer : any = require("win-version-info");
            const path : Types.NodeJS.path = require("path");
            let verInfo : any = null;
            const filePath : string = path.normalize(StringUtils.Remove($filePath, "\""));

            if (Loader.getInstance().getFileSystemHandler().Exists(filePath)) {
                verInfo = readVer(filePath);
            }
            return verInfo;
        }

        public static RunScript($script : string, $callback : () => void) : void {
            if (EnvironmentHelper.IsWindows()) {
                const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                const properties : IProperties = Loader.getInstance().getAppProperties();
                const scriptPath : string = properties.projectBase + "/" + properties.compiled + "/" +
                    "script_" + ResourceHandler.UID() + ".txt";
                if (fileSystem.Write(scriptPath, $script)) {
                    ResourceHandler.executeResourceHacker(["-script", "\"" + scriptPath + "\""], () : void => {
                        $callback();
                    });
                } else {
                    LogIt.Error("Failed to create script file.");
                }
            } else {
                LogIt.Error("Execution of scripts is supported only on Windows platform.");
            }
        }

        private static executeResourceHacker($commands : string[], $callback : () => void) : void {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const cwd : string = fileSystem.NormalizePath(Loader.getInstance().getProgramArgs().AppDataPath() +
                "/external_modules/resourcehacker");
            if (!fileSystem.Exists(cwd + "/ResourceHacker.exe")) {
                LogIt.Error("Instance of ResourceHacker has not been found. " +
                    "To fix this issue, please execute 'wui selfinstall --force' command.");
            } else {
                Loader.getInstance().getTerminal().Execute("ResourceHacker.exe", $commands, cwd,
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            $callback();
                        } else {
                            LogIt.Error("Unable to update resource.\n" + $std[0] + "\n" + $std[1]);
                        }
                    });
            }
        }
    }
}
