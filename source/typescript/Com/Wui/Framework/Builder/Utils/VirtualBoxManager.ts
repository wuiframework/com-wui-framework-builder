/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import BuilderServiceManager = Com.Wui.Framework.Builder.Connectors.BuilderServiceManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;

    export class VirtualBoxManager extends BaseObject {
        private static singleton : VirtualBoxManager;
        private connectionPoint : IVBoxSessionConfiguration;
        private sessions : ArrayList<any>;

        public static getInstanceSingleton() : VirtualBoxManager {
            if (!ObjectValidator.IsSet(this.singleton)) {
                this.singleton = new VirtualBoxManager();
            }
            return this.singleton;
        }

        constructor($connectionPoint? : IVBoxSessionConfiguration) {
            super();
            if (!ObjectValidator.IsEmptyOrNull($connectionPoint)) {
                this.connectionPoint = $connectionPoint;
            } else {
                this.connectionPoint = new IVBoxSessionConfiguration();
            }
            this.sessions = new ArrayList<any>();
        }

        public Start($machine : string, $user : string = "Guest", $password : string = "") : IVBoxPromise {
            const session : any = new VirtualBoxSessionHandler($machine, $user);
            const promise : VBoxPromise = new VBoxPromise(session);
            BuilderServiceManager.IsTaskOnline(Loader.getInstance().getAppProperties().projectBase, "vbox-server-start", false,
                ($status : boolean) : void => {
                    const createSession : any = () : void => {
                        const sessionId : string = session.getId();
                        if (this.sessions.KeyExists(sessionId)) {
                            session.setSession(this.sessions.getItem(sessionId));
                            promise.Invoke(true);
                        } else {
                            this.getUserSession($machine, $user, $password, ($session : any) : void => {
                                if (!ObjectValidator.IsBoolean($session)) {
                                    this.sessions.Add($session, sessionId);
                                    session.setSession(this.sessions.getItem(sessionId));
                                    promise.Invoke(true);
                                } else {
                                    promise.Invoke(false);
                                }
                            });
                        }
                    };
                    if (!$status) {
                        BuilderServiceManager.StartTask(Loader.getInstance().getAppProperties().projectBase, "vbox-server-start",
                            ($status : boolean, $message : string) : void => {
                                if ($status) {
                                    setTimeout(() : void => {
                                        createSession();
                                    }, 500);
                                } else {
                                    LogIt.Warning($message);
                                    promise.Invoke(false);
                                }
                            });
                    } else {
                        createSession();
                    }
                });
            return promise;
        }

        private getUserSession($machineNameOrId : string, $user : string, $pass : string, $callback : ($session : any) => void) : void {
            const execution : any = async () : Promise<any> => {
                const connection : any = await require("virtualbox-soap")(this.connectionPoint.location);
                const vbox : any = await connection.logon(this.connectionPoint.user, this.connectionPoint.pass);
                const machine : any = await vbox.findMachine($machineNameOrId);
                const session : any = await connection.getSessionObject(vbox);
                const machineState : string = await machine.getState();
                if (machineState !== "Running") {
                    const progress = await machine.launchVMProcess(session);
                    await progress.waitForCompletion(-1);
                    await session.unlockMachine();
                }
                await machine.lockMachine(session, 1);
                const console : any = await session.getConsole();
                const guest : any = await console.getGuest();
                const guestSession : any = await guest.createSession($user, $pass, "", "");
                await guestSession.waitFor(1, 30 * 1000);
                return guestSession;
            };
            execution()
                .then($callback)
                .catch(($ex : Error) : void => {
                    LogIt.Warning($ex.stack);
                    $callback(false);
                });
        }
    }

    export class IVBoxSessionConfiguration {
        public location : string = "http://localhost:18083";
        public user : string = "";
        public pass : string = "";
    }

    export interface IVBoxSession {
        Copy($sourcePath : string, $destinationPath : string) : IVBoxPromise;

        Execute($filePath : string) : IVBoxPromise;
    }

    export interface IVBoxPromise {
        Then($callback : ($status : boolean, $session? : IVBoxSession) => void) : void;
    }

    export class VirtualBoxSessionHandler extends BaseObject implements IVBoxSession {
        private readonly machine : string;
        private readonly user : string;
        private readonly sessionId : string;
        private session : any;

        constructor($machine : string, $user : string) {
            super();
            this.machine = $machine;
            this.user = $user;
            this.sessionId = StringUtils.getSha1($machine + $user);
        }

        public setSession($instance : any) : void {
            this.session = $instance;
        }

        public getMachineName() : string {
            return this.machine;
        }

        public getUser() : string {
            return this.user;
        }

        public getId() : string {
            return this.sessionId;
        }

        public Copy($sourcePath : string, $destinationPath : string) : IVBoxPromise {
            const promise : VBoxPromise = new VBoxPromise(this);
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const execution : any = async () : Promise<any> => {
                $sourcePath = fileSystem.NormalizePath($sourcePath);
                $destinationPath = fileSystem.NormalizePath($destinationPath);
                $destinationPath = require("path").dirname($destinationPath);
                LogIt.Debug("> copy {0} to {1}", $sourcePath, $destinationPath);
                const guestProgress : any = await this.session.fileCopyToGuest($sourcePath, $destinationPath, []);
                await guestProgress.waitForCompletion(-1);
            };
            execution()
                .then(() : void => {
                    promise.Invoke(true);
                })
                .catch(($ex : Error) : void => {
                    LogIt.Warning($ex.stack);
                    promise.Invoke(false);
                });
            return promise;
        }

        public Execute($filePath : string) : IVBoxPromise {
            const promise : VBoxPromise = new VBoxPromise(this);
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const execution : any = async () : Promise<any> => {
                $filePath = fileSystem.NormalizePath($filePath);
                LogIt.Debug("> execute {0}", $filePath);
                const guestProcess : any = await this.session.processCreate($filePath, [], [], 0, 0);
                const procesResult : any = await guestProcess.waitFor(1, 30 * 1000);
            };
            execution()
                .then(() : void => {
                    promise.Invoke(true);
                })
                .catch(($ex : Error) : void => {
                    LogIt.Warning($ex.stack);
                    promise.Invoke(false);
                });
            return promise;
        }
    }

    export class VBoxPromise implements IVBoxPromise {
        private callback : any;
        private session : IVBoxSession;

        constructor($session : IVBoxSession) {
            this.session = $session;
            this.callback = ($status : boolean, $session? : IVBoxSession) : void => {
                // default callback
            };
        }

        public Invoke($status : boolean) : void {
            this.callback($status, this.session);
        }

        public Then($callback : ($status : boolean, $session? : IVBoxSession) => void) : void {
            this.callback = $callback;
        }
    }
}
