/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Interfaces {
    "use strict";

    export abstract class ITypeScriptConfigOptions {
        public target : string;
        public module : string;
        public removeComments : boolean;
        public sourceMap : boolean;
        public declaration : boolean;
        public references : string;
    }

    export abstract class ITypeScriptConfig {
        public src : string[];
        public dest : string;
        public reference : string;
        public "interface" : string;
        public options : ITypeScriptConfigOptions;
    }

    export abstract class ITypeScriptConfigs {
        public source : ITypeScriptConfig;
        public unit : ITypeScriptConfig;
        public selenium : ITypeScriptConfig;
        public coverage : ITypeScriptConfig;
        public docs : ITypeScriptConfig;
        public cache : ITypeScriptConfig;
        public selfbuild : ITypeScriptConfig;
    }
}
