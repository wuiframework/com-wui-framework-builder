/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Interfaces {
    "use strict";
    import ResourceType = Com.Wui.Framework.Builder.Enums.ResourceType;
    import DeployServerType = Com.Wui.Framework.Builder.Enums.DeployServerType;

    export abstract class IProject extends Com.Wui.Framework.Connector.Interfaces.IProject {
        public schema : string;
        public builder : IProjectBuilder;
        public repository : IProjectRepository;
        public target : IProjectTarget;
        public deploy : IProjectDeploy;
        public releases : IProjectTarget[];
        public docs : IProjectDocs;
        public dependencies : IProjectDependency[];
    }

    export abstract class IProjectBuilder {
        public location : string;
        public version : string;
        public port : number;
    }

    export abstract class IProjectAuthor {
        public name : string;
        public email : string;
    }

    export abstract class IProjectRepository {
        public type : string;
        public url : string;
    }

    export abstract class IProjectDependencyScriptConfig {
        public name : string;
        public path : string;
        public "ignore-parent-attribute" : boolean;
        public attributes : string[] | string;
    }

    export abstract class IProjectDependency {
        public version : string;
        public name : string;
        public groupId : string;
        public location : string | IProjectDependencyLocation;
        public "configure-script" : IProjectDependencyScriptConfig;
        public "install-script" : IProjectDependencyScriptConfig;
        public platforms? : string | string[];
    }

    export abstract class IProjectDependencyLocation {
        public type : string;
        public path : string | IProjectDependencyLocationPath;
        public branch : string;
        public releaseName? : string;
        public platform? : string;
    }

    export abstract class IProjectDependencyLocationPath {
        public win : string | IProjectDependencyLocationArch;
        public linux : string | IProjectDependencyLocationArch;
        public imx : string | IProjectDependencyLocationArch;
        public mac : string | IProjectDependencyLocationArch;
    }

    export abstract class IProjectDependencyLocationArch {
        public x86 : string;
        public x64 : string;
    }

    export abstract class IProjectSplashScreenFont {
        public name : string;
        public size : number;
        public bold : boolean;
        public color : string;
    }

    export abstract class IProjectSplashScreenLabel {
        public text : string;
        public x : number;
        public y : number;
        public width : number;
        public height : number;
        public justify : string;
        public visible : boolean;
        public font : IProjectSplashScreenFont;
    }

    export abstract class IProjectSplashScreenProgressBar {
        public x : number;
        public y : number;
        public width : number;
        public height : number;
        public marquee : boolean;
        public visible : boolean;
        public trayProgress : boolean;
        public background : string;
        public foreground : string;
    }

    export abstract class IProjectSplashScreenControls {
        public label : IProjectSplashScreenLabel;
        public labelStatic : IProjectSplashScreenLabel;
        public progress : IProjectSplashScreenProgressBar;
    }

    export abstract class IProjectSplashScreenResource {
        public location : string;
        public copyOnly : boolean;
        public type : ResourceType;
        public name? : string;
    }

    export abstract class IProjectSplashScreen {
        public url : string;
        public width : number;
        public height : number;
        public background : string;
        public installPath : string;
        public executable : string;
        public executableArgs? : string[];
        public localization? : string;
        public userControls : IProjectSplashScreenControls;
        public resources : IProjectSplashScreenResource[];
    }

    export abstract class IProjectTarget extends Com.Wui.Framework.Localhost.Interfaces.IProjectTarget {
        public icon : string;
        public remotePort : number;
        public width : number;
        public height : number;
        public maximized : boolean;
        public resources : IProjectTargetResources[];
        public platforms : string | string[];
        public elevate : boolean;
        public singletonApp : boolean;
        public startPage : string;
        public codeSigning : ICodeSigning;
        public upx : IUpx;
        public splashScreen : IProjectSplashScreen | string;
        public toolchain : string;
        public toolchainSettings : IToolchainSettings;
        public skipped : boolean;
        public configs : string[];
        public wuiModulesType : DeployServerType;
        public hubLocation : string;
        public appDataPath : string;
    }

    export abstract class IProjectTargetResourceFiles {
        public src : string | string[];
        public dest : string;
        public cwd? : string;
    }

    export abstract class IProjectTargetResources {
        public input : string;
        public files : IProjectTargetResourceFiles[];
        public embed : boolean;
        public output : string;
        public copy : boolean;
        public "skip-replace" : boolean;
        public resx : boolean;
        public keepLinks : boolean;
        public platforms : string | string[];
        public postBuildCopy : boolean;
        public toolchain : string | string[];
    }

    export abstract class IProjectDeploy {
        public server : IDeployServers;
        public agentsHub : string;
        public updates : IProjectDeployUpdate[];
        public chunkSize : number;
    }

    export abstract class IProjectDocs {
        public systemName : string;
        public copyright : string;
        public htmlOutput : string;
    }

    export abstract class ICodeSigning {
        public enabled : boolean;
        public certPath : string;
        public serverConfig : string;
    }

    export abstract class IUpx {
        public enabled : boolean;
        public options : string[];
    }

    export abstract class IToolchainSettings {
        public cwd : string;
        public version : string;
    }

    export abstract class IProjectDeployUpdate {
        public name? : string;
        public skipped? : boolean;
        public register? : string;
        public location : IProjectDeployUpdateLocation;
        public agents : IProjectDeployUpdateAgent[];
        public app : IProjectDeployUpdateApp;
    }

    export abstract class IProjectDeployUpdateLocation {
        public path? : IDeployServers;
        public projectName? : string;
        public releaseName? : string;
        public platform? : string;
        public version? : string;
    }

    export abstract class IProjectDeployUpdateAgent {
        public skipped? : boolean;
        public hub : string;
        public name : string;
        public profile? : string;
    }

    export abstract class IProjectDeployUpdateApp {
        public name? : string;
        public path? : string;
        public config? : string;
        public args? : string[];
    }
}
