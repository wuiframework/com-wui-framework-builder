/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Interfaces {
    "use strict";
    import TaskEnvironment = Com.Wui.Framework.Builder.Structures.TaskEnvironment;

    export interface IBaseTask extends Com.Wui.Framework.Commons.Interfaces.IBaseObject {
        Name() : string;

        Process($done : () => void, $option? : string) : void;

        getDependenciesTree($option? : string) : string[];

        setEnvironment($value : TaskEnvironment) : void;

        setOwner($owner : TasksResolver) : void;
    }
}
