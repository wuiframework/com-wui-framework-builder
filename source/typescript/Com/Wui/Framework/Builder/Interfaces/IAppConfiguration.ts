/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Interfaces {
    "use strict";
    import IWuiBuilderAgent = Com.Wui.Framework.Builder.Connectors.IWuiBuilderAgent;
    import CliTaskType = Com.Wui.Framework.Builder.Enums.CliTaskType;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;

    export class IAppConfiguration extends Com.Wui.Framework.Localhost.Interfaces.IProject {
        public schema : string;
        public deployServer : IDeployServers;
        public deployAgentsHub : string;
        public repository : any;
        public devDependencies : any;
        public scripts : any;
        public engines : any;
        public wuiModules : IBuilderWuiModules;
        public tokens : IBuilderTokens;
        public servicePort : number;
        public workspaces : string[];
        public eclipseBase : string;
        public ideaBase : string;
        public symbols : any;
        public vcvarsPath : string;
        public ownCloud : IOwnCloudConfig;
        public dockerRegistry : IDockerRegistryConfig;
        public jira : IServerConfigurationJira;
        public hub : IBuilderServerCredentials;
        public agent : IWuiBuilderAgent;
        public service : IWuiServiceConfig;
        public noSudo : boolean;
        public target : IAppTargetConfiguration;
        public noReport : boolean;
        public noExternalTypes : boolean;
    }

    export class IAppTargetConfiguration extends Com.Wui.Framework.Localhost.Interfaces.IProjectTarget {
        public updater : string;
    }

    export abstract class IBuilderWuiModules {
        public "com-wui-framework-chromiumre" : IBuilderWuiModuleConfig[];
        public "com-wui-framework-connector" : IBuilderWuiModuleConfig[];
        public "com-wui-framework-launcher" : IBuilderWuiModuleConfig[];
        public "com-wui-framework-selfextractor" : IBuilderWuiModuleConfig[];
        public "com-wui-framework-idejre-eclipse" : IBuilderWuiModuleConfig[];
        public "com-wui-framework-idejre-idea" : IBuilderWuiModuleConfig[];
    }

    export abstract class IBuilderWuiModuleConfig {
        public name : string;
        public location : string | IBuilderWuiModuleLocation;
        public os : string;
        public arch : string;
        public version? : string;
        public sourcePath : string;
        public fullPath : string;
    }

    export abstract class IBuilderWuiModuleLocation {
        public path? : string;
        public projectName : string;
        public releaseName? : string;
        public platform? : string;
        public version? : string;
    }

    export abstract class IBuilderTokens {
        public phonegap : string;
    }

    export abstract class IBuilderServerCredentials {
        public url : string;
        public user : string;
        public pass : string;
    }

    export abstract class IOwnCloudConfig extends IBuilderServerCredentials {
        public syncBase : string;
    }

    export abstract class IDockerRegistryConfig extends IBuilderServerCredentials {
        public email : string;
    }

    export abstract class IServerConfigurationJira {
        public cachePath : string;
        public projects : string[];
        public source : IBuilderServerCredentials;
        public target : IBuilderServerCredentials;
        public ignoreKeys : string[];
    }

    export abstract class IWuiServiceConfig {
        public address : string;
        public port : number;
    }

    export abstract class IBuildArgs {
        public isRebuild : boolean;
        public isTest : boolean;
        public platform : string;
        public product : BuildProductArgs;
        public releaseName : string;
        public time : Date;
        public timestamp : number;
        public type : CliTaskType;
    }

    export abstract class ITestEnvironment {
        public path : string;
        public cwd : string;
    }

    export abstract class ITestConfig {
        public environment : ITestEnvironment;
    }
}
