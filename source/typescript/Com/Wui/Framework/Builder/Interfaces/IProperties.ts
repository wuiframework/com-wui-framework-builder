/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Interfaces {
    "use strict";

    export abstract class IProperties {
        public binBase : string;
        public binBaseUnix : string;
        public projectBase : string;
        public sources : string;
        public resources : string;
        public dependencies : string;
        public cppDependencies : IProjectDependency[];
        public cppIncludes : string;
        public cppLibraries : string;
        public wuiDependencies : IProjectDependency[];
        public mobilePlatforms : string[];
        public mobileExtensions : any[];
        public dependenciesResources : string;
        public tmp : string;
        public dest : string;
        public compiled : string;
        public packageName : string;
        public loaderPath : string;
        public nodejsPort : number;
        public licenseText : string;
        public projectHas : IProjectHas;
        public javaProjectVersion : string;
        public javaDependencies : IProjectDependency[];
        public mavenResourcePaths : string[];
        public mavenArtifacts : IMavenArtifact[];
        public mavenRepositories : IMavenRepository[];
        public stats : IPropertiesStats;
        public buildCheckRegister : Array<($done : () => void, $option? : string) => void>;
        public clientConfig : any;
        public serverConfig : any;
    }

    export abstract class IMavenArtifact {
        public groupId : string;
        public artifactId : string;
        public version : string;
        public location : string;
    }

    export abstract class IMavenRepository {
        public id : string;
        public url : string;
    }

    export abstract class IProjectHasCodeType {
        /* tslint:disable: variable-name */
        public Source : () => boolean;
        public Tests : () => boolean;
        /* tslint:enable */
    }

    export abstract class IProjectHas {
        /* tslint:disable: variable-name */
        public TypeScript : IProjectHasCodeType;
        public Java : IProjectHasCodeType;
        public Cpp : IProjectHasCodeType;
        public SASS : () => boolean;
        public Resources : () => boolean;
        public SeleniumTests : () => boolean;
        public WuiContent : ($pattern : string) => boolean;
        /* tslint:enable */
    }

    export abstract class IPropertiesStats {
        public buildMinutes : number;
    }
}
