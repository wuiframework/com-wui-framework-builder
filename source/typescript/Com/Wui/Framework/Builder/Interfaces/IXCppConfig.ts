/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Interfaces {
    "use strict";

    export abstract class IXCppConfigOptions {
        public targetName : string;
        public cmakeDest : string;
    }

    export abstract class IXCppConfig {
        public src : string[];
        public srcBase : string[];
        public dest : string;
        public reference : string;
        public "interface" : string;
        public reflection : string;
        public options : IXCppConfigOptions;
        public report : string;
    }

    export abstract class IXCppConfigs {
        public source : IXCppConfig;
        public unit : IXCppConfig;
        public coverage : IXCppConfig;
    }
}
