/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IBaseTask = Com.Wui.Framework.Builder.Interfaces.IBaseTask;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class TasksResolver extends BaseObject {
        private readonly tasks : ArrayList<IBaseTask>;
        private executed : string[];

        constructor() {
            super();
            this.tasks = new ArrayList<IBaseTask>();
            const reflection : Reflection = Reflection.getInstance();
            reflection.getAllClasses().forEach(($className : string) : void => {
                if (reflection.ClassHasInterface($className, IBaseTask) && $className !== BaseTask.ClassName()) {
                    const classObject : any = reflection.getClass($className);
                    const task : IBaseTask = new classObject();
                    task.setOwner(this);
                    this.tasks.Add(task, task.Name());
                }
            });
            this.executed = [];
        }

        public Resolve($tasks : string | string[], $callback : () => void) : void {
            if (ObjectValidator.IsString($tasks)) {
                $tasks = [<string>$tasks];
            }
            const resolveNextTask : any = ($index : number) : void => {
                if ($index < $tasks.length) {
                    let task : string = $tasks[$index];
                    let option : string;
                    if (StringUtils.Contains(task, ":")) {
                        option = StringUtils.Substring(task, StringUtils.IndexOf(task, ":") + 1);
                        task = StringUtils.Substring(task, 0, StringUtils.IndexOf(task, ":"));
                    }
                    if (this.tasks.KeyExists(task)) {
                        try {
                            LogIt.Info((<any>("\nRunning \"" + $tasks[$index] + "\" task")).underline);
                            this.executed.push($tasks[$index]);
                            this.tasks.getItem(task).Process(() : void => {
                                resolveNextTask($index + 1);
                            }, option);
                        } catch (ex) {
                            LogIt.Error("Task \"" + task + "\" has failed.", ex);
                        }
                    } else {
                        LogIt.Error("Required task \"" + task + "\" does not exist.");
                    }
                } else {
                    $callback();
                }
            };
            resolveNextTask(0);
        }

        public getAll() : string[] {
            let tasks : string[] = [];
            this.tasks.foreach(($task : IBaseTask, $name : string) : void => {
                tasks.push($name);
                tasks = tasks.concat($task.getDependenciesTree());
            });
            return tasks;
        }

        public getReport() : void {
            LogIt.Debug("List of all executed tasks: {0}", this.executed);
            this.executed = [];
        }
    }
}
