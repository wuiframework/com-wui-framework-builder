/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Primitives {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class OwnCloudFile extends BaseObject {
        private name : string;
        private type : string;
        private attr : any;

        constructor($name : string, $attr : any) {
            super();
            this.name = $name;
            this.type = StringUtils.EndsWith($name, "/") ? "dir" : "file";
            this.attr = $attr;
        }

        public getName() : string {
            return require("path").basename(this.name);
        }

        public getPath() : string {
            return this.name;
        }

        public getFolder() : string {
            return require("path").dirname(this.name);
        }

        public IsDir() : boolean {
            return this.type === "dir";
        }

        public getSize() : number {
            const size : number = Convert.StringToInteger(this.attr["d:getcontentlength"]);
            return ObjectValidator.IsEmptyOrNull(size) || isNaN(size) ? 0 : size;
        }

        public getETag() : string {
            return StringUtils.Remove(this.attr["d:getetag"], "\"");
        }

        public getContentType() : string {
            let type = this.attr["d:getcontenttype"];
            if (this.IsDir()) {
                type = "httpd/unix-directory";
            }
            return type;
        }

        public getLastModified() : Date {
            return new Date(this.attr["d:getlastmodified"]);
        }

        public ToString() : string {
            return this.getFolder() + "/" + this.getName() + (this.IsDir() ? "" : " (" + Convert.IntegerToSize(this.getSize()) + ")");
        }
    }
}
