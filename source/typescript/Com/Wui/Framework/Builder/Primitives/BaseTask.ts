/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Primitives {
    "use strict";
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;
    import Terminal = Com.Wui.Framework.Builder.Connectors.Terminal;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IBaseTask = Com.Wui.Framework.Builder.Interfaces.IBaseTask;
    import TaskEnvironment = Com.Wui.Framework.Builder.Structures.TaskEnvironment;
    import IBuildArgs = Com.Wui.Framework.Builder.Interfaces.IBuildArgs;
    import IAppConfiguration = Com.Wui.Framework.Builder.Interfaces.IAppConfiguration;

    export abstract class BaseTask extends BaseObject implements IBaseTask {
        protected programArgs : ProgramArgs;
        protected project : IProject;
        protected properties : IProperties;
        protected build : IBuildArgs;
        protected builderConfig : IAppConfiguration;
        protected externalModules : string;
        protected wuiModules : string;
        protected fileSystem : FileSystemHandler;
        protected terminal : Terminal;
        private environment : TaskEnvironment;
        private readonly name : string;
        private owner : TasksResolver;

        constructor() {
            super();
            const instance : Loader = Loader.getInstance();
            this.name = this.getClassNameWithoutNamespace();
            this.fileSystem = instance.getFileSystemHandler();
            this.terminal = instance.getTerminal();
            this.builderConfig = instance.getAppConfiguration();

            const environment : TaskEnvironment = new TaskEnvironment();
            environment.Project(instance.getProjectConfig());
            environment.Properties(instance.getAppProperties());
            environment.ProgramArgs(instance.getProgramArgs());
            environment.BuildArgs(instance.getEnvironmentArgs().getBuildArgs());
            this.setEnvironment(environment);
        }

        public setEnvironment($value : TaskEnvironment) : void {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.environment = $value;
                this.project = $value.Project();
                this.properties = $value.Properties();
                this.programArgs = $value.ProgramArgs();
                this.build = $value.BuildArgs();
                this.externalModules = this.programArgs.AppDataPath() + "/external_modules";
                this.wuiModules = this.programArgs.AppDataPath() + "/wui_modules/" + this.project.target.wuiModulesType;
            }
        }

        public setOwner($owner : TasksResolver) : void {
            this.owner = $owner;
        }

        public Name() : string {
            return this.getName();
        }

        public Process($done : () => void, $option? : string) : void {
            this.process($done, $option);
        }

        public getDependenciesTree($option? : string) : string[] {
            return [];
        }

        protected process($done : () => void, $option? : string) : void {
            LogIt.Info("running task: " + this.getName());
            $done();
        }

        protected getName() : string {
            return this.name;
        }

        protected runTask($done : () => void, $tasks : string | string[]) : void;
        protected runTask($done : () => void, ...$tasks : string[]) : void;
        protected runTask($done : () => void, ...$tasks : any[]) : void {
            if (ObjectValidator.IsString($tasks)) {
                $tasks = [<any>$tasks];
            } else if ($tasks.length === 1 && ObjectValidator.IsArray($tasks[0])) {
                $tasks = <string[]>$tasks[0];
            }
            this.owner.Resolve($tasks, $done);
        }
    }
}
