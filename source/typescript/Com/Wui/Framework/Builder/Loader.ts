/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IAppConfiguration = Com.Wui.Framework.Builder.Interfaces.IAppConfiguration;
    import HttpResolver = Com.Wui.Framework.Builder.HttpProcessor.HttpResolver;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;
    import HttpServer = Com.Wui.Framework.Builder.HttpProcessor.HttpServer;
    import FileSystemHandler = Com.Wui.Framework.Connector.Connectors.FileSystemHandler;
    import Terminal = Com.Wui.Framework.Connector.Connectors.Terminal;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogLevel = Com.Wui.Framework.Commons.Enums.LogLevel;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import BuilderServiceManager = Com.Wui.Framework.Builder.Connectors.BuilderServiceManager;
    import ReportServiceConnector = Com.Wui.Framework.Services.Connectors.ReportServiceConnector;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import Resources = Com.Wui.Framework.Services.DAO.Resources;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import IProjectBuilder = Com.Wui.Framework.Builder.Interfaces.IProjectBuilder;
    import CacheManager = Com.Wui.Framework.Builder.Tasks.Utils.CacheManager;
    import GitManager = Com.Wui.Framework.Builder.Tasks.Utils.GitManager;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import NewLineType = Com.Wui.Framework.Commons.Enums.NewLineType;
    import LogSeverity = Com.Wui.Framework.Commons.Enums.LogSeverity;

    export class Loader extends Com.Wui.Framework.Localhost.Loader {
        private reportMessage : string;
        private readonly startTime : number;
        private taskResolver : TasksResolver;

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        constructor() {
            super();
            this.startTime = new Date().getTime();
        }

        public getAppConfiguration() : IAppConfiguration {
            return <IAppConfiguration>super.getAppConfiguration();
        }

        public getAppProperties() : IProperties {
            return this.getEnvironmentArgs().getAppProperties();
        }

        public getEnvironmentArgs() : EnvironmentArgs {
            return <EnvironmentArgs>super.getEnvironmentArgs();
        }

        public getProjectConfig() : IProject {
            return this.getEnvironmentArgs().getTargetConfig();
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        public getProgramArgs() : ProgramArgs {
            return <ProgramArgs>super.getProgramArgs();
        }

        public getFileSystemHandler() : FileSystemHandler {
            return <FileSystemHandler>super.getFileSystemHandler();
        }

        public getTerminal() : Terminal {
            return <Terminal>super.getTerminal();
        }

        protected initProgramArgs() : ProgramArgs {
            return new ProgramArgs();
        }

        protected initEnvironment() : Com.Wui.Framework.Commons.EnvironmentArgs {
            if (isBrowser) {
                return super.initEnvironment();
            } else {
                return new EnvironmentArgs();
            }
        }

        protected initHttpServer() : HttpServer {
            return new HttpServer();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver("/");
        }

        protected initFileSystem() : FileSystemHandler {
            return new FileSystemHandler();
        }

        protected initTerminal() : Terminal {
            return new Terminal();
        }

        protected processProgramArgs() : boolean {
            const args : ProgramArgs = this.getProgramArgs();
            if (args.IsHubClient() || args.IsServiceClient()) {
                args.StartServer(false);
            }
            let processed : boolean = super.processProgramArgs();
            if (!ObjectValidator.IsEmptyOrNull(this.getAppConfiguration().target.appDataPath)) {
                args.AppDataPath(this.getAppConfiguration().target.appDataPath);
                this.setAnonymizer({
                    appData: args.AppDataPath()
                });
            }
            const reportExcludeList : string[] = [
                "run-unit-test",
                "run-coverage-test",
                "test:xcpplint",
                "project-cleanup",
                "test:lint",
                "test-lint-stagged-only",
                "Cannot start Mock server",
                "Connection has been lost",
                "Uncommitted changes has been found",
                "are lint free"
            ];
            const persistence : IPersistenceHandler = PersistenceFactory.getPersistence(this.getClassName());
            const minSeverity : LogSeverity =
                this.getProgramArgs().IsDebug() ? LogSeverity.LOW : this.getProgramArgs().getOptions().logLevel;
            (<any>LogIt).formatter = ($type : LogLevel, $message : string, $newLineType : NewLineType = NewLineType.WINDOWS,
                                      $severity? : number) : string => {
                if ($type === LogLevel.ERROR || $type === LogLevel.WARNING) {
                    this.stderrWrite($message, $type === LogLevel.WARNING ? ColorType.YELLOW : ColorType.RED);
                    if ($type === LogLevel.ERROR) {
                        if (persistence.Variable("lastError") !== $message) {
                            let reportEnabled : boolean = true;
                            reportExcludeList.forEach(($exclude : string) : void => {
                                if (args.getTasks().indexOf($exclude) !== -1 || StringUtils.ContainsIgnoreCase($message, $exclude)) {
                                    reportEnabled = false;
                                }
                            });
                            if (reportEnabled) {
                                if (!this.getAppConfiguration().noReport) {
                                    persistence.Variable("lastError", $message);
                                    this.reportMessage = $message;
                                } else {
                                    this.stderrWrite("Crash report skipped: reports are disabled by noReport option");
                                }
                            } else {
                                this.stderrWrite("Crash report skipped: error has been found at exclude list");
                            }
                        } else {
                            this.stderrWrite("Crash report skipped: report has been already send");
                        }
                        this.Exit(-1);
                    } else {
                        return "";
                    }
                } else if ($type === LogLevel.INFO && !ObjectValidator.IsEmptyOrNull($severity) && $severity < minSeverity) {
                    return "";
                } else {
                    return StringUtils.Format($message);
                }
            };
            if (args.AppDataPath() !== args.ProjectBase()) {
                const filepath : string = this.getFileSystemHandler().NormalizePath(
                    args.AppDataPath() + "/" + this.getEnvironmentArgs().getAppName() + ".config.json");
                const fs : Types.NodeJS.fs = require("fs");
                if (fs.existsSync(filepath)) {
                    try {
                        const configData : any = JSON.parse(require("strip-json-comments")(fs.readFileSync(filepath).toString()));
                        Resources.Extend(this.getAppConfiguration(), configData);
                    } catch (ex) {
                        this.stderrWrite("Unable to parse \"" + filepath + "\". " + ex.stack);
                    }
                }
            }

            const onExitHandler : any = ($exitCode : number = 0) : void => {
                this.Exit($exitCode);
            };

            process.env.PATH = this.getTerminal().NormalizeEnvironmentPath(this.getAppEnvironmentPath());

            if (!processed) {
                if (args.IsExternalModuleTask() && !args.IsForwardedTask()) {
                    processed = true;
                    const argv : string[] = args.getOptions().forwardArgs;

                    for (const key in argv) {
                        if (argv.hasOwnProperty(key) && StringUtils.Contains(argv[key], " ") && !StringUtils.StartsWith(argv[key], "\"")) {
                            argv[key] = "\"" + argv[key] + "\"";
                        }
                    }

                    const cmd : string = argv[0];
                    argv.shift();
                    process.env.PATH = this.getTerminal().NormalizeEnvironmentPath(EnvironmentHelper.getNodejsRoot());
                    this.getTerminal()
                        .Spawn(cmd, argv, {
                            advanced: {noTerminalLog: true, noExitOnStderr: true, useStdIn: true},
                            cwd     : process.cwd()
                        }, onExitHandler);
                } else {
                    processed = true;
                    BuilderServiceManager.CreatePIDRecord();

                    this.getEnvironmentArgs().LoadTargetEnvironment();
                    this.taskResolver = new TasksResolver();

                    if (!args.IsForwardedTask()) {
                        const requiredBuilder : IProjectBuilder = this.getProjectConfig().builder;
                        if (!ObjectValidator.IsEmptyOrNull(requiredBuilder.location)) {
                            requiredBuilder.location = StringUtils.Replace(requiredBuilder.location, "\\", "/");
                        }
                        let isHostForward : boolean = false;
                        if (!args.IsServiceClient() && !args.IsHubClient()) {
                            if (requiredBuilder.port > 0 || requiredBuilder.location === "service") {
                                args.IsServiceClient(true);
                            } else if (!ObjectValidator.IsEmptyOrNull(requiredBuilder.location) && requiredBuilder.location !== "host") {
                                if (requiredBuilder.location === "agent" ||
                                    !ObjectValidator.IsEmptyOrNull(requiredBuilder.version) || requiredBuilder.version === "latest") {
                                    args.IsHubClient(true);
                                } else if ((StringUtils.StartsWith(requiredBuilder.location, "/") ||
                                    StringUtils.Contains(requiredBuilder.location, ":/")) &&
                                    this.getFileSystemHandler().Exists(requiredBuilder.location)) {
                                    isHostForward = true;
                                } else {
                                    args.IsHubClient(true);
                                }
                            }
                        }
                        if (args.IsServiceClient() || args.IsHubClient()) {
                            this.taskResolver.Resolve("forward-task", onExitHandler);
                        } else if (args.StartAgent()) {
                            this.taskResolver.Resolve("builder-agent:start", onExitHandler);
                        } else if (isHostForward) {
                            const cliArgs : string[] = [].concat(process.argv);
                            if (EnvironmentHelper.IsEmbedded() || StringUtils.Contains(cliArgs[0], "nodejs")) {
                                cliArgs.shift();
                            }
                            if (StringUtils.ContainsIgnoreCase(cliArgs[0], "loader.min.js")) {
                                cliArgs.shift();
                            }
                            const env : any = process.env;
                            env.IS_WUI_PROXY_TASK = 1;
                            env.PATH = this.getTerminal().NormalizeEnvironmentPath(requiredBuilder + "/cmd");
                            this.getTerminal().Spawn("wui", cliArgs.concat(["--forward-task"]), {
                                advanced: {
                                    colored: true
                                },
                                cwd     : args.TargetBase(),
                                env
                            }, onExitHandler);
                        } else if (args.getOptions().selfextract) {
                            this.taskResolver.Resolve("selfinstall:selfextract", () : void => {
                                BuilderServiceManager.RemovePIDRecord();
                                if (EnvironmentHelper.IsWindows()) {
                                    this.getTerminal().Spawn("notifu.exe", [
                                            "/p", "\"WUI Builder - install\"",
                                            "/m", "\"The installation has been finished.\\nHave fun! :)\"",
                                            "/q",
                                            "/t", "info",
                                            "/d", "10000",
                                            "/i", "\"" + this.getFileSystemHandler().NormalizePath(args.ProjectBase(), true) +
                                            "\\resource\\graphics\\icon.ico\""
                                        ],
                                        {
                                            cwd    : args.ProjectBase() + "/resource/libs/node-notifier",
                                            verbose: false
                                        },
                                        ($exitCode : number, $std : string[]) : void => {
                                            if ($exitCode !== 0) {
                                                LogIt.Error($std[0] + $std[1]);
                                            }
                                            onExitHandler();
                                        });
                                } else {
                                    onExitHandler();
                                }
                            });
                        } else if (args.getOptions().selfupdate) {
                            this.taskResolver.Resolve("selfupdate", onExitHandler);
                        } else {
                            this.taskResolver.Resolve(args.getTasks(), onExitHandler);
                        }
                    } else if (args.getOptions().selfextract || args.getOptions().selfupdate || args.StartAgent()) {
                        LogIt.Error("Current task is not supported by remote execution.");
                    } else {
                        this.taskResolver.Resolve(args.getTasks(), onExitHandler);
                    }
                }
            }

            return processed;
        }

        protected onCloseHandler($done : ($exitCode? : number) => void) : void {
            const handleExit : any = ($message? : string) : void => {
                this.stderrWrite($message);
                super.onCloseHandler(($exitCode? : number) : void => {
                    $done($exitCode);
                });
            };
            const createReport : any = () : void => {
                if (ObjectValidator.IsEmptyOrNull(this.reportMessage)) {
                    handleExit();
                } else {
                    try {
                        const connector : ReportServiceConnector = new ReportServiceConnector();
                        connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                            handleExit($eventArgs.ToString("", false));
                        });
                        const env : Com.Wui.Framework.Commons.EnvironmentArgs = this.getEnvironmentArgs();
                        let logPath : string;
                        if (this.getProgramArgs().getOptions().noTarget) {
                            logPath = (<any>this).logPath;
                        } else {
                            logPath = this.getAppProperties().projectBase + "/log/build.log";
                        }
                        connector
                            .CreateReport({
                                appId      : StringUtils.getSha1(env.getAppName() + env.getProjectVersion()),
                                appName    : env.getProjectName(),
                                appVersion : env.getProjectVersion(),
                                log        : this.getFileSystemHandler().Read(logPath).toString(),
                                printScreen: "",
                                timeStamp  : new Date().getTime(),
                                trace      : this.reportMessage
                            })
                            .Then(() : void => {
                                handleExit();
                            });
                    } catch (ex) {
                        handleExit("Unable to send error report.");
                    }
                }
            };
            try {
                if (!this.getProgramArgs().IsBaseTask() && !this.getProgramArgs().IsExternalModuleTask()) {
                    const colors : any = require("colors");
                    LogIt.Info(colors.underline("\nRunning on close task"));
                    BuilderServiceManager.RemovePIDRecord();
                    CacheManager.ReferenceRollback();
                    GitManager.StashRestore();
                    if (!ObjectValidator.IsEmptyOrNull(this.taskResolver) && this.getProgramArgs().IsDebug()) {
                        this.taskResolver.getReport();
                    }
                    LogIt.Info(colors.underline("\nExecution Time") + " " +
                        colors.gray("(" + Convert.TimeToGMTformat(this.startTime) + ")"));
                    LogIt.Info("Total " + Convert.TimeToLongString(new Date().getTime() - this.startTime));
                }
                createReport();
            } catch (ex) {
                createReport();
            }
        }

        protected getAppEnvironmentPath() : string {
            const path : Types.NodeJS.path = require("path");
            const args : ProgramArgs = Loader.getInstance().getProgramArgs();
            const externalModules : string = args.AppDataPath() + "/external_modules";
            if (EnvironmentHelper.IsWindows()) {
                const envPath : string =
                    args.ProjectBase() + ";" +
                    externalModules + "/jdk/bin;" +
                    externalModules + "/doxygen;" +
                    externalModules + "/cppcheck;" +
                    externalModules + "/python;" + externalModules + "/python/Scripts;" +
                    externalModules + "/msys2;" + externalModules + "/msys2/mingw64/bin;" +
                    externalModules + "/cmake/bin;" +
                    externalModules + "/git/cmd;" +
                    externalModules + "/maven/bin;" +
                    externalModules + "/nasm;";
                return StringUtils.Replace(envPath, "/", path.sep);
            } else {
                return "" +
                    args.ProjectBase() + ":" +
                    externalModules + "/jdk/bin:" +
                    externalModules + "/gcc:" +
                    externalModules + "/clang:" +
                    externalModules + "/cmake/bin:" +
                    externalModules + "/git/bin:" +
                    externalModules + "/doxygen/bin:" +
                    externalModules + "/cppcheck:" +
                    externalModules + "/python:" +
                    externalModules + "/apache/bin:" +
                    externalModules + "/maven/bin:";
            }
        }

        private stderrWrite($message : string, $color : ColorType = ColorType.RED) : void {
            if (!ObjectValidator.IsEmptyOrNull($message)) {
                try {
                    if (EnvironmentHelper.IsTTY()) {
                        $message = $message[<any>$color];
                    }
                    process.stdout.write($message + require("os").EOL);
                } catch (ex) {
                    console.error($message); // tslint:disable-line
                }
            }
        }
    }
}
