/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.BuildProcessor {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import GeneralTaskType = Com.Wui.Framework.Builder.Enums.GeneralTaskType;
    import CliTaskType = Com.Wui.Framework.Builder.Enums.CliTaskType;
    import BuildTaskType = Com.Wui.Framework.Builder.Enums.BuildTaskType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class BuildStateMachine extends BaseTask {

        protected getName() : string {
            return "build-machine";
        }

        protected process($done : any, $option : string) : void {
            let chain : string[] = [];
            switch ($option) {
            case CliTaskType.EAP:
                chain = ["install:eap", "build-machine:product-eap"];
                if (ObjectValidator.IsEmptyOrNull(this.project.target.hubLocation)) {
                    this.project.target.hubLocation = this.project.deploy.server.eap;
                }
                break;
            case CliTaskType.PROD:
                chain = ["install:prod", "git-manager:porcelaincheck", "build-machine:product-prod"];
                if (ObjectValidator.IsEmptyOrNull(this.project.target.hubLocation)) {
                    this.project.target.hubLocation = this.project.deploy.server.prod;
                }
                break;
            case CliTaskType.DEV:
            case CliTaskType.DEV_SKIP_TEST:
                if (this.programArgs.getOptions().withTest) {
                    chain = ["install:dev", "test:lint", "build-machine:product-dev"];
                } else {
                    chain = [GeneralTaskType.GET_SYSTEM_INFO, "install:dev", "build-machine:product-dev-skip-test"];
                }
                break;
            case CliTaskType.REBUILD:
            case CliTaskType.REBUILD_DEV:
            case CliTaskType.REBUILD_DEV_SKIP_TEST:
                this.build.isRebuild = true;
                chain = ["project-cleanup:cache", "build:dev-skip-test"];
                break;

            case "product-dev-skip-test":
                chain = ["compile:dev", BuildTaskType.COPY_TO_TARGET];
                break;
            case "product-dev":
                chain = ["test:code", "compile:dev", BuildTaskType.COPY_TO_TARGET];
                break;
            case "product-eap":
                chain = ["compile:eap", BuildTaskType.COPY_TO_TARGET, "project-cleanup:prod"];
                break;
            case "product-prod":
                chain = ["test:all", "compile:prod", BuildTaskType.COPY_TO_TARGET, "project-cleanup:prod"];
                break;

            default:
                LogIt.Error("Unsupported build state option: " + $option);
                break;
            }
            this.runTask($done, chain);
        }
    }
}
