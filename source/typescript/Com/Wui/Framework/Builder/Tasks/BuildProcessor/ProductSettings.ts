/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.BuildProcessor {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;

    export class ProductSettings extends BaseTask {

        protected getName() : string {
            return "product-settings";
        }

        protected process($done : any, $option : string) : void {
            const executor : BuildExecutor = new BuildExecutor();
            const products : any = [];
            this.properties.mobilePlatforms = [];
            executor.getReleases().forEach(($release : IReleaseProperties) : void => {
                executor.getTargetProducts($release.target).forEach(($product : BuildProductArgs) : void => {
                    products.push({
                        args       : $product,
                        releaseName: $release.name,
                        target     : $release.target
                    });
                    if ($product.Toolchain() === ToolchainType.PHONEGAP) {
                        this.properties.mobilePlatforms.push($product.OS().toString());
                    }
                });
            });
            const index : number = StringUtils.ToInteger($option);
            const product : any = products[index];
            this.build.product = product.args;
            if (this.properties.projectHas.Cpp.Source()) {
                this.build.platform = product.args.Type();
            } else {
                this.build.platform = product.args.Value();
            }
            this.build.releaseName = product.releaseName;
            this.project.target = product.target;
            this.project.target.toolchain = product.args.Toolchain();
            $done();
        }
    }
}
