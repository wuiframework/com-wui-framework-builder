/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.BuildProcessor {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import BuildTaskType = Com.Wui.Framework.Builder.Enums.BuildTaskType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class BuildSettings extends BaseTask {

        protected getName() : string {
            return BuildTaskType.BUILD_SETTINGS;
        }

        protected process($done : any, $option : string) : void {
            const buildLog : string = this.properties.projectBase + "/log/build.log";
            this.fileSystem.Delete(buildLog);
            Loader.getInstance().setLogPath(buildLog);
            this.build.time = new Date();
            this.build.type = $option;
            this.build.timestamp = this.build.time.getTime();
            if (ObjectValidator.IsEmptyOrNull(this.project.target.hubLocation)) {
                this.project.target.hubLocation = this.project.deploy.server.dev;
            }
            LogIt.Info(">>"[ColorType.YELLOW] + " Build type: " + this.build.type + ", Build time: " + this.build.time);
            $done();
        }
    }
}
