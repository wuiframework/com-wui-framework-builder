/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Run {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import CliTaskType = Com.Wui.Framework.Builder.Enums.CliTaskType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BuildExecutor = Com.Wui.Framework.Builder.Tasks.BuildProcessor.BuildExecutor;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;

    export class Run extends BaseTask {

        protected getName() : string {
            return "run";
        }

        protected process($done : any) : void {
            const packageName : string = this.properties.packageName;
            const eclipseReleasePath : string = this.properties.projectBase + "/build/releases/eclipse/target/" + packageName + ".jar";
            const ideaReleasePath : string = this.properties.projectBase + "/build/releases/idea/target/" + packageName + ".zip";

            const chain : string[] = [CliTaskType.DEV_SKIP_TEST];
            const executor : BuildExecutor = new BuildExecutor();
            let openWebTarget : boolean = false;
            executor.getTargetProducts(this.project.target).forEach(($product : BuildProductArgs) : void => {
                if ($product.Toolchain() === ToolchainType.ECLIPSE && this.fileSystem.Exists(eclipseReleasePath)) {
                    chain.push("run-eclipse");
                } else if ($product.Toolchain() === ToolchainType.IDEA && this.fileSystem.Exists(ideaReleasePath)) {
                    chain.push("run-idea");
                } else {
                    if (this.fileSystem.Expand(this.properties.projectBase + "/build/target/*.exe")) {
                        chain.push("run-binary");
                    } else {
                        openWebTarget = true;
                    }
                }
            });
            if (openWebTarget) {
                this.terminal.Open("file://" + this.properties.projectBase + "/build/target/index.html",
                    ($exitCode : number) : void => {
                        if ($exitCode !== 0) {
                            LogIt.Error("Failed to open target index.html");
                        }
                        $done();
                    });
            } else {
                this.runTask($done, chain);
            }
        }
    }
}
