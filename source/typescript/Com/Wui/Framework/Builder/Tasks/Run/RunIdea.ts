/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Run {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class RunIdea extends BaseTask {

        protected getName() : string {
            return "run-idea";
        }

        protected process($done : any) : void {
            const ideaPath : string = this.builderConfig.ideaBase + "/bin";
            const packageName : string = this.properties.packageName;
            const ideaReleasePath : string = this.properties.projectBase + "/build/releases/idea/target/" + packageName + ".zip";

            if (!this.fileSystem.Exists(ideaPath)) {
                LogIt.Error("Unable to open IDEA at \"" + ideaPath + "\". Please validate ideaBase parameter " +
                    "at WUI Builder package.conf.json or private.conf.json.");
            }
            this.terminal.Kill(ideaPath + "/idea64.exe", ($status : boolean) : void => {
                if ($status) {
                    let pluginPath : string = process.env.home;
                    if (ObjectValidator.IsEmptyOrNull(pluginPath)) {
                        pluginPath = process.env.homedrive + process.env.homepath;
                    }
                    pluginPath += "/.IdeaIC2017.2/config/plugins/" + packageName;
                    pluginPath = StringUtils.Replace(pluginPath, "\\", "/");
                    this.fileSystem.Unpack(ideaReleasePath, <any>{
                        output  : pluginPath,
                        override: true
                    }, () : void => {
                        this.terminal.Spawn("idea64.exe", [], ideaPath, () : void => {
                            $done();
                        });
                    });
                } else {
                    LogIt.Error("Unable to kill current IDE process");
                }
            });
        }
    }
}
