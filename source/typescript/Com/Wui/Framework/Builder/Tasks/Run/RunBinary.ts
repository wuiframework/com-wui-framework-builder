/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Run {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class RunBinary extends BaseTask {

        protected getName() : string {
            return "run-binary";
        }

        protected process($done : any) : void {
            let executable : string = "WUILauncher.exe";
            if (!this.fileSystem.Exists(this.properties.projectBase + "/build/target/" + executable)) {
                executable = this.project.target.name + ".exe";
            }
            if (this.fileSystem.Exists(this.properties.projectBase + "/build/target/" + executable)) {
                this.properties.dest = this.properties.projectBase + "build/target/target";
                this.terminal.Open(this.properties.projectBase + "/build/target/" + executable);
                setTimeout($done, 500);
            } else {
                $done();
            }
        }
    }
}
