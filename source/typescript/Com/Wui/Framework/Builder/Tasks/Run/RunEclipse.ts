/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Run {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class RunEclipse extends BaseTask {

        protected getName() : string {
            return "run-eclipse";
        }

        protected process($done : any) : void {
            const eclipsePath : string = this.builderConfig.eclipseBase;
            const packageName : string = this.properties.packageName;
            const eclipseReleasePath : string = this.properties.projectBase + "/build/releases/eclipse/target/" + packageName + ".jar";

            if (!this.fileSystem.Exists(eclipsePath)) {
                LogIt.Error("Unable to open Eclipse at \"" + eclipsePath + "\". Please validate eclipseBase parameter " +
                    "at WUI Builder package.conf.json or private.conf.json.");
            }
            const dropinsPath : string = eclipsePath + "/dropins";
            this.fileSystem.Delete(dropinsPath);
            this.fileSystem.Copy(eclipseReleasePath, dropinsPath + "/" + packageName + ".jar", ($status : boolean) : void => {
                if ($status) {
                    this.terminal.Spawn("eclipse.exe", ["-clean", "-debug"], eclipsePath, () : void => {
                        $done();
                    });
                } else {
                    LogIt.Error("Unable to find plugin jar at: " + eclipseReleasePath);
                }
            });
        }
    }
}
