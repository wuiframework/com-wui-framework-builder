/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Cleanup {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ProjectCleanup extends BaseTask {
        /// TODO: create task for creation of .wui folder,
        /// TODO: where will be moved package-*.json files and content of bin folder moved to .wui/shared

        protected getName() : string {
            return "project-cleanup";
        }

        protected process($done : any, $option : string) : void {
            const config : any = {
                /* tslint:disable: object-literal-sort-keys */
                bin      : ["bin/"],
                init     : ["build/releases"],
                build    : [
                    "build/compiled", "build/apptarget", "build/webtarget", "build/target", "build/reports",
                    "build/*.*"
                ],
                source   : [
                    "source/typescript/**/*.js", "source/typescript/**/*.js.map", "source/typescript/**/*.d.ts",
                    "!source/typescript/**/reference.d.ts"
                ],
                test     : [
                    "test/unit/**/*.js", "test/unit/**/*.js.map", "test/unit/**/*.d.ts",
                    "!test/unit/**/reference.d.ts"
                ],
                prod     : [
                    "build/**/target/test", "build/**/target/**/*.*.map", "build/**/connector.config.jsonp",
                    "build/**/target/UnitTestRunner*"
                ],
                cache    : ["build_cache"],
                cache_ts : ["build_cache/**/*.ts", "build_cache/**/*.js"],
                cache_css: ["build_cache/**/*.css"]
                /* tslint:enable */
            };

            if (ObjectValidator.IsEmptyOrNull($option)) {
                this.runTask($done, "dependencies-sort", "structure-cleanup", "scr-cleanup", "typescript-autocleanup", "git-cleanup");
            } else if (config.hasOwnProperty($option) || $option === "all") {
                const clean : any = ($pattern : string[]) : void => {
                    this.fileSystem.Expand($pattern).forEach(($file : string) : void => {
                        this.fileSystem.Delete($file);
                    });
                };
                if ($option === "all") {
                    let pattern : string;
                    for (pattern in config) {
                        if (config.hasOwnProperty(pattern)) {
                            clean(config[pattern]);
                        }
                    }
                } else {
                    clean(config[$option]);
                }
                $done();
            } else {
                LogIt.Error("Unsupported cleanup option \"" + $option + "\".");
            }
        }
    }
}
