/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Cleanup {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import TypeScriptLint = Com.Wui.Framework.Builder.Tasks.TypeScript.TypeScriptLint;

    export class TypeScriptCleanup extends BaseTask {
        private autofixState : boolean;

        constructor() {
            super();
            this.autofixState = false;
        }

        protected getName() : string {
            return "typescript-autocleanup";
        }

        protected process($done : any, $option : string) : void {
            if ($option === "restore") {
                TypeScriptLint.getConfig().options.fix = this.autofixState;
                $done();
            } else {
                this.autofixState = TypeScriptLint.getConfig().options.fix;
                TypeScriptLint.getConfig().options.fix = true;
                this.runTask($done, "tslint", "typescript-autocleanup:restore");
            }
        }
    }
}
