/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Cleanup {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import GitManager = Com.Wui.Framework.Builder.Tasks.Utils.GitManager;

    export class GitCleanup extends BaseTask {

        protected getName() : string {
            return "git-cleanup";
        }

        protected process($done : any) : void {
            let defaultIgnoreFile : string = this.properties.binBase + "/resource/configs/gitignore.conf";
            const targetFile : string = this.properties.projectBase + "/.gitignore";

            if (this.properties.projectHas.Cpp.Source()) {
                defaultIgnoreFile = this.properties.binBase + "/resource/configs/gitignore-cpp.conf";
            }

            const generateDefault : any = () : void => {
                this.fileSystem.Copy(defaultIgnoreFile, targetFile, () : void => {
                    if (!this.programArgs.IsAgentTask()) {
                        GitManager.UpdateTracked($done);
                    } else {
                        $done();
                    }
                });
            };

            if (this.fileSystem.Exists(targetFile)) {
                const splittingPattern = "# Project individual ignore patterns";
                const targetData : string[] = this.fileSystem.Read(targetFile).toString().split(splittingPattern);
                const defaultData : string[] = this.fileSystem.Read(defaultIgnoreFile).toString().split(splittingPattern);

                if (targetData.length !== 2) {
                    LogIt.Warning(">>"[ColorType.YELLOW] + ".gitignore file in project root is corrupted. " +
                        "Replacing by default values. Old ones are listed below\n" + targetData[0] + "\n");
                    generateDefault();
                } else if (targetData[0] !== defaultData[0]) {
                    const newData = defaultData[0] + splittingPattern + targetData[1];
                    if (this.fileSystem.Write(targetFile, newData)) {
                        LogIt.Info(">>"[ColorType.RED] + " .gitignore has been cleaned.");
                        GitManager.UpdateTracked($done);
                    } else {
                        LogIt.Error("Failed to write data into .gitignore file.");
                    }
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " .gitignore is up to date.");
                    if (!Loader.getInstance().getProgramArgs().IsForce()) {
                        $done();
                    } else {
                        GitManager.UpdateTracked($done);
                    }
                }
            } else {
                LogIt.Info("No .gitignore file found in project, thus the default one has been generated.");
                generateDefault();
            }
        }
    }
}
