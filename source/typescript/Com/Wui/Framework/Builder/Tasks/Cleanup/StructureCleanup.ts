/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Cleanup {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class StructureCleanup extends BaseTask {

        protected getName() : string {
            return "structure-cleanup";
        }

        protected process($done : any) : void {
            const os : Types.NodeJS.os = require("os");
            const config : IProjectCleanupConfig = JSON.parse(this.fileSystem.Read(
                this.properties.binBase + "/resource/configs/project-cleanup.conf.json").toString());
            const configDestination : string = this.properties.projectBase + "/project-cleanup.conf.json";
            if (this.fileSystem.Exists(configDestination)) {
                const projectConfig : IProjectCleanupConfig = JSON.parse(this.fileSystem.Read(configDestination).toString());
                let configBlock : string;
                for (configBlock in projectConfig) {
                    if (projectConfig.hasOwnProperty(configBlock)) {
                        config[configBlock] = projectConfig[configBlock];
                    }
                }
            }
            const escapeRegExp : any = ($input : string) : string => {
                const specials : string[] = ["-", "[", "]", "/", "{", "}", "(", ")", "*", "+", "?", ".", "\\", "^", "$", "|"];
                return $input.replace(new RegExp("[" + specials.join("\\") + "]", "g"), "\\$&");
            };

            let cleanUpLog : string = "";
            const logIt : any = ($message : string, $force : boolean) : void => {
                if (config.cleanuplog) {
                    cleanUpLog += $message + os.EOL;
                }
                if (config.verbose || $force === true) {
                    LogIt.Debug($message);
                }
            };

            const target : string = "__cleanup";
            this.fileSystem.Delete(this.properties.projectBase + "/" + target);
            if (config.hasOwnProperty("targetFolder") && config.targetFolder !== "") {
                if (this.fileSystem.Exists(this.properties.projectBase + "/" + config.targetFolder)) {
                    logIt("clean up target: " + config.targetFolder);
                    this.fileSystem.Delete(this.properties.projectBase + "/" + config.targetFolder);
                }
            }

            const filePaths : string[] = config.filePaths;
            const replacements : string[] = config.contentReplacements;
            try {
                const getExpandPattern : any = () : string[] => {
                    let pattern : string[] = [this.properties.projectBase + "/*"];
                    if (config.hasOwnProperty("ignore")) {
                        pattern = [];
                        this.fileSystem.Expand(this.properties.projectBase + "/*").forEach(($file : string) : void => {
                            let contains : boolean = false;
                            config.ignore.forEach(($pattern : string) : void => {
                                if (StringUtils.PatternMatched(this.properties.projectBase + "/" + $pattern, $file)) {
                                    contains = true;
                                }
                            });
                            if (!contains && this.fileSystem.IsDirectory($file)) {
                                pattern.push($file + "/**/*");
                            }
                        });
                        config.ignore.forEach(($pattern : string) : void => {
                            if (StringUtils.EndsWith($pattern, "/*") && !StringUtils.Contains($pattern, "/**")) {
                                $pattern += "*/*";
                            }
                            pattern.push("!" + $pattern);
                        });
                    }
                    return pattern;
                };

                let files : string[] = this.fileSystem.Expand(getExpandPattern());

                let index : number;
                let filePath : string;
                let destinationPath : string;
                let origPath : string;
                let newPath : string;
                let replacement : string;
                const cleanedFiles : string[] = [];
                for (origPath in filePaths) {
                    if (!replacements.hasOwnProperty(origPath)) {
                        replacements[origPath] = filePaths[origPath];
                    }
                }

                const isIgnored : any = ($filePath : string) : boolean => {
                    let status : boolean = false;
                    for (const ignore of config.ignore) {
                        if (StringUtils.PatternMatched(this.properties.projectBase + "/" + ignore, $filePath)) {
                            status = true;
                            break;
                        }
                    }
                    return status;
                };
                for (index = 0; index < files.length; index++) {
                    filePath = files[index];
                    if (isIgnored(filePath)) {
                        continue;
                    }
                    if (this.fileSystem.IsFile(filePath)) {
                        destinationPath = this.properties.projectBase + "/" +
                            target + "/" + StringUtils.Remove(filePath, this.properties.projectBase + "/");
                        for (origPath in filePaths) {
                            if (filePaths.hasOwnProperty(origPath)) {
                                newPath = filePaths[origPath];
                                if (filePath.indexOf(origPath) !== -1) {
                                    destinationPath = destinationPath.replace(origPath, newPath);
                                }
                                break;
                            }
                        }
                        logIt("copy file " + filePath + " to " + destinationPath);
                        this.fileSystem.Write(destinationPath, this.fileSystem.Read(filePath));

                        let readFile : boolean = false;
                        if (config.hasOwnProperty("readExtensions")) {
                            /* tslint:disable: prefer-for-of */
                            for (let extensionIndex : number = 0; extensionIndex < config.readExtensions.length; extensionIndex++) {
                                if ((<any>destinationPath).endsWith(config.readExtensions[extensionIndex])) {
                                    readFile = true;
                                    break;
                                }
                            }
                            /* tslint:enable */
                        }
                        if (readFile) {
                            logIt("processing file: " + filePath);
                            let data : string = this.fileSystem.Read(destinationPath).toString();
                            const origData : string = data;
                            for (replacement in replacements) {
                                if (replacements.hasOwnProperty(replacement)) {
                                    data = data.replace(new RegExp(escapeRegExp(replacement), "gm"), replacements[replacement]);
                                }
                            }

                            if ((<any>destinationPath).endsWith(".ts")) {
                                let startIndex : number = data.indexOf("namespace");
                                if (startIndex > data.indexOf("{")) {
                                    startIndex = data.indexOf("{");
                                }
                                if (startIndex > data.indexOf(";")) {
                                    startIndex = data.indexOf(";");
                                }
                                const header : string = data.substring(0, startIndex);
                                if (header !== "") {
                                    let cleanHeader : string = header.replace(/^[\r\n]+/gm, "");
                                    if (header.indexOf("\r\n") !== -1) {
                                        cleanHeader = cleanHeader.replace(/\r/gm, "\r\n");
                                    }
                                    if (header !== cleanHeader) {
                                        logIt(">> removing header blank lines");
                                        data = data.replace(header, cleanHeader);
                                    }
                                }

                                const imports : RegExpMatchArray = data.match(/import (.*?) =/gm);
                                let importIndex : number;
                                if (imports !== null) {
                                    for (importIndex = 0; importIndex < imports.length; importIndex++) {
                                        const importName : string =
                                            imports[importIndex].replace("import ", "").replace(" =", "");
                                        if (importName !== "Echo" &&
                                            importName !== "LogIt" &&
                                            importName.indexOf(" ") === -1 &&
                                            data.indexOf(importName + ".") === -1 &&
                                            data.indexOf(" " + importName + ";") === -1 &&
                                            data.indexOf("new " + importName) === -1 &&
                                            data.indexOf(" = " + importName) === -1 &&
                                            data.indexOf(" : " + importName) === -1 &&
                                            data.indexOf("|" + importName) === -1 &&
                                            data.indexOf(importName + "|") === -1 &&
                                            data.indexOf(" | " + importName) === -1 &&
                                            data.indexOf(importName + " | ") === -1 &&
                                            data.indexOf("(" + importName) === -1 &&
                                            data.indexOf(importName + ")") === -1 &&
                                            data.indexOf("extends " + importName) === -1 &&
                                            data.indexOf("implements " + importName) === -1 &&
                                            data.indexOf(", " + importName) === -1 &&
                                            data.indexOf(importName + ", ") === -1 &&
                                            data.indexOf("<" + importName + ">") === -1 &&
                                            data.indexOf(importName + "[") === -1 &&
                                            data.indexOf("@" + importName) === -1) {
                                            logIt(">> removing unused import \"" + importName + "\"");
                                            data = data.replace(new RegExp("[\n\r]*.*import " + importName + " =[\n\r]*.*;", "gm"), "");
                                        }
                                    }
                                }
                            }
                            if (origData !== data) {
                                cleanedFiles.push(destinationPath);
                                this.fileSystem.Write(destinationPath, data);
                            }
                        }
                    }
                }

                let destinationFolder : string = "";
                if (config.hasOwnProperty("targetFolder") && config.targetFolder !== "") {
                    destinationFolder = config.targetFolder;
                }

                files = this.fileSystem.Expand(this.properties.projectBase + "/" + target + "/**/*");
                if (cleanedFiles.length > 0) {
                    LogIt.Info(">>"[ColorType.RED] + " " + cleanedFiles.length + " files cleaned at:");
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " 0 files cleaned. All " + files.length + " monitored files are clean.");
                }
                for (index = 0; index < files.length; index++) {
                    filePath = files[index];
                    if (isIgnored(filePath)) {
                        continue;
                    }
                    destinationPath = this.properties.projectBase + "/" + destinationFolder +
                        filePath.replace(this.properties.projectBase + "/" + target + "/", "");
                    if (this.fileSystem.IsFile(filePath)) {
                        if (cleanedFiles.indexOf(filePath) !== -1) {
                            LogIt.Info("   > " + destinationPath);
                            this.fileSystem.Write(destinationPath, this.fileSystem.Read(filePath));
                        }
                    } else if (destinationFolder !== "" && this.fileSystem.IsDirectory(filePath)) {
                        this.fileSystem.CreateDirectory(destinationPath);
                    }
                }
                this.fileSystem.Delete(this.properties.projectBase + "/" + target);

                if (destinationFolder === "") {
                    files = this.fileSystem.Expand(getExpandPattern());
                    for (index = 0; index < files.length; index++) {
                        filePath = files[index];
                        if (isIgnored(filePath)) {
                            continue;
                        }
                        if (this.fileSystem.Exists(filePath) && this.fileSystem.IsDirectory(filePath)) {
                            for (origPath in filePaths) {
                                if (filePaths.hasOwnProperty(origPath)) {
                                    if (filePath.indexOf(origPath) !== -1) {
                                        logIt("delete " + filePath);
                                        this.fileSystem.Delete(filePath);
                                    }
                                }
                            }
                        }
                    }
                }

                if (config.cleanuplog) {
                    this.fileSystem.Write(this.properties.projectBase + "/log/cleanup.log", cleanUpLog);
                }
                $done();
            } catch (ex) {
                LogIt.Error(ex);
            }
        }
    }

    export abstract class IProjectCleanupConfig {
        public cleanuplog : boolean;
        public verbose : boolean;
        public targetFolder : string;
        public ignore : string[];
        public readExtensions : string[];
        public filePaths : any;
        public contentReplacements : any;
    }
}
