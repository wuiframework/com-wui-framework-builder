/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Cleanup {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class SCRCleanup extends BaseTask {

        protected getName() : string {
            return "scr-cleanup";
        }

        protected process($done : any) : void {
            const os : Types.NodeJS.os = require("os");

            const scrFile : string = this.properties.projectBase + "/SW-Content-Register.txt";
            const dependenciesFiles : string[] =
                this.fileSystem.Expand(this.properties.projectBase + "/dependencies/*/SW-Content-Register.txt");

            if (this.fileSystem.Exists(scrFile)) {
                const scrData : string = this.fileSystem.Read(scrFile).toString().replace(/\n\n/gim, os.EOL);
                const scrRecords : string[] = scrData.split(os.EOL + os.EOL);
                let newScrData : string = "";
                const allDependencies : string[] = [];
                const missingDependencies : string[] = [];
                const missingRecords : IScrRecord[] = [];
                const releaseNameTag : string = "Release Name:";

                const parseRecord : any = ($data : string) : IScrRecord => {
                    const record : IScrRecord = <IScrRecord>{
                        ToString() : string {
                            return (this.optional ? "[OPTIONAL]" + os.EOL : "") +
                                this.name + (this.version !== "" ? " v" + this.version : "") + os.EOL +
                                "Description: " + this.description + os.EOL +
                                "Author: " + this.author + os.EOL +
                                "License: " + this.license + os.EOL +
                                "Format: " + this.format + os.EOL +
                                "Location: " + this.location + os.EOL;
                        },
                        author      : "",
                        description : "",
                        format      : "",
                        getHash() : string {
                            return this.name + this.version;
                        },
                        isDependency: false,
                        isPrinted   : false,
                        license     : "",
                        location    : "",
                        name        : "",
                        optional    : false,
                        version     : ""
                    };
                    if ($data.indexOf("[OPTIONAL]") !== -1) {
                        $data = $data.replace("[OPTIONAL]" + os.EOL, "");
                        record.optional = true;
                    }
                    const lines : string[] = $data.split(os.EOL);
                    if (lines[0].indexOf(" v") !== -1) {
                        const firstLine : string[] = lines[0].split(" v");
                        record.name = firstLine[0];
                        record.version = firstLine[1];
                    } else {
                        record.name = lines[0];
                    }
                    let index : number;
                    let isLocation : boolean = false;
                    for (index = 1; index < lines.length; index++) {
                        const line : string[] = lines[index].split(": ");
                        switch (line[0]) {
                        case "Description":
                            record.description = line[1];
                            break;
                        case "Author":
                            record.author = line[1];
                            break;
                        case "License":
                            record.license = line[1];
                            break;
                        case "Format":
                            record.format = line[1];
                            break;
                        case "Location":
                            record.location = line[1];
                            isLocation = true;
                            break;
                        default:
                            if (isLocation && !StringUtils.StartsWith(lines[index], os.EOL)) {
                                if (lines[index].replace(/[\r\n]/gim, "").trim() !== "") {
                                    record.location += os.EOL + lines[index];
                                }
                            } else {
                                isLocation = false;
                            }
                            break;
                        }
                    }
                    return record;
                };
                const parseSCR : any = ($data : string) : void => {
                    const release : string =
                        $data.substring($data.indexOf(releaseNameTag) + releaseNameTag.length, $data.indexOf(" v")).trim();
                    const versionTag : string = releaseNameTag + " " + release + " v";
                    const version : string =
                        $data.substring($data.indexOf(versionTag) + versionTag.length, $data.indexOf(os.EOL)).trim();

                    const records : string[] = $data.split(os.EOL + os.EOL);
                    records.forEach(($record : string) : void => {
                        if ($record.replace(/[\r\n]/gim, "").trim() !== "") {
                            if ($record.indexOf(releaseNameTag) === -1 && $record.indexOf(this.project.name) === -1) {
                                const record : IScrRecord = parseRecord($record);
                                record.isDependency = true;
                                if (record.name === release) {
                                    record.version = version;
                                }
                                if (allDependencies.indexOf(record.getHash()) === -1) {
                                    allDependencies.push(record.getHash());
                                }
                                if (scrData.indexOf(record.ToString()) === -1 &&
                                    missingDependencies.indexOf(record.getHash()) === -1) {
                                    missingDependencies.push(record.getHash());
                                    missingRecords.push(record);
                                }
                            }
                        }
                    });
                };
                dependenciesFiles.forEach(($file : string) : void => {
                    parseSCR(this.fileSystem.Read($file).toString());
                });

                scrRecords.forEach(($record : string) : void => {
                    if ($record.replace(/[\r\n]/gim, "").trim() !== "") {
                        if ($record.indexOf(releaseNameTag) === -1) {
                            const record : IScrRecord = parseRecord($record);
                            missingRecords.forEach(($dependency : IScrRecord) : void => {
                                if (record.name === $dependency.name && !$dependency.isPrinted) {
                                    record.optional = $dependency.optional;
                                    record.version = $dependency.version;
                                    record.description = $dependency.description;
                                    record.author = $dependency.author;
                                    record.license = $dependency.license;
                                    record.format = $dependency.format;
                                    record.location = $dependency.location;
                                    $dependency.isPrinted = true;
                                }
                            });
                            if (!record.isDependency ||
                                record.isDependency && allDependencies.indexOf(record.getHash()) !== -1) {
                                newScrData += record.ToString() + os.EOL;
                            }
                        } else {
                            newScrData += $record + os.EOL + os.EOL;
                        }
                    }
                });
                missingRecords.forEach(($dependency : IScrRecord) : void => {
                    if (!$dependency.isPrinted) {
                        $dependency.isPrinted = true;
                        newScrData += $dependency.ToString() + os.EOL;
                    }
                });
                if ((<any>newScrData).endsWith(os.EOL + os.EOL)) {
                    newScrData = newScrData.substring(0, newScrData.lastIndexOf(os.EOL));
                }

                if (scrData !== newScrData) {
                    this.fileSystem.Write(scrFile, newScrData);
                    LogIt.Info(">>"[ColorType.RED] + " SW-Content-Register.txt has been cleaned.");
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " SW-Content-Register.txt is up to date.");
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Clean up of SW-Content-Register.txt skipped: file does not exist");
            }
            $done();
        }
    }

    export abstract class IScrRecord {
        public optional : boolean;
        public name : string;
        public version : string;
        public description : string;
        public author : string;
        public license : string;
        public format : string;
        public location : string;
        public isPrinted : boolean;
        public isDependency : boolean;
        public getHash : () => string;
        public "ToString" : () => string;
    }
}
