/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.General {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import GeneralTaskType = Com.Wui.Framework.Builder.Enums.GeneralTaskType;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;

    export class SystemInfo extends BaseTask {

        protected getName() : string {
            return GeneralTaskType.GET_SYSTEM_INFO;
        }

        protected process($done : any) : void {
            const os : Types.NodeJS.os = require("os");
            const env : EnvironmentArgs = Loader.getInstance().getEnvironmentArgs();
            LogIt.Info(
                "WUI Builder " + env.getProjectVersion() + " (" + Convert.TimeToGMTformat(env.getBuildTime()) + "), " +
                os.type() + "(" + os.release() + ") " + os.arch() +
                ", " + os.cpus().length + "x " + os.cpus()[0].model +
                ", RAM: " + Math.round(os.totalmem() / (1024 * 1024 * 1024)) + " GB");
            $done();
        }
    }
}
