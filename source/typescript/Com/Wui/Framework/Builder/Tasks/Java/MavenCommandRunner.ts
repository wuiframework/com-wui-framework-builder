/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Java {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class MavenCommandRunner extends BaseTask {

        protected getName() : string {
            return "maven-command-runner";
        }

        protected process($done : any, $option : string) : void {
            let args : string[] = ["-f", this.properties.projectBase + "/compiled.pom.xml"];
            switch ($option) {
            case "clean":
                args = args.concat(["clean", "-o"]);
                break;
            case "compile":
                args = args.concat(["compile", "-Djacoco.skip=true", "-Dmaven.test.skip=true", "-Dskiptest=true"]);
                break;
            case "unit":
                const testSources : string[] = MavenEnv.getConfig().unit.src;
                if (!ObjectValidator.IsEmptyOrNull(testSources)) {
                    args = args.concat(["-Dtest=" + testSources.join(",")]);
                }
                args = args.concat([
                    "test", "-DfailIfNoTests=false", "-Djacoco.skip=true", "-Dcheckstyle.skip=true"
                ]);
                break;
            case "coverage":
                args = args.concat([
                    "test", "-DfailIfNoTests=false", "-Dfindbugs.skip=true", "-Dcheckstyle.skip=true"
                ]);
                break;
            case "lint":
                args = args.concat("compile");
                break;
            default:
                LogIt.Error("Unsupported Maven command \"" + $option + "\".");
                break;
            }
            this.terminal.Spawn("mvn", args, this.properties.projectBase, ($exitCode : number) : void => {
                if ($exitCode === 0) {
                    $done();
                } else {
                    LogIt.Error("Maven command \"" + $option + "\" has failed.");
                }
            });
        }
    }
}
