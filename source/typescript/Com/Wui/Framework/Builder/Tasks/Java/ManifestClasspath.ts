/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Java {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class ManifestClasspath extends BaseTask {

        protected getName() : string {
            return "manifest-classpath";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const isEclipseBuild : boolean = $option === "eclipse";
            const isIdeaBuild : boolean = $option === "idea";
            const libs : string[] = this.fileSystem.Expand(this.properties.projectBase + "/build/compiled/java/output/lib/*");

            let manifest : string = this.fileSystem.Read(
                this.properties.projectBase + "/bin/resource/configs/MANIFEST" + (isEclipseBuild ? ".BUNDLE" : "") + ".MF").toString();
            if (isEclipseBuild) {
                let classPathLibs : string = "Bundle-ClassPath: .";
                libs.forEach(($lib : string) => {
                    classPathLibs += "," + os.EOL + " lib/" + $lib.replace(/^.*[\\\/]/, "");
                });
                manifest = manifest.replace("Bundle-ClassPath: .", classPathLibs);
            } else if (isIdeaBuild) {
                manifest = manifest.replace("Class-Path: .", "");
            } else {
                let classPathLibs : string = "Class-Path: .";
                libs.forEach(($lib : string) => {
                    classPathLibs += " lib/" + $lib.replace(/^.*[\\\/]/, "");
                });
                manifest = manifest.replace("Class-Path: .", classPathLibs);
            }
            this.fileSystem.Write(this.properties.projectBase + "/build/compiled/java/output/META-INF/MANIFEST.MF", manifest);
            $done();
        }
    }
}
