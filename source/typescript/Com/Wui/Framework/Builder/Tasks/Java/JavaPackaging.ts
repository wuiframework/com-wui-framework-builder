/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Java {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class JavaPackaging extends BaseTask {

        protected getName() : string {
            return "java-packaging";
        }

        protected process($done : any, $option : string) : void {
            const targetPath : string = this.properties.projectBase + "/build/target";
            const packageName : string = this.project.target.name + "-" + this.project.version.replace(/\./g, "-");
            const tempPackage : string = targetPath + "/target";
            const packagePath : string = targetPath + "/" + packageName;
            this.fileSystem.Pack(tempPackage + "/*", <any>{output: packagePath + ".jar"}, () : void => {
                if ($option === "idea") {
                    this.fileSystem.Write(targetPath + "/lib/" + this.project.target.name + ".jar",
                        this.fileSystem.Read(packagePath + ".jar"));
                    this.fileSystem.Delete(packagePath + ".jar");
                    this.fileSystem.Copy(targetPath + "/lib", tempPackage + "/" + packageName + "/lib", () : void => {
                        this.fileSystem.Pack(tempPackage + "/" + packageName, <any>{output: packagePath + ".zip"},
                            () : void => {
                                this.fileSystem.Delete(targetPath + "/" + packageName);
                                this.fileSystem.Delete(targetPath + "/lib");
                                this.fileSystem.Delete(tempPackage);
                                $done();
                            });
                    });
                } else {
                    this.fileSystem.Delete(tempPackage);
                    $done();
                }
            });
        }
    }
}
