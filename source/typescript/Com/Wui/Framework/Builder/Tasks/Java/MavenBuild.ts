/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Java {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;

    export class MavenBuild extends BaseTask {

        protected getName() : string {
            return "maven-build";
        }

        protected process($done : any, $option : string) : void {
            const isEclipseBuild : boolean = this.project.target.toolchain === ToolchainType.ECLIPSE;
            const isIdeaBuild : boolean = this.project.target.toolchain === ToolchainType.IDEA;
            const tasks : string[] = [];
            switch ($option) {
            case "unit":
                tasks.push("maven-command-runner:unit");
                break;
            case "coverage":
                tasks.push("maven-command-runner:coverage");
                break;
            case "lint":
                tasks.push("maven-command-runner:lint");
                break;
            default:
                tasks.push("maven-command-runner:compile");
                tasks.push("config-compile");
                if (isEclipseBuild || isIdeaBuild) {
                    if (isEclipseBuild) {
                        tasks.push("plugin-configs:eclipse");
                        tasks.push("manifest-classpath:eclipse");
                        tasks.push("init-target:eclipse");
                    } else if (isIdeaBuild) {
                        tasks.push("plugin-configs:idea");
                        tasks.push("manifest-classpath:idea");
                        tasks.push("init-target:idea");
                    }
                } else {
                    tasks.push("manifest-classpath");
                    tasks.push("init-target");
                }
                break;
            }
            this.runTask($done, tasks);
        }
    }
}
