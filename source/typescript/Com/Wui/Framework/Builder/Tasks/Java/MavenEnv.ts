/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Java {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IMavenArtifact = Com.Wui.Framework.Builder.Interfaces.IMavenArtifact;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class MavenEnv extends BaseTask {
        private static config : any;

        public static getConfig() : any {
            if (ObjectValidator.IsEmptyOrNull(MavenEnv.config)) {
                MavenEnv.config = {
                    unit: {
                        src: []
                    }
                };
            }
            return MavenEnv.config;
        }

        protected getName() : string {
            return "maven-env";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            this.fileSystem.Delete(this.properties.projectBase + "/compiled.pom.xml");
            const isEclipseBuild : boolean = this.project.target.toolchain === ToolchainType.ECLIPSE;
            const isIdeaBuild : boolean = this.project.target.toolchain === ToolchainType.IDEA;
            if ($option === "init") {
                let pomFile : string = this.fileSystem.Read(this.properties.projectBase + "/bin/resource/scripts/pom.xml").toString();
                pomFile = pomFile.replace(/<!-- @parent -->/g, "javalibrary.config");
                const pomPath = this.properties.projectBase + "/compiled.pom.xml";
                this.fileSystem.Write(pomPath, pomFile);

                let excludedPackages : string = "";
                if (isEclipseBuild) {
                    excludedPackages += os.EOL +
                        "                            <exclude>**/Idea/**</exclude>";
                } else if (isIdeaBuild) {
                    excludedPackages += os.EOL +
                        "                            <exclude>**/Eclipse/**</exclude>";
                }
                const libPath : string = this.properties.projectBase + "/bin/resource/configs/javalibrary.config.xml";
                this.fileSystem.Write(libPath, this.fileSystem.Read(libPath).toString().replace("<!-- @excludes -->", excludedPackages));

                const prepareEclipseLibs : any = ($done : any) : void => {
                    if (isEclipseBuild && this.fileSystem.IsEmpty(this.properties.projectBase + "/build_cache/runtime_libs_eclipse")) {
                        this.fileSystem.Copy(
                            this.externalModules + "/eclipse/plugins",
                            this.properties.projectBase + "/build_cache/runtime_libs_eclipse",
                            $done);
                    } else {
                        $done();
                    }
                };
                const prepareIdeaLibs : any = ($done : any) : void => {
                    if (isIdeaBuild && this.fileSystem.IsEmpty(this.properties.projectBase + "/build_cache/runtime_libs_idea")) {
                        this.fileSystem.Copy(
                            this.externalModules + "/idea/lib",
                            this.properties.projectBase + "/build_cache/runtime_libs_idea",
                            $done);
                    } else {
                        $done();
                    }
                };
                prepareEclipseLibs(() : void => {
                    prepareIdeaLibs(() : void => {
                        if (this.fileSystem.IsEmpty(this.properties.projectBase + "/build_cache/test")) {
                            const testRunnerSource : string = this.properties.projectBase + "/bin/resource/scripts/";
                            const testRunnerTarget : string = this.properties.projectBase + "/build_cache/test/Com/Wui/Framework/";
                            this.fileSystem.Write(testRunnerTarget + "UnitTestRunner.java",
                                this.fileSystem.Read(testRunnerSource + "UnitTestRunner.java"));
                            this.fileSystem.Write(testRunnerTarget + "UnitTestListener.java",
                                this.fileSystem.Read(testRunnerSource + "UnitTestListener.java"));
                            this.fileSystem.Write(testRunnerTarget + "Assert.java",
                                this.fileSystem.Read(testRunnerSource + "Assert.java"));
                            this.fileSystem.Write(testRunnerTarget + "UnitTest.java",
                                this.fileSystem.Read(testRunnerSource + "UnitTest.java"));
                        }

                        if (this.fileSystem.IsEmpty(this.properties.projectBase + "/build_cache/lib")) {
                            const installLocalJar : any = ($index : number) : void => {
                                if ($index < this.properties.mavenArtifacts.length) {
                                    const artifact : IMavenArtifact = this.properties.mavenArtifacts[$index];
                                    if (artifact.location.indexOf("http://") === -1) {
                                        this.terminal.Spawn(
                                            "mvn", [
                                                "-f", pomPath,
                                                "install:install-file",
                                                "-Dfile=" + artifact.location.replace("file:///", "").replace("file://", ""),
                                                "-DgroupId=" + artifact.groupId,
                                                "-DartifactId=" + artifact.artifactId,
                                                "-Dversion=" + artifact.version,
                                                "-Dpackaging=jar"
                                            ], this.properties.projectBase, ($exitCode : number) : void => {
                                                if ($exitCode === 0) {
                                                    installLocalJar($index + 1);
                                                } else {
                                                    LogIt.Error("Jar install has failed.");
                                                }
                                            });
                                    } else {
                                        installLocalJar($index + 1);
                                    }
                                } else {
                                    $done();
                                }
                            };
                            installLocalJar(0);
                        } else {
                            $done();
                        }
                    });
                });
            } else {
                $done();
            }
        }
    }
}
