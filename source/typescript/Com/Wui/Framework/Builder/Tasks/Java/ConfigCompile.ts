/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Java {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class ConfigCompile extends BaseTask {

        protected getName() : string {
            return "config-compile";
        }

        protected process($done : any) : void {
            const configPath : string = this.properties.projectBase + "/resource/configs/default.config.jsonp";
            if (this.fileSystem.Exists(configPath)) {
                let config : string = this.fileSystem.Read(configPath).toString();
                config = config.substring(config.indexOf("({") + 1,
                    config.lastIndexOf("});") + 1);
                /* tslint:disable: no-eval */
                const parsedConfig : any = eval("(" + config + ")");
                /* tslint:enable */

                const compileToArray : any = ($object : any) : any[] => {
                    const array : any[] = [];
                    for (const property in $object) {
                        if ($object.hasOwnProperty(property)) {
                            const object = {
                                icon : $object[property].icon,
                                id   : property,
                                label: $object[property].label
                            };
                            array.push(object);
                        }
                    }
                    return array;
                };
                parsedConfig.views = compileToArray(parsedConfig.views);
                parsedConfig.menus = compileToArray(parsedConfig.menus);
                parsedConfig.tools = compileToArray(parsedConfig.tools);
                this.fileSystem.Write(this.properties.projectBase + "/build/compiled/java/output/resource/configs/compiled.config.json",
                    JSON.stringify(parsedConfig));
            } else {
                LogIt.Warning("config-compile tak skipped: default config does not exist");
            }
            $done();
        }
    }
}
