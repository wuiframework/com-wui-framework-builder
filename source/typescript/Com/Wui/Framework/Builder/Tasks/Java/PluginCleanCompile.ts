/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Java {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class PluginCleanCompile extends BaseTask {

        protected getName() : string {
            return "plugin-clean-compile";
        }

        protected process($done : any) : void {
            const outputPath : string = this.properties.projectBase + "/build/compiled/java/output";
            this.fileSystem.Delete(outputPath + "/lib");
            this.fileSystem.Delete(outputPath + "/plugin.xml");
            this.fileSystem.Delete(outputPath + "/META-INF/MANIFEST.MF");
            this.fileSystem.Delete(outputPath + "/META-INF/plugin.xml");
            $done();
        }
    }
}
