/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Java {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class InitTarget extends BaseTask {

        protected getName() : string {
            return "init-target";
        }

        protected process($done : any, $option : string) : void {
            const baseTarget : string = this.properties.projectBase + "/build/target";
            const tempPackage : string = baseTarget + "/target";

            this.fileSystem.Copy(this.properties.projectBase + "/build/compiled/java/output", tempPackage, () : void => {
                if ($option === "idea") {
                    this.fileSystem.Copy(tempPackage + "/lib", baseTarget + "/lib", () : void => {
                        this.fileSystem.Delete(tempPackage + "/lib");
                        $done();
                    });
                } else {
                    $done();
                }
            });
        }
    }
}
