/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.JavaScript {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ResourceHandler = Com.Wui.Framework.Builder.Utils.ResourceHandler;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;
    import ProductType = Com.Wui.Framework.Builder.Enums.ProductType;
    import ArchType = Com.Wui.Framework.Builder.Enums.ArchType;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;
    import TerminalOptions = Com.Wui.Framework.Services.Connectors.TerminalOptions;

    export class GypCompile extends BaseTask {

        protected getName() : string {
            return "gyp-compile";
        }

        protected process($done : any) : void {
            let cwd : string = this.project.target.toolchainSettings.cwd;
            if (ObjectValidator.IsEmptyOrNull(cwd)) {
                cwd = this.properties.projectBase;
            } else if (!StringUtils.StartsWith(cwd, this.properties.projectBase)) {
                cwd = this.properties.projectBase + "/" + cwd;
            }
            if (this.fileSystem.Exists(cwd + "/node.gyp")) {
                const product : BuildProductArgs = this.build.product;
                const isStatic : boolean = product.Type() === ProductType.STATIC;
                const is64bit : boolean = product.Arch() === ArchType.X64;
                let buildPath : string = "Release";
                if (!EnvironmentHelper.IsWindows()) {
                    buildPath = "out/" + buildPath;
                }
                const productExits : boolean = !this.fileSystem.IsEmpty(cwd + "/" + buildPath);

                const configure : any = ($callback : () => void) : void => {
                    let cmd : string;
                    const args : string[] = [];
                    const options : TerminalOptions = <TerminalOptions>{cwd};
                    if (EnvironmentHelper.IsWindows()) {
                        cmd = "vcbuild.bat";
                        args.push(
                            "release",
                            "projgen",
                            "clean",
                            "nobuild",
                            "no-cctest",
                            isStatic ? "static" : "dll",
                            is64bit ? "x64" : "x86");
                    } else {
                        cmd = "./configure";
                        args.push(
                            "--without-npm",
                            isStatic ? "--enable-static" : "--shared"
                        );
                        if (product.OS() === OSType.MAC) {
                            cmd = "CC=clang CXX=clang++ " + cmd;
                            args.push("--openssl-no-asm");
                        }
                        if (product.OS() === OSType.IMX) {
                            args.push(
                                "--dest-cpu=arm64",
                                "--dest-os=linux",
                                "--cross-compiling",
                                "--with-arm-float-abi=hard",
                                "--openssl-no-asm"  // build with asm causes errors with -fPIC somewhere in openssl, hard to identify in gyp
                            );
                            options.env = process.env;
                            options.env.CC = "/usr/bin/aarch64-linux-gnu-gcc-8";
                            options.env.CXX = "/usr/bin/aarch64-linux-gnu-g++-8";
                            options.env.LINK = "/usr/bin/aarch64-linux-gnu-g++-8";
                            options.env.RANLIB = "/usr/bin/aarch64-linux-gnu-ranlib";
                            options.env.AR = "/usr/bin/aarch64-linux-gnu-ar";

                            options.env.CC_host = "gcc";
                            options.env.CXX_host = "g++";
                            options.env.LINK_host = "g++";
                            options.env.RANLIB_host = "ranlib";
                            options.env.AR_host = "ar";
                        }
                        if (this.build.isRebuild && productExits) {
                            cmd = "make clean && " + cmd;
                        }
                    }

                    this.terminal.Spawn(cmd, args, options, ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            $callback();
                        } else {
                            LogIt.Error("Failed to configure GYP project. " + $std[1]);
                        }
                    });
                };
                const compile : any = () : void => {
                    let cmd : string;
                    const args : string[] = [];
                    if (EnvironmentHelper.IsWindows()) {
                        cmd = "vcbuild.bat";
                        args.push("release",
                            "noprojgen",
                            "no-cctest",
                            is64bit ? "x64" : "x86",
                            isStatic ? "static" : "dll");
                    } else {
                        cmd = "make -j" + EnvironmentHelper.getCores();

                        // On some configurations, even the configure with specified CC and CXX is not enough, and we have to force it here
                        if (product.OS() === OSType.MAC) {
                            cmd = "CC=clang CXX=clang++ " + cmd;
                        }
                    }
                    this.terminal.Spawn(cmd, args, cwd, ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0 && !StringUtils.ContainsIgnoreCase($std[0] + $std[1],
                            "Building Node with reused solution failed.")) {
                            $done();
                        } else {
                            LogIt.Error("Failed to compile GYP project. " + $std[1]);
                        }
                    });
                };
                if (this.build.isRebuild || !productExits) {
                    if (EnvironmentHelper.IsWindows()) {
                        const versionRc : string =
                            "0 ICON \"src/res/icon.ico\"\r\n" +
                            "\r\n" +
                            ResourceHandler.GenerateVersionRc("", this.project.target.name);
                        if (this.fileSystem.Write(cwd + "/src/res/node.rc", versionRc)) {
                            this.fileSystem.Copy(this.properties.projectBase + "/" + this.project.target.icon,
                                cwd + "/src/res/icon.ico", () : void => {
                                    configure(compile);
                                });
                        } else {
                            LogIt.Error("Failed to create version.rc file");
                        }
                    } else {
                        configure(compile);
                    }
                } else {
                    if (!EnvironmentHelper.IsWindows()) {
                        configure(compile);
                    } else {
                        compile();
                    }
                }
            } else {
                LogIt.Error("GYP project has not been detected at: " + cwd);
            }
        }
    }
}
