/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.JavaScript {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ResourceHandler = Com.Wui.Framework.Builder.Utils.ResourceHandler;
    import ResourceType = Com.Wui.Framework.Builder.Enums.ResourceType;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;

    export class NodejsPackaging extends BaseTask {

        protected getName() : string {
            return "nodejs-packaging";
        }

        protected process($done : any) : void {
            this.prepareBasePackage(() : void => {
                this.embedResources(() : void => {
                    this.createRelease($done);
                });
            });
        }

        private prepareBasePackage($callback : () => void) {
            const binExtension : string = EnvironmentHelper.IsWindows() ? ".exe" : "";
            let nodeLibName : string = "node.dll";
            if (EnvironmentHelper.IsMac()) {
                nodeLibName = "libnode.dylib";
            } else if (!EnvironmentHelper.IsWindows()) {
                nodeLibName = "libnode.so";
            }
            const nodePath : string = this.getNodePath() + "/node" + binExtension;
            const libPath : string = this.getNodePath() + "/" + nodeLibName;
            if (!this.fileSystem.Exists(nodePath)) {
                LogIt.Error("Node.js instance has not been found at: " + nodePath);
            }
            const pkgPath : string = this.properties.projectBase + "/build_cache/NodejsRE/" + this.build.product.OS()
                + "/" + this.project.target.name + binExtension;
            if (!this.fileSystem.Exists(pkgPath)) {
                this.fileSystem.Copy(nodePath, pkgPath, () : void => {
                    if (this.fileSystem.Exists(libPath)) {
                        this.fileSystem.Copy(libPath, this.properties.projectBase + "/build_cache/NodejsRE/" +
                            this.build.product.OS() + "/" + nodeLibName, () : void => {
                            ResourceHandler.ModifyIcon(pkgPath, this.properties.projectBase + "/" + this.project.target.icon, () : void => {
                                ResourceHandler.ModifyVersionInfo(pkgPath, this.project.target.name, $callback);
                            });
                        });
                    } else {
                        LogIt.Warning("Using static build of NodejsRE is deprecated.");
                        ResourceHandler.ModifyIcon(pkgPath, this.properties.projectBase + "/" + this.project.target.icon, () : void => {
                            ResourceHandler.ModifyVersionInfo(pkgPath, this.project.target.name, $callback);
                        });
                    }
                });
            } else {
                $callback();
            }
        }

        private embedResources($callback : () => void) : void {
            const dependencyTree : any = require("dependency-tree");
            const vm : Types.NodeJS.vm = require("vm");
            const fs : Types.NodeJS.fs = require("fs");

            const extension : string = EnvironmentHelper.IsWindows() ? ".exe" : "";
            const pkgPath : string = this.properties.projectBase + "/build_cache/NodejsRE/" + this.build.product.OS()
                + "/" + this.project.target.name + extension;
            const pkgConfigPath : string = this.getNodeBuildPath() + "/package.json";
            const pkgConfig : any = JSON.parse(this.fileSystem.Read(pkgConfigPath).toString()).pkg;
            const nodeModules : string = this.fileSystem.NormalizePath(this.getNodeBuildPath() + "/node_modules");
            const mainFile : string =
                this.properties.projectBase + "/build/target/resource/javascript/" + this.properties.packageName + ".min.js";

            if (ObjectValidator.IsEmptyOrNull(pkgConfig.scripts)) {
                pkgConfig.scripts = [];
            }
            if (ObjectValidator.IsEmptyOrNull(pkgConfig.assets)) {
                pkgConfig.assets = [];
            }
            const patchesScripts : any[] = [];
            if (!ObjectValidator.IsEmptyOrNull(pkgConfig.patches)) {
                const patches = {};
                pkgConfig.patches.forEach(($patch : any) : void => {
                    let patchRoot : string = nodeModules;
                    if (!StringUtils.IsEmpty($patch.cwd)) {
                        patchRoot += "/" + $patch.cwd;
                    }
                    if (!StringUtils.IsEmpty($patch.src) && !StringUtils.IsEmpty($patch.dest)) {
                        patches[patchRoot + "/" + $patch.dest] = $patch;
                    }
                    if (!StringUtils.IsEmpty($patch.script)) {
                        if (StringUtils.IsEmpty($patch.cwd)) {
                            $patch.cwd = nodeModules;
                        }
                        if (ObjectValidator.IsEmptyOrNull($patch.attributes)) {
                            $patch.attributes = [];
                        }
                        patchesScripts.push($patch);
                    }
                });
                pkgConfig.patches = patches;
            } else {
                pkgConfig.patches = {};
            }
            const scripts : string[] = [mainFile];
            const assets : string[] = this.fileSystem.Expand(nodeModules + "/**/package.json");
            assets.push(pkgConfigPath);
            if (this.fileSystem.Exists(mainFile + ".map")) {
                scripts.push(nodeModules + "/source-map-support/source-map-support.js");
                assets.push(mainFile + ".map");
            }
            pkgConfig.scripts.forEach(($scriptPattern : string) : void => {
                this.fileSystem.Expand(nodeModules + "/" + $scriptPattern).forEach(($script : string) : void => {
                    if (scripts.indexOf($script) === -1) {
                        scripts.push($script);
                    }
                });
            });
            const aliases : any[] = [];
            pkgConfig.assets.forEach(($assetPattern : string) : void => {
                this.fileSystem.Expand([
                    this.properties.projectBase + "/build/target/" + $assetPattern,
                    nodeModules + "/" + $assetPattern
                ]).forEach(($asset : string) : void => {
                    if (this.fileSystem.IsFile($asset)) {
                        if (StringUtils.EndsWith($asset, ".ico")) {
                            aliases.push({
                                file: $asset,
                                res : $asset + "_res"
                            });
                            $asset += "_res";
                        } else if (!StringUtils.Contains($asset, ".")) {
                            aliases.push({
                                file: $asset,
                                res : $asset + ".bin_res"
                            });
                            $asset += ".bin_res";
                        }
                        if (assets.indexOf($asset) === -1) {
                            assets.push($asset);
                        }
                    }
                });
            });
            const rollbackAliasName : any = ($path : string) : string => {
                if (StringUtils.EndsWith($path, ".ico_res")) {
                    $path = StringUtils.Replace($path, ".ico_res", ".ico");
                } else if (StringUtils.EndsWith($path, ".bin_res")) {
                    $path = StringUtils.Replace($path, ".bin_res", "");
                }
                return $path;
            };
            const processAliases : any = ($revert : boolean = false) : void => {
                aliases.forEach(($alias : any) : void => {
                    if (!$revert) {
                        this.fileSystem.Rename($alias.file, $alias.res);
                    } else {
                        this.fileSystem.Rename($alias.res, $alias.file);
                    }
                });
            };
            const packagedMapPath : string = this.properties.projectBase + "/build_cache/NodejsRE/" +
                this.build.product.OS() + "/packedMap.json";
            let packageMap : any = {};
            let isUpdate : boolean = false;
            if (this.fileSystem.Exists(packagedMapPath)) {
                isUpdate = true;
                packageMap = JSON.parse(this.fileSystem.Read(packagedMapPath).toString());
            }

            const resources : string[] = [];
            if (!isUpdate) {
                LogIt.Debug("> scanning dependencies ...");
                const nonExistent : string[] = [];
                scripts.forEach(($file : string) : void => {
                    LogIt.Debug("> based on script: {0}", $file);
                    dependencyTree.toList({
                        directory: nodeModules,
                        filename : $file,
                        nonExistent
                    }).forEach(($resource : string) : void => {
                        if (resources.indexOf($resource) === -1) {
                            resources.push($resource);
                        }
                    });
                });
                if (!ObjectValidator.IsEmptyOrNull(nonExistent)) {
                    LogIt.Debug("WARNING: Can't resolve dependencies: {0}", nonExistent);
                }
            }
            assets.forEach(($asset : string) : void => {
                if (resources.indexOf($asset) === -1) {
                    resources.push($asset);
                }
            });
            const embedMap : any[] = [];
            const registerEmbedRecord : any = ($type : ResourceType, $name : string, $source : string,
                                               $isUpdate : boolean = false) : void => {
                $source = this.fileSystem.NormalizePath($source);
                embedMap.push({type: $type, name: $name, source: $source, isUpdate: $isUpdate});
            };
            const finishEmbedding : any = () : void => {
                processAliases(true);
                LogIt.Info(">>"[ColorType.YELLOW] + " " + embedMap.length + " files have been embedded.");
                $callback();
            };

            const processNextPatch : any = ($index : number) : void => {
                if ($index < patchesScripts.length) {
                    const patch : any = patchesScripts[$index];
                    const sandbox : any = {
                        /* tslint:disable: object-literal-sort-keys */
                        process,
                        require,
                        console,
                        Com,
                        Process($cwd : string, $args : string[], $done : () => void) : void {
                            LogIt.Warning("Patch skipped: script did not override global Process method");
                            $done();
                        }
                        /* tslint:enable */
                    };
                    vm.createContext(sandbox);
                    const scriptName : string = this.findPatch(patch.script);
                    vm.runInContext(<string>this.fileSystem.Read(scriptName), sandbox, scriptName);
                    const done : any = () : void => {
                        processNextPatch($index + 1);
                    };
                    try {
                        sandbox.Process(patch.cwd, patch.attributes, done);
                    } catch (ex) {
                        LogIt.Error("Patch script has failed.", ex);
                    }
                } else {
                    this.embed(pkgPath, embedMap, finishEmbedding);
                }
            };

            const snapshotPath : string = "/snapshot";
            const resourcesMap : string[] = [
                snapshotPath + "/",
                snapshotPath + "/node_modules/",
                snapshotPath + "/resource/"
            ];
            packageMap[snapshotPath] = {
                name : null,
                stats: null
            };
            packageMap[snapshotPath + "/node_modules"] = {
                name : null,
                stats: null
            };
            packageMap[snapshotPath + "/resource"] = {
                name : null,
                stats: this.serializeStats(fs.lstatSync(this.properties.projectBase + "/build/target/resource"))
            };
            resources.forEach(($file : string) : void => {
                $file = this.fileSystem.NormalizePath($file);
                let module : string;
                if ($file === pkgConfigPath) {
                    module = snapshotPath + "/package.json";
                } else if (pkgConfig.patches.hasOwnProperty($file)) {
                    const patch : any = pkgConfig.patches[$file];
                    LogIt.Debug("Patch {0} by {1}", $file, patch.src);
                    $file = this.properties.projectBase + "/" + patch.src;
                    module = snapshotPath + "/node_modules/" + patch.dest;
                } else {
                    module = StringUtils.Replace($file, nodeModules + "/", snapshotPath + "/node_modules/");
                    module = StringUtils.Replace(module, this.properties.projectBase + "/build/target/", snapshotPath + "/");
                    module = rollbackAliasName(module);
                }
                const record : any = {
                    name : "RES_" + StringUtils.ToUpperCase(StringUtils.getSha1($file)),
                    stats: null
                };
                const exists : boolean = packageMap.hasOwnProperty(module);
                const isModule : boolean = StringUtils.Contains(module, "node_modules");
                if (!exists || !isModule) {
                    registerEmbedRecord(ResourceType.RESOURCE, record.name, $file, exists && isUpdate);
                }
                record.stats = this.serializeStats(fs.lstatSync(rollbackAliasName($file)));
                if (StringUtils.StartsWith(module, snapshotPath)) {
                    let modulePath : string = "/";
                    StringUtils.Split(StringUtils.Substring(module, 1, StringUtils.IndexOf(module, "/", false)), "/")
                        .forEach(($path : string) : void => {
                            modulePath += $path;
                            if (resourcesMap.indexOf(modulePath) === -1) {
                                let localPath : string = modulePath + "/";
                                if (StringUtils.Contains(localPath, "/node_modules/")) {
                                    localPath = StringUtils.Replace(localPath, snapshotPath + "/node_modules/", nodeModules + "/");
                                } else {
                                    localPath = StringUtils.Replace(localPath,
                                        snapshotPath + "/", this.properties.projectBase + "/build/target/");
                                }
                                packageMap[modulePath] = {
                                    name : null,
                                    stats: this.serializeStats(fs.lstatSync(localPath))
                                };
                            }
                            modulePath += "/";
                        });
                }
                packageMap[module] = record;
            });

            processAliases();
            if (this.fileSystem.Write(packagedMapPath, JSON.stringify(packageMap, null, 2))) {
                registerEmbedRecord(ResourceType.CONFIG, "PACKAGE_MAP", packagedMapPath, isUpdate);
            } else {
                LogIt.Error("Failed to create map of embedded resources.");
            }
            if (!isUpdate) {
                processNextPatch(0);
            } else {
                this.embed(pkgPath, embedMap, finishEmbedding);
            }
        }

        private findPatch($scriptName : string) : string {
            const getPath : any = ($pattern : string) : string => {
                $pattern = this.fileSystem.NormalizePath($pattern);
                const fileList : string[] = this.fileSystem.Expand([$pattern]);
                if (fileList.length > 0) {
                    return fileList[0];
                } else {
                    return "";
                }
            };
            // todo use DependenciesInstall find script method instead
            const availablePaths : string[] = [
                this.properties.projectBase + "/" + $scriptName,
                this.properties.projectBase + "/bin/resource/scripts/" + $scriptName,
                this.properties.projectBase + "/resource/scripts/" + $scriptName,
                this.properties.projectBase + "/dependencies/*/resource/scripts/" + $scriptName
            ];
            let path : string = "";
            let index : number;
            for (index = 0; index < availablePaths.length; index++) {
                if (ObjectValidator.IsEmptyOrNull(path)) {
                    path = getPath(availablePaths[index]);
                } else {
                    break;
                }
            }
            return path;
        }

        private serializeStats($stats : any) : string {
            const statsRecord : any = {};
            if (!$stats.isFile()) {
                statsRecord.isFile = false;
            }
            if ($stats.isDirectory()) {
                statsRecord.isDirectory = true;
            }
            if ($stats.isBlockDevice()) {
                statsRecord.isBlockDevice = true;
            }
            if ($stats.isCharacterDevice()) {
                statsRecord.isCharacterDevice = true;
            }
            if ($stats.isSymbolicLink()) {
                statsRecord.isSymbolicLink = true;
            }
            if ($stats.isFIFO()) {
                statsRecord.isFIFO = true;
            }
            if ($stats.isSocket()) {
                statsRecord.isSocket = true;
            }
            statsRecord.dev = $stats.dev;
            statsRecord.mode = $stats.mode;
            if ($stats.nlink !== 1) {
                statsRecord.nlink = $stats.nlink;
            }
            if ($stats.uid !== 0) {
                statsRecord.uid = $stats.uid;
            }
            if ($stats.gid !== 0) {
                statsRecord.gid = $stats.gid;
            }
            if ($stats.rdev !== 0) {
                statsRecord.rdev = $stats.rdev;
            }
            statsRecord.ino = $stats.ino;
            statsRecord.size = $stats.size;
            statsRecord.atimeMs = $stats.atimeMs;
            statsRecord.mtimeMs = $stats.mtimeMs;
            statsRecord.ctimeMs = $stats.ctimeMs;
            statsRecord.atime = $stats.atime;
            statsRecord.mtime = $stats.mtime;
            statsRecord.ctime = $stats.ctime;
            statsRecord.birthtime = $stats.birthtime;
            return JSON.stringify(statsRecord);
        }

        private createRelease($callback : () => void) : void {
            const targetPath : string = this.properties.projectBase + "/build/target";
            const cleanTarget : string = this.properties.projectBase + "/build/cleantarget";

            let targetName : string = this.project.target.name;
            if (EnvironmentHelper.IsWindows()) {
                targetName += ".exe";
            }
            this.terminal.Kill(targetPath + "/" + targetName, ($status : boolean) : void => {
                if ($status || EnvironmentHelper.IsLinux()) {
                    const pkgConfig : any = JSON.parse(this.fileSystem.Read(this.getNodeBuildPath() + "/package.json").toString()).pkg;
                    let assets : string[] = [
                        targetPath + "/localStorage",
                        targetPath + "/log",
                        targetPath + "/**/connector.config.jsonp",
                        targetPath + "/resource/javascript/" + this.properties.packageName + ".min.js"
                    ];
                    if (!ObjectValidator.IsEmptyOrNull(pkgConfig.assets)) {
                        pkgConfig.assets.forEach(($asset : string) : void => {
                            $asset = targetPath + "/" + $asset;
                            if (assets.indexOf($asset) === -1) {
                                assets.push($asset);
                            }
                        });
                    }
                    assets = this.fileSystem.Expand(assets);

                    const targetFiles : string[] = this.fileSystem.Expand(targetPath + "/**/*");
                    const processFile : any = ($index : number) : void => {
                        if ($index < targetFiles.length) {
                            const file : string = targetFiles[$index];
                            if (assets.indexOf(file) === -1 && this.fileSystem.IsFile(file)) {
                                this.fileSystem.Copy(file,
                                    cleanTarget + "/" + StringUtils.Remove(file, targetPath + "/"), () : void => {
                                        processFile($index + 1);
                                    });
                            } else {
                                processFile($index + 1);
                            }
                        } else {
                            this.fileSystem.Delete(targetPath);
                            this.fileSystem.Copy(cleanTarget, targetPath, () : void => {
                                this.fileSystem.Delete(cleanTarget);
                                this.fileSystem.Copy(
                                    this.properties.projectBase + "/build_cache/NodejsRE/" + this.build.product.OS() + "/" + targetName,
                                    targetPath + "/" + targetName,
                                    () : void => {
                                        let libName : string = "node.dll";
                                        if (this.build.product.OS() === OSType.MAC) {
                                            libName = "libnode.dylib";
                                        } else if (this.build.product.OS() !== OSType.WIN) {
                                            libName = "libnode.so";
                                        }
                                        this.fileSystem.Copy(
                                            this.properties.projectBase + "/build_cache/NodejsRE/" + this.build.product.OS() +
                                            "/" + libName,
                                            targetPath + "/" + libName,
                                            $callback);
                                    });
                            });
                        }
                    };
                    processFile(0);
                } else {
                    LogIt.Error("Unable to clean up target folder. " +
                        "Validated that \"" + targetPath + "\" is not hold by another process.");
                }
            });
        }

        private getNodePath() : string {
            let nodePath : string = this.properties.projectBase + "/dependencies/nodejs";
            if (this.fileSystem.Exists(nodePath + "/" + this.build.product.OS().toString())) {
                nodePath = nodePath + "/" + this.build.product.OS().toString();
            } else if (this.fileSystem.Exists(nodePath)) {
                LogIt.Info(">>"[ColorType.YELLOW] + " Node.js selected for default (single) prepared platform.");
            } else {
                LogIt.Error("Node.js instance has not been found at: " + nodePath);
            }
            return nodePath;
        }

        private getNodeBuildPath() : string {
            let node : string = this.getNodePath();
            if (this.fileSystem.Exists(node + "/build_" + this.build.product.OS().toString())) {
                node = node + "/build_" + this.build.product.OS().toString();
            } else {
                // fallback to old nodejs package structure
                node = node + "/build";
            }
            return node;
        }

        private embed($pkgPath : string, $resourcesMap : any[], $callback : () => void) : void {
            if (EnvironmentHelper.IsWindows()) {
                let embeddingScript : string =
                    "[FILENAMES]\n" +
                    "Exe=   \"" + $pkgPath + "\"\n" +
                    "SaveAs=\"" + $pkgPath + "\"\n" +
                    "[COMMANDS]\n";
                $resourcesMap.forEach(($resource : any) : void => {
                    const source = this.fileSystem.NormalizePath($resource.source);
                    embeddingScript += "-addoverwrite \"" + source + "\", " + $resource.type + "," + $resource.name + ",\n";
                });
                ResourceHandler.RunScript(embeddingScript, () : void => {
                    $callback();
                });
            } else {
                try {
                    const fs : Types.NodeJS.fs = require("fs");
                    const input : any = fs.readFileSync($pkgPath);
                    const output : any[] = [];

                    let inputLength : number = input.readInt32LE(input.length - 4, 4);
                    const sections : any = {};
                    if (inputLength > 0) {
                        output.push(input.slice(0, inputLength));
                        let index : number = inputLength;
                        try {
                            while (index < input.length - 4) {
                                const nameLength : number = input.readInt16LE(index, 2);
                                index += 2;
                                const section : string = input.toString("utf8", index, index + nameLength);
                                index += nameLength;
                                const contentLength : number = input.readInt32LE(index, 4);
                                index += 4;
                                sections[section] = input.slice(index, index + contentLength);
                                index += contentLength;
                            }
                        } catch (ex) {
                            LogIt.Error(ex);
                        }
                    } else {
                        inputLength = input.length;
                        output.push(input);
                    }

                    const contentOffset : any = Buffer.allocUnsafe(4);
                    contentOffset.writeInt32LE(inputLength);

                    const writeSection : any = ($name : string, $value : any) : void => {
                        const nameValue : any = Buffer.from($name);
                        const nameLength : any = Buffer.allocUnsafe(2);
                        nameLength.writeInt16LE($name.length);
                        const contentLength : any = Buffer.allocUnsafe(4);
                        contentLength.writeInt32LE($value.length);
                        output.push(nameLength);
                        output.push(nameValue);
                        output.push(contentLength);
                        output.push($value);
                    };

                    for (const resource of $resourcesMap) {
                        if (this.fileSystem.Exists(resource.source)) {
                            const resName : string = "." + resource.type + "/" + resource.name;
                            if (!resource.isUpdate || resource.isUpdate && !sections.hasOwnProperty(resName)) {
                                writeSection(resName, fs.readFileSync(resource.source));
                            } else if (resource.isUpdate && sections.hasOwnProperty(resName)) {
                                sections[resName] = fs.readFileSync(resource.source);
                            }
                        } else {
                            LogIt.Warning("Resource not found: " + resource.source);
                        }
                    }
                    let sectionName;
                    for (sectionName in sections) {
                        if (sections.hasOwnProperty(sectionName)) {
                            writeSection(sectionName, sections[sectionName]);
                        }
                    }
                    output.push(contentOffset);

                    fs.writeFileSync($pkgPath, Buffer.concat(output));
                    $callback();
                } catch (ex) {
                    LogIt.Error("Failed to embed resources.", ex);
                }
            }
        }
    }
}
