/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.JavaScript {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import Resources = Com.Wui.Framework.Builder.DAO.Resources;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class JSLint extends BaseTask {

        protected getName() : string {
            return "jshint";
        }

        protected process($done : any, $option : string) : void {
            const jshint : any = require("jshint").JSHINT;
            const config : any = {
                bin    : [
                    this.properties.projectBase + "/bin/resource/scripts/**/*.js"
                ],
                configs: [
                    this.properties.projectBase + "/" + this.properties.resources + "/data/**/*.jsonp"
                ],
                source : [
                    this.properties.projectBase + "/" + this.properties.resources + "/javascript/**/*.js",
                    this.properties.projectBase + "/xcpp_scripts/**/*.js"
                ]
            };
            if (this.programArgs.getOptions().withDependencies) {
                config.configs.push(this.properties.projectBase + "/" + this.properties.dependenciesResources + "/data/**/*.jsonp");
                config.source.push(this.properties.projectBase + "/" + this.properties.dependenciesResources + "/javascript/**/*.js");
            }
            const options : any = JSON.parse(
                this.fileSystem.Read(this.properties.binBase + "/resource/configs/jslint.conf.json").toString());
            const overridePath : string = this.properties.projectBase + "/resource/configs/jslint.conf.json";
            if (this.fileSystem.Exists(overridePath)) {
                Resources.Extend(options, JSON.parse(this.fileSystem.Read(overridePath).toString()));
            }
            if (config.hasOwnProperty($option)) {
                let filesCount : number = 0;
                const errors : any[] = [];
                this.fileSystem.Expand(config[$option]).forEach(($file : string) : void => {
                    jshint(this.fileSystem.Read($file).toString(), options, options.globals);
                    if (!ObjectValidator.IsEmptyOrNull(jshint.data().errors)) {
                        jshint.data().errors.forEach(($error : any) : void => {
                            $error.file = $file;
                            errors.push($error);
                        });
                    }
                    filesCount++;
                });
                if (!ObjectValidator.IsEmptyOrNull(errors)) {
                    errors.forEach(($error : any) : void => {
                        LogIt.Warning(StringUtils.Format("[{0}]{1}"[ColorType.RED] + " {2}:{3}:{4}\n   {5}",
                            $error.code, $error.id, $error.file, $error.line + "", $error.character + "", $error.reason));
                    });
                    LogIt.Error("Not all of JavaScript files are lint free.");
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " " + filesCount + " files lint free.");
                }
            } else {
                LogIt.Error("Unsupported jshint option \"" + $option + "\"");
            }
            $done();
        }
    }
}
