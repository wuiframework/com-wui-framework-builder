/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.JavaScript {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class JSCompress extends BaseTask {

        protected getName() : string {
            return "uglify";
        }

        protected process($done : any, $option : string) : void {
            const uglify : any = require("uglify-js");
            const os : Types.NodeJS.os = require("os");

            const configs : any = {
                /* tslint:disable: object-literal-sort-keys */
                prod  : {
                    options: {
                        compress: {
                            booleans     : true,
                            collapse_vars: true,
                            comparisons  : true,
                            conditionals : true,
                            dead_code    : true,
                            drop_console : false,
                            drop_debugger: true,
                            evaluate     : true,
                            expression   : true,
                            hoist_funs   : true,
                            hoist_vars   : false,
                            if_return    : true,
                            inline       : true,
                            join_vars    : true,
                            keep_fargs   : false,
                            keep_fnames  : false,
                            keep_infinity: false,
                            loops        : true,
                            negate_iife  : true,
                            properties   : true,
                            pure_getters : true,
                            reduce_vars  : true,
                            sequences    : true,
                            side_effects : true,
                            switches     : true,
                            unsafe       : false,
                            unsafe_comps : false,
                            unsafe_math  : false,
                            unsafe_proto : false,
                            unsafe_regexp: false,
                            unused       : false
                        },
                        ie8     : true
                    },
                    source : [this.properties.tmp + "/typescript/*.js"],
                    dest   : this.properties.dest + "/resource/javascript/" + this.properties.packageName + ".min.js"
                },
                loader: {
                    options: {
                        compress: {
                            hoist_vars: true
                        },
                        ie8     : true
                    },
                    source : ["build/compiled/resource/javascript/Loader.js"],
                    dest   : this.properties.dest + "/" + this.properties.loaderPath
                }
                /* tslint:enable */
            };
            if (ObjectValidator.IsEmptyOrNull($option)) {
                LogIt.Error("uglify task requires option specification");
            }
            if (!configs.hasOwnProperty($option)) {
                LogIt.Error("uglify config \"" + $option + "\" has not been found");
            }
            const optionConfig : any = configs[$option];
            if ($option === "loader" &&
                !this.fileSystem.Exists(this.properties.projectBase + "/build/compiled/resource/javascript/Loader.js")) {
                optionConfig.source = ["build/compiled/dependencies/**/resource/javascript/Loader.js"];
            }
            let banner : string = this.properties.licenseText;
            if (!ObjectValidator.IsEmptyOrNull(banner)) {
                banner += os.EOL;
            }
            optionConfig.source.forEach(($source : string) : void => {
                const code : any = {};
                this.fileSystem.Expand($source).forEach(($file : string) : void => {
                    code[$file] = this.fileSystem.Read($file).toString();
                });
                const result : any = uglify.minify(code, optionConfig.options);
                if (!ObjectValidator.IsEmptyOrNull(result.error)) {
                    LogIt.Error("Uglify task has failed.", result.error);
                }
                this.fileSystem.Write(this.properties.projectBase + "/" + optionConfig.dest, banner + result.code);
            });
            $done();
        }
    }
}
