/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Docs {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class Docs extends BaseTask {

        protected getName() : string {
            return "docs";
        }

        protected process($done : any) : void {
            const tasks : string[] = [];
            if (this.properties.projectHas.TypeScript.Source()) {
                tasks.push("typedoc-generate");
            }
            if (this.properties.projectHas.Cpp.Source()) {
                tasks.push("doxygen-generate:cpp");
            }
            if (this.properties.projectHas.Java.Source()) {
                tasks.push("doxygen-generate:java");
            }
            this.runTask($done, tasks.concat(["copy:license", "compress:docs"]));
        }
    }
}
