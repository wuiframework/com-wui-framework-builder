/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Docs {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class TypedocGenerate extends BaseTask {

        protected getName() : string {
            return "typedoc-generate";
        }

        protected process($done : any) : void {
            if (this.properties.projectHas.TypeScript.Source()) {
                const typedoc : any = require("typedoc");
                const app : any = new typedoc.Application({
                    /* tslint:disable: object-literal-sort-keys object-literal-key-quotes object-literal-shorthand */
                    "ignoreCompilerErrors" : true,
                    "module"               : "amd",
                    "target"               : "ES5",
                    "mode"                 : "file",
                    "sourcefile-url-prefix": StringUtils.Remove(this.project.repository.url, ".git"),
                    "gitRevision"          : StringUtils.Remove(this.project.version, "-alpha", "-beta"),
                    "hideGenerator"        : true,
                    "entryPoint"           : "Com.Wui.Framework",
                    "name"                 : this.project.docs.systemName,
                    "readme"               : this.properties.projectBase + "/README.md"
                    /* tslint:enable */
                });
                app.generateDocs(this.fileSystem.Expand([
                        this.properties.projectBase + "/source/typescript/**/*.ts",
                        this.properties.projectBase + "/dependencies/*/source/typescript/**/*.ts"
                    ]),
                    this.properties.projectBase + "/build/target/docs/typescript/");
                $done();
            } else {
                LogIt.Info("Skipping task typedoc-generate: No parsable TypeScript files were found.");
                $done();
            }
        }
    }
}
