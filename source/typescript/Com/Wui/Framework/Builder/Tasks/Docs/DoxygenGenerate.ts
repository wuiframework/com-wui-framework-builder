/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Docs {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;

    export class DoxygenGenerate extends BaseTask {

        protected getName() : string {
            return "doxygen-generate";
        }

        protected process($done : any, $option : string) : void {
            const isCpp : boolean = this.properties.projectHas.Cpp.Source() && $option === "cpp";
            const isJava : boolean = this.properties.projectHas.Java.Source() && $option === "java";
            if (isCpp || isJava) {
                const buildPath : string = isCpp ?
                    this.properties.projectBase + "/build/docs/cpp" : this.properties.projectBase + "/build/target/docs/java";
                const confPath : string = this.properties.projectBase + "/build/doxygen.conf";
                this.project.docs.htmlOutput = isCpp ? "cpp" : "java";

                this.fileSystem.Delete(buildPath);
                this.fileSystem.CreateDirectory(buildPath);
                StringReplace.File(this.properties.binBase + "/resource/configs/doxygen.conf", StringReplaceType.VARIABLES, confPath);

                this.terminal.Spawn("doxygen", [confPath], this.properties.projectBase, ($exitCode : number) : void => {
                    if ($exitCode === 0) {
                        $done();
                    } else {
                        LogIt.Error("doxygen-generate task has failed");
                    }
                });
            } else {
                LogIt.Info("Skipping task doxygen-generate: No parsable C++ or Java files were found.");
                $done();
            }
        }
    }
}
