/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.TypeScript {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Resources = Com.Wui.Framework.Builder.DAO.Resources;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;

    export class TypeScriptLint extends BaseTask {
        private static config : any;

        public static getConfig() : any {
            if (ObjectValidator.IsEmptyOrNull(TypeScriptLint.config)) {
                const properties : IProperties = Loader.getInstance().getAppProperties();
                TypeScriptLint.config = {
                    options: {
                        fix: false
                    },
                    project: [properties.sources + "/*.ts", "test/**/*.ts", "!**/*.d.ts"]
                };
                if (Loader.getInstance().getProgramArgs().getOptions().withDependencies) {
                    TypeScriptLint.config.project.push(properties.dependencies + "/*.ts");
                }
            }
            return TypeScriptLint.config;
        }

        protected getName() : string {
            return "tslint";
        }

        protected process($done : any, $option : string) : void {
            const tslint : any = require("tslint");
            const taskConfig : any = TypeScriptLint.getConfig();
            if (ObjectValidator.IsEmptyOrNull(taskConfig)) {
                LogIt.Error("tslint config is missing");
            }
            const lintConfig : any = (<any>tslint.Linter).loadConfigurationFromPath(this.properties.binBase +
                "/resource/configs/tslint.conf.json");
            const lintOptions : any = {
                fix      : false,
                force    : false,
                formatter: "prose"
            };

            if (taskConfig.hasOwnProperty("options")) {
                if (taskConfig.options.fix) {
                    lintOptions.fix = true;
                }
                if (taskConfig.options.force) {
                    lintOptions.force = true;
                }
                if (!ObjectValidator.IsEmptyOrNull(taskConfig.options.formatter)) {
                    lintOptions.formatter = taskConfig.options.formatter;
                }
                Resources.Extend(lintConfig, taskConfig.options);
            }

            let src : string[] = [];
            if ($option) {
                if (taskConfig.hasOwnProperty($option)) {
                    src = src.concat(taskConfig[$option]);
                } else {
                    LogIt.Error("tslint config \"" + $option + "\" has not been found");
                }
            } else {
                let type : string;
                for (type in taskConfig) {
                    if (taskConfig.hasOwnProperty(type) && type !== "options") {
                        src = src.concat(taskConfig[type]);
                    }
                }
            }

            const linter : any = new tslint.Linter(lintOptions);
            const files : string[] = this.fileSystem.Expand(src);
            const nextFiles = ($index : number) : void => {
                if ($index < files.length) {
                    const filepath : string = files[$index];
                    linter.lint(filepath, this.fileSystem.Read(filepath).toString(), lintConfig);
                    nextFiles($index + 1);
                } else {
                    const result : any = linter.getResult();
                    if (result.warningCount > 0 || result.failureCount > 0) {
                        LogIt.Info(result.output[ColorType.YELLOW]);
                        LogIt.Error("Not all of TypeScript files are lint free.");
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " " + files.length + " files lint free.");
                    }
                    $done();
                }
            };
            nextFiles(0);
        }
    }
}
