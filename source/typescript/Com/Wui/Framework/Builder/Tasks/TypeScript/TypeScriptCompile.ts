/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.TypeScript {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Resources = Com.Wui.Framework.Builder.DAO.Resources;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;

    export class TypeScriptCompile extends BaseTask {
        private static config : any;

        public static getConfig() : any {
            if (ObjectValidator.IsEmptyOrNull(TypeScriptCompile.config)) {
                const properties : IProperties = Loader.getInstance().getAppProperties();
                TypeScriptCompile.config = {
                    /* tslint:disable: object-literal-sort-keys */
                    source  : {
                        src      : [
                            properties.sources + "/*.ts", properties.dependencies + "/*.ts",
                            "!**/*.d.ts"
                        ],
                        dest     : properties.tmp + "/typescript/source.js",
                        reference: "source/typescript/reference.d.ts",
                        interface: "source/typescript/interfacesMap.ts",
                        options  : {}
                    },
                    unit    : {
                        src      : [
                            properties.sources + "/*.ts", properties.dependencies + "/*.ts",
                            "test/unit/**/*.ts", "bin/resource/scripts/UnitTestRunner.ts",
                            "!**/*.d.ts"
                        ],
                        dest     : "build/compiled/test/unit/typescript/source.js",
                        reference: "test/unit/typescript/reference.d.ts",
                        options  : {
                            sourceMap: true
                        }
                    },
                    selenium: {
                        src      : [
                            properties.sources + "/*.ts", properties.dependencies + "/*.ts",
                            "test/selenium/**/*.ts", "bin/resource/scripts/UnitTestRunner.ts",
                            "!**/*.d.ts"
                        ],
                        dest     : "build/compiled/test/selenium/typescript/source.js",
                        reference: "test/selenium/reference.d.ts",
                        options  : {
                            sourceMap: true
                        }
                    },
                    cache   : {
                        src      : [
                            properties.dependencies + "/*.ts",
                            "!**/*.d.ts"
                        ],
                        dest     : "build_cache/dependencies.js",
                        reference: "dependencies/*/source/typescript/reference.d.ts",
                        options  : {
                            removeComments: true,
                            sourceMap     : true,
                            declaration   : true
                        }
                    }
                    /* tslint:enable */
                };
            }
            return TypeScriptCompile.config;
        }

        protected getName() : string {
            return "typescript";
        }

        protected process($done : any, $option : string) : void {
            const ts : any = require("typescript");

            const taskConfig : any = TypeScriptCompile.getConfig();
            if (ObjectValidator.IsEmptyOrNull(taskConfig)) {
                LogIt.Error("typescript config is missing");
            }
            if (!ObjectValidator.IsEmptyOrNull($option) && !taskConfig.hasOwnProperty($option)) {
                LogIt.Error("typescript config \"" + $option + "\" has not been found");
            }

            const compile : any = ($fileNames : string[], $options : any, $callback : () => void) : void => {
                $options.target = ts.ScriptTarget[$options.target];
                $options.module = ts.ModuleKind[$options.module];
                const program : any = ts.createProgram($fileNames, $options);
                const emitResult : any = program.emit();

                const allDiagnostics : any[] = ts.getPreEmitDiagnostics(program).concat(emitResult.diagnostics);

                allDiagnostics.forEach(($diagnostic : any) : void => {
                    if ($diagnostic.file) {
                        const report : any = $diagnostic.file.getLineAndCharacterOfPosition($diagnostic.start!);
                        const message : string = ts.flattenDiagnosticMessageText($diagnostic.messageText, "");
                        LogIt.Warning(StringUtils.Format("{0} ({1},{2}): {3}",
                            $diagnostic.file.fileName, (report.line + 1).toString(), (report.character + 1).toString(), message));
                    } else {
                        LogIt.Warning(ts.flattenDiagnosticMessageText($diagnostic.messageText, ""));
                    }
                });

                const exitCode : number = emitResult.emitSkipped || allDiagnostics.length > 0 ? 1 : 0;
                if (exitCode === 0) {
                    $callback();
                } else {
                    LogIt.Error("TypeScript compile has failed.");
                }
            };

            const runTask : any = ($config : any, $callback : () => void) : void => {
                let reference : string[] = $config.reference;
                if (ObjectValidator.IsEmptyOrNull(reference)) {
                    reference = [];
                } else if (ObjectValidator.IsString(reference)) {
                    reference = [this.properties.projectBase + "/" + reference];
                }
                const files : string[] = [];
                this.fileSystem.Expand(reference)
                    .concat(this.fileSystem.Expand($config.src)).forEach(($file : string) : void => {
                    if (StringUtils.StartsWith($file, "./")) {
                        $file = StringUtils.Replace($file, "./", this.properties.projectBase + "/");
                    }
                    if (!StringUtils.StartsWith($file, this.properties.projectBase)) {
                        $file = this.properties.projectBase + "/" + $file;
                    }
                    if (files.indexOf($file) === -1) {
                        files.push($file);
                    }
                });
                const typesRoot : string = this.properties.projectBase + "/dependencies/nodejs/build/node_modules/@types";
                const types : string[] = [];
                if (!this.builderConfig.noExternalTypes) {
                    if (!this.fileSystem.Exists(typesRoot)) {
                        if (EnvironmentHelper.IsEmbedded()) {
                            types.push(this.properties.binBase + "/node_modules/@types/node");
                        } else {
                            types.push(this.properties.binBase + "/../../dependencies/nodejs/build/node_modules/@types/node");
                        }
                    } else {
                        this.fileSystem.Expand(typesRoot + "/*").forEach(($type : string) : void => {
                            if (this.fileSystem.Expand($type + "/*.d.ts").length > 0) {
                                types.push(StringUtils.Remove($type, typesRoot + "/"));
                            }
                        });
                    }
                }
                const compilerOptions : any = {
                    declaration           : false,
                    emitDecoratorMetadata : true,
                    experimentalDecorators: true,
                    module                : "AMD",
                    noImplicitUseStrict   : true,
                    pretty                : true,
                    removeComments        : false,
                    sourceMap             : false,
                    target                : "ES5",
                    typeRoots             : [typesRoot],
                    types
                };
                Resources.Extend(compilerOptions, $config.options);
                let dest : string = $config.dest;
                if (StringUtils.StartsWith(dest, "./")) {
                    dest = StringUtils.Replace(dest, "./", this.properties.projectBase + "/");
                } else {
                    dest = this.properties.projectBase + "/" + dest;
                }
                if (StringUtils.EndsWith($config.dest, ".js")) {
                    compilerOptions.outFile = dest;
                } else {
                    compilerOptions.outDir = dest;
                }
                compile(files, compilerOptions, $callback);
            };

            const tasks : string[] = [];
            let task : string;
            for (task in taskConfig) {
                if (taskConfig.hasOwnProperty(task) && (ObjectValidator.IsEmptyOrNull($option) || $option === task)) {
                    tasks.push(task);
                }
            }
            const nextTask : any = ($index : number) : void => {
                if ($index < tasks.length) {
                    LogIt.Info(">>"[ColorType.YELLOW] + " Running TypeScript compile for \"" + tasks[$index] + "\"");
                    runTask(taskConfig[tasks[$index]], () : void => {
                        nextTask($index + 1);
                    });
                } else {
                    $done();
                }
            };
            nextTask(0);
        }
    }
}
