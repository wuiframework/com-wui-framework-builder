/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.TypeScript {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class TypeScriptTest extends BaseTask {

        protected getName() : string {
            return "typescript-test";
        }

        protected process($done : any, $option : string) : void {
            const instrument : any = require("istanbul-lib-instrument");
            switch ($option) {
            case "unit":
            case "coverage":
            case "selenium":
                this.runTask($done, "unit-test-loader:" + $option);
                break;
            case "instrument":
                const inst : any = instrument.createInstrumenter({compact: false, esModules: false, produceSourceMap: true});
                const file : string = this.properties.projectBase + "/build/target/resource/javascript/" +
                    this.properties.packageName + ".min.js";
                LogIt.Info("Processing instrumentation for: " + file);

                const namespaces : string[] = [];
                const getNamespaces : ($fileName : string) => string[] = ($fileName : string) : string[] => {
                    $fileName = $fileName.substring(0, $fileName.lastIndexOf("/"))
                        .replace("./source/typescript", "")
                        .replace("./dependencies/", "");
                    const index : number = $fileName.indexOf("/typescript");
                    if (index !== -1) {
                        $fileName = $fileName.substring(index + 12);
                    }
                    if ((<any>$fileName).startsWith("/")) {
                        $fileName = $fileName.substring(1);
                    }
                    if ($fileName !== "" && namespaces.indexOf($fileName) === -1) {
                        namespaces.push($fileName);
                        return $fileName.split("/");
                    }
                    return [];
                };

                const preprocessor : any = ($data : any, $namespaces : string[]) : string => {
                    $data = $data.replace(") || this;", ") || /* istanbul ignore next */ this;");
                    const replaceAll : any = ($data : string, $oldValue : string, $newValue : string) : string => {
                        while ($data.indexOf($oldValue) !== -1) {
                            $data = $data.replace($oldValue, $newValue);
                        }
                        return $data;
                    };
                    let index : number;
                    for (index = 0; index < $namespaces.length; index++) {
                        if (index === 0) {
                            $data = replaceAll($data,
                                "})(" + $namespaces[index] + " || (" + $namespaces[index] + " = {}));",
                                "})(" + $namespaces[index] + " || /* istanbul ignore next */ (" + $namespaces[index] + " = {}));");
                        } else {
                            $data = replaceAll($data,
                                "})(" + $namespaces[index] + " = " + $namespaces[index - 1] + "." + $namespaces[index] +
                                " || (" + $namespaces[index - 1] + "." + $namespaces[index] + " = {}));",
                                "})(" + $namespaces[index] + " = " + $namespaces[index - 1] + "." + $namespaces[index] +
                                " || /* istanbul ignore next */ (" + $namespaces[index - 1] + "." + $namespaces[index] + " = {}));");
                        }
                    }
                    return $data;
                };

                let content : string = this.fileSystem.Read(file).toString();
                const files : string[] = this.fileSystem.Expand([
                    this.properties.projectBase + "/source/typescript/**/*.ts",
                    this.properties.projectBase + "/dependencies/*/source/typescript/**/*.ts"
                ]);
                files.forEach(($file : string) : void => {
                    const namespaces : string[] = getNamespaces($file);
                    if (namespaces.length > 0) {
                        content = preprocessor(content, <any>namespaces);
                    }
                });

                content = inst.instrumentSync(content, file);
                const coveradeId : string = content.substr(0, content.indexOf("\n"))
                    .replace("var ", "").replace(" = function () {", "").trim();

                this.fileSystem.Write(file, "var getCoverageDataId=function(){return \"" + coveradeId + "\";};" + content);
                $done();
                break;
            case "remap":
                const remapData : string = this.properties.projectBase + "/build/reports/remap-coverage.json";
                if (this.fileSystem.Exists(remapData)) {
                    const buildPath : string = this.properties.projectBase + "/build/reports/coverage/typescript";
                    this.fileSystem.Delete(buildPath);
                    this.fileSystem.CreateDirectory(buildPath);
                    this.terminal.Spawn(
                        "remap-istanbul", [
                            "-i", this.properties.projectBase + "/build/reports/remap-coverage.json",
                            "-t", "html",
                            "-o", this.properties.projectBase + "/build/reports/coverage/typescript",
                            "-e", "RuntimeTests"
                        ], process.env.NODE_PATH + "/.bin",
                        ($exitCode : number) : void => {
                            if ($exitCode === 0) {
                                $done();
                            } else {
                                LogIt.Warning("typescript-coverage has failed");
                            }
                        });
                } else {
                    LogIt.Info("Skipping task typescript-coverage: Source map has not been found.");
                    $done();
                }
                break;
            default:
                LogIt.Warning("Skipping task typescript-coverage: task \"" + $option + "\" not found.");
                $done();
                break;
            }
        }
    }
}
