/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.TypeScript {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ITypeScriptConfig = Com.Wui.Framework.Builder.Interfaces.ITypeScriptConfig;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;

    export class TypeScriptReference extends BaseTask {

        protected getName() : string {
            return "typescript-reference";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const conf : ITypeScriptConfig = TypeScriptCompile.getConfig();
            const startTag : string = "// generated-code-start";
            const endTag : string = "// generated-code-end";
            const generated : string[] = [];
            let referencePath : string;

            let namespacesList : string[] = [];
            let namespacesMap : any = {};
            const orderedNamesList : string[] = [];
            let classesNamesList : string[] = [];
            const classesMap : any = {};
            let recursiveDependency : string[] = [];

            const sortNumberArray : any = ($a : number, $b : number) : number => {
                return $a - $b;
            };

            const sortNamespace : any = ($namespacesList : string[]) : string[] => {
                const orderedNamespacesList : string[] = [];
                const namespaceLookup : any = ($namespace : string) : void => {
                    if (orderedNamespacesList.indexOf($namespace) === -1) {
                        $namespacesList.forEach(($subNamespace : string) : void => {
                            if ($subNamespace !== $namespace && (<any>$subNamespace).startsWith($namespace)) {
                                namespaceLookup($subNamespace);
                            }
                        });
                        orderedNamespacesList.push($namespace);
                    }
                };
                $namespacesList.forEach(($namespace : string) : void => {
                    namespaceLookup($namespace);
                });
                return orderedNamespacesList;
            };

            const getDependenciesMap : any = ($className : string) : string[] => {
                const dependenciesLookup : any = ($class : TypeScriptClassDefinition, $dependencies : string[]) : void => {
                    if ($class.extends !== "" && classesNamesList.indexOf($class.extends) !== -1 &&
                        $dependencies.indexOf(classesMap[$class.extends].name) === -1) {
                        $dependencies.push(classesMap[$class.extends].name);
                        dependenciesLookup(classesMap[$class.extends], $dependencies);
                    }
                };
                const dependencies : string[] = [];
                if (dependencies.indexOf(classesMap[$className].name) === -1) {
                    dependenciesLookup(classesMap[$className], dependencies);
                    dependencies.reverse();
                }
                return dependencies;
            };

            const collectDependencies : any = ($name : string) : void => {
                classesMap[$name].dependencies = getDependenciesMap($name);
            };

            const collectImports : any = ($name : string) : void => {
                classesMap[$name].imports.forEach(($import : string) : void => {
                    if (classesNamesList.indexOf($import) === -1) {
                        classesNamesList.forEach(($className : string) : void => {
                            if ($className !== $name && $className !== $import && $className.indexOf($import) !== -1) {
                                classesMap[$name].imports.push($className);
                            }
                        });
                    }
                });
            };

            const mergeDependencies : any = ($className : string) : void => {
                const $dependencies : string[] = [];
                const importsLookup : any = ($name : string, $dependencies : string[]) : void => {
                    if (classesNamesList.indexOf($name) !== -1) {
                        classesMap[$name].imports.forEach(($import : string) : void => {
                            if ($name !== $import && $dependencies.indexOf($import) === -1) {
                                if ($import !== $className) {
                                    $dependencies.push($import);
                                }
                                if (classesNamesList.indexOf($import) !== -1) {
                                    classesMap[$import].dependencies.forEach(($dependency : string) : void => {
                                        if ($import !== $dependency && $dependencies.indexOf($dependency) === -1) {
                                            if ($dependency !== $className) {
                                                $dependencies.push($dependency);
                                            }
                                            importsLookup($dependency, $dependencies);
                                        }
                                    });
                                }
                                importsLookup($import, $dependencies);
                            }
                        });
                    }
                };
                classesMap[$className].dependencies.forEach(($dependency : string) : void => {
                    if ($className !== $dependency && $dependencies.indexOf($dependency) === -1) {
                        $dependencies.push($dependency);
                        importsLookup($dependency, $dependencies);
                    }
                });
                importsLookup(classesMap[$className].name, $dependencies);
                classesMap[$className].dependencies = $dependencies;
            };

            const getRequiredByMap : any = ($name : string) : void => {
                const classObject : TypeScriptClassDefinition = classesMap[$name];
                classesNamesList.forEach(($name : string) : void => {
                    if (classesMap[$name].dependencies.indexOf(classObject.name) !== -1) {
                        classObject.requiredBy.push($name);
                    }
                });
            };

            const getSubclassDependencies : any = ($namespace : string) : void => {
                const classes : string[] = namespacesMap[$namespace];
                if (classes.length > 0) {
                    const subNamespace : string[] = [];
                    namespacesList.forEach(($name : string) : void => {
                        if ((<any>$name).startsWith($namespace + ".")) {
                            subNamespace.push($name.replace($namespace + ".", ""));
                        }
                    });

                    const dependenciesLookup : any = ($classes : string[], $className : string, $namespace : string) : void => {
                        $classes.forEach(($name : string) : void => {
                            if ($className !== $name &&
                                classesMap[$name].requiredBy.indexOf($className) === -1) {
                                if ((
                                        classesMap[$className].data.indexOf($name + "\"") === -1 && (
                                            classesMap[$className].data.indexOf(" " + $name) !== -1
                                        )
                                    ) ||
                                    (
                                        classesMap[$className].data.indexOf($namespace + classesMap[$name].className + "\"") === -1 && (
                                            classesMap[$className].data.indexOf(" " + $namespace + classesMap[$name].className) !== -1 ||
                                            classesMap[$className].data.indexOf("." + classesMap[$name].className) !== -1
                                        )
                                    )) {
                                    classesMap[$name].requiredBy.push($className);
                                }
                            }
                        });
                    };

                    classes.forEach(($className : string) : void => {
                        dependenciesLookup(classes, $className, "");
                        subNamespace.forEach(($name : string) : void => {
                            dependenciesLookup(namespacesMap[$namespace + "." + $name], $className, $name + ".");
                        });
                    });
                }
            };

            const sortClasses : any = ($namespacesList : string[]) : string[] => {
                const orderedClassesNamesList : string[] = [];
                $namespacesList.forEach(($namespace : string) : void => {
                    const classes : string[] = namespacesMap[$namespace];
                    if (classes.length > 0) {
                        if (classes.length === 1) {
                            orderedClassesNamesList.push(classes[0]);
                        } else {
                            const levelsMap : number[] = [];
                            classes.forEach(($name : string) : void => {
                                const level : number = parseInt(classesMap[$name].requiredBy.length, 10);
                                if (levelsMap.indexOf(level) === -1) {
                                    levelsMap.push(level);
                                }
                            });
                            levelsMap.sort(($a : number, $b : number) : number => {
                                return $a - $b;
                            });
                            levelsMap.reverse();
                            levelsMap.forEach(($level : number) : void => {
                                classes.forEach(($name : string) : void => {
                                    if (classesMap[$name].requiredBy.length === $level) {
                                        orderedClassesNamesList.push($name);
                                    }
                                });
                            });
                        }
                    }
                });
                return orderedClassesNamesList;
            };

            const getDependenciesOrder : any = ($classesNamesList : string[]) : number[] => {
                const levelsMap : number[] = [];
                $classesNamesList.forEach(($name : string) : void => {
                    const level : number = classesMap[$name].dependencies.length;
                    if (levelsMap.indexOf(level) === -1) {
                        levelsMap.push(level);
                    }
                });
                levelsMap.sort(sortNumberArray);
                return levelsMap;
            };

            const getOrderedFilesList : any = ($level : number) : void => {
                const getPrependMap : any = ($className : string) : string[] => {
                    const prependMap : string[] = [];
                    classesMap[$className].dependencies.forEach(($name : string) : void => {
                        if (classesNamesList.indexOf($name) !== -1 &&
                            orderedNamesList.indexOf(classesMap[$name].file) === -1 &&
                            prependMap.indexOf($name) === -1) {
                            prependMap.push($name);
                        }
                    });
                    return prependMap;
                };

                const findRecursiveDependency : any = ($name : string) : void => {
                    classesMap[$name].dependencies.forEach(($dependency : string) : void => {
                        if (classesMap[$name].requiredBy.indexOf($dependency) !== -1) {
                            if (recursiveDependency.indexOf(classesMap[$name].file) === -1) {
                                recursiveDependency.push(classesMap[$name].file);
                            }
                            if (classesNamesList.indexOf($dependency) !== -1 &&
                                recursiveDependency.indexOf(classesMap[$dependency].file) === -1) {
                                recursiveDependency.push(classesMap[$dependency].file);
                            }
                        }
                    });
                };

                const dependencyLookup : any = ($className : string) : void => {
                    getPrependMap($className).forEach(($name : string) : void => {
                        findRecursiveDependency($name);
                        if (orderedNamesList.indexOf(classesMap[$name].file) === -1) {
                            orderedNamesList.push(classesMap[$name].file);
                        }
                    });
                };

                let index : number;
                for (index = 0; index < classesNamesList.length; index++) {
                    const name : string = classesNamesList[index];
                    if (classesMap[name].dependencies.length === $level) {
                        if (orderedNamesList.indexOf(classesMap[name].file) === -1) {
                            dependencyLookup(name);
                            if (orderedNamesList.indexOf(classesMap[name].file) === -1) {
                                orderedNamesList.push(classesMap[name].file);
                            }
                        }
                    }
                }
            };

            const getDynamicReference : any = () : string => {
                let output : string = "";
                orderedNamesList.forEach(($file : string) : void => {
                    output += os.EOL + "/// <reference path=\"" + $file + "\" />";
                });
                return output;
            };

            const reportRecursiveDependency : any = () : void => {
                if (recursiveDependency.length > 0) {
                    let warning : string =
                        "WARNING: recursive dependencies have been identified. "[ColorType.RED] +
                        "Manual reference ordering may be needed for:" + os.EOL;
                    recursiveDependency.forEach(($name : string) : void => {
                        warning += "./" + referencePath + "/" + $name + os.EOL;
                    });
                    LogIt.Warning(warning);
                }
            };

            let confBlock : string;
            for (confBlock in conf) {
                if (conf.hasOwnProperty(confBlock) && confBlock === $option) {
                    if (conf[confBlock].hasOwnProperty("reference")) {
                        const referenceFile : string = conf[confBlock].reference;
                        let src : string[] = conf[confBlock].src.slice(0);
                        src.push("!" + this.properties.dependencies);
                        const hash : string = (referenceFile + src)
                            .replace(/\*/g, "")
                            .replace(/\./g, "")
                            .replace(/\//g, "")
                            .replace(/<%= /g, "")
                            .replace(/ %>/g, "")
                            .replace(/,/g, "");
                        if (generated.indexOf(hash) === -1) {
                            referencePath = referenceFile.substring(0, referenceFile.lastIndexOf("/") + 1).replace("./", "");

                            let staticDataStart : string = "";
                            let staticDataEnd : string = "";
                            let data : string;
                            if (this.fileSystem.Exists(this.properties.projectBase + "/" + referenceFile)) {
                                data = this.fileSystem.Read(this.properties.projectBase + "/" + referenceFile).toString();
                                let startIndex : number = data.indexOf(startTag);
                                if (startIndex === -1) {
                                    startIndex = data.indexOf("//grunt-start");
                                }
                                if (startIndex !== -1) {
                                    staticDataStart = data.substring(0, startIndex);
                                    let endIndex : number = data.indexOf(endTag);
                                    let endTagLength : number = endTag.length;
                                    if (endIndex === -1) {
                                        endIndex = data.indexOf("//grunt-end");
                                        endTagLength = 11;
                                    }
                                    if (endIndex !== -1) {
                                        staticDataEnd = data.substring(endIndex + endTagLength + os.EOL.length);
                                        if (staticDataEnd !== "") {
                                            staticDataEnd = staticDataEnd.substring(0, staticDataEnd.lastIndexOf("/>") + 2);
                                        }
                                    }
                                }
                            }

                            if (typeof src === "string") {
                                src = [<any>src];
                            }
                            src.push("!" + referenceFile.replace("./", ""));
                            const files : string[] = this.fileSystem.Expand(src);
                            namespacesList = [];
                            namespacesMap = {};
                            let index : number;
                            for (index = 0; index < files.length; index++) {
                                const file : string = StringUtils.Remove(files[index], referencePath);
                                if (staticDataStart.indexOf(file) === -1 && staticDataEnd.indexOf(file) === -1) {
                                    data = this.fileSystem.Read(files[index]).toString();
                                    data = data.replace(/(?:\/\*(?:[\s\S]*?)\*\/)|(?:\/\/(?:.*)$)/gm, "");
                                    data = data.replace(/export abstract class /gm, "export class ");

                                    let module : string = "";
                                    let moduleIndex : number = data.indexOf("namespace");
                                    if (moduleIndex === -1) {
                                        moduleIndex = data.indexOf("module");
                                        if (moduleIndex !== -1) {
                                            moduleIndex += 6;
                                        }
                                    } else {
                                        moduleIndex += 9;
                                    }

                                    if (moduleIndex !== -1) {
                                        module = data.substring(moduleIndex, data.indexOf("{")).replace(/ /gm, "");
                                        if (namespacesList.indexOf(module) === -1) {
                                            namespacesList.push(module);
                                            namespacesMap[module] = [];
                                        }
                                        const classIndex : number = data.indexOf("export class ");
                                        if (classIndex !== -1) {
                                            const classBodies : string[] = data.substring(classIndex).split("export class ");
                                            for (let bodyIndex : number = 1; bodyIndex < classBodies.length; bodyIndex++) {
                                                const classBody : string = classBodies[bodyIndex];
                                                const importsBody : string =
                                                    data.substring(moduleIndex, data.indexOf(classBody)).replace(/[\n\r]/gm, "");

                                                let importIndex : number = importsBody.indexOf("import ");
                                                const imports : string[] = [];
                                                while (importIndex !== -1) {
                                                    importIndex += 7;
                                                    const importName : string =
                                                        importsBody.substring(importIndex, importsBody.indexOf(";", importIndex));
                                                    imports.push(importName.substring(importName.indexOf(" = ") + 3).replace(/ /gm, ""));
                                                    importIndex = importsBody.indexOf("import ", importIndex);
                                                }

                                                let className : string = classBody.substring(0, classBody.indexOf(" "));
                                                const extendsIndex : number = classBody.indexOf(" extends ");
                                                let extendsName : string = "";
                                                let interfaceName : string = "";
                                                const implementsIndex : number = classBody.indexOf(" implements ", extendsIndex);
                                                if (extendsIndex !== -1) {
                                                    if (implementsIndex !== -1) {
                                                        extendsName = classBody.substring(extendsIndex + 8, implementsIndex);
                                                        interfaceName =
                                                            classBody.substring(implementsIndex + 12, classBody.indexOf("{", extendsIndex));
                                                    } else {
                                                        extendsName =
                                                            classBody.substring(extendsIndex + 8, classBody.indexOf("{", extendsIndex));
                                                    }
                                                    extendsName = extendsName.replace(/ /gm, "");
                                                } else if (implementsIndex !== -1) {
                                                    interfaceName =
                                                        classBody.substring(implementsIndex + 12, classBody.indexOf("{", implementsIndex));
                                                }

                                                let genericIndex : number = className.indexOf("<");
                                                if (genericIndex !== -1) {
                                                    className = className.substring(0, genericIndex);
                                                }

                                                const classNameWithNamespace : string = (module + "." + className).replace(/ /gm, "");

                                                if (extendsName !== "") {
                                                    if (extendsName.indexOf(".") === -1) {
                                                        extendsName = module + "." + extendsName;
                                                    }
                                                }

                                                if (interfaceName !== "") {
                                                    interfaceName = interfaceName.replace(/ /gm, "");
                                                    genericIndex = interfaceName.indexOf("<");
                                                    if (genericIndex !== -1) {
                                                        interfaceName = interfaceName.substring(0, genericIndex);
                                                    }
                                                    if (interfaceName.indexOf(".") === -1) {
                                                        interfaceName = module + "." + interfaceName;
                                                    }
                                                }

                                                namespacesMap[module].push(classNameWithNamespace);
                                                classesNamesList.push(classNameWithNamespace);
                                                classesMap[classNameWithNamespace] = <TypeScriptClassDefinition>{
                                                    className,
                                                    data,
                                                    dependencies: [],
                                                    extends     : extendsName,
                                                    file,
                                                    imports,
                                                    interface   : interfaceName,
                                                    name        : classNameWithNamespace,
                                                    namespace   : module,
                                                    requiredBy  : []
                                                };

                                                if (extendsName === "" && imports.length === 0) {
                                                    if (orderedNamesList.indexOf(file) === -1) {
                                                        orderedNamesList.push(file);
                                                    }
                                                }
                                            }
                                        } else {
                                            let varIndex : number = data.indexOf("export var ");
                                            if (varIndex !== -1) {
                                                while (varIndex !== -1) {
                                                    varIndex += 11;
                                                    const varBody : string = data.substring(varIndex, data.indexOf(" =", varIndex));
                                                    const varName : string = varBody.substring(0, varBody.indexOf(" : "));
                                                    let varNameWithNamespace : string = varName;
                                                    if (varName.indexOf(".") === -1) {
                                                        varNameWithNamespace = module + "." + varName;
                                                    }
                                                    let varInterface : string = varBody.substring(varBody.indexOf(" : ") + 3);
                                                    if (varInterface.indexOf(".") === -1) {
                                                        varInterface = module + "." + varInterface;
                                                    }
                                                    if (classesNamesList.indexOf(varNameWithNamespace) === -1) {
                                                        namespacesMap[module].push(varNameWithNamespace);
                                                        classesNamesList.push(varNameWithNamespace);
                                                        classesMap[varNameWithNamespace] = <TypeScriptClassDefinition>{
                                                            className   : varName,
                                                            data,
                                                            dependencies: [],
                                                            extends     : "",
                                                            file,
                                                            imports     : [],
                                                            interface   : varInterface,
                                                            name        : varNameWithNamespace,
                                                            namespace   : module,
                                                            requiredBy  : []
                                                        };
                                                    }
                                                    varIndex = data.indexOf("export var ", varIndex);
                                                }
                                            } else if (orderedNamesList.indexOf(file) === -1) {
                                                orderedNamesList.push(file);
                                            }
                                        }
                                    } else if (orderedNamesList.indexOf(file) === -1) {
                                        orderedNamesList.push(file);
                                    }
                                }
                            }

                            namespacesList.sort();
                            namespacesList = sortNamespace(namespacesList);
                            classesNamesList.forEach(collectDependencies);
                            classesNamesList.forEach(collectImports);
                            classesNamesList.forEach(mergeDependencies);
                            classesNamesList.forEach(getRequiredByMap);
                            namespacesList.forEach(getSubclassDependencies);
                            classesNamesList = sortClasses(namespacesList);

                            recursiveDependency = [];
                            getDependenciesOrder(classesNamesList).forEach(getOrderedFilesList);
                            reportRecursiveDependency();

                            const dynamicData : string = getDynamicReference();
                            let content : string = "";
                            if (staticDataStart !== "") {
                                content += staticDataStart;
                            }
                            content += startTag +
                                dynamicData + os.EOL +
                                endTag + os.EOL;
                            if (staticDataEnd !== "") {
                                content += staticDataEnd + os.EOL;
                            }
                            if (dynamicData !== "") {
                                LogIt.Info("Automatically added references to file \"" + referenceFile + "\":");
                                LogIt.Info(dynamicData);
                            }
                            if (staticDataStart !== "" || dynamicData !== "" || staticDataEnd !== "") {
                                this.fileSystem.Write(this.properties.projectBase + "/" + referenceFile, content);
                            }

                            generated.push(hash);
                        }
                    }
                }
            }

            $done();
        }
    }

    export abstract class TypeScriptClassDefinition {
        public name : string;
        public namespace : string;
        public className : string;
        public "extends" : string;
        public "interface" : string;
        public imports : string[];
        public dependencies : string[];
        public requiredBy : string[];
        public file : string;
        public data : string;
    }
}
