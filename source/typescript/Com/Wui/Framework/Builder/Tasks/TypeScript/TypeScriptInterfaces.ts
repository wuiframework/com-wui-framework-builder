/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.TypeScript {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ITypeScriptConfig = Com.Wui.Framework.Builder.Interfaces.ITypeScriptConfig;

    export class TypeScriptInterfaces extends BaseTask {

        protected getName() : string {
            return "typescript-interfaces";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const conf : ITypeScriptConfig = TypeScriptCompile.getConfig();

            const generated : string[] = [];
            const interfacesNamesList : string[] = [];
            const interfacesMap : any = {};
            const getDependenciesMap : any = ($interfaceName : string) : string[] => {
                const dependenciesLookup : any = ($interface : TypeScriptInterface, $extensions : string[]) : void => {
                    if ($interface.extends !== "" && interfacesNamesList.indexOf($interface.extends) !== -1) {
                        $extensions.push($interface.extends);
                        dependenciesLookup(interfacesMap[$interface.extends], $extensions);
                    }
                };
                const extensions : string[] = [];
                dependenciesLookup(interfacesMap[$interfaceName], extensions);
                extensions.reverse();
                extensions.push($interfaceName);
                return extensions;
            };

            let confBlock : string;
            for (confBlock  in conf) {
                if (conf.hasOwnProperty(confBlock) && confBlock === $option) {
                    if (conf[confBlock].hasOwnProperty("interface")) {
                        const interfaceFile : string = this.properties.projectBase + "/" + conf[confBlock].interface;
                        const src : string[] = conf[confBlock].src.slice(0);
                        src.push("!" + this.properties.dependencies);
                        src.push("!source/typescript/**/TypeScriptInterfaces.ts");
                        const hash : string = (interfaceFile + src.join(""))
                            .replace(/\*/g, "")
                            .replace(/\./g, "")
                            .replace(/\//g, "")
                            .replace(/<%= /g, "")
                            .replace(/ %>/g, "")
                            .replace(/,/g, "");
                        if (generated.indexOf(hash) === -1) {
                            const files : string[] = this.fileSystem.Expand(src);
                            let index : number;
                            const orderedNamesList : string[] = [];
                            try {
                                for (index = 0; index < files.length; index++) {
                                    let data : string = this.fileSystem.Read(this.properties.projectBase + "/" + files[index]).toString();
                                    if (data.indexOf("export interface") !== -1 || data.indexOf("export type") !== -1) {
                                        data = data.replace(/(?:\/\*(?:[\s\S]*?)\*\/)|(?:\/\/(?:.*)$)/gm, "");

                                        let module : string = "";
                                        let moduleIndex : number = data.indexOf("namespace");
                                        if (moduleIndex === -1) {
                                            moduleIndex = data.indexOf("module");
                                            if (moduleIndex !== -1) {
                                                moduleIndex += 6;
                                            }
                                        } else {
                                            moduleIndex += 9;
                                        }
                                        if (moduleIndex !== -1) {
                                            module = data.substring(moduleIndex, data.indexOf("{")).replace(/ /gm, "");
                                        }

                                        const interfaceBodies : any[] =
                                            data.substring(data.indexOf("export interface")).split("export interface ");
                                        for (let bodyIndex : number = 1; bodyIndex < interfaceBodies.length; bodyIndex++) {
                                            let interfaceBody : string =
                                                "export interface " +
                                                interfaceBodies[bodyIndex].substring(0, interfaceBodies[bodyIndex].indexOf("}",
                                                    interfaceBodies[bodyIndex].indexOf("{") + 1) + 1);

                                            const extendsIndex : number = interfaceBody.indexOf("extends");
                                            let interfaceName : string = "";
                                            let extending : string = "";
                                            if (extendsIndex !== -1) {
                                                extending = interfaceBody.substring(extendsIndex + 7, interfaceBody.indexOf("{"));
                                                interfaceName = interfaceBody.substring(16, extendsIndex);
                                                extending = extending.replace(/ /gm, "");
                                            } else {
                                                interfaceName = interfaceBody.substring(16, interfaceBody.indexOf("{"));
                                            }

                                            const genericIndex : number = interfaceName.indexOf("<");
                                            if (genericIndex !== -1) {
                                                interfaceName = interfaceName.substring(0, genericIndex).replace(/ /gm, "");
                                            }

                                            interfaceName = interfaceName.replace(/ /gm, "");

                                            interfaceBody = interfaceBody
                                                .substring(interfaceBody.indexOf("{") + 1, interfaceBody.lastIndexOf("}"))
                                                .replace(/\n\r/gm, "")
                                                .replace(/\r/gm, "")
                                                .replace(/\n/gm, "")
                                                .replace(/ /gm, "");

                                            const declarations : string[] = interfaceBody.split(";");
                                            const required : string[] = [];
                                            let declared : string = "";
                                            let declarationIndex : number;
                                            for (declarationIndex = 0; declarationIndex < declarations.length; declarationIndex++) {
                                                let declaration : string = declarations[declarationIndex];
                                                const typeIndex : number = declaration.indexOf("<");
                                                let methodIndex : number = declaration.indexOf("(");
                                                if (typeIndex !== -1 && typeIndex < methodIndex) {
                                                    methodIndex = typeIndex;
                                                }
                                                if (methodIndex !== -1) {
                                                    declaration = declaration.substring(0, methodIndex);
                                                } else {
                                                    const fieldIndex : number = declaration.indexOf(":");
                                                    declaration = declaration.substring(0, fieldIndex);
                                                }
                                                if (declaration !== "" && required.indexOf(declaration) === -1) {
                                                    required.push(declaration);
                                                    if (declared !== "") {
                                                        declared += "," + os.EOL;
                                                    }
                                                    declared += "                \"" + declaration + "\"";
                                                }
                                            }

                                            let extendingClassName : string = extending;
                                            if (extending !== "") {
                                                if (extendingClassName.indexOf(".") === -1) {
                                                    extendingClassName = module + "." + extending;
                                                }
                                                extending = ", " + extending;
                                            }
                                            if (declared !== "") {
                                                declared += os.EOL;
                                            }

                                            const interfaceNamespace : string = module + "." + interfaceName;
                                            interfacesNamesList.push(interfaceNamespace);
                                            interfacesMap[interfaceNamespace] = {
                                                definition: "" +
                                                    "namespace " + module + " {" + os.EOL +
                                                    "    \"use strict\";" + os.EOL +
                                                    "    export let " + interfaceName + " : " +
                                                    "Com.Wui.Framework.Commons.Interfaces.Interface =" + os.EOL +
                                                    "        function () : Com.Wui.Framework.Commons.Interfaces.Interface {" + os.EOL +
                                                    "            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([" +
                                                    os.EOL +
                                                    declared +
                                                    "            ]" + extending + ");" + os.EOL +
                                                    "        }();" + os.EOL +
                                                    "}",
                                                extends   : extendingClassName,
                                                name      : interfaceNamespace
                                            };
                                            if (extendingClassName === "") {
                                                orderedNamesList.push(interfaceNamespace);
                                            }
                                        }

                                        if (data.indexOf("export type") !== -1) {
                                            const typeBodies : string[] =
                                                data.substring(data.indexOf("export type")).split("export type ");
                                            for (let bodyIndex : number = 1; bodyIndex < typeBodies.length; bodyIndex++) {
                                                const body : string = typeBodies[bodyIndex];
                                                const interfaceName : string = body.substring(0, body.indexOf(" = ")).replace(/ /gm, "");
                                                const interfaceNamespace : string = module + "." + interfaceName;
                                                interfacesNamesList.push(interfaceNamespace);
                                                interfacesMap[interfaceNamespace] = {
                                                    definition: "" +
                                                        "namespace " + module + " {" + os.EOL +
                                                        "    \"use strict\";" + os.EOL +
                                                        "    export let " + interfaceName +
                                                        " : Com.Wui.Framework.Commons.Interfaces.Interface =" + os.EOL +
                                                        "        function () : Com.Wui.Framework.Commons.Interfaces.Interface {" + os.EOL +
                                                        "            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([" +
                                                        os.EOL +
                                                        "            ]);" + os.EOL +
                                                        "        }();" + os.EOL +
                                                        "}",
                                                    extends   : "",
                                                    name      : interfaceNamespace
                                                };
                                                orderedNamesList.push(interfaceNamespace);
                                            }
                                        }
                                    }
                                }
                            } catch (ex) {
                                LogIt.Error(ex);
                            }

                            for (index = 0; index < interfacesNamesList.length; index++) {
                                const dependencies : string[] = getDependenciesMap(interfacesNamesList[index]);
                                for (const dependency of dependencies) {
                                    if (orderedNamesList.indexOf(dependency) === -1) {
                                        orderedNamesList.push(dependency);
                                    }
                                }
                            }

                            let fileContent : string = "";
                            for (index = 0; index < orderedNamesList.length; index++) {
                                fileContent += interfacesMap[orderedNamesList[index]].definition + os.EOL + os.EOL;
                            }

                            if (fileContent !== "") {
                                LogIt.Info("Automatically added interfacesMap file to: \"" + interfaceFile + "\"");
                                fileContent = "/** WARNING: this file has been automatically generated from typescript interfaces, " +
                                    "which exist in this package. */" + os.EOL + os.EOL +
                                    "/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */" + os.EOL +
                                    fileContent +
                                    "/* tslint:enable */" + os.EOL;
                                this.fileSystem.Write(interfaceFile, fileContent);
                                generated.push(hash);
                            }
                        }
                    }
                }
            }

            $done();
        }
    }

    export abstract class TypeScriptInterface {
        public "extends" : string;
    }
}
