/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Testing {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import TypeScriptCompile = Com.Wui.Framework.Builder.Tasks.TypeScript.TypeScriptCompile;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;

    export class TestAll extends BaseTask {

        protected getName() : string {
            return "test";
        }

        protected process($done : any, $option : string) : void {
            let chain : string[] = [];
            switch ($option) {
            case "all":
                chain = ["test:lint", "test:code"];
                break;
            case "lint":
                chain = ["test:tslint", "test:sasslint", "test:jslint", "test:xcpplint", "test:javalint"];
                break;
            case "code":
                chain = ["run-coverage-test", "test:selenium"];
                break;

            case "tslint":
                chain = ["tslint:project"];
                break;
            case "sasslint":
                chain = ["sass-lint"];
                break;
            case "jslint":
                chain = ["jshint:source", "jshint:configs"];
                break;
            case "xcpplint":
                chain = ["xcpp-lint:source", "xcpp-lint:dependencies", "xcpp-lint:unit"];
                break;
            case "javalint":
                chain = ["java-lint"];
                break;
            case "typescriptunit":
                chain = ["typescript-test:unit"];
                break;
            case "xcppunit":
                chain = ["xcpp-test:unit"];
                break;
            case "xcppcoverage":
                chain = ["xcpp-test:coverage"];
                break;
            case "coverage":
                chain = ["typescript:unit", "typescript-test:coverage"];
                break;
            case "selenium":
                if (this.properties.projectHas.SeleniumTests()) {
                    if (this.properties.projectHas.TypeScript.Tests()) {
                        if (this.programArgs.getOptions().withDependencies) {
                            const config : any = TypeScriptCompile.getConfig().selenium;
                            config.src = [
                                this.properties.sources + "/*.ts", this.properties.dependencies + "/*.ts",
                                "test/selenium/**/*.ts", "dependencies/*/test/selenium/**/*.ts",
                                "bin/resource/scripts/UnitTestRunner.ts",
                                "!**/*.d.ts"
                            ];
                            LogIt.Info("Selenium tests will include tests from dependencies.");
                        }
                    }
                    chain = [
                        "typescript:selenium", "selenium-server-stop", "selenium-server-start", "mock-server", "typescript-test:selenium",
                        "selenium-server-stop"
                    ];
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " Selenium test task skipped: test files not found");
                }
                break;

            default:
                LogIt.Error("Unsupported test option: " + $option);
                break;
            }
            this.runTask($done, chain);
        }
    }
}
