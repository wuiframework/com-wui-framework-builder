/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Testing {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import VirtualBoxManager = Com.Wui.Framework.Builder.Utils.VirtualBoxManager;
    import MockServer = Com.Wui.Framework.Builder.Tasks.Servers.MockServer;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    class ActiveXObject {
        constructor($name : string) {
            // default constructor without implementation
        }
    }

    declare const __unitTestRunner : any;

    export class UnitTestLoader extends BaseTask {

        protected getName() : string {
            return "unit-test-loader";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const fs : Types.NodeJS.fs = require("fs");
            const vm : Types.NodeJS.vm = require("vm");
            const path : Types.NodeJS.path = require("path");
            const url : Types.NodeJS.url = require("url");
            const mime : any = require("mime");
            const LocalStorage : any = require("node-localstorage").LocalStorage;
            const FileAPI : any = require("file-api");
            const instrument : any = require("istanbul-lib-instrument");
            const sourceMapSupport : any = require("source-map-support");
            const {JSDOM, ResourceLoader, VirtualConsole} = require("jsdom");
            /* tslint:disable: no-submodule-imports */
            const loadCoverage : any = require("remap-istanbul/lib/loadCoverage.js");
            const remap : any = require("remap-istanbul/lib/remap.js");
            const writeReport : any = require("remap-istanbul/lib/writeReport.js");
            /* tslint:enable */

            process.cwd = () : string => {
                return process.env.NODE_PATH;
            };

            const localStorage : any = new LocalStorage(this.properties.projectBase + "/build/compiled/localStorageTemp");

            FileAPI.File = function File($input : string) : void {
                this.path = $input;
                this.name = this.name || path.basename(this.path || "");
                if (!this.name) {
                    throw new Error("No name");
                }
                this.type = this.type || mime.getType(this.name);
            };

            let stdOut : string = "";
            let sandbox : any = {};

            const origResourceFetch : any = ResourceLoader.prototype.fetch;
            ResourceLoader.prototype.fetch = function ($url : string, $options : any) : any {
                try {
                    if (StringUtils.StartsWith($url, "file://")) {
                        if (StringUtils.Contains($url, "?")) {
                            $url = StringUtils.Substring($url, 0, StringUtils.IndexOf($url, "?"));
                        }
                        if (!fs.existsSync(StringUtils.Remove($url, "file:///", "file://"))) {
                            return null;
                        }
                    }
                } catch (ex) {
                    stdOut += os.EOL + ">>"[ColorType.RED] + " Failed to load resource from: " + $url + os.EOL +
                        ex.message;
                    return null;
                }
                return new Promise(($resolve : ($data : any) => void, $reject : ($ex : Error) => void) : void => {
                    origResourceFetch
                        .apply(this, [$url, $options])
                        .then(($data : string) : void => {
                            try {
                                vm.runInContext($data, sandbox, $url);
                                $resolve(Buffer.from(""));
                            } catch (ex) {
                                stdOut += os.EOL + ">>"[ColorType.RED] + " Failed to execute resource from: " + $url + os.EOL +
                                    ex.stack;
                                $resolve($data);
                            }
                        })
                        .catch(() : void => {
                            stdOut += os.EOL + ">>"[ColorType.RED] + " Failed to load resource from: " + $url;
                            $reject(new Error("Failed to load resource from: " + $url));
                        });
                });
            };
            const virtualConsole = new VirtualConsole();
            virtualConsole.on("jsdomError", ($error : Error) : void => {
                stdOut += os.EOL + ">>"[ColorType.RED] + " " + $error.stack;
            });
            const jsdom : any = new JSDOM("", {
                /* tslint:disable: object-literal-sort-keys */
                url                 : "http://builder.wuiframework.com/#UnitTestLoader",
                contentType         : "text/html",
                userAgent           : "Node.js/" + process.version + " (" + os.platform() + "; rv: " + process.version + ")",
                includeNodeLocations: true,
                resources           : new ResourceLoader({
                    strictSSL: false
                }),
                runScripts          : "dangerously",
                virtualConsole
                /* tslint:enable */
            });
            const sandboxWindow : any = jsdom.window;
            sandboxWindow.scrollTo = () : void => {
                // default handler
            };
            sandboxWindow.WebSocket = WebSocket;
            sandboxWindow.ActiveXObject = ActiveXObject;
            sandboxWindow.localStorage = localStorage;
            sandboxWindow.File = FileAPI.File;
            sandboxWindow.FileList = FileAPI.FileList;
            sandboxWindow.FileReader = FileAPI.FileReader;
            sandboxWindow.Blob = {};
            const createElement : any = sandboxWindow.document.createElement;

            const event = undefined;
            const projectBase : string = this.properties.projectBase;

            const reportNums : any = {
                failing: 0,
                passing: 0,
                skipped: 0,
                total  : 0
            };
            const errors : string[] = [];
            const generateReport : any = ($callback : () => void) : void => {
                if (!ObjectValidator.IsEmptyOrNull(stdOut)) {
                    LogIt.Debug(os.EOL +
                        "=====================================================" + os.EOL +
                        "              SANDBOX console printout               "[ColorType.YELLOW] + os.EOL +
                        "=====================================================");
                    LogIt.Debug(stdOut + os.EOL);
                    LogIt.Debug("" +
                        "=====================================================" + os.EOL +
                        "            SANDBOX console printout end             "[ColorType.YELLOW] + os.EOL +
                        "=====================================================");
                }
                errors.forEach(($message) : void => {
                    LogIt.Info($message[ColorType.RED]);
                });
                let report : string = reportNums.failing === 0 ? "SUCCESS" : "FAILURES!";
                report += os.EOL;
                if (reportNums.skipped > 0) {
                    report += "Skipped tests: " + reportNums.skipped + os.EOL;
                }
                report += "Tests: " + reportNums.total + ", Failures: " + reportNums.failing;
                report = os.EOL + report;
                if (reportNums.failing > 0) {
                    LogIt.Info(report[ColorType.RED]);
                } else if (reportNums.skipped > 0) {
                    LogIt.Info(report[ColorType.YELLOW]);
                } else {
                    LogIt.Info(report[ColorType.GREEN]);
                }

                if ($option === "coverage") {
                    const buildPath : string = this.properties.projectBase + "/build/reports/coverage/typescript";
                    const dataPath : string = StringUtils.Replace(sourceFile, "/", path.sep);
                    this.fileSystem.Delete(buildPath);
                    this.fileSystem.CreateDirectory(buildPath);
                    let testName : string = this.programArgs.getOptions().file;
                    let allowFilter : boolean = false;
                    if (!ObjectValidator.IsEmptyOrNull(testName)) {
                        testName = StringUtils.Replace(testName, "\\", "/");
                        testName = StringUtils.Substring(testName, StringUtils.IndexOf(testName, "/unit/") + 6);
                        testName = StringUtils.Remove(testName, "Test.ts");
                        allowFilter = this.fileSystem.Exists(this.properties.projectBase + "/source/" + testName + ".ts");
                        if (allowFilter) {
                            LogIt.Debug("Filtering of coverage report for: {0}", StringUtils.Remove(testName, "typescript/"));
                        } else {
                            LogIt.Warning("Unable to filter coverage report for selected file. Processing all reports instead ...");
                        }
                    }
                    const data : any = {
                        _coverageSchema: "",
                        b              : {},
                        branchMap      : {},
                        f              : {},
                        fnMap          : {},
                        hash           : "",
                        path           : dataPath,
                        s              : {},
                        statementMap   : {}
                    };
                    let statementMapIndex : number = 0;
                    let branchMapIndex : number = 0;
                    let fnMapIndex : number = 0;
                    sandbox.getCoverageData().forEach(($coverageData : any) : void => {
                        data._coverageSchema = $coverageData._coverageSchema;
                        data.hash = $coverageData.hash;
                        let item : string;
                        for (item in $coverageData.statementMap) {
                            if ($coverageData.statementMap.hasOwnProperty(item)) {
                                data.statementMap[statementMapIndex + ""] = $coverageData.statementMap[item];
                                data.s[statementMapIndex + ""] = $coverageData.s[item];
                                statementMapIndex++;
                            }
                        }
                        for (item in $coverageData.fnMap) {
                            if ($coverageData.fnMap.hasOwnProperty(item)) {
                                data.fnMap[fnMapIndex + ""] = $coverageData.fnMap[item];
                                data.f[fnMapIndex + ""] = $coverageData.f[item];
                                fnMapIndex++;
                            }
                        }
                        for (item in $coverageData.branchMap) {
                            if ($coverageData.branchMap.hasOwnProperty(item)) {
                                data.branchMap[branchMapIndex + ""] = $coverageData.branchMap[item];
                                data.b[branchMapIndex + ""] = $coverageData.b[item];
                                branchMapIndex++;
                            }
                        }
                    });
                    const coverageData : any = {};
                    coverageData[dataPath] = data;

                    LogIt.Info("> Generating coverage report ...");
                    const coverage : any = loadCoverage("coverage-data", {
                        readJSON() : any {
                            return coverageData;
                        }
                    });
                    const filterDependencies : boolean = !this.programArgs.getOptions().withDependencies;
                    const collector : any = remap(coverage, {
                        exclude($file : string) : boolean {
                            $file = StringUtils.Replace($file, "\\", "/");
                            if (StringUtils.EndsWith($file, "source.js")) {
                                return false;
                            }
                            if (filterDependencies && StringUtils.Contains($file, "/dependencies/")) {
                                return true;
                            }
                            if (allowFilter && StringUtils.Contains($file, "/source/")) {
                                return !StringUtils.Contains($file, testName + ".ts");
                            }
                            return StringUtils.Contains($file, "/bin/", "/test/", "/RuntimeTests/", "interfacesMap.ts");
                        }
                    });
                    writeReport(collector, "html", {}, this.properties.projectBase + "/build/reports/coverage/typescript")
                        .then(() : void => {
                            $callback();
                        })
                        .catch(($error : any) : void => {
                            LogIt.Error("Coverage report error.", $error);
                        });
                } else {
                    $callback();
                }
            };

            sandbox = {
                /* tslint:disable: object-literal-sort-keys no-console */
                __unitTestRunner: {
                    done() : void {
                        generateReport(() : void => {
                            process.cwd = () : string => {
                                return projectBase;
                            };
                            $done();
                        });
                    },
                    assert: require("chai").assert,
                    report: {
                        pass  : ($testName : string, $time : number) : void => {
                            reportNums.total++;
                            console.log(("+ " + $testName + " (" + $time + "ms)")[ColorType.GREEN]);
                        },
                        fail  : ($testName : string, $time : number) : void => {
                            reportNums.total++;
                            reportNums.failing++;
                            console.log(("- " + $testName + " (" + $time + "ms)")[ColorType.RED]);
                        },
                        skip  : ($testName : string) : void => {
                            reportNums.total++;
                            reportNums.skipped++;
                            console.log(("~ " + $testName)[ColorType.YELLOW]);
                        },
                        assert: ($testName : string, $ex : Error) : void => {
                            errors.push(
                                os.EOL +
                                $testName + " (ASSERT ERROR DETAILS):" + os.EOL +
                                $ex.stack + os.EOL);
                        },
                        error : ($ownerName : string, $ex : Error) : void => {
                            reportNums.total++;
                            reportNums.failing++;
                            errors.push(
                                os.EOL +
                                $ownerName + " (ERROR DETAILS):" + os.EOL +
                                $ex.stack + os.EOL);
                        }
                    },
                    events: {
                        onSuiteStart   : () : void => {
                            // default handler
                        },
                        onSuiteEnd     : () : void => {
                            // default handler
                        },
                        onTestCaseStart: ($testName : string) : void => {
                            // default handler
                        },
                        onTestCaseEnd  : ($testName : string) : void => {
                            // default handler
                        },
                        onAssert       : ($status : boolean) : void => {
                            // default handler
                        }
                    }
                },
                jsdom,
                assert          : {},
                isBrowserRun    : false,
                File            : FileAPI.File,
                FileList        : FileAPI.FileList,
                FileReader      : FileAPI.FileReader,
                NodeFile        : FileAPI.File,
                HTMLElement     : sandboxWindow.HTMLElement,
                WebSocket,
                ActiveXObject,
                XMLHttpRequest  : sandboxWindow.XMLHttpRequest,
                event,
                localStorage,
                setTimeout      : ($handler : any, $timeout? : number, ...$arguments : any[]) : any => {
                    return setTimeout.apply(this, [
                        (...$args : any[]) : void => {
                            try {
                                $handler.apply(this, $args);
                            } catch (ex) {
                                if (ex.message !== "__ASYNC_ASSERT_FAIL__") {
                                    console.log(">>"[ColorType.RED] + " Uncatched timeout error detected: " + ex.stack + os.EOL);
                                    throw ex;
                                }
                            }
                        }, $timeout
                    ].concat($arguments));
                },
                setInterval,
                clearTimeout,
                clearInterval,
                console,
                window          : {
                    location: {
                        __href: sandboxWindow.location.href
                    }
                },
                document        : sandboxWindow.document,
                getCoverageData() : any[] {
                    return [];
                },
                require,
                module          : {
                    exports: {}
                },
                process         : {
                    argv  : ["--test-run"],
                    cwd() : string {
                        return projectBase;
                    },
                    exit($code : number) : void {
                        console.log(">>"[ColorType.RED] + " ATTEMPTING TO CLOSE Application with exit code: " + $code + os.EOL);
                    },
                    on($eventName : string, $handler : () => any) : void {
                        console.log(">>"[ColorType.YELLOW] + " ATTEMPTING TO Align process event: " + $eventName + os.EOL);
                    },
                    stdout: {
                        write($message : string) : void {
                            console.log($message);
                        }
                    },
                    stderr: {
                        write($message : string) : void {
                            console.log($message);
                        }
                    },
                    env   : process.env,
                    chdir($dir : string) : void {
                        console.log(">>"[ColorType.RED] + " Changing CWD to \"" + $dir + "\"");
                        process.chdir($dir);
                    },
                    kill($pid : number) : void {
                        console.log(">>"[ColorType.RED] + " Going to kill process with PID: " + $pid);
                        process.kill($pid);
                    }
                },
                global          : {},
                Buffer,
                Error           : sandboxWindow.Error,
                __dirname,
                __filename,
                nodejsRoot      : projectBase + "/dependencies/nodejs",
                builder         : Loader.getInstance(),
                JsonpData       : () : void => {
                    // default handler
                }
                /* tslint:enable */
            };

            if (!Loader.getInstance().getProgramArgs().IsDebug()) {
                sandbox.console = {
                    log($message : string) : void {
                        stdOut += os.EOL + $message;
                    },
                    info($message : string) : void {
                        stdOut += os.EOL + $message;
                    },
                    debug($message : string) : void {
                        stdOut += os.EOL + $message;
                    },
                    warn($message : string) : void {
                        stdOut += os.EOL + $message;
                    },
                    error($message : string) : void {
                        stdOut += os.EOL + $message;
                    },
                    clear() : void {
                        // dummy
                    }
                };

                sandbox.process.exit = ($code : number) : void => {
                    stdOut += os.EOL + ">>"[ColorType.RED] + " ATTEMPTING TO CLOSE Application with exit code: " + $code + os.EOL;
                };
                sandbox.process.on = ($eventName : string, $handler : () => any) : void => {
                    stdOut += os.EOL + ">>"[ColorType.YELLOW] + " ATTEMPTING TO Align process event: " + $eventName + os.EOL;
                };
                sandbox.process.stdout = {
                    write($message : string) : void {
                        stdOut += $message;
                    }
                };
                sandbox.process.stderr = {
                    write($message : string) : void {
                        stdOut += $message;
                    }
                };
            }
            sandbox.process.env.NODE_PATH = projectBase + "/dependencies/nodejs/build/node_modules";

            this.project.namespaces.forEach(($item : string) : void => {
                const rootPart = $item.split(".")[0];
                if (!sandbox.hasOwnProperty(rootPart)) {
                    sandbox[rootPart] = {};
                    sandbox.global[rootPart] = sandbox[rootPart];
                    sandbox.window[rootPart] = sandbox[rootPart];
                }
            });

            Object.defineProperty(sandbox.window.location, "href", {
                get() : string {
                    return this.__href;
                },
                set($value : string) : void {
                    this.__href = $value;
                    const parsedUrl : any = url.parse($value);
                    let property : string;
                    for (property in parsedUrl) {
                        if (parsedUrl.hasOwnProperty(property) && property !== "href") {
                            this[property] = parsedUrl[property];
                        }
                    }
                }
            });
            let domProperty : string;
            for (domProperty in sandboxWindow) {
                if (sandboxWindow.hasOwnProperty(domProperty) && domProperty !== "location") {
                    sandbox.window[domProperty] = sandboxWindow[domProperty];
                }
            }
            for (domProperty in sandboxWindow.location) {
                if (sandboxWindow.location.hasOwnProperty(domProperty) && domProperty !== "href") {
                    sandbox.window.location[domProperty] = sandboxWindow.location[domProperty];
                }
            }

            const origSend : any = sandbox.XMLHttpRequest.prototype.send;
            const origGetAllResponseHeaders : any = sandbox.XMLHttpRequest.prototype.getAllResponseHeaders;
            sandbox.XMLHttpRequest.prototype.send = function ($data : string) : void {
                if ($data !== null) {
                    origSend.apply(this, [$data]);
                }
            };
            sandbox.XMLHttpRequest.prototype.getAllResponseHeaders = function () : string {
                if (!ObjectValidator.IsEmptyOrNull(sandbox.window.location.headers)) {
                    return sandbox.window.location.headers;
                }
                return origGetAllResponseHeaders.apply(this, []);
            };

            sandbox.window.document.createElement = ($name : string) : HTMLElement => {
                let element : any = null;
                try {
                    element = createElement.apply(sandbox.window.document, [$name]);
                } catch (ex) {
                    LogIt.Debug(ex.stack);
                }

                return element;
            };

            if ($option === "selenium") {
                sandbox.isBrowserRun = true;
                let port = "";
                if (this.properties.nodejsPort !== 80) {
                    port = ":" + this.properties.nodejsPort;
                }
                sandbox.selenium = require("selenium-webdriver");
                sandbox.chromeBuilder = new sandbox.selenium.Builder()
                    .forBrowser("chrome")
                    .usingServer("http://127.0.0.1" + port + "/wd/hub");
            }
            sandbox.vbox = VirtualBoxManager.getInstanceSingleton();

            vm.createContext(sandbox);

            let sourceFile : string = this.properties.projectBase + "/build/compiled/test";
            if ($option === "selenium") {
                sourceFile += "/selenium";
            } else {
                sourceFile += "/unit";
            }
            sourceFile += "/typescript/source.js";
            try {
                let content : string = this.fileSystem.Read(sourceFile).toString();
                const contentMap : string = JSON.parse(this.fileSystem.Read(sourceFile + ".map").toString());
                const coverageIds : string[] = [];
                const loadTests : any = () : void => {
                    const unitRootPath : string = this.properties.projectBase + "/build/target";
                    const mockServerLocation : string = MockServer.getLocation();
                    if (ObjectValidator.IsEmptyOrNull(mockServerLocation)) {
                        LogIt.Error("Unable to find instance of Mock server.");
                    }

                    const loader : any = ($imports : any, $namespaces : string[], $unitRootPath : string,
                                          $mockServerLocation : string) : void => {
                        $imports.Reflection.setInstanceNamespaces.apply($imports.Reflection, $namespaces);
                        const reflection : Reflection = $imports.Reflection.getInstance();
                        const instances : any[] = [];
                        const nextTest : any = ($index : number) : void => {
                            if ($index < instances.length) {
                                instances[$index].Process(() : void => {
                                    nextTest($index + 1);
                                });
                            } else {
                                __unitTestRunner.done();
                            }
                        };
                        reflection.getAllClasses().forEach(($class : string) : void => {
                            if ($imports.StringUtils.EndsWith($class, "Test")) {
                                const classObject : any = reflection.getClass($class);
                                if (classObject.prototype.prepareAssert !== undefined) {
                                    const instance : any = new classObject();
                                    if (reflection.IsMemberOf(instance, $imports.UnitTestRunner)) {
                                        instance.unitRootPath = $unitRootPath;
                                        instance.mockServerLocation = $mockServerLocation;
                                        instances.push(instance);
                                    }
                                }
                            }
                        });
                        nextTest(0);
                    };
                    content += os.EOL + "(" + Convert.FunctionToString(loader) + ")" +
                        "({" +
                        "Reflection: Com.Wui.Framework.Commons.Utils.Reflection, " +
                        "UnitTestRunner: Com.Wui.Framework.UnitTestRunner, " +
                        "StringUtils: Com.Wui.Framework.Commons.Utils.StringUtils" +
                        "}, " + JSON.stringify(this.project.namespaces) + ", \"" + unitRootPath + "\", \"" + mockServerLocation + "\");";
                    if ($option === "coverage") {
                        content += os.EOL +
                            "getCoverageData = function(){return [" + coverageIds.join(",") + "];}";
                    }

                    const sandboxUrl : string = "file:///" + unitRootPath + "/index.html#UnitTestLoader";
                    sandbox.window.location.href = sandboxUrl;
                    jsdom.reconfigure({url: sandboxUrl});

                    sourceMapSupport.install({
                        environment             : "node",
                        handleUncaughtExceptions: false,
                        retrieveSourceMap($source : string) : any {
                            if ($source === sourceFile || StringUtils.Contains($source, "evalmachine")) {
                                return {
                                    map: contentMap,
                                    url: $source + ".map"
                                };
                            }
                            return null;
                        }
                    });

                    LogIt.Debug("> Loading tests ... ");
                    vm.runInContext(content, sandbox, sourceFile);
                };
                if ($option === "coverage") {
                    LogIt.Debug("> Prepare for instrumentation");
                    content = StringUtils.Replace(content, ") || this;", ") || /* istanbul ignore next */ this;");
                    let counter : number = 0;
                    const excludedBranch : string[] = [];
                    const files : string[] = this.fileSystem.Expand([
                        this.properties.projectBase + "/source/typescript/**/*.ts",
                        this.properties.projectBase + "/test/typescript/**/*.ts",
                        this.properties.projectBase + "/dependencies/**/source/typescript/**/*.ts",
                        this.properties.projectBase + "/dependencies/**/test/typescript/**/*.ts"
                    ]);
                    let fileIndex : number = 0;
                    files.forEach(($file : string) : void => {
                        if (counter < 100) {
                            Echo.Print(".");
                        } else {
                            Echo.Println(" (" + Math.floor(100 / files.length * fileIndex) + "%)");
                            counter = 0;
                        }
                        $file = StringUtils.Replace($file, "\\", "/");
                        $file = StringUtils.Replace($file, "//", "/");
                        $file = StringUtils.Substring($file, StringUtils.IndexOf($file, "/typescript/") + 12);
                        const namespaces : string[] = StringUtils.Split($file, "/");
                        let index : number;
                        for (index = 0; index < namespaces.length; index++) {
                            if (excludedBranch.indexOf(namespaces[index]) === -1) {
                                excludedBranch.push(namespaces[index]);
                                if (index === 0) {
                                    content = StringUtils.Replace(content,
                                        "})(" + namespaces[index] + " || (" + namespaces[index] + " = {}));",
                                        "})(" + namespaces[index] + " || /* istanbul ignore next */ (" + namespaces[index] + " = {}));");
                                } else {
                                    content = StringUtils.Replace(content,
                                        "})(" + namespaces[index] + " = " + namespaces[index - 1] + "." + namespaces[index] +
                                        " || (" + namespaces[index - 1] + "." + namespaces[index] + " = {}));",
                                        "})(" + namespaces[index] + " = " + namespaces[index - 1] + "." + namespaces[index] +
                                        " || /* istanbul ignore next */ (" + namespaces[index - 1] + "." + namespaces[index] + " = {}));");
                                }
                            }
                        }
                        fileIndex++;
                        counter++;
                    });
                    LogIt.Info(" (100%)");

                    LogIt.Debug("> Instrumenting code");
                    const splitContent : string[] = StringUtils.Split(content, os.EOL + "var ");
                    const inst : any = instrument.createInstrumenter({compact: false, esModules: false, produceSourceMap: true});
                    content = "";
                    counter = 0;
                    let linesCount : number = 0;
                    const instrumentNextContent : any = ($index : number) : void => {
                        if ($index < splitContent.length) {
                            if (counter < 100) {
                                Echo.Print(".");
                            } else {
                                Echo.Println(" (" + Math.floor(100 / splitContent.length * $index) + "%)");
                                counter = 0;
                            }
                            let instrumentCode : string = splitContent[$index];
                            if ($index > 0) {
                                instrumentCode = "var " + instrumentCode;
                            }
                            if ($index < splitContent.length - 1) {
                                instrumentCode += os.EOL;
                            }
                            instrumentCode = Array(linesCount + 1).join(os.EOL) + instrumentCode;
                            linesCount += StringUtils.OccurrenceCount(splitContent[$index], os.EOL) + 1;
                            inst.instrument(instrumentCode, "source_" + $index + ".js", ($error : Error, $code : string) : void => {
                                if ($error === null) {
                                    coverageIds.push($code.substr(0, $code.indexOf("\n"))
                                        .replace("var ", "").replace(" = function () {", "").trim());
                                    content += $code + os.EOL;
                                    counter++;
                                    instrumentNextContent($index + 1);
                                } else {
                                    LogIt.Error("Code instrumentation error.", $error);
                                }
                            });
                        } else {
                            LogIt.Info(" (100%)");
                            loadTests();
                        }
                    };
                    instrumentNextContent(0);
                } else {
                    loadTests();
                }
            } catch (ex) {
                LogIt.Error("Unit sandbox error.", ex);
            }
        }
    }
}
