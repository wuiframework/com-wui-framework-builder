/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Testing {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;

    export class BuildCheck extends BaseTask {

        public static RegisterCheck($rule : ($done : () => void, $option? : string) => void) : void {
            Loader.getInstance().getAppProperties().buildCheckRegister.push($rule);
        }

        protected getName() : string {
            return "build-check";
        }

        protected process($done : any, $option : string) : void {
            const rulesCount : number = this.properties.buildCheckRegister.length;
            const getNextCheck : any = ($index : number) : void => {
                if ($index < rulesCount) {
                    this.properties.buildCheckRegister[$index](() : void => {
                        getNextCheck($index + 1);
                    }, $option);
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " " + rulesCount + " rules have passed.");
                    $done();
                }
            };
            getNextCheck(0);
        }
    }
}
