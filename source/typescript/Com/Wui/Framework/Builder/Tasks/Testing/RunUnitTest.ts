/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Testing {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import GeneralTaskType = Com.Wui.Framework.Builder.Enums.GeneralTaskType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import BuildExecutor = Com.Wui.Framework.Builder.Tasks.BuildProcessor.BuildExecutor;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;
    import MavenEnv = Com.Wui.Framework.Builder.Tasks.Java.MavenEnv;
    import XCppTest = Com.Wui.Framework.Builder.Tasks.XCpp.XCppTest;
    import TypeScriptCompile = Com.Wui.Framework.Builder.Tasks.TypeScript.TypeScriptCompile;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class RunUnitTest extends BaseTask {
        protected taskType : string;

        constructor() {
            super();
            this.taskType = "unit";
        }

        public getDependenciesTree($option? : string) : string[] {
            const executor : BuildExecutor = new BuildExecutor();
            const products : BuildProductArgs[] = executor.getTargetProducts(this.project.target);
            if (products.length > 1) {
                LogIt.Error("Project contains multiple platforms: " + JSON.stringify(products));
            }

            const testFile : string = this.programArgs.getOptions().file;
            if (!ObjectValidator.IsEmptyOrNull(testFile)) {
                this.getSingleTestFilter(testFile);
            } else {
                this.getMultiTestFiler();
            }

            const skipBuild : boolean = $option === "skipbuild";
            let chain : string[] = [];
            if (!skipBuild) {
                chain.push("product-settings:0");
            }
            chain = chain.concat(this.getUnitTestChain(skipBuild));
            if (this.programArgs.IsAgentTask()) {
                chain.push("cloud-manager:push");
            }
            return chain;
        }

        protected getName() : string {
            return "run-unit-test";
        }

        protected getInitCompileTasks($isCpp : boolean) : string[] {
            const tasks : string[] = [
                "install:dev",
                "mock-server",
                "builder-server:start",
                GeneralTaskType.VAR_REPLACE
            ];
            if ($isCpp) {
                tasks.push("string-replace:xcpp");
            }
            tasks.push(
                "copy-staticfiles:targetresource",
                "copy-staticfiles:resource");
            return tasks;
        }

        protected getTypeScriptCompileTasks() : string[] {
            return [
                "typescript-interfaces:test",
                "typescript-reference:test",
                "typescript:unit"
            ];
        }

        protected getCppCompileTasks() : string[] {
            return [
                "xcpp-interfaces:source",
                "xcpp-reference:source",
                "xcpp-compile:" + this.taskType,
                "copy-staticfiles:targetresource-postbuild"
            ];
        }

        protected getSingleTestFilter($fileName : string) : void {
            const path : Types.NodeJS.path = require("path");
            $fileName = this.fileSystem.NormalizePath($fileName);
            if (StringUtils.StartsWith($fileName, "/")) {
                $fileName = StringUtils.Substring($fileName, 1);
            }
            const ext : string = path.extname($fileName);

            switch (ext) {
            case ".ts":
                const config : any = TypeScriptCompile.getConfig().unit;
                config.src = [
                    this.properties.sources + "/*.ts", this.properties.dependencies + "/*.ts",
                    "test/" + $fileName,
                    "bin/resource/scripts/UnitTestRunner.ts",
                    "!**/*.d.ts"
                ];
                LogIt.Info("Run typescript " + this.taskType + " tests from source: " + config.src);
                break;

            case ".cpp":
                XCppTest.getConfig().filter = $fileName.replace(".cpp", ".*");
                LogIt.Info("Run C++ " + this.taskType + " tests from source: " + $fileName);
                break;

            case ".java":
                MavenEnv.getConfig().unit.src = [
                    $fileName.replace(".java", "").replace(/\//g, ".")
                ];
                LogIt.Info("Run Java " + this.taskType + " tests from source: " + $fileName);
                break;

            default:
                LogIt.Error("Unsupported single " + this.taskType + " test file \"" + ext + "\" extension. " +
                    "Supported only \"*.ts\", \"*.cpp\" or \"*.java\"");
                break;
            }
        }

        protected getMultiTestFiler() : void {
            LogIt.Info("Run all " + this.taskType + " tests from source: test/**/*");

            if (this.properties.projectHas.TypeScript.Tests()) {
                if (this.programArgs.getOptions().withDependencies) {
                    const config : any = TypeScriptCompile.getConfig().unit;
                    config.src = [
                        this.properties.sources + "/*.ts", this.properties.dependencies + "/*.ts",
                        "test/unit/**/*.ts", "dependencies/*/test/unit/**/*.ts",
                        "bin/resource/scripts/UnitTestRunner.ts",
                        "!**/*.d.ts"
                    ];
                    LogIt.Info("Typescript " + this.taskType + " tests will include tests from dependencies.");
                }
            }
            if (this.properties.projectHas.Cpp.Tests()) {
                XCppTest.getConfig().filter = "*";
            }
        }

        protected getUnitTestChain($skipBuild : boolean) : string[] {
            let compileTasks : string[] = this.getInitCompileTasks(this.properties.projectHas.Cpp.Tests());
            const testTasks : string[] = [];
            if (this.properties.projectHas.TypeScript.Tests()) {
                compileTasks = compileTasks.concat(this.getTypeScriptCompileTasks());
                testTasks.push("typescript-test:" + this.taskType);
            }
            if (this.properties.projectHas.Cpp.Tests()) {
                compileTasks = compileTasks.concat(this.getCppCompileTasks());
                testTasks.push("test:xcpp" + this.taskType);
            }
            if (this.properties.projectHas.Java.Tests()) {
                testTasks.push(
                    "maven-env:init",
                    "maven-build:" + this.taskType,
                    "maven-env:clean");
            }

            if (!$skipBuild) {
                return compileTasks.concat(testTasks);
            } else {
                return [
                    "install:dev",
                    "mock-server",
                    "builder-server:start"
                ].concat(testTasks);
            }
        }

        protected process($done : any, $option : string) : void {
            this.build.isTest = true;
            this.runTask($done, this.getDependenciesTree($option));
        }
    }
}
