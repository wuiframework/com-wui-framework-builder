/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Testing {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class VirtualBoxServer extends BaseTask {

        protected getName() : string {
            return "vbox-server-start";
        }

        protected process($done : any) : void {
            this.terminal.Execute("tasklist", [], null, ($exitCode : number, $std : string[]) : void => {
                if ($exitCode === 0) {
                    if (StringUtils.ContainsIgnoreCase($std[0], "VBoxWebSrv.exe")) {
                        $done();
                    } else {
                        this.terminal.Spawn("VBoxWebSrv.exe", ["-A", "null"], "C:/Program Files/Oracle/VirtualBox",
                            ($exitCode : number, $std : string[]) : void => {
                                if ($exitCode === 0) {
                                    $done();
                                } else {
                                    LogIt.Error($std[0] + $std[1]);
                                }
                            });
                    }
                } else {
                    LogIt.Error($std[0] + $std[1]);
                }
            });
        }
    }
}
