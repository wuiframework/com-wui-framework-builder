/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Testing {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class TestLintStaggedOnly extends BaseTask {

        protected getName() : string {
            return "test-lint-stagged-only";
        }

        protected process($done : any) : void {
            this.runTask($done, "git-manager:backup", "test:tslint", "test:sasslint", "test:jslint", "test:xcpplint",
                "test:javalint", "git-manager:restore");
        }
    }
}
