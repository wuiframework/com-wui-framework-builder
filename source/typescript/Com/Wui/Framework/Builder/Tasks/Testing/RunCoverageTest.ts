/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Testing {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class RunCoverageTest extends RunUnitTest {

        constructor() {
            super();
            this.taskType = "coverage";
        }

        protected getName() : string {
            return "run-coverage-test";
        }

        protected getSingleTestFilter($fileName : string) : void {
            if (StringUtils.EndsWith($fileName, ".ts")) {
                super.getSingleTestFilter($fileName);
            } else {
                LogIt.Error("Single coverage tests are supported only for TypeScript files with \"*.ts\" extension.");
            }
        }
    }
}
