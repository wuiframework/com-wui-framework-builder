/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Client {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;

    export class ClientReport extends BaseTask {

        protected getName() : string {
            return "client-report";
        }

        protected process() : void {
            LogIt.Info("-------------------------------"[ColorType.GREEN]);
            LogIt.Info("Build report for all platforms:"[ColorType.GREEN]);
            let success : boolean = true;
            BuilderClient.Register.foreach(($task : ITaskRegisterRecord) : void => {
                LogIt.Info("[" + $task.platform + "] exit code: " + $task.exitCode);
                if ($task.exitCode !== 0) {
                    success = false;
                }
            });
            Loader.getInstance().Exit(success ? 0 : -1);
        }
    }
}
