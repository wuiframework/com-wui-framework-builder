/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Client {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import BuilderHubConnector = Com.Wui.Framework.Builder.Connectors.BuilderHubConnector;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogLevel = Com.Wui.Framework.Commons.Enums.LogLevel;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BuilderServiceConnector = Com.Wui.Framework.Builder.Connectors.BuilderServiceConnector;
    import StdinManager = Com.Wui.Framework.Localhost.Utils.StdinManager;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import BuildExecutor = Com.Wui.Framework.Builder.Tasks.BuildProcessor.BuildExecutor;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;
    import IWuiBuilderAgent = Com.Wui.Framework.Builder.Connectors.IWuiBuilderAgent;
    import IWuiBuilderTaskProtocol = Com.Wui.Framework.Builder.Tasks.Servers.IWuiBuilderTaskProtocol;
    import IReleaseProperties = Com.Wui.Framework.Builder.Tasks.BuildProcessor.IReleaseProperties;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;

    export class BuilderClient extends BaseTask {
        public static Register : ArrayList<ITaskRegisterRecord>;

        protected getName() : string {
            return "builder-client";
        }

        protected process($done : any, $option : string) : void {
            const stdinManager : StdinManager = new StdinManager();

            const isAgent : boolean = $option === "agent";
            let wuiArgs : string[] = this.programArgs.getOptions().forwardArgs;
            if (ObjectValidator.IsEmptyOrNull(wuiArgs)) {
                wuiArgs = this.programArgs.getTasks();
            }
            const forwardArgs : ProgramArgs = new ProgramArgs();
            forwardArgs.Parse(wuiArgs);
            const isBaseTask : boolean = forwardArgs.IsBaseTask();

            let base : string = this.properties.projectBase;
            let workspace : string;
            for (workspace in this.builderConfig.workspaces) {
                if (this.builderConfig.workspaces.hasOwnProperty(workspace)) {
                    const path : string = this.builderConfig.workspaces[workspace];
                    if (path !== "" && base.indexOf(path) !== -1) {
                        base = base.replace(path, "[WORKSPACE_" + workspace + "]/");
                        break;
                    }
                }
            }

            BuilderClient.Register = new ArrayList<ITaskRegisterRecord>();
            const handleMessage : any = ($message : IWuiBuilderTaskProtocol, $taskId : string) : void => {
                try {
                    if (ObjectValidator.IsObject($message)) {
                        const task : ITaskRegisterRecord = BuilderClient.Register.getItem($taskId);
                        switch ($message.type) {
                        case EventType.ON_START:
                            const onStartArgs : ProgramArgs = new ProgramArgs();
                            onStartArgs.Parse($message.data.split(" "));
                            if (!onStartArgs.IsBaseTask()) {
                                LogIt.Info("WUI Builder task has been accepted" + (isAgent ? " with id: " + $taskId : "."));
                            }
                            break;
                        case EventType.ON_CHANGE:
                            if (ObjectValidator.IsString($message.data)) {
                                if (isAgent && !isBaseTask && !ObjectValidator.IsEmptyOrNull(task.platform)) {
                                    const prefix : string = "[" + task.platform + "] ";
                                    $message.data = StringUtils.Replace($message.data, "\n", "\n" + prefix);
                                }
                                Echo.Print($message.data);
                            } else {
                                stdinManager.setQuestions([
                                    {
                                        callback: () : void => {
                                            // default callback
                                        },
                                        hidden  : $message.data.hidden,
                                        prefix  : Buffer.from($message.data.prefix, "base64").toString("utf8")
                                    }
                                ]);
                                stdinManager.NextQuestion();
                            }
                            break;
                        case EventType.ON_COMPLETE:
                            const onCompleteArgs : ProgramArgs = new ProgramArgs();
                            onCompleteArgs.Parse($message.data.cmd.split(" "));
                            if (!onCompleteArgs.IsBaseTask()) {
                                let message : string = "WUI Builder task has been completed with " +
                                    "exit code: " + $message.data.exitCode;
                                if (ObjectValidator.IsDigit($message.data.taskMinutes)) {
                                    message += " in " + $message.data.taskMinutes + "ms";
                                    if (ObjectValidator.IsDigit($message.data.buildMinutes)) {
                                        message += " from " + Convert.TimeToLongString($message.data.buildMinutes);
                                    }
                                }
                                LogIt.Info(message);
                            }
                            if (!ObjectValidator.IsEmptyOrNull($message.data.exitCode)) {
                                if (isAgent && !isBaseTask) {
                                    task.completed = true;
                                    task.exitCode = $message.data.exitCode;
                                    let completed : boolean = true;
                                    BuilderClient.Register.foreach(($task : ITaskRegisterRecord) : void => {
                                        if (!$task.completed) {
                                            completed = false;
                                        }
                                    });
                                    if (completed) {
                                        $done();
                                    }
                                } else {
                                    Loader.getInstance().Exit($message.data.exitCode);
                                }
                            } else {
                                $done();
                            }
                            break;
                        default:
                            LogIt.Debug($message);
                            break;
                        }
                    } else if (!ObjectValidator.IsEmptyOrNull($message)) {
                        LogIt.Debug($message);
                    }
                } catch (ex) {
                    LogIt.Debug("Unable to handle client message. {0}", ex.message);
                }
            };

            if (isAgent) {
                if (isBaseTask) {
                    LogIt.setLevel(LogLevel.ERROR);
                }
                const connector : BuilderHubConnector = new BuilderHubConnector();
                if (base.indexOf("[WORKSPACE_") === -1) {
                    base = "[CLOUDBASE_" + this.builderConfig.ownCloud.user + "]/" + require("path").basename(base);
                }

                const platforms : string[] = [];
                const executor : BuildExecutor = new BuildExecutor();
                executor.getReleases().forEach(($release : IReleaseProperties) : void => {
                    executor.getTargetProducts($release.target).forEach(($platform : BuildProductArgs) : void => {
                        platforms.push($platform.Value());
                    });
                });
                let responseIndex : number = 0;
                let agent : IWuiBuilderAgent = this.builderConfig.agent;
                if (ObjectValidator.IsEmptyOrNull(agent)) {
                    agent = <any>{};
                }
                if (!ObjectValidator.IsEmptyOrNull(this.project.builder.location) &&
                    this.project.builder.location !== "agent") {
                    agent.name = this.project.builder.location;
                }
                if (!ObjectValidator.IsEmptyOrNull(this.project.builder.version)) {
                    agent.version = this.project.builder.version;
                }
                if (ObjectValidator.IsEmptyOrNull(agent.version)) {
                    agent.version = "latest";
                }
                connector
                    .RunTask({
                        agent,
                        args: wuiArgs.concat(["--project", this.project.name, "--base", base, "--agent-task"]),
                        platforms
                    })
                    .OnError(($message : string) : void => {
                        LogIt.Error("Task has been rejected by WUI Hub: " + $message);
                    })
                    .OnMessage(handleMessage)
                    .Then(($status : boolean, $builderId? : string, $taskId? : string) : void => {
                        if ($status) {
                            if (!isBaseTask) {
                                LogIt.Info("Task has been accepted by WUI Hub. Forwarding to builder with suitable platform.");
                                BuilderClient.Register.Add({
                                    completed: false,
                                    exitCode : 0,
                                    platform : platforms[responseIndex]
                                }, $taskId);
                            }
                            responseIndex++;
                            stdinManager.setOnKeyPress(($value : string) : void => {
                                connector.ForwardMessage($builderId, <IWuiBuilderTaskProtocol>{
                                    data  : $value,
                                    taskId: $taskId
                                });
                            });
                        } else {
                            LogIt.Error("Task has been rejected by WUI Hub: builder with suitable platform has not been found");
                        }
                    });
            } else {
                const connector : BuilderServiceConnector =
                    new BuilderServiceConnector(this.project.builder.port, this.project.builder.location);
                connector
                    .RunTask(wuiArgs.concat(["--base", base, "--service-task"]))
                    .Then(handleMessage);
                stdinManager.setOnKeyPress(($value : string) : void => {
                    connector.RunTask([$value]).Then(handleMessage);
                });
            }
        }
    }

    export abstract class ITaskRegisterRecord {
        public completed : boolean;
        public exitCode : number;
        public platform : string;
    }
}
