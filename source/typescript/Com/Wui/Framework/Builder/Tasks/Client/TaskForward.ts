/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Client {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;

    export class TaskForward extends BaseTask {

        protected getName() : string {
            return "forward-task";
        }

        protected process($done : any) : void {
            let chain : string[] = [];
            if (this.programArgs.IsHubClient()) {
                if (ObjectValidator.IsEmptyOrNull(this.builderConfig.hub.url)) {
                    LogIt.Error("Unable to forward task to WUI Hub due to missing hub url at builder configuration.");
                }
                let wuiArgs : string[] = this.programArgs.getOptions().forwardArgs;
                if (ObjectValidator.IsEmptyOrNull(wuiArgs)) {
                    wuiArgs = this.programArgs.getTasks();
                }
                const agentArgs : ProgramArgs = new ProgramArgs();
                agentArgs.Parse(wuiArgs);
                if (!agentArgs.IsBaseTask() && !agentArgs.getOptions().noTarget) {
                    chain = ["cloud-manager:push", "builder-client:agent", "cloud-manager:pull", "client-report"];
                } else {
                    chain = ["builder-client:agent"];
                }
            } else {
                chain = ["builder-client:service"];
            }
            this.runTask($done, chain);
        }
    }
}
