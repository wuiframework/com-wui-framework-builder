/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Servers {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import BuilderHubConnector = Com.Wui.Framework.Builder.Connectors.BuilderHubConnector;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LiveContentResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.LiveContentResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IResponsePromise = Com.Wui.Framework.Localhost.Interfaces.IResponsePromise;
    import StdinType = Com.Wui.Framework.Localhost.Enums.StdinType;
    import TerminalOptions = Com.Wui.Framework.Services.Connectors.TerminalOptions;
    import ILiveContentProtocol = Com.Wui.Framework.Services.Interfaces.ILiveContentProtocol;
    import HttpServer = Com.Wui.Framework.Builder.HttpProcessor.HttpServer;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;
    import Terminal = Com.Wui.Framework.Builder.Connectors.Terminal;
    import IPropertiesStats = Com.Wui.Framework.Builder.Interfaces.IPropertiesStats;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class BuilderServer extends BaseTask {

        public static TaskProxy(...$args : string[]) : void {
            const startTime : number = new Date().getTime();
            const terminal : Terminal = Loader.getInstance().getTerminal();
            let cmd : string;
            const baseArgs : string[] = [];
            let path : string = "";
            if (!EnvironmentHelper.IsEmbedded()) {
                cmd = "node";
                baseArgs.push(Loader.getInstance().getProgramArgs().BinBase() + "/resource/javascript/loader.min.js");
                path = Loader.getInstance().getProgramArgs().BinBase() + "/../../dependencies/nodejs";
            } else {
                cmd = Loader.getInstance().getEnvironmentArgs().getAppName();
                if (!EnvironmentHelper.IsWindows()) {
                    cmd = "./" + cmd;
                }
            }

            const response : IResponse = ResponseFactory.getResponse(<any>$args[$args.length - 1]);
            $args.splice(-1, 1);
            let projectName : string = "undefined";
            let projectBase : string = "";
            const projectIndex = $args.indexOf("--project");
            if (projectIndex !== -1) {
                projectName = Loader.getInstance().getEnvironmentArgs().getRealProjectPath($args[projectIndex + 1]);
                $args.splice(projectIndex, 2);
            }
            const baseIndex = $args.indexOf("--base");
            if (baseIndex !== -1) {
                projectBase = Loader.getInstance().getEnvironmentArgs().getRealProjectPath($args[baseIndex + 1]);
                $args.splice(baseIndex + 1, 1);
                $args[baseIndex] = "--base=\"" + projectBase + "\"";
            }

            const env : any = process.env;
            env.IS_WUI_PROXY_TASK = 1;
            env.PATH = terminal.NormalizeEnvironmentPath(path);
            const terminalOptions : TerminalOptions = {
                advanced: {
                    colored: true
                },
                cwd     : Loader.getInstance().getProgramArgs().ProjectBase(),
                env
            };

            const terminalProxy : LiveContentResponse = new LiveContentResponse(response);
            terminalProxy.OnStart = () : IResponsePromise => {
                response.OnStart($args.join(" "));
                return {
                    Then: ($callback : ($data : string | ILiveContentProtocol) => void) : void => {
                        (<any>response).callbacks.onstart = $callback;
                    }
                };
            };
            terminalProxy.OnChange = ($stdout : string, $stdin? : any) : IResponsePromise => {
                if (ObjectValidator.IsSet($stdin)) {
                    response
                        .OnChange({
                            args  : $args,
                            hidden: $stdout.indexOf(StdinType.PASS) !== -1,
                            prefix: Buffer.from($stdout.replace(StdinType.STRING, "")
                                .replace(StdinType.PASS, "")).toString("base64")
                        })
                        .Then(($message : string) : void => {
                            $stdin.write($message + "\n");
                        });
                } else {
                    response.OnChange($stdout);
                }
                return {
                    Then: ($callback : ($data : string | ILiveContentProtocol) => void) : void => {
                        (<any>response).callbacks.onchange = $callback;
                    }
                };
            };
            const stats : IPropertiesStats = Loader.getInstance().getAppProperties().stats;
            terminalProxy.OnComplete = ($exitCode : number) : IResponsePromise => {
                const taskMinutes : number = new Date().getTime() - startTime;
                if ($args.indexOf("snippet") === -1 && !ObjectValidator.IsEmptyOrNull(stats.buildMinutes)) {
                    stats.buildMinutes += taskMinutes;
                    Loader.getInstance().getEnvironmentArgs().UpdateStats();
                }
                response.OnComplete({
                    buildMinutes: stats.buildMinutes,
                    cmd         : $args.join(" "),
                    exitCode    : $exitCode,
                    taskMinutes
                });
                return {
                    Then: ($callback : ($data : string | ILiveContentProtocol) => void) : void => {
                        (<any>response).callbacks.oncomplete = $callback;
                    }
                };
            };
            const createTaskProcess : any = () : void => {
                terminal.Spawn(cmd, baseArgs.concat($args), terminalOptions, terminalProxy);
            };

            const forwardArgs : ProgramArgs = new ProgramArgs();
            forwardArgs.Parse($args);
            if (!forwardArgs.IsBaseTask() && !forwardArgs.getOptions().noTarget && forwardArgs.IsAgentTask()) {
                const syncProxy : LiveContentResponse = new LiveContentResponse(response);
                syncProxy.OnStart = () : IResponsePromise => {
                    return {
                        Then: ($callback : ($data : string | ILiveContentProtocol) => void) : void => {
                            (<any>response).callbacks.onstart = $callback;
                        }
                    };
                };
                syncProxy.OnChange = ($stdout : string) : IResponsePromise => {
                    response.OnChange($stdout);
                    return {
                        Then: ($callback : ($data : string | ILiveContentProtocol) => void) : void => {
                            (<any>response).callbacks.onchange = $callback;
                        }
                    };
                };
                syncProxy.OnComplete = ($exitCode : number) : IResponsePromise => {
                    if ($exitCode === 0) {
                        createTaskProcess();
                    } else {
                        response.OnComplete({
                            cmd     : $args.join(" "),
                            exitCode: $exitCode
                        });
                    }
                    return {
                        Then: ($callback : ($data : string | ILiveContentProtocol) => void) : void => {
                            (<any>response).callbacks.oncomplete = $callback;
                        }
                    };
                };
                terminal.Spawn(cmd, baseArgs.concat([
                    "cloud-manager:pull", "--project=\"" + projectName + "\"", "--to=\"" + projectBase + "\"", "--no-target", "--agent-task"
                ]), terminalOptions, syncProxy);
            } else {
                createTaskProcess();
            }
        }

        protected getName() : string {
            return "builder-server";
        }

        protected process($done : any, $option : string) : void {
            if ($option === "start" || $option === "restart" || $option === "service") {
                const server : HttpServer = new HttpServer();
                server.setOnCloseHandler(() : void => {
                    Loader.getInstance().Exit();
                });
                server.getEvents().OnStart(() : void => {
                    if ($option === "start") {
                        $done();
                    }
                });
                server.StartConnector($option !== "start");
            } else if ($option === "agent") {
                const connector : BuilderHubConnector = new BuilderHubConnector(false);
                let platform : string = "unrecognized";
                if (EnvironmentHelper.IsWindows()) {
                    platform = "win";
                } else if (EnvironmentHelper.IsLinux()) {
                    platform = "linux";
                } else if (EnvironmentHelper.IsMac()) {
                    platform = "mac";
                }
                platform += EnvironmentHelper.Is64bit() ? "64" : "32";
                const responses : any = {};
                LogIt.Debug("Sending registration request for WUI Builder agent to WUI Hub ...");
                let domain : string = this.builderConfig.domain.location;
                if (ObjectValidator.IsEmptyOrNull(domain)) {
                    domain = Loader.getInstance().getHttpManager().getRequest().getServerIP();
                }
                let name : string = this.programArgs.AgentName();
                if (ObjectValidator.IsEmptyOrNull(name)) {
                    if (!ObjectValidator.IsEmptyOrNull(this.builderConfig.agent) &&
                        !ObjectValidator.IsEmptyOrNull(this.builderConfig.agent.name)) {
                        name = this.builderConfig.agent.name;
                    } else {
                        name = StringUtils.getSha1(this.programArgs.ProjectBase() + (new Date()).getTime().toString());
                    }
                }
                connector.getEvents().OnStart(() : void => {
                    connector
                        .RegisterAgent({
                            domain,
                            name,
                            platform,
                            version: this.builderConfig.version
                        })
                        .OnTaskRequest(($args : string[], $taskId : string) : void => {
                            const responseProxy : any = new LiveContentResponse(null);
                            const promise : any = (<any>responseProxy).callbacks;
                            const callback : any = ($type : EventType, $data : any) : IResponsePromise => {
                                connector.ForwardMessage($taskId, <IWuiBuilderTaskProtocol>{
                                    data  : $data,
                                    taskId: $taskId,
                                    type  : $type
                                });
                                return {
                                    Then: ($callback : (...$args : any[]) => void) : void => {
                                        promise[$type + ""] = $callback;
                                    }
                                };
                            };
                            responseProxy.OnStart = (...$args : any[]) : IResponsePromise => {
                                return callback(EventType.ON_START, $args[0]);
                            };
                            /// TODO: fix override of Then callback for ability to pass value into stdin
                            responseProxy.OnChange = (...$args : any[]) : IResponsePromise => {
                                return callback(EventType.ON_CHANGE, $args[0]);
                            };
                            responseProxy.OnComplete = (...$args : any[]) : IResponsePromise => {
                                return callback(EventType.ON_COMPLETE, $args[0]);
                            };
                            responseProxy.FireEvent = () : void => {
                                // default handler
                            };
                            responses[$taskId] = promise;
                            $args.push(responseProxy);
                            BuilderServer.TaskProxy.apply(this, $args);
                        })
                        .OnMessage(($data : any, $taskId : string) : void => {
                            if (responses.hasOwnProperty($taskId)) {
                                responses[$taskId].onchange($data);
                            }
                        })
                        .Then(($status : boolean) : void => {
                            if (!$status) {
                                LogIt.Warning("Unable to register agent!");
                                $done();
                            } else {
                                LogIt.Info("Agent registered.");
                            }
                        });
                });
                connector.StartCommunication();
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Skipped: WUI Builder server is not running.");
                $done();
            }
        }
    }

    export abstract class IWuiBuilderTaskProtocol {
        public builderId : string;
        public taskId : string;
        public data? : any;
        public type? : string;
    }
}
