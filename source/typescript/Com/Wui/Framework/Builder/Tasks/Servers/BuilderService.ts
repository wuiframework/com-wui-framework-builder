/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Servers {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BuilderServiceManager = Com.Wui.Framework.Builder.Connectors.BuilderServiceManager;

    export class BuilderService extends BaseTask {

        protected getName() : string {
            return "builder-service";
        }

        protected process($done : any, $option : string) : void {
            const asyncHandler : any = ($status : boolean, $message : string) : void => {
                if ($status) {
                    LogIt.Info($message);
                    $done();
                } else {
                    LogIt.Error($message);
                }
            };
            if ($option === "start") {
                BuilderServiceManager.Start(this.properties.projectBase, asyncHandler);
            } else if ($option === "stop") {
                BuilderServiceManager.Stop(this.properties.projectBase, asyncHandler);
            } else if ($option === "stop-all") {
                BuilderServiceManager.Clear(asyncHandler);
            } else {
                asyncHandler(false, "Unsupported service option: " + $option);
            }
        }
    }
}
