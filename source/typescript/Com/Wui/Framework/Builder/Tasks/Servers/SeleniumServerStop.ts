/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Servers {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;

    export class SeleniumServerStop extends BaseTask {

        protected getName() : string {
            return "selenium-server-stop";
        }

        protected process($done : any) : void {
            const port : number = this.properties.nodejsPort;
            const description : string = "Selenium server on port " + port;

            let cmd : string = "";
            if (EnvironmentHelper.IsWindows()) {
                cmd = "netstat -a -o -n | find \"0.0.0.0:" + port + "\"";
            } else {
                cmd = "netstat -anp 2> /dev/null | grep 0.0.0.0:" + port + " | egrep -o \"[0-9]+/node\" | cut -d\"/\" -f1";
            }
            this.terminal.Execute(cmd, [], null, ($exitCode : number, $std : string[]) : void => {
                    if ($exitCode === 0) {
                        let pid : number = null;
                        const stdout : string = $std[0];
                        if (EnvironmentHelper.IsWindows()) {
                            pid = parseInt(stdout.substring(stdout.length - 10), 10);
                            cmd = "taskkill /F /PID " + pid;
                        } else {
                            pid = parseInt(stdout, 10);
                            cmd = "kill -9 " + pid;
                        }
                        if (!isNaN(pid)) {
                            this.terminal.Execute(cmd, [], null, ($exitCode : number) : void => {
                                    if ($exitCode === 0) {
                                        LogIt.Info(description + " has been stopped.");
                                        $done();
                                    } else {
                                        LogIt.Error("Unable to stop " + description + ".");
                                    }
                                }
                            );
                        } else {
                            LogIt.Info(description + " has not been found.");
                            $done();
                        }
                    } else {
                        if ($std[0] + $std[1] !== "") {
                            LogIt.Error("Unable to stop " + description + ".");
                        } else {
                            LogIt.Info(description + " has not been found.");
                            $done();
                        }
                    }
                }
            );
        }
    }
}
