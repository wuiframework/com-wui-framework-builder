/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Servers {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BuilderServiceManager = Com.Wui.Framework.Builder.Connectors.BuilderServiceManager;
    import HttpServer = Com.Wui.Framework.Builder.HttpProcessor.HttpServer;

    export class Dashboard extends BaseTask {

        protected getName() : string {
            return "dashboard";
        }

        protected process($done : any, $option : string) : void {
            const asyncHandler : any = ($status : boolean, $message : string) : void => {
                if ($status) {
                    LogIt.Info($message);
                    $done();
                } else {
                    LogIt.Error($message);
                }
            };
            if ($option === "stop") {
                BuilderServiceManager.StopTask(this.properties.projectBase, "dashboard:start", asyncHandler);
            } else if ($option === "start") {
                const server : HttpServer = new HttpServer();
                server.setOnCloseHandler(() : void => {
                    Loader.getInstance().Exit();
                });
                server.getEvents().OnStart(() : void => {
                    if ($option !== "start") {
                        $done();
                    } else {
                        LogIt.Info("Dashboard has been started");
                    }
                });
                this.programArgs.AppDataPath(this.properties.binBase);
                server.Start();
            } else if ($option === "open") {
                this.terminal.Open("https://board.localhost.wuiframework.com/#/Settings/" + this.builderConfig.servicePort, $done);
            } else {
                BuilderServiceManager.StartTask(this.properties.projectBase, "dashboard:start", asyncHandler,
                    "Dashboard has been started");
            }
        }
    }
}
