/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Servers {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BuilderServiceManager = Com.Wui.Framework.Builder.Connectors.BuilderServiceManager;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import Resources = Com.Wui.Framework.Builder.DAO.Resources;

    export class MockServer extends BaseTask {
        private static register : any = {};

        public static getLocation() : string {
            const instanceKey : string = StringUtils.getSha1(
                StringUtils.Replace(Loader.getInstance().getProgramArgs().TargetBase(), "\\", "/"));
            if (MockServer.register.hasOwnProperty(instanceKey)) {
                return "http://localhost:" + MockServer.register[instanceKey];
            }
            return null;
        }

        protected getName() : string {
            return "mock-server";
        }

        protected process($done : any, $option : string) : void {
            const asyncHandler : any = ($status : boolean, $message : string) : void => {
                if ($status) {
                    LogIt.Info($message);
                    $done();
                } else {
                    LogIt.Error($message);
                }
            };

            if ($option === "start") {
                BuilderServiceManager.StartTask(this.properties.projectBase, "mock-server:service", asyncHandler,
                    "Mock server has been started");
            } else if ($option === "stop") {
                BuilderServiceManager.StopTask(this.properties.projectBase, "mock-server:service", asyncHandler);
            } else if (!ObjectValidator.IsEmptyOrNull(MockServer.getLocation())) {
                LogIt.Info("> skipped: mock server is already running");
                $done();
            } else {
                const http : Types.NodeJS.http = require("http");
                const fs : Types.NodeJS.fs = require("fs");
                const server : any = http.createServer();
                const configName : string = "MockServer.config.json";
                const configFiles : string[] = this.fileSystem.Expand([
                    this.properties.projectBase + "/dependencies/*/test/resource/config/**/" + configName,
                    this.properties.projectBase + "/test/resource/**/" + configName
                ]);
                const configuration : any = {};
                if (configFiles.length === 0) {
                    LogIt.Warning("Mock server has been skipped: Unable to find configuration file.");
                    $done();
                } else {
                    configFiles.forEach(($file : string) => {
                        Resources.Extend(configuration, JSON.parse(this.fileSystem.Read($file).toString()));
                    });
                    const logResponse : any = ($status : HttpStatusType, $headers : any, $body : string, $chunked : boolean) : void => {
                        LogIt.Debug("response status: {0}, headers: {1}, body: \"{2}\", chunked: {3}",
                            $status + "", JSON.stringify($headers, null, 2), $body, $chunked + "");
                    };
                    server.on("request", ($request : any, $response : any) : void => {
                        LogIt.Debug("request {0}, headers: {1}",
                            JSON.stringify($request.url, null, 2),
                            JSON.stringify($request.headers, null, 2));
                        if (configuration.hasOwnProperty($request.url)) {
                            const data : any = configuration[$request.url];
                            let status : number = 200;
                            if (data.hasOwnProperty("status")) {
                                status = data.status;
                            }
                            let headers : any = {};
                            if (data.hasOwnProperty("headers")) {
                                headers = data.headers;
                            }
                            $response.writeHead(status, headers);
                            let body : string = "";
                            let stream : Types.NodeJS.Modules.stream.Readable;
                            const chunked : boolean = $request.headers.hasOwnProperty("transfer-encoding") &&
                                StringUtils.ContainsIgnoreCase($request.headers["transfer-encoding"], "chunked");
                            if (data.hasOwnProperty("body")) {
                                if (ObjectValidator.IsDigit(data.body)) {
                                    logResponse(status, headers, "random with length " + Convert.IntegerToSize(data.body), chunked);
                                    const crypto : any = require("crypto");
                                    body = crypto.randomBytes(data.body).toString("hex");
                                    if (chunked) {
                                        const Readable : any = require("stream").Readable;
                                        stream = new Readable();
                                        stream.push(body);
                                        stream.push(null);
                                    }
                                } else {
                                    body = data.body;
                                    if (StringUtils.Contains(body, "\\", "/", ":")) {
                                        let path : string = body;
                                        if (!this.fileSystem.Exists(path)) {
                                            if (!StringUtils.StartsWith(path, "/")) {
                                                path = "/" + path;
                                            }
                                            path = this.properties.projectBase + path;
                                        }
                                        if (this.fileSystem.Exists(path) && this.fileSystem.IsFile(path)) {
                                            logResponse(status, headers, "from file " + body, chunked);
                                            if (chunked) {
                                                stream = fs.createReadStream(path);
                                            } else {
                                                body = this.fileSystem.Read(path).toString();
                                            }
                                        } else {
                                            LogIt.Warning("Configured body file has not been found at: " + path);
                                            logResponse(status, headers, "", chunked);
                                        }
                                    } else {
                                        logResponse(status, headers, body, chunked);
                                    }
                                }
                            } else {
                                logResponse(status, headers, body, chunked);
                            }

                            if (!ObjectValidator.IsEmptyOrNull(stream)) {
                                stream.pipe($response);
                            } else {
                                $response.write(body);
                                $response.end();
                            }
                        } else {
                            logResponse(HttpStatusType.NOT_FOUND, {}, "", false);
                            $response.writeHead(HttpStatusType.NOT_FOUND);
                            $response.end();
                        }
                    });

                    server.listen(0, () : void => {
                        try {
                            const instanceKey : string = StringUtils.getSha1(StringUtils.Replace(this.properties.projectBase, "\\", "/"));
                            MockServer.register[instanceKey] = server.address().port;
                            LogIt.Info("Mock server has been started at: http://localhost:" + MockServer.register[instanceKey]);
                            if ($option !== "service") {
                                $done();
                            }
                        } catch (ex) {
                            LogIt.Error("Unable to register instance of Mock server.", ex);
                        }
                    });
                }
            }
        }
    }
}
