/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Servers {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BuilderServiceManager = Com.Wui.Framework.Builder.Connectors.BuilderServiceManager;
    import BuilderHubConnector = Com.Wui.Framework.Builder.Connectors.BuilderHubConnector;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IWuiBuilderAgent = Com.Wui.Framework.Builder.Connectors.IWuiBuilderAgent;

    export class BuilderAgent extends BaseTask {

        protected getName() : string {
            return "builder-agent";
        }

        protected process($done : any, $option : string) : void {
            if ($option === "start") {
                BuilderServiceManager.StartTask(this.properties.projectBase, "builder-server:agent",
                    ($status : boolean, $message : string) : void => {
                        if ($status) {
                            LogIt.Info($message);
                            $done();
                        } else {
                            LogIt.Error($message);
                        }
                    }, "Agent registered");
            } else if ($option === "list") {
                const connector : BuilderHubConnector = new BuilderHubConnector();
                connector.getAgentsList().Then(($list : IWuiBuilderAgent[]) : void => {
                    Echo.Printf($list);
                    $done();
                });
            } else {
                LogIt.Error("Unsupported builder-agent option: " + $option);
            }
        }
    }
}
