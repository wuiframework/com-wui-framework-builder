/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Servers {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Network = Com.Wui.Framework.Builder.Connectors.Network;

    export class SeleniumServerStart extends BaseTask {

        protected getName() : string {
            return "selenium-server-start";
        }

        protected process($done : any) : void {
            const selenium : any = require("selenium-standalone");
            const version : string = "3.0.1";
            const drivers : any = {
                chrome: {
                    arch   : process.arch,
                    baseURL: "https://chromedriver.storage.googleapis.com",
                    version: "2.27"
                }
            };

            const start : any = () : void => {
                const port : number = this.properties.nodejsPort;
                LogIt.Info("Starting Selenium server v" + version + " on port " + port + " ...");
                selenium.start({
                    drivers,
                    seleniumArgs: ["-port", port],
                    spawnOptions: {
                        stdio: "inherit"
                    },
                    version
                }, ($err : Error) : void => {
                    if ($err === null) {
                        LogIt.Info("Selenium server v" + version + " on port " + port + " has been started.");
                        $done();
                    } else {
                        LogIt.Error($err.message, $err);
                    }
                });
            };

            const init : any = () : void => {
                const networking : Network = new Network();
                networking.IsPortFree(this.properties.nodejsPort, ($status : boolean) : void => {
                    if (!$status) {
                        networking.getFreePort(($port : number) : void => {
                            this.properties.nodejsPort = $port;
                            start();
                        });
                    } else {
                        start();
                    }
                });
            };
            if (this.fileSystem.Exists(process.env.NODE_PATH + "/selenium-standalone/.selenium/chromedriver") &&
                this.fileSystem.Exists(process.env.NODE_PATH + "/selenium-standalone/.selenium/selenium-server")) {
                init();
            } else {
                selenium.install({
                    baseURL: "https://selenium-release.storage.googleapis.com",
                    drivers,
                    logger($message : string) : void {
                        LogIt.Info($message);
                    },
                    version
                }, ($err : Error) : void => {
                    if ($err === null) {
                        init();
                    } else {
                        LogIt.Error($err.message, $err);
                    }
                });
            }
        }
    }
}
