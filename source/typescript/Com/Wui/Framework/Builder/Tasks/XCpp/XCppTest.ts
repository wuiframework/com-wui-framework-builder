/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.XCpp {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IXCppConfigs = Com.Wui.Framework.Builder.Interfaces.IXCppConfigs;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class XCppTest extends BaseTask {
        private static config : any;

        public static getConfig() : IXCppUnitConfig {
            if (ObjectValidator.IsEmptyOrNull(XCppTest.config)) {
                XCppTest.config = {
                    filter: "*",
                    target: "./build/target/UnitTestRunner"
                };
            }
            return XCppTest.config;
        }

        protected getName() : string {
            return "xcpp-test";
        }

        protected process($done : any, $option : string) : void {
            const path : Types.NodeJS.path = require("path");
            const xcppConfig : IXCppConfigs = XCppCompile.getConfig();
            const unitConfig : IXCppUnitConfig = XCppTest.getConfig();
            const gcovReporterPath : string = (Loader.getInstance().getProgramArgs().ProjectBase() +
                "/resource/libs/gcov_reporter").replace(/\//gi, path.sep);

            const runTest : any = ($target : string, $filter : string, $quiet : boolean, $callback : () => void) : void => {
                const buildPath : string =
                    (this.properties.projectBase + "/" + $target.substring(0, $target.lastIndexOf("/")).replace("./", ""))
                        .replace(/\//gi, path.sep);
                let cmd = unitConfig.target.substring($target.lastIndexOf("/") + 1);
                if (!EnvironmentHelper.IsWindows()) {
                    cmd = "./" + cmd;
                }

                this.terminal.Spawn(cmd,
                    ["--gtest_filter=" + $filter], buildPath, ($exitCode : number) : void => {
                        if (ObjectValidator.IsEmptyOrNull($quiet) || $quiet === false) {
                            if ($exitCode === 0) {
                                $callback();
                            } else {
                                LogIt.Error("XCpp unit task has failed");
                            }
                        } else {
                            $callback();
                        }
                    });
            };

            const runCoverage : any = () : void => {
                LogIt.Info("Run coverage collect.");

                this.fileSystem.Delete(xcppConfig.coverage.dest);
                this.fileSystem.Delete(xcppConfig.coverage.report);
                this.fileSystem.CreateDirectory(xcppConfig.coverage.dest);
                this.fileSystem.CreateDirectory(xcppConfig.coverage.report);

                const dest : string = path.join(this.properties.projectBase, xcppConfig.coverage.dest);
                const report : string = path.join(this.properties.projectBase, xcppConfig.coverage.report);
                let buildPath : string = this.properties.projectBase + "/build_cache";
                if (EnvironmentHelper.IsWindows()) {
                    buildPath += "/win";
                } else if (EnvironmentHelper.IsMac()) {
                    buildPath += "/mac";
                } else {
                    buildPath += "/linux";
                }

                this.terminal.Spawn("python", [
                    "gcov_reporter.py",
                    "-s", path.join(this.properties.projectBase, "source/cpp"),
                    "-d", path.join(buildPath),
                    "-o", dest,
                    "-r", report
                ], gcovReporterPath, ($exitCode : number) : void => {
                    if ($exitCode === 0) {
                        $done();
                    } else {
                        LogIt.Error("XCpp coverage task has failed");
                    }
                });
            };

            if (EnvironmentHelper.IsWindows()) {
                let cmd = unitConfig.target;
                if (!StringUtils.EndsWith(cmd, ".exe")) {
                    cmd += ".exe";
                }
                unitConfig.target = cmd;
            }

            switch ($option) {
            case "unit":
                if (!ObjectValidator.IsEmptyOrNull(unitConfig)) {
                    if (this.fileSystem.Exists(unitConfig.target)) {
                        runTest(
                            unitConfig.target,
                            unitConfig.filter,
                            false,
                            () : void => {
                                $done();
                            });
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " XCpp unit task skipped: " +
                            "target \"" + unitConfig.target + "\" has not been found");
                        $done();
                    }
                } else {
                    LogIt.Error("Configuration for XCpp unit task does not exist.");
                }
                break;
            case "coverage":
                if (!ObjectValidator.IsEmptyOrNull(unitConfig)) {
                    if (this.fileSystem.Exists(unitConfig.target)) {
                        runTest(
                            unitConfig.target,
                            unitConfig.filter,
                            true,
                            runCoverage);
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " XCpp unit task skipped: " +
                            "target \"" + unitConfig.target + "\" has not been found");
                        $done();
                    }
                } else {
                    LogIt.Error("Configuration for XCpp unit task does not exist.");
                }
                break;
            default:
                LogIt.Error("Skipping task xcpp-test: task \"" + $option + "\" not found.");
                break;
            }
        }
    }

    export abstract class IXCppUnitConfig {
        public target : string;
        public filter : string;
    }
}
