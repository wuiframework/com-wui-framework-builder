/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.XCpp {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import CopyScripts = Com.Wui.Framework.Builder.Tasks.Composition.CopyScripts;
    import IBuildArgs = Com.Wui.Framework.Builder.Interfaces.IBuildArgs;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;

    export class XCppCacheRegister extends BaseTask {
        private static registerPath : string = "";

        public static SaveRegister() : void {
            Loader.getInstance().getFileSystemHandler()
                .Write(XCppCacheRegister.registerPath, JSON.stringify(XCppCacheRegister.createRegisterData()));
        }

        public static IsChanged() : boolean {
            let retVal : boolean = true;

            if (Loader.getInstance().getFileSystemHandler().Exists(XCppCacheRegister.registerPath)) {
                retVal = Loader.getInstance().getFileSystemHandler()
                    .Read(XCppCacheRegister.registerPath).toString() !== JSON.stringify(XCppCacheRegister.createRegisterData());
            }

            return retVal;
        }

        private static createRegisterData() : any {
            const getScriptsHash : any = () : string => {
                let retVal : string = "";
                let item : string;
                const config : any = CopyScripts.getConfig();
                for (item in config.cmake) {
                    if (config.cmake.hasOwnProperty(item)) {
                        retVal += Loader.getInstance().getFileSystemHandler().Read("bin/" + config.cmake[item]).toString();
                    }
                }
                return StringUtils.getSha1(retVal);
            };

            const build : IBuildArgs = Loader.getInstance().getEnvironmentArgs().getBuildArgs();
            const project : IProject = Loader.getInstance().getProjectConfig();

            return {
                buildType         : build.type,
                cmakeHash         : getScriptsHash(),
                dependenciesConfig: project.dependencies,
                platformType      : build.platform,
                project           : project.name,
                releasesConfig    : project.releases,
                sourceHash        : StringUtils.getSha1(Loader.getInstance().getFileSystemHandler()
                    .Expand(Loader.getInstance().getAppProperties().projectBase + "/source/cpp/**/*").join("")),
                targetName        : project.target.name,
                toolchainType     : project.target.toolchain,
                version           : project.version
            };
        }

        constructor() {
            super();
            XCppCacheRegister.registerPath = this.properties.projectBase + "/build_cache/CacheRegister.json";
        }

        protected getName() : string {
            return "xcpp-cache-register";
        }
    }
}
