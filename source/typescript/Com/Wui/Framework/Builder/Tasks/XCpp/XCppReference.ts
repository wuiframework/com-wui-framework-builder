/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.XCpp {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IXCppConfigs = Com.Wui.Framework.Builder.Interfaces.IXCppConfigs;

    export class XCppReference extends BaseTask {

        protected getName() : string {
            return "xcpp-reference";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const conf : IXCppConfigs = XCppCompile.getConfig();
            const startTag : string = "// generated-code-start";
            const endTag : string = "// generated-code-end";
            const generated : string[] = [];
            let confBlock : string;
            for (confBlock in conf) {
                if (conf.hasOwnProperty(confBlock) && confBlock === $option) {
                    if (conf[confBlock].hasOwnProperty("reference")) {
                        const referenceFile : string = conf[confBlock].reference;
                        let src : string[] = conf[confBlock].src;
                        src.push("!" + this.properties.dependencies);
                        const hash : string = (referenceFile + src)
                            .replace(/\*/g, "")
                            .replace(/\./g, "")
                            .replace(/\//g, "")
                            .replace(/<%= /g, "")
                            .replace(/ %>/g, "")
                            .replace(/,/g, "");
                        if (generated.indexOf(hash) === -1) {
                            const referencePath : string = referenceFile.substring(0, referenceFile.lastIndexOf("/") + 1).replace("./", "");

                            let staticDataStart : string = "";
                            let staticDataEnd : string = "";
                            let dynamicData : string = "";
                            if (this.fileSystem.Exists(referenceFile)) {
                                const data : string = this.fileSystem.Read(referenceFile).toString();
                                let startIndex : number = data.indexOf(startTag);
                                if (startIndex === -1) {
                                    startIndex = data.indexOf("//grunt-start");
                                }
                                if (startIndex !== -1) {
                                    staticDataStart = data.substring(0, startIndex);
                                    let endIndex : number = data.indexOf(endTag);
                                    let endTagLength : number = endTag.length;
                                    if (endIndex === -1) {
                                        endIndex = data.indexOf("//grunt-end");
                                        endTagLength = 11;
                                    }
                                    if (endIndex !== -1) {
                                        staticDataEnd = data.substring(endIndex + endTagLength + os.EOL.length);
                                        if (staticDataEnd !== "") {
                                            let lastIndex : number = -1;
                                            const endMarks : string[] = ["#endif", "_H", "_H_", "_HPP_", "NOLINT"];
                                            for (const endMark of endMarks) {
                                                const dataEndIndex : number =
                                                    staticDataEnd.lastIndexOf(endMark) + endMark.length;
                                                if (lastIndex < dataEndIndex) {
                                                    lastIndex = dataEndIndex;
                                                }
                                            }
                                            staticDataEnd = staticDataEnd.substring(0, lastIndex);
                                        }
                                    }
                                }
                            }

                            if (typeof src === "string") {
                                src = [<any>src];
                            }
                            src.push("!" + referenceFile.replace("./", ""));
                            for (let srcIndex : number = 0; srcIndex < src.length; srcIndex++) {
                                src[srcIndex] = src[srcIndex].replace("*.cpp", "*.hpp");
                            }
                            const files : string[] = this.fileSystem.Expand(src);
                            for (let file of files) {
                                file = file.replace(referencePath, "");
                                if ((<any>file).endsWith(".hpp") &&
                                    staticDataStart.indexOf(file) === -1 &&
                                    staticDataEnd.indexOf(file) === -1) {
                                    dynamicData += os.EOL + "#include \"" + file + "\"";
                                }
                            }

                            let content : string = "";
                            if (staticDataStart !== "") {
                                content += staticDataStart;
                            }
                            content += startTag +
                                dynamicData + os.EOL +
                                endTag + os.EOL;
                            if (staticDataEnd !== "") {
                                content += staticDataEnd + os.EOL;
                            }
                            if (dynamicData !== "") {
                                LogIt.Info("Automatically added references to file \"" + referenceFile + "\":");
                                LogIt.Info(dynamicData);
                            }
                            if (staticDataStart !== "" || dynamicData !== "" || staticDataEnd !== "") {
                                let oldFileContent : string = "";
                                if (this.fileSystem.Exists(referenceFile)) {
                                    oldFileContent = this.fileSystem.Read(referenceFile).toString();
                                }
                                if (oldFileContent !== content) {
                                    this.fileSystem.Write(referenceFile, content);
                                } else {
                                    LogIt.Info("Reference file generation skipped: no change were found.");
                                }
                            }

                            generated.push(hash);
                        }
                    }
                }
            }

            $done();
        }
    }
}
