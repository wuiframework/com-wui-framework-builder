/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.XCpp {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IXCppConfigs = Com.Wui.Framework.Builder.Interfaces.IXCppConfigs;
    import IXCppConfig = Com.Wui.Framework.Builder.Interfaces.IXCppConfig;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class XCppIntefaces extends BaseTask {

        protected getName() : string {
            return "xcpp-interfaces";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const path : Types.NodeJS.path = require("path");
            const confs : IXCppConfigs = XCppCompile.getConfig();
            const generated : string[] = [];

            let classInfoList : CppClassDefinition = <any>{};
            const classInfoListName : string = "__$classInfoList";
            const interfaceBaseName : string = "__$interfaceBaseName";

            let interfacesBody : string = "";
            let ifDefine : string = "";
            let addIfDefine : boolean = true;
            const excludeProperty : any = {};
            let reflectionBody : string = "";

            const getClassInfo : any = ($classInfoName : string) : any => {
                if (!ObjectValidator.IsEmptyOrNull($classInfoName)) {
                    const $parts : string[] = $classInfoName.split("/");
                    let classInfo : CppClassDefinition = classInfoList;
                    for (const part of $parts) {
                        if (!classInfo.hasOwnProperty(part)) {
                            classInfo[part] = {};
                        }
                        classInfo = classInfo[part];
                    }
                    if (!classInfo.hasOwnProperty(classInfoListName)) {
                        classInfo.__$classInfoList = [];
                    }
                    return classInfo;
                }
                return null;
            };

            const generateClassInfoMap : any = ($filename : string, $rootdir : string) : void => {
                if (StringUtils.EndsWith($filename, ".hpp") &&
                    !StringUtils.EndsWith($filename, "interfacesMap.hpp") &&
                    !StringUtils.EndsWith($filename, "reference.hpp")) {
                    const subdir : string = path.dirname(StringUtils.Remove($filename, $rootdir + "/"));
                    if (subdir !== ".") {
                        const namespaceInfo : CppClassDefinition = getClassInfo(subdir);
                        if (StringUtils.EndsWith($filename, "sourceFilesMap.hpp")) {
                            namespaceInfo.__$interfaceBaseName = subdir;
                        }
                        if (namespaceInfo !== null) {
                            const data : string = this.fileSystem.Read($filename).toString();
                            let regex : RegExp = new RegExp(
                                "^(?:\\s*(template<.*?>)\\s*)?\\s*(class|enum|struct)\\s(([A-Za-z0-9]+::)*([A-Za-z]+[A-Za-z0-9]*))" +
                                "(?=\\s*(?:$)?(?:|(?:final)|:\\s*(?:$)?(?:(?:public|private|protected|public\\s+virtual|public\\s+final)" +
                                "\\s*)?(?:(?:$)?(?:(?:[A-Za-z0-9_]+::)*([a-zA-Z_]+[a-zA-Z0-9_]*)))\\s*)\\s*(?:$)?[,<{])", "im");

                            const def : RegExpMatchArray = data.match(regex);
                            if (def !== null && def.length >= 5) { // matched result array length corresponds to regex structure
                                const classNameDef : string = def[5];
                                const classAttribute : string = def[1];
                                const classEntityType : string = def[2];
                                const classBaseNameDef : string = def[6];
                                const classFullName : string = def[3];
                                let namespace : string = "";
                                regex = new RegExp("namespace\\s(\\w(?:\\w|::)+)\\s*{");
                                const ns : RegExpMatchArray = data.match(regex);
                                if (ns !== null && ns.length === 2) {
                                    namespace = ns[1];
                                }

                                const classInfo : CppClassDefinition = <any>{};
                                if (!ObjectValidator.IsEmptyOrNull(classNameDef)) {
                                    classInfo.__$namespace = namespace;
                                    classInfo.__$classEntityType = classEntityType;
                                    classInfo.__$classType = classNameDef;
                                    classInfo.__$classFullTypename = classFullName;
                                    if (!ObjectValidator.IsEmptyOrNull(classAttribute)) {
                                        classInfo.__$classAttribute = classAttribute
                                            .replace("/*", "")
                                            .replace("*/", "");
                                    }
                                    if (!ObjectValidator.IsEmptyOrNull(classBaseNameDef)) {
                                        classInfo.__$classBaseTypeDefName = classBaseNameDef;
                                    }
                                }
                                namespaceInfo.__$classInfoList.push(classInfo);
                            }
                        }
                    }
                }
            };

            const generateInterface : any = ($infoList : CppClassDefinition[], $padding : string) : void => {
                let item : string;
                for (item in $infoList) {
                    if (!excludeProperty.hasOwnProperty(item) && $infoList.hasOwnProperty(item)) {
                        if (item !== classInfoListName && item !== interfaceBaseName) {
                            interfacesBody += $padding + "namespace " + item + " {" + os.EOL;

                            if (addIfDefine) {
                                ifDefine += item.toUpperCase() + "_";
                            }
                        }
                        if ($infoList[item].hasOwnProperty(interfaceBaseName)) {
                            addIfDefine = false;
                        }
                        if ($infoList[item].hasOwnProperty(classInfoListName)) {
                            const classInfos : CppClassDefinition[] = $infoList[item].__$classInfoList;
                            for (const classInfo of classInfos) {
                                let typedef : string = classInfo.__$classEntityType;
                                let baseTypeDef : string = "";
                                if (classInfo.hasOwnProperty("__$classBaseTypeDefName") && typedef === "enum") {
                                    baseTypeDef = " : " + classInfo.__$classBaseTypeDefName;
                                }
                                if (classInfo.hasOwnProperty("__$classAttribute")) {
                                    typedef = classInfo.__$classAttribute + " " + typedef;
                                }
                                interfacesBody += $padding + "    " + typedef + " " + classInfo.__$classType + baseTypeDef + ";" +
                                    os.EOL;
                                addIfDefine = false;
                            }
                        }
                        if (item !== classInfoListName && typeof $infoList[item] === "object") {
                            generateInterface($infoList[item], $padding + "    ");
                        }
                        if (item !== classInfoListName && item !== interfaceBaseName) {
                            interfacesBody += $padding + "}" + os.EOL;
                        }
                    }
                }
            };

            const generateReflectionData : any = ($infoList : CppClassDefinition[], $padding : string) : void => {
                let item : string;
                for (item in $infoList) {
                    if (!excludeProperty.hasOwnProperty(item) && $infoList.hasOwnProperty(item)) {
                        if ($infoList[item].hasOwnProperty(classInfoListName)) {
                            const classInfos : CppClassDefinition[] = $infoList[item].__$classInfoList;

                            for (const classInfo of classInfos) {
                                const methodName : string = classInfo.__$namespace + "::" + classInfo.__$classFullTypename +
                                    "::Load";
                                if (methodName.match(/(?:::)?Loader::/)) {
                                    reflectionBody += "    {\"" + methodName.replace(/::/ig, ".") + "\"," +
                                        " boost::bind(&" + methodName + ", boost::placeholders::_1, boost::placeholders::_2)}," + os.EOL;
                                }
                            }
                        }
                        if (item !== classInfoListName && typeof $infoList[item] === "object") {
                            generateReflectionData($infoList[item], $padding);
                        }
                    }
                }
            };

            let confBlock : string;
            for (confBlock in confs) {
                if (confs.hasOwnProperty(confBlock) && confBlock === $option) {
                    if (confs[confBlock].hasOwnProperty("interface")) {
                        const conf : IXCppConfig = confs[confBlock];
                        const interfaceFile : string = this.properties.projectBase + "/" + conf.interface;
                        const reflectionFile : string = this.properties.projectBase + "/" + conf.reflection;
                        const src : string[] = conf.src;
                        src.push("!" + this.properties.dependencies);
                        const hash : string = (interfaceFile + src.join(""))
                            .replace(/\*/g, "")
                            .replace(/\./g, "")
                            .replace(/\//g, "")
                            .replace(/<%= /g, "")
                            .replace(/ %>/g, "")
                            .replace(/,/g, "");
                        if (generated.indexOf(hash) === -1) {
                            classInfoList = <any>{};
                            let isRootGenerated : boolean = false;
                            let subSrc : string;
                            const writeOnlyChanged : any = ($path : string, $data : string) => {
                                let oldFileContent : string = "";
                                if (this.fileSystem.Exists($path)) {
                                    oldFileContent = this.fileSystem.Read($path).toString();
                                }
                                if (oldFileContent !== $data) {
                                    this.fileSystem.Write($path, $data);
                                } else {
                                    LogIt.Info("File \"" + $path + "\" regeneration skipped: no change were found.");
                                }
                            };

                            for (subSrc in conf.srcBase) {
                                if (conf.srcBase.hasOwnProperty(subSrc)) {
                                    const foundRoot : string[] = this.fileSystem.Expand(conf.srcBase[subSrc]);
                                    if (foundRoot.length === 1) {
                                        if (isRootGenerated === false) {
                                            this.fileSystem.Expand(foundRoot[0] + "/**/*.*").forEach(($file : string) : void => {
                                                generateClassInfoMap($file, foundRoot[0]);
                                            });

                                            interfacesBody = "";
                                            ifDefine = "";
                                            addIfDefine = true;

                                            generateInterface(classInfoList, "");

                                            if (interfacesBody !== "") {
                                                LogIt.Info("Automatically added interfacesMap file to: \"" + interfaceFile + "\"");
                                                ifDefine += "INTERFACESMAP_HPP_";
                                                const interfacesMap : string =
                                                    "/** WARNING: this file has been automatically generated from C++ interfaces " +
                                                    "and classes, which exist in this package. */" + os.EOL +
                                                    "// NOLINT (legal/copyright)" + os.EOL + os.EOL +
                                                    "#ifndef " + ifDefine + "  // NOLINT" + os.EOL +
                                                    "#define " + ifDefine + os.EOL + os.EOL +
                                                    interfacesBody + os.EOL +
                                                    "#endif  // " + ifDefine + "  // NOLINT" + os.EOL;
                                                writeOnlyChanged(interfaceFile, interfacesMap);
                                                generated.push(hash);
                                            }
                                            isRootGenerated = true;
                                        } else {
                                            this.fileSystem.Expand(foundRoot[0] + "/**/*.*").forEach(($file : string) : void => {
                                                generateClassInfoMap($file, foundRoot[0]);
                                            });
                                        }
                                    }
                                }
                            }
                            reflectionBody = "";
                            generateReflectionData(classInfoList, "");

                            if (reflectionBody !== "") {
                                LogIt.Info("Automatically generated reflection data to: \"" + reflectionFile + "\"");

                                const reflectionMap : string = "" +
                                    "/** WARNING: This file contains data generated from all sources in project and dependencies. */" +
                                    os.EOL + os.EOL +
                                    "#ifndef REFLECTIONDATA_HPP_" + os.EOL +
                                    "#define REFLECTIONDATA_HPP_" + os.EOL + os.EOL +
                                    "#include <boost/bind.hpp>" + os.EOL +
                                    "#include \"../../source/cpp/reference.hpp\"" + os.EOL +
                                    os.EOL +
                                    "using std::string;" + os.EOL +
                                    os.EOL +
                                    "static std::map<string, std::function<int(const int, const char **)>> map = {" + os.EOL +
                                    reflectionBody +
                                    "};" + os.EOL +
                                    os.EOL +
                                    "class ReflectionData {" + os.EOL +
                                    " public:" + os.EOL +
                                    "    static std::map<string, std::function<int(const int, const char **)>> getReflectionData() {" +
                                    os.EOL +
                                    "        return map;" + os.EOL +
                                    "    }" + os.EOL +
                                    "};" + os.EOL +
                                    os.EOL +
                                    "#endif  // REFLECTION_DATA_HPP_" + os.EOL;
                                writeOnlyChanged(reflectionFile, reflectionMap);
                            }
                        }
                    }
                }
            }

            $done();
        }
    }

    export abstract class CppClassDefinition {
        public "__$classInfoList" : CppClassDefinition[];
        public "__$classType" : string;
        public "__$classAttribute" : string;
        public "__$classEntityType" : string;
        public "__$interfaceBaseName" : string;
        public "__$classBaseTypeDefName" : string;
        public "__$classFullTypename" : string;
        public "__$namespace" : string;
    }
}
