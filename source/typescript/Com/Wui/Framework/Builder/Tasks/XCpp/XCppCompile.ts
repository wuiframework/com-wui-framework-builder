/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.XCpp {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import IXCppConfigs = Com.Wui.Framework.Builder.Interfaces.IXCppConfigs;
    import IXCppConfig = Com.Wui.Framework.Builder.Interfaces.IXCppConfig;
    import IXCppConfigOptions = Com.Wui.Framework.Builder.Interfaces.IXCppConfigOptions;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ResourceHandler = Com.Wui.Framework.Builder.Utils.ResourceHandler;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;
    import StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;

    export class XCppCompile extends BaseTask {
        private static config : any;

        public static getConfig() : IXCppConfigs {
            if (ObjectValidator.IsEmptyOrNull(XCppCompile.config)) {
                const properties : IProperties = Loader.getInstance().getAppProperties();
                XCppCompile.config = {
                    /* tslint:disable: object-literal-sort-keys */
                    source  : {
                        src       : [properties.sources + "/*.cpp", properties.dependencies + "/*.cpp"],
                        srcBase   : [properties.sources + "/cpp", properties.dependencies + "/cpp"],
                        dest      : "build/target",
                        reference : "source/cpp/reference.hpp",
                        interface : "source/cpp/interfacesMap.hpp",
                        reflection: "build/compiled/reflectionData.hpp",
                        options   : {
                            cmakeDest: "build_cache"
                        }
                    },
                    unit    : {
                        src    : [properties.sources + "/*.cpp", properties.dependencies + "/*.cpp"],
                        dest   : "build/target",
                        options: {
                            targetName: "UnitTestRunner",
                            cmakeDest : "build_cache"
                        }
                    },
                    coverage: {
                        src   : [
                            properties.sources + "/*.cpp", properties.dependencies + "/*.cpp",
                            "test/unit/**/*.cpp", "dependencies/**/test/unit/**/*.cpp"
                        ],
                        dest  : "build/compiled/test/coverage/source/cpp",
                        report: "build/reports/coverage/source/cpp"
                    }
                    /* tslint:enable */
                };
            }
            return XCppCompile.config;
        }

        protected getName() : string {
            return "xcpp-compile";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const configs : IXCppConfigs = XCppCompile.getConfig();

            const binaryResource : any = ($options : IXCppConfigOptions, $isSharedLib? : boolean) : void => {
                const EOL : string = os.EOL;
                const versions : string[] = this.project.version.split("-")[0].split(".");
                let content : string = "";

                let isSharedLib : boolean = false;
                if (!ObjectValidator.IsEmptyOrNull($isSharedLib)) {
                    isSharedLib = $isSharedLib;
                }

                if (!isSharedLib) {
                    const iconPath : string = this.properties.projectBase + "/" + this.project.target.icon;
                    if (this.fileSystem.Exists(iconPath)) {
                        content += "0 ICON \"" + iconPath.replace(/\\/gi, "/") + "\"" + EOL + EOL;
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " XCpp compile task \"" + $option + "\": used default icon for executable, " +
                            "because custom icon has not been found at \"" + iconPath + "\"");
                    }
                }
                content += "1 VERSIONINFO" + EOL +
                    "FILEVERSION     " + versions[0] + "," + versions[1] + "," + versions[2] + ",0" + EOL +
                    "PRODUCTVERSION  " + versions[0] + "," + versions[1] + "," + versions[2] + ",0" + EOL +
                    "BEGIN" + EOL +
                    "   BLOCK \"StringFileInfo\"" + EOL +
                    "   BEGIN" + EOL +
                    "       BLOCK \"040904E4\"" + EOL +
                    "       BEGIN" + EOL +
                    "           VALUE \"ProductName\", \"" + $options.targetName + "\"" + EOL +
                    "           VALUE \"ProductVersion\", \"" + this.project.version + "\"" + EOL +
                    "           VALUE \"CompanyName\", \"" + this.project.target.companyName + "\"" + EOL +
                    "           VALUE \"FileDescription\", \"" + this.project.target.description + "\"" + EOL +
                    "           VALUE \"LegalCopyright\", \"" + this.project.target.copyright + "\"" + EOL +
                    "           VALUE \"InternalName\", \"" + this.project.name + "\"" + EOL +
                    "           VALUE \"FileVersion\", \"" + this.project.version + "\"" + EOL +
                    "           VALUE \"OriginalFilename\", \"" + this.project.name + (isSharedLib ? ".dll\"" : ".exe\"") + EOL +
                    "           VALUE \"BaseProjectName\", \"" + this.project.name + "\"" + EOL +
                    "       END" + EOL +
                    "   END" + EOL + EOL +
                    "   BLOCK \"VarFileInfo\"" + EOL +
                    "   BEGIN" + EOL +
                    "       VALUE \"Translation\", 0x409, 1252" + EOL +
                    "   END" + EOL +
                    "END";
                this.fileSystem.Write(this.properties.projectBase + "/build/compiled/version.rc", content);
            };

            const compile : any = ($options : IXCppConfigOptions, $dest : string, $withCoverage : boolean) : void => {
                if (!$options.hasOwnProperty("targetName") || $options.targetName === "") {
                    $options.targetName = this.project.target.name.replace(" ", "_");
                }
                if (this.build.platform === "app") {
                    if (EnvironmentHelper.IsWindows()) {
                        binaryResource($options);
                    } else if (EnvironmentHelper.IsLinux()) {
                        LogIt.Info(this.properties.projectBase + "\n" + this.project.target.name + "\n" +
                            this.project.target.icon + "\n" + this.project.target.description);
                        ResourceHandler.CreateDesktopEntryTemplate(this.project.target.name, this.project.target.icon);
                    }
                } else if (this.build.platform === "shared") {
                    if (EnvironmentHelper.IsWindows()) {
                        binaryResource($options, true);

                        const defs : string[] = this.fileSystem.Expand([
                            this.properties.projectBase + "/resource/configs/*.def"
                        ]);

                        let item : string;
                        for (item in defs) {
                            if (defs.hasOwnProperty(item)) {
                                this.fileSystem.Write("build/compiled/" + defs[item],
                                    StringReplace.Content(this.fileSystem.Read(defs[item]).toString(), StringReplaceType.VARIABLES,
                                        ($value : string) : string => {
                                            if (StringUtils.Contains($value, "project.version")) {
                                                return this.project.version.match(/\d+.\d+/)[0];
                                            }
                                            return $value;
                                        }));
                            }
                        }
                    }
                }

                let cmd : string = "cmake";
                let buildPath : string = this.properties.projectBase + "/build_cache";
                if ($options.hasOwnProperty("cmakeDest")) {
                    buildPath = $options.cmakeDest.replace("./", this.properties.projectBase + "/");
                }
                if (EnvironmentHelper.IsWindows()) {
                    if (StringUtils.StartsWith(this.project.target.toolchain, ToolchainType.MSVC)) {
                        if (this.fileSystem.Exists(this.builderConfig.vcvarsPath + "/vcvarsall.bat")) {
                            cmd = "vcvarsall.bat ";
                            if (EnvironmentHelper.Is64bit()) {
                                cmd += "x64 ";
                            }
                            cmd += "&& cmake";
                        } else {
                            LogIt.Info(">>"[ColorType.YELLOW] + " vcvarsall.bat not found. Validate global existence of " +
                                "MSBuild instance or specify correct path to script at {wui_builder_root}/private.conf.json " +
                                "by vcvarsPath.");
                        }
                    }
                    buildPath += "/win";
                } else if (EnvironmentHelper.IsMac()) {
                    buildPath += "/mac";
                } else {
                    buildPath += "/linux";
                }

                if (!this.fileSystem.Exists($dest)) {
                    this.fileSystem.CreateDirectory($dest);
                }
                if (!this.fileSystem.Exists(this.properties.projectBase + "/" + this.properties.compiled + "/lib")) {
                    this.fileSystem.CreateDirectory(this.properties.projectBase + "/" + this.properties.compiled + "/lib");
                }
                if (!this.fileSystem.Exists(this.properties.projectBase + "/" + this.properties.compiled + "/bin")) {
                    this.fileSystem.CreateDirectory(this.properties.projectBase + "/" + this.properties.compiled + "/bin");
                }

                const make : any = () : void => {
                    const args : string[] = ["--build", ".", "--target", $options.targetName, "--"];
                    if (this.project.target.toolchain === ToolchainType.GCC ||
                        this.project.target.toolchain === ToolchainType.ARM ||
                        this.project.target.toolchain === ToolchainType.CLANG) {
                        args.push("--jobs=" + EnvironmentHelper.getCores());
                    }
                    this.terminal.Spawn(cmd, args, buildPath, ($exitCode : number) : void => {
                        XCppCacheRegister.SaveRegister();
                        if (this.fileSystem.Expand(this.properties.projectBase + "/" + this.properties.compiled + "/lib/*").length === 0) {
                            this.fileSystem.Delete(this.properties.projectBase + "/" + this.properties.compiled + "/lib");
                        }
                        if (this.fileSystem.Expand(this.properties.projectBase + "/" + this.properties.compiled + "/bin/*").length === 0) {
                            this.fileSystem.Delete(this.properties.projectBase + "/" + this.properties.compiled + "/bin");
                        }
                        if ($exitCode === 0) {
                            $done();
                        } else {
                            LogIt.Error("XCpp compile task \"" + $option + "\" has failed");
                        }
                    });
                };

                if (!this.fileSystem.Exists(buildPath) || XCppCacheRegister.IsChanged()) {
                    this.fileSystem.CreateDirectory(buildPath);
                    let args : string[];
                    if (EnvironmentHelper.IsWindows()) {
                        if (StringUtils.StartsWith(this.project.target.toolchain, ToolchainType.MSVC)) {
                            args = ["-G", "\"NMake Makefiles\""];
                            if (this.project.target.toolchain !== ToolchainType.MSVC) {
                                let toolset : string = "";
                                if (this.project.target.toolchain === "msvcp120d") {
                                    toolset = "v120";
                                } else if (this.project.target.toolchain === "msvcp140d") {
                                    toolset = "v140";
                                } else {
                                    LogIt.Error("Supported MSVC types are only \"msvcp120d\" or \"msvcp140d\".");
                                }
                                if (toolset !== "") {
                                    args.push("-DCMAKE_VS_PLATFORM_TOOLSET=" + toolset);
                                }
                            }
                        } else if (this.project.target.toolchain === ToolchainType.GCC) {
                            args = ["-G", "\"MinGW Makefiles\""];
                            if ($withCoverage) {
                                args.push("-DCOVERAGE=1");
                            }
                        } else {
                            LogIt.Error("Windows platform supports only \"gcc\" or \"msvc\" toolchain.");
                        }
                    } else if (this.project.target.toolchain === ToolchainType.ARM) {
                        args = [
                            "-G", "\"Unix Makefiles\"",
                            "-DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc-8",
                            "-DCMAKE_CXX_COMPILER=aarch64-linux-gnu-g++-8"
                        ];
                    } else if (this.project.target.toolchain === ToolchainType.CLANG) {
                        args = [
                            "-G", "\"Unix Makefiles\"",
                            "-DCMAKE_C_COMPILER:PATH=" + this.externalModules + "/clang/cc",
                            "-DCMAKE_CXX_COMPILER:PATH=" + this.externalModules + "/clang/c++"
                        ];
                    } else {
                        args = ["-G", "\"Unix Makefiles\""];
                    }
                    this.terminal.Spawn(cmd, args.concat("../.."), {cwd: buildPath, env: process.env}, ($exitCode : number) : void => {
                        if ($exitCode === 0) {
                            make();
                        } else {
                            LogIt.Error("XCpp compile task \"" + $option + "\" has failed");
                        }
                    });
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " XCpp compile task \"" + $option + "\": " +
                        "cmake skipped, because makefile already exists");
                    make();
                }
            };

            if (!ObjectValidator.IsEmptyOrNull(configs)) {
                if (configs.hasOwnProperty($option)) {
                    let withCoverage : boolean = false;
                    if ($option === "coverage") {
                        $option = "unit";
                        withCoverage = true;
                    }
                    const config : IXCppConfig = configs[$option];
                    if (this.properties.projectHas.Cpp.Source()) {
                        compile(config.options, config.dest, withCoverage);
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " XCpp compile task \"" + $option + "\" skipped: " +
                            "0 source files have been found");
                        $done();
                    }
                } else {
                    LogIt.Error("Configuration for XCpp compile task \"" + $option + "\" does not exist.");
                }
            } else {
                LogIt.Error("Configuration for XCpp compile task does not exist.");
            }
        }
    }
}
