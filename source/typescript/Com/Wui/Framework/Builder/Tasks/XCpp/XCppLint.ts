/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.XCpp {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Linter = Com.Wui.Framework.XCppLint.Core.Linter;
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;
    import Resources = Com.Wui.Framework.Builder.DAO.Resources;

    export class XCppLint extends BaseTask {

        protected getName() : string {
            return "xcpp-lint";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const path : Types.NodeJS.path = require("path");
            const configs : IXCppLintConfigs = <any>{
                /* tslint:disable: object-literal-sort-keys */
                options     : {
                    root: {
                        source      : "source/cpp",
                        dependencies: "dependencies/*/source/cpp",
                        unit        : "test/unit/cpp"
                    }
                },
                source      : [
                    this.properties.sources + "/*.cpp",
                    this.properties.sources + "/*.h",
                    this.properties.sources + "/*.hpp"
                ],
                dependencies: [
                    this.properties.dependencies + "/*.cpp",
                    this.properties.dependencies + "/*.h",
                    this.properties.dependencies + "/*.hpp"
                ],
                unit        : [
                    "test/unit/**/*.cpp",
                    "test/unit/**/*.h",
                    "test/unit/**/*.hpp"
                ]
                /* tslint:enable */
            };
            const filters : any = JSON.parse(
                this.fileSystem.Read(this.properties.binBase + "/resource/configs/cpplint.conf.json").toString());
            const overridePath : string = this.properties.projectBase + "/resource/configs/cpplint.conf.json";
            if (this.fileSystem.Exists(overridePath)) {
                Resources.Extend(filters, JSON.parse(this.fileSystem.Read(overridePath).toString()));
            }
            const options : IXCppLintConfigOptions = {
                counting  : "detailed",
                extensions: ["h", "hpp", "cpp"],
                filters,
                headers   : ["h", "hpp"],
                linelength: 140,
                root      : {
                    dependencies: "",
                    source      : ""
                },
                verbose   : 0
            };

            const mergeOptions : any = ($parent : IXCppLintConfigOptions, $child : IXCppLintConfigOptions) : void => {
                let property : string;
                for (property in $child) {
                    if ($child.hasOwnProperty(property)) {
                        if ($parent.hasOwnProperty(property) && typeof $child[property] === "object" && $child[property] !== null) {
                            mergeOptions($parent[property], $child[property]);
                        } else {
                            $parent[property] = $child[property];
                        }
                    }
                }
            };

            const generateArgs : any = ($options : IXCppLintConfigOptions) : string[] => {
                const output : string[] = [];
                let property : string;
                let value : string;
                let filterSettings : string[];
                const resolveCategories : any = ($categoryName : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull(value)) {
                        const category : any = value[$categoryName];
                        const subcategories : string[] = Object.keys(category);
                        for (const subcategory of subcategories) {
                            let filterSetting : string = "";
                            if (category[subcategory]) {
                                filterSetting += "+";
                            } else {
                                filterSetting += "-";
                            }
                            filterSetting += $categoryName + "/" + subcategory;
                            filterSettings.push(filterSetting);
                        }
                    }
                };

                for (property in $options) {
                    if ($options.hasOwnProperty(property)) {
                        let key : string = property;

                        if (key === "root") {
                            value = $options[key][$option].replace("./", "").replace(/\\/gi, "/");
                            if ((<any>value).endsWith("/")) {
                                value = value.substr(0, value.length - 1);
                            }
                        } else {
                            value = $options[property];
                            if (!ObjectValidator.IsEmptyOrNull(value)) {
                                switch (key) {
                                case "filters":
                                    const categories : string[] = Object.keys(value);
                                    filterSettings = [];
                                    categories.forEach(resolveCategories);
                                    if (filterSettings.length > 0) {
                                        key = "filter";
                                        value = filterSettings.join(",");
                                    } else {
                                        value = "";
                                    }
                                    break;
                                case "headers":
                                case "extensions":
                                    value = value.length > 0 ? (<any>value).join(",") : "";
                                    break;
                                default:
                                    break;
                                }
                            }
                        }

                        if (value !== "") {
                            output.push("--" + key + "=" + value);
                        }
                    }
                }
                return output;
            };

            const cppcheck : any = ($options : IXCppLintConfigOptions, $files : string[]) : void => {
                let filesToCheck : number = $files.length;
                $files.forEach(($value : string) => {
                    if (StringUtils.EndsWith($value, "reference.hpp") ||
                        StringUtils.EndsWith($value, "interfacesMap.hpp") ||
                        StringUtils.EndsWith($value, "sourceFilesMap.hpp")) {
                        filesToCheck--;
                    }
                });
                if (filesToCheck > 0) {
                    if (this.properties.projectHas.WuiContent($options.root[$option].replace(/\//gi, path.sep) + "/**/*.cpp")) {
                        this.terminal.Spawn("cppcheck", [
                            "-j", os.cpus().length + "",
                            "--enable=warning,style,performance,portability",
                            "--inconclusive",
                            "-I", "\"" + (this.properties.projectBase + "/" + this.properties.compiled)
                                .replace(/\//gi, path.sep) + "\"",
                            "--inline-suppr",
                            "--suppress=syntaxError",
                            "--language=c++",
                            "--std=c++11",
                            "--platform=native",
                            "--template", "\"[{file}:{line}]: ({severity}:{id}) {message}\"",
                            "\"" + $options.root[$option].replace(/\//gi, path.sep) + "\""
                        ], this.properties.projectBase, ($exitCode : number, $std : string[]) : void => {
                            if ($exitCode === 0) {
                                LogIt.Info(">>"[ColorType.YELLOW] + " " + $files.length + " files lint free.");
                                $done();
                            } else {
                                if ($std[1].indexOf(":0]: (error:syntaxError) syntax error") > -1) {
                                    LogIt.Warning("XCpp lint (cppcheck task) warns about syntax error " +
                                        "(cppcheck do not understand file syntax which could be valid).");
                                    $done();
                                } else {
                                    LogIt.Error("XCpp lint[static check] task \"" + $option + "\" has failed");
                                }
                            }
                        });
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " " + $files.length + " files lint free. " +
                            "XCpp lint[static check] skipped 0 source files.");
                        $done();
                    }
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " CppCheck skipped: test root contains only project reference files.");
                    $done();
                }
            };

            const wuiLint : any = ($options : IXCppLintConfigOptions, $files : string[]) : void => {
                const linter : Linter = new Linter();
                $files.forEach(($file : string) : void => {
                    linter.Run(this.fileSystem.Read($file).toString(), $file, "");
                });
                linter.getErrors().forEach(($error : LintError) : void => {
                    LogIt.Warning($error.ToString());
                });
            };

            const compilator : any = ($options : IXCppLintConfigOptions, source : string[]) : void => {
                const fileBlockSize : number = 20;
                const cpplintPath : string = (Loader.getInstance().getProgramArgs().ProjectBase() +
                    "/resource/libs/cpplint").replace(/\//gi, path.sep);
                const files : string[] = this.fileSystem.Expand(source);

                if ($options.verbose < 0) {
                    $options.verbose = 0;
                }
                if ($options.verbose > 5) {
                    $options.verbose = 5;
                }

                const runCpplint : any = ($index : number) : void => {
                    const $files : string[] = files.slice($index * fileBlockSize, fileBlockSize * ($index + 1));
                    if ($files.length > 0) {
                        this.terminal.Spawn("python", ["cpplint.py"].concat(generateArgs($options)).concat($files), cpplintPath,
                            ($exitCode : number) : void => {
                                if ($exitCode === 0) {
                                    runCpplint($index + 1);
                                } else {
                                    LogIt.Error("XCpp lint task \"" + $option + "\" has failed");
                                }
                            });
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " Starting WUI XCppLint revalidation task...");
                        try {
                            wuiLint($options, files);
                        } catch (ex) {
                            LogIt.Info(">>"[ColorType.RED] + " WUI XCppLint error: " + ex.stack);
                        }
                        LogIt.Info(">>"[ColorType.YELLOW] + " XCpp lint task succeed. Starting cppcheck task...");
                        cppcheck($options, files);
                    }
                };

                let index : number;
                for (index = 0; index < files.length; index++) {
                    files[index] = (this.properties.projectBase + "/" + files[index].replace("./", "")).replace(/\//gi, path.sep);
                }

                runCpplint(0);
            };

            if (!ObjectValidator.IsEmptyOrNull(configs)) {
                if (configs.hasOwnProperty($option)) {
                    if (configs.hasOwnProperty("options")) {
                        mergeOptions(options, configs.options);
                    }
                    configs.options = options;

                    let tmp : string[] = [];
                    if (configs.options.root.source !== "") {
                        tmp = this.fileSystem.Expand(configs.options.root.source);
                        if (tmp.length === 1) {
                            configs.options.root.source = tmp[0];
                        }
                    }
                    if (configs.options.root.dependencies !== "") {
                        tmp = this.fileSystem.Expand(configs.options.root.dependencies);
                        if (tmp.length === 1) {
                            configs.options.root.dependencies = tmp[0];
                        }
                    }

                    // todo xcpplint will not work properly for more than one wui-dependencies

                    if (this.fileSystem.Expand(configs[$option]).length > 0) {
                        compilator(
                            configs.options,
                            configs[$option]);
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " XCpp lint task \"" + $option + "\" skipped: 0 source files have been found");
                        $done();
                    }
                } else {
                    LogIt.Error("Configuration for XCpp lint task \"" + $option + "\" does not exist.");
                }
            } else {
                LogIt.Error("Configuration for XCpp lint task does not exist.");
            }
        }
    }

    export abstract class IXCppLintRootOptions {
        public source : string;
        public dependencies : string;
    }

    export abstract class IXCppLintConfigOptions {
        public verbose : number;
        public filters : any;
        public counting : string;
        public root : IXCppLintRootOptions;
        public linelength : number;
        public extensions : string[];
        public headers : string[];
    }

    export abstract class IXCppLintConfigs {
        public options : IXCppLintConfigOptions;
        public source : string[];
        public dependencies : string[];
        public unit : string[];
    }
}
