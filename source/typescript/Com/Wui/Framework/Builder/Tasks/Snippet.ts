/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class Snippet extends BaseTask {

        protected getName() : string {
            return "snippet";
        }

        protected process($done : any, $option : string) : void {
            /* THIS TASK IS ONLY FOR DEVELOPMENT, DO NOT COMMIT CHANGES HERE */
            // cli usage: wui snippet [--no-target]

            LogIt.Info("Hello from WUI Builder snippet task");
            $done();
        }
    }
}
