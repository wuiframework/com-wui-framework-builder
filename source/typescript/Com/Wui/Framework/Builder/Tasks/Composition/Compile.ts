/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import GeneralTaskType = Com.Wui.Framework.Builder.Enums.GeneralTaskType;
    import CliTaskType = Com.Wui.Framework.Builder.Enums.CliTaskType;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;
    import TypeScriptCompile = Com.Wui.Framework.Builder.Tasks.TypeScript.TypeScriptCompile;

    export class Compile extends BaseTask {

        protected getName() : string {
            return "compile";
        }

        protected process($done : any, $option : string) : void {
            const taskList : string[] = [GeneralTaskType.VAR_REPLACE];
            if (this.properties.projectHas.TypeScript.Source()) {
                if ($option === CliTaskType.PROD || $option === CliTaskType.EAP) {
                    taskList.push("typescript-interfaces:source", "typescript-reference:source", "typescript:source",
                        "dev-blocks-cleanup", "uglify:prod", "uglify:loader");
                } else if ($option === CliTaskType.DEV) {
                    const options : any = TypeScriptCompile.getConfig().source.options;
                    options.removeComments = true;
                    options.sourceMap = true;
                    options.declaration = true;
                    taskList.push("cache-manager:generate-ts",
                        "typescript-interfaces:source", "typescript-reference:source", "cache-manager:prepare-ts",
                        "typescript:source", "uglify:loader", "cache-manager:finalize-ts", "copy-local-scripts:declaration");
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " TypeScript compile skipped: no *.ts files have been found");
            }
            if (this.properties.projectHas.SASS()) {
                if ($option === CliTaskType.DEV) {
                    taskList.push("cache-manager:generate-sass", "sass-compile:source", "cache-manager:finalize-sass");
                } else {
                    taskList.push("sass-compile:dependencies", "sass-compile:source", "cssmin");
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " SASS compile skipped: no *.scss files have been found");
            }
            if (this.project.target.toolchain === ToolchainType.GYP) {
                taskList.push("copy-staticfiles:targetresource", "gyp-compile", "copy-staticfiles:targetresource-postbuild");
            } else {
                if (this.properties.projectHas.Cpp.Source()) {
                    taskList.push(
                        "xcpp-interfaces:source", "xcpp-reference:source", "string-replace:xcpp", "copy-staticfiles:targetresource",
                        "xcpp-compile:source", "copy-staticfiles:targetresource-postbuild", "copy-staticfiles:xcppresx"
                    );
                    if ($option === CliTaskType.PROD) {
                        taskList.push("xcpp-compile:unit");
                    }
                } else {
                    taskList.push("copy-staticfiles:targetresource");
                    LogIt.Info(">>"[ColorType.YELLOW] + " C++ compile skipped: no *.cpp files have been found");
                }
            }
            if (this.properties.projectHas.Java.Source()) {
                taskList.push("maven-env:init", "maven-build", "maven-env:clean");
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " JAVA compile skipped: no *.java files have been found");
            }
            this.runTask($done, taskList);
        }
    }
}
