/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class CopyScripts extends BaseTask {
        private static config : any;

        public static getConfig() : any {
            if (ObjectValidator.IsEmptyOrNull(CopyScripts.config)) {
                CopyScripts.config = {
                    /* tslint:disable: object-literal-sort-keys */
                    typescript: [
                        "resource/scripts/UnitTestEnvironment.d.ts",
                        "resource/scripts/UnitTestRunner.ts"
                    ],
                    java      : [
                        "resource/scripts/pom.xml",
                        "resource/configs/javalibrary.config.xml",
                        "resource/configs/MANIFEST.MF",
                        "resource/configs/checkstyle-rules.xml",
                        "resource/configs/log4j2.xml",
                        "resource/configs/log4j2-test.xml",
                        "resource/scripts/UnitTest.java",
                        "resource/scripts/UnitTestRunner.java",
                        "resource/scripts/UnitTestListener.java",
                        "resource/scripts/Assert.java"
                    ],
                    eclipse   : [
                        "resource/configs/MANIFEST.BUNDLE.MF",
                        "resource/configs/plugin.eclipse.xml",
                        "resource/configs/fragment.e4xmi"
                    ],
                    idea      : [
                        "resource/configs/plugin.idea.xml"
                    ],
                    cpp       : [
                        "resource/scripts/UnitTestLoader.cpp",
                        "resource/scripts/UnitTestRunner.cpp",
                        "resource/scripts/UnitTestRunner.hpp",
                        "resource/scripts/boost_install.js",
                        "xcpp_scripts/**/*"
                    ],
                    cmake     : [
                        "resource/scripts/CMakeLists.txt",
                        "resource/scripts/boost_configure.cmake"
                    ],
                    phonegap  : [
                        "resource/configs/phonegap.config.xml"
                    ]
                    /* tslint:enable */
                };
            }
            return CopyScripts.config;
        }

        protected getName() : string {
            return "copy-scripts";
        }

        protected process($done : any, $option : string) : void {
            if (this.properties.projectBase !== "..") {
                const copyScript : any = CopyScripts.getConfig();

                if (copyScript.hasOwnProperty($option)) {
                    let patterns : string[] = [];
                    copyScript[$option].forEach(($pattern : string) : void => {
                        patterns.push(this.properties.binBase + "/" + $pattern);
                    });
                    let files : string[] = this.fileSystem.Expand(patterns);
                    const getPlatformFiles : any = ($platform : string) : void => {
                        if (copyScript.hasOwnProperty($platform) && copyScript[$platform].hasOwnProperty($option)) {
                            patterns = [];
                            copyScript[$platform][$option].forEach(($pattern : string) : void => {
                                patterns.push(this.properties.binBase + "/" + $pattern);
                            });
                            files = files.concat(this.fileSystem.Expand(patterns));
                        }
                    };
                    if (EnvironmentHelper.IsWindows()) {
                        getPlatformFiles("windowsOnly");
                    } else if (EnvironmentHelper.IsMac()) {
                        getPlatformFiles("macOnly");
                    } else {
                        getPlatformFiles("linuxOnly");
                    }

                    const binBase : string = this.properties.binBase + "/";
                    for (let file of files) {
                        file = StringUtils.Substring(file, StringUtils.IndexOf(file, binBase) + binBase.length);
                        const sourceFile : string = binBase + file;
                        const destFile : string = this.properties.projectBase + "/bin/" + file;
                        StringReplace.File(sourceFile, StringReplaceType.FOR_EACH, destFile);
                        StringReplace.File(destFile, StringReplaceType.VARIABLES);
                    }
                    for (let file of files) {
                        file = StringUtils.Substring(file, StringUtils.IndexOf(file, binBase) + binBase.length);
                        StringReplace.File(this.properties.projectBase + "/bin/" + file, StringReplaceType.IMPORTS_FINISH);
                    }
                    LogIt.Info("Scripts have been copied");
                } else {
                    LogIt.Info("Copy script task \"" + $option + "\" does not exist");
                }
            } else {
                LogIt.Info("Scripts copy skipped: running from local instance of WUI Builder");
            }

            $done();
        }
    }
}
