/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import CliTaskType = Com.Wui.Framework.Builder.Enums.CliTaskType;

    export class Install extends BaseTask {

        protected getName() : string {
            return CliTaskType.INSTALL;
        }

        protected process($done : any, $option : string) : void {
            let chain : string[] = [];
            if ($option === "eap" || $option === "dev") {
                let devInstall : string[] = [];
                if (!this.programArgs.IsAgentTask()) {
                    devInstall = [
                        "selfupdate:check", "wui-app-kill", "project-cleanup:build", "project-cleanup:source", "project-cleanup:test",
                        "install-local-script"
                    ];
                } else {
                    devInstall = [
                        "project-cleanup:build", "project-cleanup:source", "project-cleanup:test", "install-local-script"
                    ];
                }
                if ($option === "eap") {
                    chain = ["build-settings:prod"].concat(devInstall);
                } else {
                    chain = ["build-settings:dev"].concat(devInstall);
                }
            } else {
                const prodInstall : string[] = ["build-settings:prod"];
                if (!this.programArgs.IsAgentTask()) {
                    prodInstall.push("selfupdate:check");
                }
                chain = prodInstall.concat([
                    "wui-app-kill", "project-cleanup:build", "project-cleanup:cache", "project-cleanup:source", "project-cleanup:test",
                    "project-cleanup:all", "install-local-script:force"
                ]);
            }
            this.runTask($done, chain);
        }
    }
}
