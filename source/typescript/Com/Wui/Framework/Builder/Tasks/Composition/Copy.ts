/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class Copy extends BaseTask {

        protected getName() : string {
            return "copy";
        }

        protected process($done : any, $option : string) : void {
            const path : Types.NodeJS.path = require("path");
            const config : any = {
                configs: [
                    "build/compiled/resource/configs/wuirunner.config.jsonp",
                    "resource/configs/robots.txt"
                ],
                html   : [
                    this.properties.tmp + "/html/**/*",
                    "!**/noscript.html"
                ],
                license: [
                    "LICENSE.txt",
                    "SW-Content-Register.txt"
                ]
            };
            if (config.hasOwnProperty($option)) {
                this.fileSystem.Expand(config[$option]).forEach(($file : string) : void => {
                    this.fileSystem.Write(this.properties.projectBase + "/" + this.properties.dest + "/" + path.basename($file),
                        this.fileSystem.Read($file));
                });
            } else {
                LogIt.Error("Unsupported copy option \"" + $option + "\".");
            }
            $done();
        }
    }
}
