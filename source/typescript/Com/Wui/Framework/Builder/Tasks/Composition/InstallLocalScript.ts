/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import IProjectDependencyLocation = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocation;
    import IProjectDependency = Com.Wui.Framework.Builder.Interfaces.IProjectDependency;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import XCppCacheRegister = Com.Wui.Framework.Builder.Tasks.XCpp.XCppCacheRegister;

    export class InstallLocalScript extends BaseTask {

        private static cacheRegister : any;

        public static SaveRegister() : void {
            if (!ObjectValidator.IsEmptyOrNull(InstallLocalScript.cacheRegister)) {
                Loader.getInstance().getFileSystemHandler().Write(
                    Loader.getInstance().getProgramArgs().ProjectBase() + "/build_cache/CacheRegister.json",
                    JSON.stringify(InstallLocalScript.cacheRegister));
            }
        }

        protected getName() : string {
            return "install-local-script";
        }

        protected process($done : any, $option : string) : void {
            const tasks : string[] = [];
            let hasAnyDependency : boolean = false;
            let dependencyName : string;

            if (this.project.hasOwnProperty("dependencies")) {
                if (this.project.dependencies instanceof Array && this.project.dependencies.length > 0) {
                    hasAnyDependency = true;
                }
                for (dependencyName in this.project.dependencies) {
                    if (this.project.dependencies.hasOwnProperty(dependencyName)) {
                        hasAnyDependency = true;
                        break;
                    }
                }
            }

            const dependencyExists : boolean = this.fileSystem.Expand(this.properties.projectBase + "/dependencies/*").length > 0;
            let initProject : boolean = $option === "force" || hasAnyDependency && !dependencyExists;
            if (initProject && hasAnyDependency && !dependencyExists) {
                initProject = false;
                const resolvePath : any = ($value : any) : string => {
                    if ((<any>$value).startsWith("dir+")) {
                        $value = StringUtils.Remove($value, "dir+");
                    }
                    const branchTag : number = StringUtils.IndexOf($value, "#");
                    if (branchTag !== -1) {
                        $value = StringUtils.Substring($value, 0, branchTag);
                    }
                    return $value;
                };
                let path : string[] = [];
                for (dependencyName in this.project.dependencies) {
                    if (this.project.dependencies.hasOwnProperty(dependencyName)) {
                        const dependency : IProjectDependency = this.project.dependencies[dependencyName];
                        if (ObjectValidator.IsString(dependency)) {
                            path.push(resolvePath(dependency));
                        } else if (ObjectValidator.IsString(dependency.location)) {
                            path.push(resolvePath(dependency.location));
                        } else if (ObjectValidator.IsObject(dependency.location) && dependency.location.hasOwnProperty("path")) {
                            if (!dependency.location.hasOwnProperty("type") ||
                                (<IProjectDependencyLocation>dependency.location).type !== "mvn") {
                                path = path.concat(new DependenciesDownload().ResolveDependencyPath(
                                    (<IProjectDependencyLocation>dependency.location).path));
                            }
                        }
                    }
                }

                path.forEach(($value : string) : void => {
                    if (StringUtils.StartsWith($value, "$")) {
                        const index : number = StringUtils.IndexOf($value, "$", true, 1);
                        const platform : string = StringUtils.Substring($value, 1, index);
                        $value = StringUtils.Substring($value, index + 1) + "/" + platform;
                    }
                    if (!StringUtils.StartsWith($value, "mvn+") && !this.fileSystem.Exists($value)) {
                        initProject = true;
                    }
                });
            }

            if (!initProject && !this.fileSystem.Exists(this.properties.projectBase + "/.git/hooks/pre-commit")) {
                initProject = true;
            }

            if (initProject || $option === "force") {
                tasks.push("git-manager:hooks", "project-cleanup:bin");
            }
            if (initProject) {
                if (!this.programArgs.IsAgentTask()) {
                    tasks.push("selfinstall");
                }
                tasks.push("dependencies-download", "string-replace:dependencies", "dependencies-sort",
                    "copy-local-scripts", "dependencies-install");
            } else {
                tasks.push("dependencies-sort", "copy-local-scripts");
                if (!this.fileSystem.Exists(this.properties.projectBase + "/build_cache") && !this.build.isTest) {
                    tasks.push("dependencies-install");
                } else {
                    tasks.push("dependencies-install:configure");
                }
            }
            if (this.properties.projectHas.Cpp.Source()) {
                if (XCppCacheRegister.IsChanged() || initProject) {
                    tasks.push("copy-scripts:cmake");
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " Copy of CMake scripts skipped: " +
                        "project structure changes has not been detected");
                }
            }
            if (initProject) {
                tasks.push("project-cleanup");
            }

            this.runTask($done, tasks);
        }
    }
}
