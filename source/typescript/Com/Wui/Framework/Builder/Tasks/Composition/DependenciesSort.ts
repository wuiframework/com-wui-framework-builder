/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import IProjectDependency = Com.Wui.Framework.Builder.Interfaces.IProjectDependency;
    import IProjectDependencyLocation = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocation;
    import IMavenArtifact = Com.Wui.Framework.Builder.Interfaces.IMavenArtifact;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Resources = Com.Wui.Framework.Builder.DAO.Resources;

    export class DependenciesSort extends BaseTask {

        constructor() {
            super();
        }

        protected getName() : string {
            return "dependencies-sort";
        }

        protected isPlatformMatch($platform : string) : boolean {
            let output : boolean = false;

            for (const product of (new Com.Wui.Framework.Builder.Tasks.BuildProcessor.BuildExecutor()).getTargetProducts()) {
                if (product.OS().toString() === $platform) {
                    output = true;
                    break;
                }
            }

            return output;
        }

        protected process($done : any) : void {
            this.properties.cppIncludes = "";
            this.properties.cppLibraries = "";
            let dependencyName : string;
            let dependencies : any = {};

            if (this.project.hasOwnProperty("dependencies")) {
                dependencies = JSON.parse(JSON.stringify(this.project.dependencies));
                const files : string[] = this.fileSystem.Expand(this.properties.projectBase + "/dependencies/*/package.conf.json");
                files.forEach(($file : string) : void => {
                    const config : IProject = JSON.parse(this.fileSystem.Read($file).toString());
                    if (config.hasOwnProperty("dependencies")) {
                        for (dependencyName in config.dependencies) {
                            if (config.dependencies.hasOwnProperty(dependencyName)) {
                                if (!dependencies.hasOwnProperty(dependencyName)) {
                                    dependencies[dependencyName] = config.dependencies[dependencyName];
                                } else if (ObjectValidator.IsObject(dependencies[dependencyName]) &&
                                    ObjectValidator.IsObject(config.dependencies[dependencyName])) {
                                    // todo this inheritance will work properly only in single chain, to achieve proper extension
                                    //  a full dependency chain has to be resolved first!
                                    dependencies[dependencyName] = Resources.Extend(config.dependencies[dependencyName],
                                        dependencies[dependencyName]);
                                }
                            }
                        }
                    }
                });
            }

            this.project.dependencies = <any>{};
            for (dependencyName in dependencies) {
                if (dependencies.hasOwnProperty(dependencyName)) {
                    if (!this.project.dependencies.hasOwnProperty(dependencyName)) {
                        let isRequired : boolean = false;
                        if (!ObjectValidator.IsString(dependencies[dependencyName]) &&
                            dependencies[dependencyName].hasOwnProperty("platforms")) {
                            if (!ObjectValidator.IsArray(dependencies[dependencyName].platforms)) {
                                dependencies[dependencyName].platforms = [<string>dependencies[dependencyName].platforms];
                            }
                            (<string[]>dependencies[dependencyName].platforms).forEach(($platforms : string) : void => {
                                if (this.isPlatformMatch($platforms)) {
                                    isRequired = true;
                                }
                            });
                        } else {
                            isRequired = true;
                        }
                        if (isRequired) {
                            this.project.dependencies[dependencyName] = dependencies[dependencyName];
                        }
                    }
                }
            }
            for (dependencyName in this.project.dependencies) {
                if (this.project.dependencies.hasOwnProperty(dependencyName)) {
                    if (typeof this.project.dependencies[dependencyName] === "object") {
                        this.project.dependencies[dependencyName].name = dependencyName;
                    } else {
                        this.project.dependencies[dependencyName] = <any>{
                            location: this.project.dependencies[dependencyName],
                            name    : dependencyName
                        };
                    }
                    const dependency : IProjectDependency = this.project.dependencies[dependencyName];
                    if (!this.fileSystem.Exists(
                        this.properties.projectBase + "/dependencies/" + dependencyName + "/package.conf.json")) {
                        if (typeof dependency === "object" && (
                            typeof dependency.location === "string" && StringUtils.StartsWith(<string>dependency.location, "mvn+") ||
                            typeof dependency.location === "object" &&
                            (<IProjectDependencyLocation>dependency.location).type === "mvn")) {
                            this.properties.javaDependencies.push(dependency);
                            let location : string = "";
                            if (typeof dependency.location === "string") {
                                location = (<string>dependency.location);
                            } else {
                                location = <string>(<IProjectDependencyLocation>dependency.location).path;
                            }
                            location = location.replace("mvn+", "");

                            this.properties.mavenArtifacts.push(<IMavenArtifact>{
                                artifactId: dependency.name,
                                groupId   : dependency.groupId,
                                location,
                                owner     : this.project.name,
                                version   : dependency.version
                            });
                            this.properties.mavenRepositories.push({
                                id : dependency.groupId + "-" + dependency.name + "-" + dependency.version,
                                url: location
                            });
                        } else if (dependency.hasOwnProperty("configure-script")) {
                            const location : IProjectDependencyLocation[] =
                                (new Com.Wui.Framework.Builder.Tasks.Composition.DependenciesDownload()).ParseLocation(dependency.location);
                            if (location.length === 1 && !this.fileSystem.Exists(<string>(location[0].path))) {
                                dependency.location = location[0];
                                dependency.location.path = this.properties.projectBase + "/dependencies/" + dependencyName;
                            }
                            this.properties.cppDependencies.push(dependency);
                            if (this.properties.cppIncludes !== "") {
                                this.properties.cppIncludes += " ";
                            }
                            if (this.properties.cppLibraries !== "") {
                                this.properties.cppLibraries += " ";
                            }
                            this.properties.cppIncludes += "${" + dependencyName.toUpperCase().replace(/-/, "_") + "_INCLUDE_DIRS}";
                            this.properties.cppLibraries += "${" + dependencyName.toUpperCase().replace(/-/, "_") + "_LIBRARIES}";
                        }
                    } else {
                        this.properties.wuiDependencies.push(dependency);
                        this.properties.mavenResourcePaths.push(
                            "${project.basedir}/dependencies/" + dependency.name + "/resource/");
                    }
                }
            }
            $done();
        }
    }
}
