/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;

    export class Upx extends BaseTask {

        protected getName() : string {
            return "upx";
        }

        protected process($done : any) : void {
            if (this.project.target.upx.enabled) {
                let executables : string[] = this.fileSystem.Expand(this.properties.projectBase + "/build/*.{exe,dll}");
                if (executables.length === 0) {
                    executables = this.fileSystem.Expand(this.properties.projectBase + "/build/target/**/*.{exe,dll}");
                }

                const nextExecutable : any = ($index : number) : void => {
                    if ($index < executables.length) {
                        this.pack(executables[$index], () : void => {
                            nextExecutable($index + 1);
                        });
                    } else {
                        $done();
                    }
                };
                nextExecutable(0);
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Executable pack skipped: target configuration is missing enabled upx");
                $done();
            }
        }

        private pack($executablePath : string, $callback : () => void) : void {
            if (this.fileSystem.Exists($executablePath)) {
                if (EnvironmentHelper.IsWindows()) {
                    const options : string[] = <string[]>JSON.parse(JSON.stringify(this.project.target.upx.options));
                    options.push("-o\"" + $executablePath + ".pack" + "\"");
                    options.push("\"" + $executablePath + "\"");
                    this.terminal.Spawn("upx.exe", options, this.externalModules + "/upx/",
                        ($exitCode : number) : void => {
                            if ($exitCode !== 0) {
                                if ($exitCode === 2) {
                                    LogIt.Warning(">>"[ColorType.YELLOW] + " Executable is already packed.");
                                    $callback();
                                } else {
                                    LogIt.Error("Upx has failed");
                                }
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " Executable has been packed successfully");
                                this.fileSystem.Delete($executablePath);
                                this.fileSystem.Rename($executablePath + ".pack", $executablePath);
                                $callback();
                            }
                        });
                } else if (EnvironmentHelper.IsMac()) {
                    LogIt.Warning(">>"[ColorType.YELLOW] + " Upx is not currently supported on MAC");
                    $callback();
                } else {
                    LogIt.Warning(">>"[ColorType.YELLOW] + " Upx is not currently supported on LINUX");
                    $callback();
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Upx skipped: executable for pack does not exit");
                $callback();
            }
        }
    }
}
