/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IProjectTargetResources = Com.Wui.Framework.Builder.Interfaces.IProjectTargetResources;
    import IProjectSplashScreenResource = Com.Wui.Framework.Builder.Interfaces.IProjectSplashScreenResource;
    import ResourceHandler = Com.Wui.Framework.Builder.Utils.ResourceHandler;
    import ResourceType = Com.Wui.Framework.Builder.Enums.ResourceType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IProjectDependencyLocation = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocation;
    import IProjectTargetResourceFiles = Com.Wui.Framework.Builder.Interfaces.IProjectTargetResourceFiles;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;

    export class CopyStaticFiles extends BaseTask {

        protected getName() : string {
            return "copy-staticfiles";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const path : Types.NodeJS.path = require("path");
            const fs : Types.NodeJS.fs = require("fs");
            const encoding : any = require("encoding");
            const istextorbinary : any = require("istextorbinary");

            let embResourceHeaderTemplate : string = "" +
                "/*" + os.EOL +
                "    WARNING: This file contains data generated from resources." + os.EOL +
                "    Please check if all used resources match with application license" + os.EOL +
                "*/" + os.EOL +
                "#ifndef RESOURCES_DATA_H_" + os.EOL +
                "#define RESOURCES_DATA_H_" + os.EOL +
                os.EOL +
                "#include <stdlib.h>" + os.EOL +
                "#include <string>" + os.EOL +
                os.EOL +
                "#include <boost/unordered_map.hpp>" + os.EOL +
                "#include <boost/assign.hpp>" + os.EOL +
                os.EOL +
                "namespace Resources {" + os.EOL +
                os.EOL +
                "    namespace _private_resource_data {" + os.EOL +
                os.EOL +
                "<? @data ?>" +
                "<? @map ?>" + os.EOL +
                "    };" + os.EOL +
                os.EOL +
                "    class ResourcesData {" + os.EOL +
                "    private:" + os.EOL +
                "        static std::string *getResourceItem(const std::string &key) {" + os.EOL +
                "            boost::unordered_map<std::string, std::string *>::const_iterator item = " + os.EOL +
                "                    _private_resource_data::map.find(key);" + os.EOL +
                "            if (item != _private_resource_data::map.end()) {" + os.EOL +
                "                return item->second;" + os.EOL +
                "            } else {" + os.EOL +
                "                return nullptr;" + os.EOL +
                "            }" + os.EOL +
                "        }" + os.EOL +
                os.EOL +
                "    public:" + os.EOL +
                "        static const std::string getString(const std::string &key) {" + os.EOL +
                "            std::string *str = ResourcesData::getResourceItem(key);" + os.EOL +
                "            if (str != nullptr) {" + os.EOL +
                "                return std::string(*str);" + os.EOL +
                "            } else {" + os.EOL +
                "                return \"\";" + os.EOL +
                "            }" + os.EOL +
                "        };" + os.EOL +
                os.EOL +
                "        static void setString(const std::string &key, const std::string &data, const bool append = false) {" + os.EOL +
                "            std::string *str = ResourcesData::getResourceItem(key);" + os.EOL +
                "            if (str != nullptr) {" + os.EOL +
                "                if (append) {" + os.EOL +
                "                    str->append(data);" + os.EOL +
                "                } else {" + os.EOL +
                "                    str->assign(data);" + os.EOL +
                "                }" + os.EOL +
                "            } else {" + os.EOL +
                "                _private_resource_data::map.emplace(key, new std::string(data));" + os.EOL +
                "            }" + os.EOL +
                "        };" + os.EOL +
                "    };" + os.EOL +
                "};" + os.EOL +
                os.EOL +
                "#endif //RESOURCES_DATA_H_";

            const embResourceHeaderTemplateData : string = "" +
                "        static const uint8_t <? @name ?>Data[] = {" + os.EOL +
                "                <? @embData ?>" +
                "        };" + os.EOL +
                os.EOL +
                "        static std::string <? @name ?>StringUtils((char *) <? @name ?>Data, sizeof(<? @name ?>Data));" + os.EOL +
                os.EOL;

            let embResourceHeaderTemplateMap : string = "" +
                "        // map for all serialized resources" + os.EOL +
                "        static boost::unordered_map<std::string, std::string *> map = boost::assign::map_list_of<? @resMap ?>;";

            let embResourceDataMap : string = "<? @res ?>";

            let anyEmbeddedResource : boolean = false;

            const flattenResources : any = () : IProjectTargetResources[] => {
                const resources : IProjectTargetResources[] = [];
                this.project.target.resources.forEach(($resource : IProjectTargetResources) : void => {
                    if ($resource.hasOwnProperty("files")) {
                        $resource.files.forEach(($file : IProjectTargetResourceFiles) : void => {
                            const cwd : string = $file.hasOwnProperty("cwd") ? $file.cwd + "/" : "";
                            const addResource : any = ($source : string) : void => {
                                const resource : IProjectTargetResources = <any>{
                                    input: cwd + $source
                                };
                                if ($file.hasOwnProperty("dest")) {
                                    resource.output = $file.dest;
                                }
                                [
                                    "skip-replace",
                                    "copy",
                                    "embed",
                                    "platforms",
                                    "toolchain",
                                    "resx",
                                    "postBuildCopy",
                                    "keepLinks"
                                ].forEach(($parameter : string) : void => {
                                    if ($resource.hasOwnProperty($parameter)) {
                                        resource[$parameter] = $resource[$parameter];
                                    }
                                });
                                resources.push(resource);
                            };
                            if (ObjectValidator.IsString($file.src)) {
                                addResource($file.src);
                            } else {
                                (<string[]>$file.src).forEach(($input : string) : void => {
                                    addResource($input);
                                });
                            }
                        });
                    } else {
                        resources.push($resource);
                    }
                });
                return resources;
            };

            const copy : any = ($root : string, $cwd : string, $dest : string, $exclude : string, $callback : () => void) : void => {
                let filesRoot : string = "";
                if ($root !== "") {
                    $root += "/";
                    filesRoot = $root + "*/";
                }
                const expandPaths : string[] = [filesRoot + $cwd + "/**/*"];
                if (StringUtils.Contains($root, "dependencies")) {
                    expandPaths.push(filesRoot + this.build.product.OS().toString() + "/" + $cwd + "/**/*");
                }
                if ($exclude && $exclude !== "") {
                    expandPaths.push("!" + $exclude);
                }
                for (let index : number = 0; index < expandPaths.length; index++) {
                    if (!StringUtils.StartsWith(expandPaths[index], this.properties.projectBase)) {
                        expandPaths[index] = this.properties.projectBase + "/" + expandPaths[index];
                    }
                }
                const files : string[] = this.fileSystem.Expand(expandPaths);
                if (!$dest || $dest === "") {
                    $dest = this.properties.dest + "/" + $cwd;
                } else {
                    $dest = this.properties.dest + "/" + $dest;
                }

                if (files.length > 0) {
                    LogIt.Info("copy directory: \"" + $root + $cwd + "\" to \"" + $dest + "\", items found count: " + files.length);

                    const processFile : any = ($index : number) : void => {
                        const next : any = () : void => {
                            processFile($index + 1);
                        };
                        if ($index < files.length) {
                            const file : string = files[$index];
                            if (!StringUtils.EndsWith(file, "/Loader.js")) {
                                let path : string =
                                    $dest + file.substr(file.indexOf($cwd) + $cwd.length, file.length);
                                if (fs.statSync(file).isFile()) {
                                    if (StringUtils.EndsWith(path, ".jsonp")) {
                                        if (this.programArgs.IsDebug()) {
                                            path = path.replace(".jsonp", ".js");
                                        }
                                        StringReplace.File(file, StringReplaceType.VARIABLES, path);
                                        next();
                                    } else {
                                        this.fileSystem.Copy(file, path, () : void => {
                                            next();
                                        });
                                    }
                                } else {
                                    next();
                                }
                            } else {
                                next();
                            }
                        } else {
                            $callback();
                        }
                    };
                    processFile(0);
                } else {
                    $callback();
                }
            };

            const isPlatformMatch : any = ($resource : IProjectTargetResources) : boolean => {
                let retVal : boolean = false;

                const match : any = ($platform : string) : boolean => {
                    let retVal : boolean = false;
                    if ($platform === "any" || $platform === "all") {
                        retVal = true;
                    } else {
                        retVal = this.build.product.OS().toString() === $platform;
                    }
                    return retVal;
                };

                if (!ObjectValidator.IsEmptyOrNull($resource.platforms)) {
                    if (ObjectValidator.IsString($resource.platforms)) {
                        retVal = match(<string>$resource.platforms);
                    } else {
                        let resItem : string;
                        for (resItem in <string[]>$resource.platforms) {
                            if ($resource.platforms.hasOwnProperty(resItem)) {
                                retVal = retVal || match($resource.platforms[resItem]);
                            }
                        }
                    }
                } else {
                    retVal = true;
                }
                return retVal;
            };

            const isToolchainMatch : any = ($resource : IProjectTargetResources) : boolean => {
                let retVal : boolean = false;

                function check($item : string) : boolean {
                    let status = false;

                    if ($item === this.project.target.toolchain ||
                        (this.project.target.toolchain.startsWith($item) && $item.length <= this.project.target.toolchain.length)) {
                        status = true;
                    }

                    return status;
                }

                if (!ObjectValidator.IsEmptyOrNull($resource.toolchain)) {
                    if (typeof $resource.toolchain === "string") {
                        retVal = check.apply(this, [$resource.toolchain]);
                    } else {
                        let resItem : string;
                        for (resItem in <string[]>$resource.toolchain) {
                            if ($resource.toolchain.hasOwnProperty(resItem)) {
                                retVal = retVal || check.apply(this, [$resource.toolchain[resItem]]);
                            }
                        }
                    }
                } else {
                    retVal = true;
                }

                return retVal;
            };

            const resolveResourcePaths : any = ($resObject : IProjectTargetResources, $resInput : string,
                                                $handler : ($paths : string[], $resPath? : string) => void) : void => {
                let resInput : string = $resInput;
                let platformScope : boolean = false;
                if (StringUtils.Contains(resInput, "<platform>")) {
                    resInput = StringUtils.Replace(resInput, "<platform>", this.build.product.OS().toString());
                    platformScope = true;
                }
                let files : string[] = this.fileSystem.Expand([resInput]);
                if (files.length === 0 && platformScope) {
                    LogIt.Info("Platform key identified in resource location, but no file was found. Trying without platform...");
                    resInput = StringUtils.Replace(StringUtils.Replace($resInput, "<platform>", ""), "//", "/");
                    files = this.fileSystem.Expand([resInput]);
                }
                if (files.length === 0 && (StringUtils.StartsWith(resInput, "dependencies/") ||
                    StringUtils.StartsWith($resObject.input, "/dependencies/"))) {
                    resInput = StringUtils.Replace(StringUtils.Replace(resInput, "/dependencies/", ""),
                        "dependencies/", "");
                    const resDepName : string = StringUtils.Substring(resInput, 0, StringUtils.IndexOf(resInput, "/", true));

                    for (const key in this.project.dependencies) {
                        if (this.project.dependencies.hasOwnProperty(key)) {
                            const dependency = this.project.dependencies[key];
                            if ((key === resDepName) ||
                                (StringUtils.Replace(key, "-", "_") === resDepName)) {
                                const tmp : any = dependency.location;
                                let tmpPath : string = "";
                                if (typeof tmp === "string") {
                                    tmpPath = tmp;
                                } else {
                                    if ((<IProjectDependencyLocation>tmp).type === "dir" ||
                                        (<IProjectDependencyLocation>tmp).type === "url") {
                                        tmpPath = <string>(<IProjectDependencyLocation>tmp).path;
                                    }
                                }
                                tmpPath = StringUtils.Replace(tmpPath, "file:///", "");
                                tmpPath = StringUtils.Replace(tmpPath, "file://", "");
                                if (StringUtils.StartsWith(resInput, resDepName)) {
                                    resInput = StringUtils.Substring(resInput, resDepName.length);
                                }
                                resInput = tmpPath + resInput;
                                LogIt.Info("Resource found outside project directory: " + resInput);
                            }
                        }
                    }
                    files = this.fileSystem.Expand([resInput]);
                }

                $handler(files, resInput);
            };

            const copyTargetResources : any = ($postBuild : boolean, $callback : () => void) : void => {
                const resources : IProjectTargetResources[] = flattenResources();
                let embedStr : string = "";
                let data : any = "";
                const embedData : any = ($element : string, $index : number) : void => {
                    embedStr += "0x" + ("0" + (<any>$element).toString(16)).slice(-2);
                    if ($index < data.length - 1) {
                        embedStr += ",";
                    }
                    if (($index + 1) % 16 === 0) {
                        embedStr += os.EOL + "                ";
                    } else {
                        embedStr += " ";
                    }
                };

                let embeddedCounter : number = 0;
                let copiedCounter : number = 0;

                const finalize : any = () : void => {
                    if (this.properties.projectHas.Cpp.Source() && !$postBuild) {
                        embResourceDataMap = embResourceDataMap.replace(/<\? @res \?>/ig, "");
                        embResourceHeaderTemplateMap = embResourceHeaderTemplateMap.replace(/<\? @resMap \?>/ig, embResourceDataMap);
                        embResourceHeaderTemplate = embResourceHeaderTemplate.replace(/<\? @data \?>/ig, "").replace(/<\? @map \?>/ig,
                            anyEmbeddedResource ? embResourceHeaderTemplateMap :
                                "        // map for all serialized resources" + os.EOL +
                                "        static boost::unordered_map<std::string, std::string *> map;");

                        this.fileSystem.Write(this.properties.compiled + "/resourcesData.hpp", embResourceHeaderTemplate);
                    }

                    LogIt.Info(">>"[ColorType.RED] + " " + embeddedCounter + " files were embedded " +
                        "and " + copiedCounter + " files were copied");
                    $callback();
                };

                const processResource : any = ($index : number) : void => {
                    if ($index < resources.length) {
                        const resObject : IProjectTargetResources = resources[$index];
                        const postBuild : boolean = resObject.hasOwnProperty("postBuildCopy") && resObject.postBuildCopy;
                        if ((!resObject.hasOwnProperty("resx") || !resObject.resx) &&
                            isPlatformMatch(resObject) && isToolchainMatch(resObject) &&
                            postBuild === $postBuild) {
                            if (resObject.hasOwnProperty("input")) {
                                let resInput : string = resObject.input;
                                let files : string[] = [];

                                resolveResourcePaths(
                                    resObject,
                                    resInput,
                                    ($files : string[], $resPath : string) : void => {
                                        files = $files;
                                        resInput = $resPath;
                                    });

                                let sourceRoot : string = resInput;
                                const wildStart : number = sourceRoot.indexOf("/*");
                                if (wildStart !== -1) {
                                    sourceRoot = sourceRoot.substring(0, wildStart);
                                }

                                const processFile : any = ($fileIndex : number) : void => {
                                    if ($fileIndex < files.length) {
                                        const file : string = files[$fileIndex];
                                        if (this.fileSystem.Exists(file) && fs.statSync(file).isFile()) {
                                            const buffer : any = fs.readFileSync(file);
                                            const isText : boolean = istextorbinary.isTextSync(file, buffer);

                                            if (resObject.hasOwnProperty("embed") && resObject.embed && !$postBuild) {
                                                if (!this.properties.projectHas.Cpp.Source()) {
                                                    LogIt.Error("Embed resource is allowed only for project with Cpp source.");
                                                }
                                                const ext : string = path.extname(file);
                                                const name : string = path.basename(file)
                                                    .replace(ext, ext.charAt(1).toUpperCase() + ext.slice(2))
                                                    .replace(/\./g, "_")
                                                    .replace(/\s/g, "_");

                                                embedStr = "";

                                                if (isText) {
                                                    data = encoding.convert(StringReplace.Content(this.fileSystem.Read(file).toString(),
                                                        StringReplaceType.VARIABLES), "UTF-8");
                                                } else {
                                                    data = fs.readFileSync(file);
                                                }

                                                embResourceDataMap = embResourceDataMap.replace(/<\? @res \?>/ig,
                                                    os.EOL + "                (\"" + file + "\", &" + name + "StringUtils)<? @res ?>");

                                                data.forEach(embedData);
                                                if (data.length % 16 !== 0) {
                                                    embedStr += os.EOL;
                                                }
                                                embResourceHeaderTemplate = embResourceHeaderTemplate.replace(/<\? @data \?>/ig,
                                                    embResourceHeaderTemplateData.replace(/<\? @name \?>/ig, name)
                                                        .replace(/<\? @embData \?>/ig, embedStr) + "<? @data ?>");

                                                anyEmbeddedResource = true;

                                                if (this.programArgs.IsDebug()) {
                                                    LogIt.Info("\"" + file + "\" was embedded.");
                                                } else {
                                                    embeddedCounter++;
                                                }
                                                processFile($fileIndex + 1);
                                            } else if (!resObject.hasOwnProperty("copy") || resObject.copy) {
                                                let resOutput : string = "resource";
                                                if (resObject.hasOwnProperty("output")) {
                                                    resOutput = resObject.output;
                                                } else {
                                                    LogIt.Warning("Default resource output path is used for resource file " +
                                                        resObject.input);

                                                    if ((<any>file).startsWith("resource/")) {
                                                        resOutput = file;
                                                    } else {
                                                        resOutput += file;
                                                    }
                                                }

                                                let outPath : string = resOutput;
                                                if (this.fileSystem.Exists(outPath) && !fs.statSync(outPath).isFile() ||
                                                    (<any>outPath).endsWith("/*") ||
                                                    (<any>outPath).endsWith("\\*")) {
                                                    if (outPath.indexOf("/**") === -1) {
                                                        outPath = outPath
                                                                .replace("/*", "")
                                                                .replace("\\*", "") +
                                                            "/" + path.basename(file);
                                                    } else {
                                                        outPath = outPath
                                                                .replace("/**", "")
                                                                .replace("/*", "")
                                                                .replace("\\*", "") +
                                                            file.substring(file.indexOf(sourceRoot) + sourceRoot.length);
                                                    }
                                                }

                                                const finalizeFile : any = () : void => {
                                                    if (this.programArgs.IsDebug()) {
                                                        LogIt.Info("\"" + file + "\" was copied.");
                                                    } else {
                                                        copiedCounter++;
                                                    }
                                                    processFile($fileIndex + 1);
                                                };

                                                const destination : string = this.properties.dest + "/" + outPath;
                                                if (!isText) {
                                                    this.fileSystem.Copy(file, destination, () : void => {
                                                        finalizeFile();
                                                    });
                                                } else {
                                                    const stats : any = fs.lstatSync(file);
                                                    if (resObject.hasOwnProperty("keepLinks") && resObject.keepLinks &&
                                                        stats.isSymbolicLink()) {
                                                        LogIt.Info("> copy link from \"" + file + "\" to \"" + destination + "\"");
                                                        this.fileSystem.CreateDirectory(path.dirname(destination));
                                                        const link : string = fs.readlinkSync(file);
                                                        fs.symlinkSync(link, destination);
                                                    } else {
                                                        // tslint:disable no-bitwise
                                                        const mode : number = stats.mode & parseInt("777", 8);
                                                        // tslint:enable no-bitwise
                                                        let repData : string = this.fileSystem.Read(file).toString();
                                                        if (!resObject.hasOwnProperty("skip-replace") ||
                                                            resObject["skip-replace"] === false) {
                                                            repData = StringReplace.Content(repData, StringReplaceType.VARIABLES);
                                                        }
                                                        this.fileSystem.CreateDirectory(path.dirname(destination));
                                                        fs.writeFileSync(destination, repData, {mode});
                                                    }
                                                    finalizeFile();
                                                }
                                            } else {
                                                processFile($fileIndex + 1);
                                            }
                                        } else {
                                            processFile($fileIndex + 1);
                                        }
                                    } else {
                                        processResource($index + 1);
                                    }
                                };

                                processFile(0);
                            } else {
                                LogIt.Warning("Target resource file path is not specified");
                                LogIt.Info(JSON.stringify(resObject, null, 2));
                                processResource($index + 1);
                            }
                        } else {
                            processResource($index + 1);
                        }
                    } else {
                        finalize();
                    }
                };

                processResource(0);
            };

            if ($option === "resource") {
                if (this.properties.projectHas.Resources()) {
                    if (this.properties.projectHas.TypeScript.Tests()) {
                        const src : string[] = [
                            "resource/graphics",
                            "resource/javascript",
                            "resource/libs",
                            "resource/data"
                        ];

                        const process : any = ($index : number) : void => {
                            if ($index < src.length) {
                                const currentSrc : string = src[$index];
                                copy("dependencies", currentSrc, "", "*/test/**", () : void => {
                                    copy("dependencies", "test/" + currentSrc, "", "", () : void => {
                                        copy("", currentSrc, "", "*/test/**", () : void => {
                                            copy("", "test/" + currentSrc, "", "", () : void => {
                                                process($index + 1);
                                            });
                                        });
                                    });
                                });
                            } else {
                                $done();
                            }
                        };
                        process(0);
                    } else {
                        if (this.properties.projectHas.Cpp.Tests()) {
                            const src : string[] = [
                                "resource/graphics",
                                "resource/javascript",
                                "resource/libs",
                                "resource/data"
                            ];
                            const process : any = ($index : number) : void => {
                                if ($index < src.length) {
                                    const currentSrc : string = src[$index];
                                    copy("dependencies", "test/" + currentSrc, "", "", () : void => {
                                        copy("", "test/" + currentSrc, "", "", () : void => {
                                            process($index + 1);
                                        });
                                    });
                                } else {
                                    $done();
                                }
                            };
                            process(0);
                        } else {
                            LogIt.Warning("Copy static files task option \"" + $option + "\" skipped: " +
                                "resource files should be defined by target configuration for current project.");
                            $done();
                        }
                    }
                } else {
                    LogIt.Warning("Copy static files task option \"" + $option + "\" skipped: resource files has not been found.");
                    $done();
                }
            } else if ($option === "targetresource") {
                copyTargetResources(false, $done);
            } else if ($option === "targetresource-postbuild") {
                copyTargetResources(true, $done);
            } else if ($option === "xcppresx") {
                class ResxInfo {
                    public name : string;
                    public file : string;
                    public object : IProjectSplashScreenResource;

                    constructor($name : string, $file : string, $object : IProjectSplashScreenResource) {
                        this.name = $name;
                        this.file = $file;
                        this.object = $object;
                    }
                }

                const resources : IProjectTargetResources[] = flattenResources();
                const resItems : ResxInfo[] = [];
                let targetExtension : string = "";
                if (EnvironmentHelper.IsWindows()) {
                    targetExtension = ".exe";
                }

                const finalize : any = () => {
                    if (resItems.length === 0) {
                        $done();
                    } else {
                        const cfgPath : string = this.properties.projectBase + "/build/compiled/intern.conf.json";
                        const infos : IProjectSplashScreenResource[] = [];
                        resItems.forEach(($item) => {
                            infos.push($item.object);
                        });
                        this.fileSystem.Write(cfgPath, JSON.stringify(infos, null, 2));
                        const extension : string = EnvironmentHelper.IsWindows() ? ".exe" : "";
                        ResourceHandler.EmbedFile(this.properties.projectBase + "/build/target/" + this.project.target.name + extension,
                            "CONFIG", "INTERN_CONF_JSON", cfgPath, () : void => {
                                $done();
                            });
                    }
                };

                let counter : number = 0;

                const process : any = ($callback : () => void) => {
                    if (counter < resItems.length) {
                        const tmp : ResxInfo = resItems[counter++];
                        ResourceHandler.EmbedFile(this.properties.projectBase + "/build/target/" +
                            this.project.target.name + targetExtension, tmp.object.type.toString(),
                            tmp.name, this.properties.projectBase + "/" + tmp.file,
                            () : void => {
                                process($callback);
                            });
                    } else {
                        $callback();
                    }
                };

                resources.forEach(($resObject : IProjectTargetResources) : void => {
                    if ($resObject.hasOwnProperty("input") && ($resObject.hasOwnProperty("resx") && $resObject.resx) &&
                        isPlatformMatch($resObject) && isToolchainMatch($resObject)) {
                        resolveResourcePaths(
                            $resObject,
                            $resObject.input,
                            ($files : string[], $resPath : string) : void => {
                                for (const key in $files) {
                                    if ($files.hasOwnProperty(key)) {
                                        if (this.fileSystem.Exists($files[key])) {
                                            const name : string = $files[key]
                                                .replace(/[\/\\.\-]/g, "_")
                                                .toUpperCase();

                                            let output : string = $files[key];
                                            if ($resObject.hasOwnProperty("output") && $resObject.output.length > 0) {
                                                output = $resObject.output;
                                                if (output.indexOf("/**") === -1) {
                                                    output = output
                                                            .replace("/*", "")
                                                            .replace("\\*", "") +
                                                        "/" + path.basename($files[key]);
                                                } else {
                                                    let sourceRoot : string = $resPath;
                                                    const wildStart : number = sourceRoot.indexOf("/*");
                                                    if (wildStart !== -1) {
                                                        sourceRoot = sourceRoot.substring(0, wildStart);
                                                    }

                                                    output = output
                                                            .replace("/**", "")
                                                            .replace("/*", "")
                                                            .replace("\\*", "") +
                                                        $files[key].substring($files[key].indexOf(sourceRoot) + sourceRoot.length);
                                                }

                                                LogIt.Debug("file: {0}\nout:  {1}", $files[key], output);
                                            }

                                            const tmpResX : IProjectSplashScreenResource = <IProjectSplashScreenResource>{
                                                copyOnly: true,
                                                location: output,
                                                name,
                                                type    : ResourceType.RESOURCE
                                            };
                                            resItems.push(new ResxInfo(name, $files[key], tmpResX));
                                        }
                                    }
                                }
                            });
                    }
                });

                process(() => {
                    finalize();
                });
            } else {
                LogIt.Warning("Copy static files task option \"" + $option + "\" has not been found.");
                $done();
            }
        }
    }
}
