/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class CodeSign extends BaseTask {

        protected getName() : string {
            return "code-sign";
        }

        protected process($done : any) : void {
            if (this.project.target.codeSigning.enabled) {
                let executables : string[] = this.fileSystem.Expand(this.properties.projectBase + "/build/*.exe");
                if (executables.length === 0) {
                    executables = this.fileSystem.Expand(this.properties.projectBase + "/build/target/**/*.exe");
                }

                const nextExecutable : any = ($index : number) : void => {
                    if ($index < executables.length) {
                        if (this.project.target.codeSigning.serverConfig !== "") {
                            this.remoteSign(executables[$index], () : void => {
                                nextExecutable($index + 1);
                            });
                        } else {
                            this.localSign(executables[$index], () : void => {
                                nextExecutable($index + 1);
                            });
                        }
                    } else {
                        $done();
                    }
                };
                nextExecutable(0);
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Code signing skipped: target configuration is missing enabled codeSigning");
                $done();
            }
        }

        private localSign($executablePath : string, $callback : () => void) : void {
            if (EnvironmentHelper.IsWindows()) {
                if (this.properties.projectHas.Cpp.Source() && this.fileSystem.Exists($executablePath)) {
                    this.terminal.Spawn("signtool.exe", [
                        "sign", "/v",
                        "/f", "\"" + this.project.target.codeSigning.certPath.replace(/\//g, "\\") + "\"",
                        "/t", "http://timestamp.comodoca.com/authenticode",
                        $executablePath
                    ], Loader.getInstance().getProgramArgs().ProjectBase() + "/resource/libs/signtool/", ($exitCode : number) : void => {
                        if ($exitCode !== 0) {
                            LogIt.Error("Code signing has failed");
                        } else {
                            LogIt.Info(">>"[ColorType.YELLOW] + " Target has been signed successfully");
                            $callback();
                        }
                    });
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " Code signing skipped: executable for signing does not exit");
                    $callback();
                }
            } else {
                LogIt.Warning(">>"[ColorType.YELLOW] + " Code signing skipped: signing is supported only at WINDOWS environment");
                $callback();
            }
        }

        private remoteSign($executablePath : string, $callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            const fs : Types.NodeJS.fs = require("fs");

            const sign : any = ($config : any, $callback : () => void) : void => {
                let body : string = "";
                for (const variable in $config.request.settings) {
                    if ($config.request.settings.hasOwnProperty(variable)) {
                        if (body !== "") {
                            body += "&";
                        }
                        body += variable + "=" + encodeURIComponent($config.request.settings[variable]);
                    }
                }
                this.fileSystem.Download(<any>{
                    body,
                    headers     : {
                        "Accept"                   : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                        "Accept-Language"          : "en-US,en;q=0.5",
                        "Connection"               : "keep-alive",
                        "Content-Length"           : body.length,
                        "Content-Type"             : "application/x-www-form-urlencoded",
                        "Upgrade-Insecure-Requests": "1"
                    },
                    method      : $config.request.method,
                    streamOutput: true,
                    url         : $config.request.url
                }, ($headers : string, $body : string) : void => {
                    LogIt.Debug($body.toString());
                    $callback();
                });
            };

            let counter : number = 0;
            const download : any = ($source : string, $dest : string) : void => {
                if (counter < 100) {
                    Echo.Print(".");
                } else {
                    Echo.Println(".");
                    counter = 0;
                }
                if (fs.existsSync($source)) {
                    Echo.Println("");
                    try {
                        LogIt.Info("Downloading from " + $source + " to " + $dest);
                        fs.copyFileSync($source, $dest);
                        fs.unlinkSync($source);
                        $callback();
                    } catch (ex) {
                        LogIt.Error("Unable to download file.", ex);
                    }
                } else {
                    setTimeout(() : void => {
                        counter++;
                        download($source, $dest);
                    }, 5000);
                }
            };

            const getConfig : any = ($callback : ($config : any) => void) : void => {
                let serverConfig : string = this.project.target.codeSigning.serverConfig;
                if (!StringUtils.Contains(serverConfig, "http://", "https://", "file://") &&
                    !StringUtils.StartsWith(serverConfig, "/")) {
                    serverConfig = "file:///" + this.properties.projectBase + "/" + serverConfig;
                }
                this.fileSystem.Download(<any>{
                    streamOutput: true,
                    url         : serverConfig
                }, ($headers : string, $body : string) : void => {
                    $body = StringUtils.Replace($body, "<? @var this.fileName ?>", this.getUID() + path.extname($executablePath));
                    $body = StringReplace.Content($body, StringReplaceType.VARIABLES);
                    try {
                        /* tslint:disable: no-eval */
                        $callback(eval("(" + $body + ")"));
                        /* tslint:enable */
                    } catch (ex) {
                        LogIt.Error(ex);
                    }
                });
            };

            if (this.fileSystem.Exists($executablePath)) {
                getConfig(($config : any) : void => {
                    if (!fs.existsSync($config.uploadUrl)) {
                        LogIt.Info("Uploading from " + $executablePath + " to " + $config.uploadUrl);
                        fs.copyFileSync($executablePath, $config.uploadUrl);
                        try {
                            sign($config, () : void => {
                                Echo.Print("Waiting for sign process ...");
                                download($config.downloadUrl, $executablePath);
                            });
                        } catch (ex) {
                            LogIt.Error("Unable to upload file.", ex);
                        }
                    } else {
                        sign(() : void => {
                            Echo.Print("Waiting for sign process ...");
                            download($config.downloadUrl, $executablePath);
                        });
                    }
                });
            } else {
                LogIt.Error("File does not exist");
            }
        }
    }
}
