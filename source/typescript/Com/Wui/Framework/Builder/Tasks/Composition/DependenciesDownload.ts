/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import IProjectDependency = Com.Wui.Framework.Builder.Interfaces.IProjectDependency;
    import IProjectDependencyLocation = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocation;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import GitManager = Com.Wui.Framework.Builder.Tasks.Utils.GitManager;
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import BuildExecutor = Com.Wui.Framework.Builder.Tasks.BuildProcessor.BuildExecutor;
    import IProjectDependencyLocationPath = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocationPath;
    import IProjectDependencyLocationArch = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocationArch;

    export class DependenciesDownload extends BaseTask {
        private processed : any;
        private registered : any;
        private downloaded : number;
        private buildExecutor : BuildExecutor;

        constructor() {
            super();

            this.buildExecutor = new BuildExecutor();
            this.processed = {};
            this.registered = {};
            this.downloaded = 0;
        }

        public ParseLocation($source : IProjectDependencyLocation | string | string[]) : IProjectDependencyLocation[] {
            const path : Types.NodeJS.path = require("path");
            let output : IProjectDependencyLocation[] = [];

            if (ObjectValidator.IsString($source)) {
                let isRemote : boolean = true;
                let source : string = <string>$source;
                output[0] = <IProjectDependencyLocation>{
                    branch     : "",
                    path       : "",
                    platform   : "",
                    releaseName: "",
                    type       : ""
                };
                output[0].branch = "master";
                output[0].type = "git";

                if (StringUtils.StartsWith(source, "$")) {
                    const index : number = StringUtils.IndexOf(source, "$", true, 1);
                    output[0].platform = StringUtils.Substring(source, 1, index);
                    source = StringUtils.Substring(source, index + 1);
                }

                if (StringUtils.StartsWith(source, "dir+")) {
                    output[0].type = "dir";
                    isRemote = false;
                } else if (StringUtils.StartsWith(source, "mvn+")) {
                    output[0].type = "mvn";
                } else if (StringUtils.StartsWith(source, "file://")) {
                    output[0].type = "url";
                }
                source = StringUtils.Remove(source, "git+", "dir+", "mvn+");

                const branchTag : number = StringUtils.IndexOf(<string>source, "#");
                if (branchTag !== -1) {
                    output[0].branch = StringUtils.Substring(<string>source, branchTag + 1);
                    source = StringUtils.Substring(<string>source, 0, branchTag);
                }
                if (!isRemote) {
                    source = StringUtils.Replace(<string>source, "\\", "/");
                    source = StringUtils.Replace(<string>source, "/", path.sep);
                }
                output[0].path = source;
            } else if (ObjectValidator.IsArray($source) && (<string[]>$source).length > 0 && ObjectValidator.IsString($source[0])) {
                for (const singleSource of <string[]>$source) {
                    output = output.concat(this.ParseLocation(singleSource));
                }
            } else {
                const multiLoc : IProjectDependencyLocation[] =
                    this.ParseLocation(this.ResolveDependencyPath((<IProjectDependencyLocation>$source).path));

                for (const singleLoc of multiLoc) {
                    if ($source.hasOwnProperty("type") && (singleLoc.type === "git")) {
                        singleLoc.type = (<IProjectDependencyLocation>$source).type;
                    }
                    if ($source.hasOwnProperty("branch") && (singleLoc.type === "master")) {
                        singleLoc.branch = (<IProjectDependencyLocation>$source).branch;
                    }
                    if ($source.hasOwnProperty("releaseName")) {
                        singleLoc.releaseName = (<IProjectDependencyLocation>$source).releaseName;
                    }
                    if ($source.hasOwnProperty("platform")) {
                        singleLoc.platform = (<IProjectDependencyLocation>$source).platform;
                    }
                }
                output = output.concat(multiLoc);
            }
            return output;
        }

        public getSource($dependencyName : string, $source : IProjectDependencyLocation | string, $dependency : IProjectDependency,
                         $targetPath : string, $callback : () => void) : void {
            const parsedSource : IProjectDependencyLocation[] = this.ParseLocation($source);

            const process : any = ($index : number) : void => {
                if ($index < parsedSource.length) {
                    let targetPath : string = $targetPath;
                    if (parsedSource.length > 1) {
                        targetPath = $targetPath + "/" + parsedSource[$index].platform;
                    }
                    this.processSingleSource($dependencyName, parsedSource[$index], $dependency, targetPath, () : void => {
                        process($index + 1);
                    });
                } else {
                    $callback();
                }
            };
            process(0);
        }

        public ResolveDependencyPath($path : string | IProjectDependencyLocationPath) : string[] {
            let path : string[] = [];
            if (ObjectValidator.IsString($path)) {
                path = [<string>$path];
            } else {
                for (const product of this.buildExecutor.getTargetProducts()) {
                    let pathWithArch : string | IProjectDependencyLocationArch = "";

                    if ((<IProjectDependencyLocationPath>$path).hasOwnProperty(product.OS().toString())) {
                        pathWithArch = (<IProjectDependencyLocationPath>$path)[product.OS().toString()];

                        if (!ObjectValidator.IsEmptyOrNull(pathWithArch)) {
                            if (ObjectValidator.IsString(pathWithArch)) {
                                pathWithArch = <string>pathWithArch;
                            } else {
                                if ((<IProjectDependencyLocationArch>pathWithArch).hasOwnProperty(product.Arch().toString())) {
                                    pathWithArch = (<IProjectDependencyLocationArch>pathWithArch)[product.Arch().toString()];
                                } else {
                                    LogIt.Error("Dependency location path contains wrong definition of architecture.");
                                }
                            }
                        }
                        // todo this could be replaced by change interface to IProj...Path[] and proper loop in location parser
                        path.push("$" + product.OS() + "$" + <string>pathWithArch);
                    }

                    if (ObjectValidator.IsEmptyOrNull(pathWithArch)) {
                        LogIt.Error("Unmet dependency path based on builder platform and architecture. " +
                            "Specify platform specific paths for win, linux and mac as location.path attribute. " +
                            "Optionally can be specified x86 or x64 architecture.");
                    }
                }
            }
            return path;
        }

        protected processSingleSource($dependencyName : string, $source : IProjectDependencyLocation, $dependency : IProjectDependency,
                                      $targetPath : string, $callback : () => void) {
            const dependencyKey : string = $dependencyName + ":" + $targetPath;
            if (this.processed.hasOwnProperty(dependencyKey) || $source.type === "mvn") {
                $callback();
            } else {
                this.fileSystem.Delete($targetPath);
                if ($source.type === "git" || $source.type === "dir" && this.fileSystem.Exists($source.path + "/.git")) {
                    this.clone(<string>$source.path, $source.branch, $targetPath,
                        ($source : string, $branch : string, $destination : string) : void => {
                            this.downloaded++;
                            this.processed[dependencyKey] = $branch;
                            let dependencies : IProjectDependency[] = null;
                            const configPath : string = $destination + "/package.conf.json";
                            if (this.fileSystem.Exists(configPath)) {
                                const config : IProject = JSON.parse(this.fileSystem.Read(configPath).toString());
                                if (config.hasOwnProperty("dependencies")) {
                                    dependencies = config.dependencies;
                                }
                            }
                            if (dependencies !== null) {
                                this.processDependencies(dependencies, () : void => {
                                    $callback();
                                });
                            } else {
                                $callback();
                            }
                        });
                } else if ($source.type === "wuicloud") {
                    let projectName : string = (<IProjectDependency>$dependency).name;
                    if (ObjectValidator.IsEmptyOrNull(projectName)) {
                        projectName = $dependencyName;
                    }
                    if (ObjectValidator.IsEmptyOrNull($source.releaseName)) {
                        $source.releaseName = "null";
                    }
                    if (ObjectValidator.IsEmptyOrNull($source.platform)) {
                        $source.platform = "null";
                    }
                    let downloadUrl : string = <string>$source.path + "/Update/" +
                        projectName + "/" + $source.releaseName + "/" + $source.platform;
                    if (!ObjectValidator.IsEmptyOrNull((<IProjectDependency>$dependency).version) &&
                        (<IProjectDependency>$dependency).version !== "latest") {
                        downloadUrl += "/" + (<IProjectDependency>$dependency).version;
                    }
                    this.download(downloadUrl, $targetPath, () : void => {
                        this.downloaded++;
                        this.processed[dependencyKey] = (<IProjectDependency>$dependency).location;
                        $callback();
                    });
                } else if ($source.type !== "mvn") {
                    if (!(<any>$source.path).startsWith("file://") &&
                        (this.fileSystem.IsDirectory((<any>$source).path) || this.fileSystem.IsFile((<any>$source).path))) {
                        $source.path = "file:///" + $source.path;
                    }
                    this.download($source.path.toString(), $targetPath, () : void => {
                        this.downloaded++;
                        this.processed[dependencyKey] = (<IProjectDependency>$dependency).location;
                        $callback();
                    });
                } else {
                    $callback();
                }
            }
        }

        protected isPlatformMatch($platform : string) : boolean {
            let output : boolean = false;

            for (const product of this.buildExecutor.getTargetProducts()) {
                if (product.OS().toString() === $platform) {
                    output = true;
                    break;
                }
            }

            return output;
        }

        protected clone($repoSource : string, $branch : string, $targetPath : string,
                        $callback : ($repoSource : string, $branch : string, $targetPath : string) => void) : void {
            const isRemote : boolean = StringUtils.Contains($repoSource, "http://", "https://", "ssh://", "git://");

            if (isRemote) {
                LogIt.Info("Clone of dependency \"" + $repoSource + "\" to \"" + $targetPath + "\".");
            } else {
                LogIt.Info("WARNING:"[ColorType.YELLOW] + " Clone of dependency \"" + $repoSource + "\" " +
                    "to \"" + $targetPath + "\" from local repository.");
            }

            let cwd : string = this.fileSystem.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() +
                "/downloads/" + StringUtils.getSha1($targetPath);
            let archiveSource : string = $repoSource;
            if (isRemote) {
                cwd += ".git";
                archiveSource = cwd;
                this.fileSystem.Delete(cwd);
            }

            const archive : any = () : void => {
                const path : Types.NodeJS.path = require("path");
                const outputName : string = path.basename($targetPath).replace(/\//gi, "_") + ".zip";
                const outputPath : string = StringUtils.Replace(
                    StringUtils.Replace(archiveSource, "\\", "/") + "/" + outputName, "/", path.sep);
                if (!this.fileSystem.Exists(archiveSource)) {
                    LogIt.Error("Dependency local path \"" + archiveSource + "\" does not exist.");
                }
                GitManager.Execute(["archive", "--format", "zip", "-0", "--output", "\"" + outputName + "\"", $branch],
                    () : void => {
                        this.fileSystem.Unpack(outputPath, {output: $targetPath, autoStrip: false}, () : void => {
                            if (this.fileSystem.Exists($targetPath)) {
                                this.fileSystem.Delete(isRemote ? cwd : outputPath);
                                $callback($repoSource, $branch, $targetPath);
                            } else {
                                LogIt.Error("Dependency unpack has failed.");
                            }
                        });
                    },
                    () : void => {
                        this.fileSystem.Delete(isRemote ? cwd : outputPath);
                        LogIt.Error("Dependency clone has failed.");
                    },
                    archiveSource);
            };

            if (isRemote) {
                GitManager.Execute(["clone", "--depth=1", "--branch", $branch, $repoSource, "\"" + cwd + "\""],
                    () : void => {
                        archive();
                    },
                    () : void => {
                        this.fileSystem.Delete(cwd);
                        LogIt.Error("Dependency clone has failed.");
                    });
            } else {
                archive();
            }
        }

        protected download($location : string | FileSystemDownloadOptions, $targetPath : string,
                           $callback : () => void) : void {
            const source : string = ObjectValidator.IsString($location) ? "\"" + $location + "\"" : "from WUI Cloud";
            LogIt.Info("Download of dependency " + source + " to \"" + $targetPath + "\".");
            this.fileSystem.Download($location, ($headers : string, $tempPath : string) : void => {
                if (this.fileSystem.Exists($tempPath)) {
                    if (this.fileSystem.IsDirectory($tempPath)) {
                        this.fileSystem.Copy($tempPath, $targetPath, ($status : boolean) : void => {
                            if ($status) {
                                this.fileSystem.Delete($tempPath);
                                $callback();
                            } else {
                                LogIt.Error("Dependency copy has failed.");
                            }
                        });
                    } else {
                        this.fileSystem.Unpack($tempPath, {output: $targetPath}, () : void => {
                            if (this.fileSystem.Exists($targetPath)) {
                                this.fileSystem.Delete($tempPath);
                                $callback();
                            } else {
                                LogIt.Error("Dependency unpack has failed.");
                            }
                        });
                    }
                } else {
                    $callback();
                }
            });
        }

        protected processDependency($dependencyName : string, $dependency : IProjectDependency | string,
                                    $callback : () => void) : void {
            const targetPath : string = this.properties.projectBase + "/dependencies/" + $dependencyName;

            if (ObjectValidator.IsString($dependency) && !ObjectValidator.IsEmptyOrNull($dependency)) {
                this.getSource($dependencyName, <string>$dependency, <any>$dependency, targetPath, () : void => {
                    $callback();
                });
            } else if ($dependency.hasOwnProperty("groupId")) {
                $callback();
            } else if (ObjectValidator.IsObject($dependency)) {
                if ($dependency.hasOwnProperty("location")) {
                    const projectDependency : IProjectDependency = <IProjectDependency>$dependency;
                    if (ObjectValidator.IsString(projectDependency.location) &&
                        !ObjectValidator.IsEmptyOrNull(projectDependency.location)) {
                        this.getSource($dependencyName, projectDependency.location, <IProjectDependency>$dependency, targetPath,
                            () : void => {
                                $callback();
                            });
                    } else if (ObjectValidator.IsObject(projectDependency.location)) {
                        const dependencyLocation : IProjectDependencyLocation =
                            <IProjectDependencyLocation>projectDependency.location;
                        if (projectDependency.location.hasOwnProperty("path")) {
                            const branch : string = projectDependency.location.hasOwnProperty("branch") ?
                                "#" + dependencyLocation.branch : "";
                            switch (dependencyLocation.type) {
                            case "dir":
                                if (this.fileSystem.Exists(<string>dependencyLocation.path)) {
                                    if (this.fileSystem.Exists(dependencyLocation.path + "/.git")) {
                                        this.getSource(
                                            $dependencyName,
                                            "dir+" + dependencyLocation.path + branch,
                                            <IProjectDependency>$dependency, targetPath,
                                            () : void => {
                                                $callback();
                                            });
                                    } else {
                                        LogIt.Info("WARNING:"[ColorType.YELLOW] +
                                            " Dependency \"" + $dependencyName + "\" loaded from local path \"" +
                                            dependencyLocation.path + "\".");
                                        $callback();
                                    }
                                } else {
                                    LogIt.Error("Dependency \"" + $dependencyName + "\" not exists at local path \"" +
                                        dependencyLocation.path + "\".");
                                }
                                break;
                            case "git":
                                const path : string = dependencyLocation.path + branch;
                                if (StringUtils.Contains(path, "http://", "https://", "ssh://", "git://")) {
                                    this.getSource($dependencyName, "git+" + path, <IProjectDependency>$dependency, targetPath,
                                        () : void => {
                                            $callback();
                                        });
                                } else {
                                    this.getSource($dependencyName, "dir+" + path, <IProjectDependency>$dependency, targetPath,
                                        () : void => {
                                            $callback();
                                        });
                                }
                                break;
                            case "mvn":
                                $callback();
                                break;
                            default:
                                this.getSource($dependencyName, projectDependency.location, <IProjectDependency>$dependency,
                                    targetPath, () : void => {
                                        $callback();
                                    });
                                break;
                            }
                        } else {
                            LogIt.Error("Location path is not specified for dependency \"" + $dependencyName + "\".");
                        }
                    }
                } else {
                    LogIt.Warning("Location is not specified for dependency \"" + $dependencyName +
                        "\". Nothing going to be prepared for next install phase.\nContinue in processing of others...");
                    $callback();
                }
            } else if (ObjectValidator.IsEmptyOrNull($dependency)) {
                LogIt.Warning("Download of dependency \"" + $dependencyName + "\" skipped: source has not been defined");
                $callback();
            } else {
                LogIt.Error("Unsupported dependency configuration type: supported are only string or JSON");
            }
        }

        protected collectDependencies($dependencies : IProjectDependency[]) : string[] {
            const dependenciesList : string[] = [];
            let dependencyName : string;
            for (dependencyName in $dependencies) {
                if ($dependencies.hasOwnProperty(dependencyName)) {
                    let registeredCheckSkip : boolean = false;
                    if (ObjectValidator.IsString($dependencies[dependencyName])) {
                        const source : string = "" + $dependencies[dependencyName];
                        registeredCheckSkip = StringUtils.StartsWith(source, "!");
                        if (registeredCheckSkip) {
                            $dependencies[dependencyName] =
                                <any>StringUtils.Substring("" + $dependencies[dependencyName], 1);
                        }
                    } else if (ObjectValidator.IsString($dependencies[dependencyName].location)) {
                        registeredCheckSkip = StringUtils.StartsWith(<string>$dependencies[dependencyName].location, "!");
                        if (registeredCheckSkip) {
                            $dependencies[dependencyName].location =
                                StringUtils.Substring(<string>$dependencies[dependencyName].location, 1);
                        }
                    }
                    if (!this.registered.hasOwnProperty(dependencyName)) {
                        let isRequired : boolean = false;
                        if (!ObjectValidator.IsString($dependencies[dependencyName]) &&
                            $dependencies[dependencyName].hasOwnProperty("platforms")) {
                            if (!ObjectValidator.IsArray($dependencies[dependencyName].platforms)) {
                                $dependencies[dependencyName].platforms = [<string>$dependencies[dependencyName].platforms];
                            }
                            (<string[]>$dependencies[dependencyName].platforms).forEach(($platforms : string) : void => {
                                if (this.isPlatformMatch($platforms)) {
                                    isRequired = true;
                                }
                            });
                        } else {
                            isRequired = !(ObjectValidator.IsObject($dependencies[dependencyName]) &&
                                !$dependencies[dependencyName].hasOwnProperty("location"));
                        }
                        if (isRequired) {
                            this.registered[dependencyName] = {
                                dependency: $dependencies[dependencyName],
                                registeredCheckSkip
                            };
                            dependenciesList.push(dependencyName);
                        }
                    } else if (typeof $dependencies[dependencyName] === "string" && <any>$dependencies[dependencyName] !== "" &&
                        (!this.registered[dependencyName].hasOwnProperty("registeredCheckSkip") ||
                            !this.registered[dependencyName].registeredCheckSkip)) {
                        const newSource : IProjectDependencyLocation = this.ParseLocation($dependencies[dependencyName].toString())[0];
                        const registeredSource : IProjectDependencyLocation =
                            typeof this.registered[dependencyName].dependency === "string" ?
                                this.ParseLocation(this.registered[dependencyName].dependency)[0] :
                                this.ParseLocation(this.registered[dependencyName].dependency.location)[0];
                        if (registeredSource.branch !== newSource.branch) {
                            LogIt.Error("Dependencies configuration is corrupted: " +
                                "required repository \"" + dependencyName + ":" + newSource.branch + "\" " +
                                "is not in match with previously cloned repository \"" + dependencyName + ":" +
                                registeredSource.branch + "\". Please, fix the required reference.");
                        }
                    }
                }
            }
            return dependenciesList;
        }

        protected processDependencies($dependencies : IProjectDependency[], $callback : () => void) : void {
            const dependenciesList : string[] = this.collectDependencies($dependencies);

            const nextDependency : any = ($index : number) : void => {
                if ($index < dependenciesList.length) {
                    const dependencyName : string = dependenciesList[$index];
                    const dependency : IProjectDependency | string = $dependencies[dependencyName];

                    this.processDependency(dependencyName, dependency, () : void => {
                        nextDependency($index + 1);
                    });
                } else {
                    $callback();
                }
            };

            nextDependency(0);
        }

        protected initDependencies($callback : () => void) : void {
            this.fileSystem.Delete(this.properties.projectBase + "/dependencies", ($status : boolean) : void => {
                if ($status) {
                    this.fileSystem.CreateDirectory(this.properties.projectBase + "/dependencies");
                    this.processDependencies(this.project.dependencies, () : void => {
                        if (this.downloaded === 0) {
                            LogIt.Warning("Dependencies download skipped: dependencies are not configured");
                        }
                        $callback();
                    });
                } else {
                    LogIt.Error("Dependencies directory can not be removed. Please check if directory is blocked by any process" +
                        "and re-run install task. You can also delete directory manually.");
                }
            });
        }

        protected getName() : string {
            return "dependencies-download";
        }

        protected process($done : any) : void {
            this.processed = {};
            this.registered = {};
            this.downloaded = 0;

            if (this.project.hasOwnProperty("dependencies")) {
                if (this.programArgs.IsAgentTask()) {
                    LogIt.Warning("Dependencies download in agent mode may require GIT or WAN proxy for proper functionality");
                }
                this.initDependencies($done);
            } else {
                LogIt.Warning("Dependencies download skipped: dependencies are not configured");
                $done();
            }
        }
    }
}
