/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import BuildExecutor = Com.Wui.Framework.Builder.Tasks.BuildProcessor.BuildExecutor;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;

    export class CopyLocalScript extends BaseTask {

        protected getName() : string {
            return "copy-local-scripts";
        }

        protected process($done : any, $option : string) : void {
            if ($option === "phonegap") {
                this.fileSystem.Copy(
                    this.properties.projectBase + "/bin/resource/configs/phonegap.config.xml",
                    this.properties.projectBase + "/build/target/config.xml", $done);
            } else if ($option === "declarations") {
                if (this.fileSystem.Exists(this.properties.projectBase + "/build/compiled/source/typescript/source.d.ts")) {
                    this.fileSystem.Write(this.properties.projectBase + "/build/target/" +
                        "resource/javascript/" + this.properties.packageName + ".d.ts",
                        this.fileSystem.Read(this.properties.projectBase + "/build/compiled/source/typescript/source.d.ts"));
                }
                $done();
            } else if ($option === "target") {
                this.runTask($done, "copy-staticfiles:resource", "copy:html", "copy:license", "copy:configs");
            } else if ($option === "schema") {
                this.fileSystem.Write(this.properties.projectBase + "/build/target/" +
                    "resource/configs/package.conf.schema.json", JSON.stringify(this.project.schema, null, 2));
                $done();
            } else {
                const tasks : string[] = [];
                if (this.properties.projectHas.TypeScript.Source()) {
                    tasks.push("copy-scripts:typescript");
                }
                if (this.properties.projectHas.Cpp.Source()) {
                    tasks.push("copy-scripts:cpp");
                }
                if (this.properties.projectHas.Java.Source()) {
                    tasks.push("copy-scripts:java");
                    const executor : BuildExecutor = new BuildExecutor();
                    executor.getTargetProducts(this.project.target).forEach(($product : BuildProductArgs) : void => {
                        if ($product.Toolchain() === ToolchainType.ECLIPSE) {
                            tasks.push("copy-scripts:eclipse");
                        }
                        if ($product.Toolchain() === ToolchainType.IDEA) {
                            tasks.push("copy-scripts:idea");
                        }
                    });
                }
                this.runTask($done, tasks);
            }
        }
    }
}
