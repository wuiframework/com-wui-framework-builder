/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Composition {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import IProjectDependencyScriptConfig = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyScriptConfig;
    import IProjectDependency = Com.Wui.Framework.Builder.Interfaces.IProjectDependency;
    import IProjectDependencyLocation = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocation;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;
    import BuildExecutor = Com.Wui.Framework.Builder.Tasks.BuildProcessor.BuildExecutor;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;

    export class DependenciesInstall extends BaseTask {
        private static currentOs : OSType;
        private static currentToolchain : ToolchainType;
        private installed : number;
        private configured : number;

        public static getCurrentInstallOsType() : OSType {
            return DependenciesInstall.currentOs;
        }

        public static getCurrentToolchain() : ToolchainType {
            return DependenciesInstall.currentToolchain;
        }

        protected findScript($basePath : string, $scriptName : string) : string {
            const getPath : any = ($pattern : string) : string => {
                $pattern = $pattern.replace(/\\/gim, "/").replace(/\/\//gim, "/");
                const fileList : string[] = this.fileSystem.Expand($pattern);
                if (fileList.length > 0) {
                    return fileList[0];
                } else {
                    return "";
                }
            };
            if (ObjectValidator.IsEmptyOrNull($basePath) || !ObjectValidator.IsString($basePath)) {
                $basePath = this.properties.projectBase;
            }
            const availablePaths : string[] = [
                $basePath + "/" + $scriptName,
                this.properties.projectBase + "/bin/resource/scripts/" + $scriptName,
                this.properties.projectBase + "/bin/xcpp_scripts/" + $scriptName,
                this.properties.projectBase + "/resource/scripts/" + $scriptName,
                this.properties.projectBase + "/xcpp_scripts/" + $scriptName,
                this.properties.projectBase + "/dependencies/*/resource/scripts/" + $scriptName,
                this.properties.projectBase + "/dependencies/*/xcpp_scripts/" + $scriptName
            ];
            let path : string = "";
            let index : number;
            for (index = 0; index < availablePaths.length; index++) {
                if (path === "") {
                    path = getPath(availablePaths[index]);
                } else {
                    break;
                }
            }
            return path;
        }

        protected configDataLookup($dependencyName : string, $scriptType : string,
                                   $data : IProjectDependencyScriptConfig) : IProjectDependencyScriptConfig {
            if (!ObjectValidator.IsEmptyOrNull($data)) {
                if (ObjectValidator.IsEmptyOrNull($data.attributes) || !ObjectValidator.IsArray($data.attributes)) {
                    $data.attributes = [];
                }
                if (!$data.hasOwnProperty("ignore-parent-attribute")) {
                    $data["ignore-parent-attribute"] = false;
                }
                const files : string[] = this.fileSystem.Expand(this.properties.projectBase + "/dependencies/*/package.conf.json");
                files.forEach(($file : string) : void => {
                    const config : IProject = JSON.parse(this.fileSystem.Read($file).toString());
                    if (config.hasOwnProperty("dependencies") &&
                        config.dependencies.hasOwnProperty($dependencyName) &&
                        config.dependencies[$dependencyName].hasOwnProperty($scriptType) &&
                        config.dependencies[$dependencyName][$scriptType].hasOwnProperty("attributes")) {
                        if (!$data.hasOwnProperty("ignore-parent-attribute") || $data["ignore-parent-attribute"] !== true) {
                            config.dependencies[$dependencyName][$scriptType].attributes.forEach(($attribute : string) : void => {
                                if ($data.attributes.indexOf($attribute) === -1) {
                                    (<string[]>$data.attributes).push($attribute);
                                }
                            });
                        }
                    }
                });
            }
            return $data;
        }

        protected configure($dependencyName : string, $dependency : IProjectDependency) : boolean {
            let retVal : boolean = false;
            const path : Types.NodeJS.path = require("path");
            const tagName : string = "configure-script";
            if (!ObjectValidator.IsEmptyOrNull($dependency) && $dependency.hasOwnProperty(tagName) &&
                $dependency[tagName].hasOwnProperty("name")) {
                const configScript : IProjectDependencyScriptConfig =
                    this.configDataLookup($dependencyName, tagName, $dependency[tagName]);

                LogIt.Info("Looking for config script \"" + configScript.name + "\"");
                const scriptName : string = this.findScript(configScript.path, configScript.name);

                if (scriptName !== "") {
                    configScript.path = path.dirname(scriptName);
                    let attributesList : string = "";
                    (<string[]>configScript.attributes).forEach(($attribute : string) : void => {
                        if (attributesList !== "") {
                            attributesList += ",";
                        }
                        attributesList += $attribute;
                    });
                    configScript.attributes = attributesList;
                    configScript["macro-name"] = (configScript.name.split(".")[0]).toUpperCase();
                    this.configured++;
                    retVal = true;
                } else {
                    LogIt.Info("WARNING: "[ColorType.YELLOW] + "Specified configuration script \"" + configScript.name +
                        "\" not found in search paths. See documentation for more info.");
                }
            }
            return retVal;
        }

        protected installSingle($script : string, $dependencyName : string, $dependencyPath : string, $osType : OSType,
                                $installOptions : IProjectDependencyScriptConfig, $callback : () => void) : void {
            const vm : Types.NodeJS.vm = require("vm");
            LogIt.Info(">>"[ColorType.YELLOW] + " Running script " + $script +
                (ObjectValidator.IsEmptyOrNull($osType) ? "" : (" for " + OSType.getKey($osType) + " platform")));

            if (!this.fileSystem.IsDirectory($dependencyPath)) {
                $dependencyPath = this.fileSystem.NormalizePath(this.properties.projectBase + "/dependencies/" + $dependencyName);
            }
            if (this.fileSystem.Exists($dependencyPath)) {
                const sandbox : any = {
                    /* tslint:disable: object-literal-sort-keys */
                    process,
                    require,
                    console,
                    module: {
                        exports: null
                    },
                    Com,
                    Process($cwd : string, $args : string[], $done : () => void) : void {
                        LogIt.Warning("Install skipped: script did not specify module.exports and " +
                            "also did not override global Process method");
                        $done();
                    }
                    /* tslint:enable */
                };
                vm.createContext(sandbox);
                vm.runInContext(<string>this.fileSystem.Read($script), sandbox, $script);
                const attributes : string[] = <string[]>$installOptions.attributes;
                const done : any = () : void => {
                    DependenciesInstall.currentOs = null;
                    this.installed++;
                    $callback();
                };
                try {
                    DependenciesInstall.currentOs = $osType;
                    sandbox.Process($dependencyPath, attributes, done);
                } catch (ex) {
                    if (!ObjectValidator.IsEmptyOrNull(sandbox.module.exports)) {
                        LogIt.Error("Install script has failed: usage of module.exports API is deprecated. " +
                            "Migrate to older version of builder or convert install script.");
                    } else {
                        LogIt.Error("Install script has failed.", ex);
                    }
                }
            } else {
                LogIt.Error("Dependency install failed because dependency folder is not prepared properly before running install script." +
                    " Generally this can happen only in customized call of dependencies install task without download.");
            }
        }

        protected install($dependencyName : string, $dependency : IProjectDependency, $callback : () => void) : void {
            const tagName : string = "install-script";
            if ($dependency.hasOwnProperty(tagName) && $dependency[tagName].hasOwnProperty("name")) {
                const installScriptObject : IProjectDependencyScriptConfig =
                    this.configDataLookup($dependencyName, tagName, $dependency[tagName]);

                LogIt.Info("Looking for install script \"" + installScriptObject.name + "\"");
                const scriptPath : string = this.findScript(installScriptObject.path, installScriptObject.name);

                if (typeof $dependency.location === "string") {
                    $dependency.location = <any>{path: $dependency.location};
                }

                if (scriptPath !== "") {
                    const dependencyPaths : any[] = [];
                    if (ObjectValidator.IsSet(<IProjectDependencyLocation>$dependency.location)) {
                        const depResolvedPaths : string[] =
                            (new DependenciesDownload()).ResolveDependencyPath((<IProjectDependencyLocation>$dependency.location).path);
                        depResolvedPaths.forEach(($value : string) : void => {
                            let platform : string = "";
                            if (StringUtils.StartsWith($value, "$")) {
                                const index : number = StringUtils.IndexOf($value, "$", true, 1);
                                platform = StringUtils.Substring($value, 1, index);
                                $value = StringUtils.Substring($value, index + 1) + "/" + platform;
                            }
                            if (!StringUtils.StartsWith($value, "mvn+") &&
                                (<IProjectDependencyLocation>$dependency.location).type !== "mvn") {
                                if (!this.fileSystem.Exists($value)) {
                                    $value = this.properties.projectBase + "/dependencies/" + $dependencyName;
                                    if (!ObjectValidator.IsEmptyOrNull(platform) && depResolvedPaths.length > 1) {
                                        $value += "/" + platform;
                                    }
                                }
                                dependencyPaths.push({
                                    os  : platform,
                                    path: $value
                                });
                            }
                        });
                    } else {
                        dependencyPaths.push({
                            os  : null,
                            path: this.properties.projectBase + "/dependencies/" + $dependencyName
                        });
                    }

                    let index : number = 0;
                    let loopOver : any[] = dependencyPaths;
                    let singlePath : string = "";
                    if (loopOver.length === 1) {
                        loopOver = [];
                        const requestedProducts : BuildProductArgs[] = new BuildExecutor().getTargetProducts();
                        requestedProducts.forEach(($product : BuildProductArgs) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($dependency.platforms)) {
                                let depPlatforms : string[] = [];
                                if (ObjectValidator.IsString($dependency.platforms)) {
                                    depPlatforms = [<string>$dependency.platforms];
                                } else {
                                    depPlatforms = <string[]>$dependency.platforms;
                                }
                                if (depPlatforms.indexOf($product.OS().toString()) !== -1) {
                                    loopOver.push($product.OS());
                                }
                            }
                        });
                        singlePath = dependencyPaths[0].path;
                    }
                    const processNext : any = () : void => {
                        if (index < loopOver.length ||
                            (loopOver.length === 0 && index === 0 && !ObjectValidator.IsEmptyOrNull(singlePath))) {
                            let item : any = loopOver.length > 0 ? loopOver[index] : dependencyPaths[0].os;
                            if (!ObjectValidator.IsEmptyOrNull(singlePath)) {
                                item = {os: item, path: singlePath};
                            }
                            this.installSingle(scriptPath, $dependencyName, item.path, item.os, installScriptObject, () : void => {
                                index++;
                                processNext();
                            });
                        } else {
                            $callback();
                        }
                    };

                    processNext();
                } else {
                    LogIt.Info("WARNING: "[ColorType.YELLOW] +
                        "Specified install script \"" + installScriptObject.name + "\" not found  in search paths. " +
                        "See documentation for more info.");
                    $callback();
                }
            } else {
                $callback();
            }
        }

        protected processDependency($configureOnly : boolean, $dependencyName : string, $dependency : IProjectDependency | string,
                                    $callback : () => void) : void {
            let dependency : IProjectDependency;
            if (typeof $dependency === "string") {
                dependency = <any>{location: $dependency};
            } else {
                dependency = <IProjectDependency>$dependency;
            }

            this.configure($dependencyName, dependency);

            if (!$configureOnly) {
                this.install($dependencyName, dependency, $callback);
            } else {
                $callback();
            }
        }

        protected processDependencies($configureOnly : boolean, $callback : () => void) : void {
            if (this.project.hasOwnProperty("dependencies")) {
                const dependenciesList : string[] = [];
                const nextDependency : any = ($index : number) : void => {
                    if ($index < dependenciesList.length) {
                        const dependencyName : string = dependenciesList[$index];
                        const dependency : IProjectDependency = this.project.dependencies[dependencyName];
                        this.processDependency($configureOnly, dependencyName, dependency, () : void => {
                            nextDependency($index + 1);
                        });
                    } else {
                        if (this.installed === 0) {
                            LogIt.Warning("Dependencies install skipped: none of dependency requires installation");
                        }
                        if (this.configured === 0) {
                            LogIt.Warning("Dependencies configure skipped: no configuration has been processed.");
                        }
                        $callback();
                    }
                };
                let dependencyName : string;
                for (dependencyName in this.project.dependencies) {
                    if (this.project.dependencies.hasOwnProperty(dependencyName)) {
                        dependenciesList.push(dependencyName);
                    }
                }
                nextDependency(0);
            } else {
                LogIt.Warning("Dependencies install and configure skipped: dependencies are not configured");
                $callback();
            }
        }

        protected getName() : string {
            return "dependencies-install";
        }

        protected process($done : any, $option : string) : void {
            this.installed = 0;
            this.configured = 0;
            let configureOnly : boolean = false;
            if ($option === "configure") {
                configureOnly = true;
            }
            this.processDependencies(configureOnly, $done);
        }
    }
}
