/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Runtimes {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ResourceHandler = Com.Wui.Framework.Builder.Utils.ResourceHandler;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;
    import BuildExecutor = Com.Wui.Framework.Builder.Tasks.BuildProcessor.BuildExecutor;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;
    import StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;

    export class WuiModulesRelease extends BaseTask {

        protected getName() : string {
            return "wui-modules-release";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            let connectorLocation : string = "resource/libs/WuiConnector";
            let launcherName : string = "WuiLauncher";
            let launcherIcon : string = "";
            let launcherType : string = "Browser";
            const remotePort : number = this.project.target.remotePort;

            if (this.project.target.name !== "") {
                launcherName = this.project.target.name;
            }

            switch ($option) {
            case ToolchainType.CHROMIUM_RE:
                launcherType = "WuiChromiumRE";
                break;
            case ToolchainType.IDEA:
                launcherType = "WuiIdeaRE";
                break;
            case ToolchainType.ECLIPSE:
                launcherType = "WuiEclipseRE";
                break;
            case ToolchainType.NODEJS:
                launcherType = "WuiNodejsRE";
                break;
            case "Browser":
                connectorLocation = "";
                break;
            default:
                launcherType = $option;
                $option = ToolchainType.NONE;
                break;
            }

            const runtimeTarget : string = ($option === ToolchainType.ECLIPSE || $option === ToolchainType.IDEA) ? "target/" : "";
            launcherIcon = this.fileSystem.NormalizePath(this.properties.projectBase + "/build/target/target/" +
                runtimeTarget + this.project.target.icon);

            /// TODO: add ability to override Connector description
            const resourcesUpdate : any = ($filePath : string, $callback : () => void) : void => {
                ResourceHandler.ModifyIcon($filePath, launcherIcon, () : void => {
                    ResourceHandler.ModifyVersionInfo($filePath, launcherName, $callback);
                });
            };

            let files : string[];
            let copyPath : string;
            let sourcePath : string;
            let connectorPath : string = "";

            if (connectorLocation !== "") {
                sourcePath = this.properties.projectBase + "/build_cache/WuiConnector/";
                const sourcePathLength : number = sourcePath.length;
                for (const file of this.fileSystem.Expand(sourcePath + "**/*.*")) {
                    if (!this.fileSystem.IsDirectory(file) &&
                        file.indexOf("/log/") === -1 &&
                        file.indexOf(".exe.config") === -1 &&
                        file.indexOf(".config.jsonp") === -1 &&
                        file.indexOf("README.md") === -1) {
                        copyPath = this.properties.projectBase + "/build/target/" + connectorLocation + "/" +
                            file.substr(sourcePathLength, file.length);
                        if (StringUtils.EndsWith(copyPath, ".exe")) {
                            copyPath = copyPath.replace("WuiConnector.exe", launcherName + " Connector.exe");
                            connectorPath = copyPath;
                        }
                        this.fileSystem.Write(copyPath, this.fileSystem.Read(file));
                    }
                }
            }

            files = this.fileSystem.Expand(this.properties.projectBase + "/build/target/**/*.*");
            if (files.length > 0) {
                for (const file of files) {
                    if (!this.fileSystem.IsDirectory(file)) {
                        this.fileSystem.Write(file.replace("/build/target/", "/build/apptarget/"), this.fileSystem.Read(file));
                    }
                }
                this.fileSystem.Delete(this.properties.projectBase + "/build/target/");

                sourcePath = this.properties.projectBase + "/build_cache/WuiLauncher";
                if (!this.fileSystem.Exists(sourcePath)) {
                    LogIt.Error("Cannot find WuiLauncher prepared in cache \"" + sourcePath + "\"");
                }

                this.fileSystem.Write(this.properties.projectBase + "/build/target/" + runtimeTarget + launcherName + ".exe",
                    this.fileSystem.Read(sourcePath + "/WuiLauncher.exe"));
                let appConfig : string = this.fileSystem.Read(sourcePath + "/app.config.template.jsonp").toString();
                appConfig = appConfig
                    .replace("target: \"<? @var target ?>\"", "target: \"\"")
                    .replace("    port: \"<? @var port ?>\"," + os.EOL, "    port: " + remotePort + "," + os.EOL)
                    .replace("restart: \"<? @var restart ?>\"", "restart: false")
                    .replace("runtimeEnv: \"<? @var runtime-env ?>\"", "runtimeEnv: \"" + launcherType.toLowerCase() + "\"")
                    .replace("connectorName: \"<? @var connector-name ?>\"", "connectorName: \"" + launcherName + " Connector\"")
                    .replace("elevateConnector: \"<? @var elevate-connector ?>\"",
                        "elevateConnector: " + this.project.target.elevate)
                    .replace("singleton: \"<? @var singleton-app ?>\"", "singleton: " + this.project.target.singletonApp);
                this.fileSystem.Write(this.properties.projectBase + "/build/target/" + runtimeTarget + launcherName + ".config.jsonp",
                    appConfig);

                files = this.fileSystem.Expand(this.properties.projectBase + "/build/apptarget/**/*.*");
                if (files.length > 0) {
                    for (const file of files) {
                        if (!this.fileSystem.IsDirectory(file) &&
                            file.indexOf("/log/") === -1 &&
                            file.indexOf("debug.log") === -1 &&
                            file.indexOf("console.log") === -1) {
                            this.fileSystem.Write(file.replace("/build/apptarget/", "/build/target/target/" + runtimeTarget),
                                this.fileSystem.Read(file));
                        }
                    }
                    this.fileSystem.Delete(this.properties.projectBase + "/build/apptarget/");
                }

                if ($option === ToolchainType.CHROMIUM_RE) {
                    for (const file of this.fileSystem.Expand(this.properties.projectBase + "/build_cache/WuiChromiumRE/**/*.*")) {
                        if (!this.fileSystem.IsDirectory(file)) {
                            this.fileSystem.Write(file.replace("/build_cache/WuiChromiumRE/", "/build/target/wuichromiumre/"),
                                this.fileSystem.Read(file));
                        }
                    }
                    this.fileSystem.Write(this.properties.projectBase + "/build/target/wuichromiumre/" + launcherName + ".exe",
                        this.fileSystem.Read(this.properties.projectBase + "/build/target/wuichromiumre/WuiChromiumRE.exe"));
                    this.fileSystem.Delete(this.properties.projectBase + "/build/target/wuichromiumre/WuiChromiumRE.exe");
                }

                if ($option === ToolchainType.ECLIPSE || $option === ToolchainType.IDEA) {
                    if ($option === ToolchainType.ECLIPSE) {
                        for (const file of this.fileSystem.Expand(this.properties.projectBase + "/build_cache/WuiEclipseRe/**/*.*")) {
                            if (!this.fileSystem.IsDirectory(file)) {
                                this.fileSystem.Write(file.replace("/build_cache/WuiEclipseRe/", "/build/target/"),
                                    this.fileSystem.Read(file));
                            }
                        }
                        this.fileSystem.Write(this.properties.projectBase + "/build/target/META-INF/MANIFEST.MF",
                            this.fileSystem.Read(this.properties.projectBase + "/build_cache/WuiEclipseRe/META-INF/MANIFEST.MF").toString()
                                .replace(/Bundle-Name.*/g, "Bundle-Name: " + launcherName)
                                .replace(/Bundle-SymbolicName.*/g, "Bundle-SymbolicName: " + launcherName + ";singleton=true")
                                .replace(/Bundle-Version.*/g, "Bundle-Version: " + this.properties.javaProjectVersion)
                                .replace(/Bundle-License.*/g, "Bundle-License: " + this.project.license)
                        );
                        this.fileSystem.Write(this.properties.projectBase + "/build/target/plugin.xml",
                            this.fileSystem.Read(this.properties.binBase + "/resource/configs/plugin.eclipse.xml"));
                    } else {
                        for (const file of this.fileSystem.Expand(this.properties.projectBase + "/build_cache/WuiIdeaRe/app/**/*.*")) {
                            if (!this.fileSystem.IsDirectory(file)) {
                                this.fileSystem.Write(file.replace("/build_cache/WuiIdeaRe/app/", "/build/target/"),
                                    this.fileSystem.Read(file));
                            }
                        }

                        this.fileSystem.Write(this.properties.projectBase + "/build/target/META-INF/MANIFEST.MF",
                            this.fileSystem.Read(this.properties.projectBase + "/build/target/META-INF/MANIFEST.MF").toString()
                                .replace(/Specification-Title.*/g, "Specification-Title: " + launcherName)
                                .replace(/Specification-Version.*/g, "Specification-Version: " + this.properties.javaProjectVersion)
                        );
                        this.fileSystem.Write(this.properties.projectBase + "/build/target/META-INF/plugin.xml",
                            this.fileSystem.Read(this.properties.binBase + "/resource/configs/plugin.idea.xml"));
                    }
                    const pluginConfigPath : string = this.properties.projectBase + "/build/target/resource/configs/default.config.jsonp";
                    const executor : BuildExecutor = new BuildExecutor();
                    executor.getTargetProducts(this.project.target).forEach(($product : BuildProductArgs) : void => {
                        if (this.fileSystem.Exists(pluginConfigPath) && (
                            $product.Toolchain() === ToolchainType.ECLIPSE || $product.Toolchain() === ToolchainType.IDEA)) {
                            StringReplace.File(pluginConfigPath, StringReplaceType.VARIABLES);
                        }
                    });
                }

                if ($option === ToolchainType.NODEJS) {
                    for (const file of this.fileSystem.Expand(this.properties.projectBase + "/build_cache/WuiNodejsRE/**/*.*")) {
                        if (!this.fileSystem.IsDirectory(file) &&
                            file.indexOf(".config.jsonp") === -1 &&
                            file.indexOf("/resource/graphics/") === -1 &&
                            file.indexOf("/resource/css/") === -1) {
                            this.fileSystem.Write(file.replace("/build_cache/WuiNodejsRE/", "/build/target/wuinodejsre/"),
                                this.fileSystem.Read(file));
                        }
                    }
                    this.fileSystem.Write(this.properties.projectBase + "/build/target/wuinodejsre/" + launcherName + ".exe",
                        this.fileSystem.Read(this.properties.projectBase + "/build/target/wuinodejsre/WuiNodejsRE.exe"));
                    this.fileSystem.Delete(this.properties.projectBase + "/build/target/wuinodejsre/WuiNodejsRE.exe");
                }

                this.fileSystem.Write(this.properties.projectBase + "/build/target/" + runtimeTarget + "LICENSE.txt",
                    this.fileSystem.Read(this.properties.projectBase + "/build/target/target/" + runtimeTarget + "LICENSE.txt"));
                this.fileSystem.Write(this.properties.projectBase + "/build/target/" + runtimeTarget + "SW-Content-Register.txt",
                    this.fileSystem.Read(this.properties.projectBase + "/build/target/target/" +
                        runtimeTarget + "SW-Content-Register.txt"));
                this.fileSystem.Delete(this.properties.projectBase + "/build/target/target/" + runtimeTarget + "LICENSE.txt");
                this.fileSystem.Delete(this.properties.projectBase + "/build/target/target/" + runtimeTarget + "SW-Content-Register.txt");

                if (launcherIcon !== "") {
                    const replacingBins : string[] =
                        this.fileSystem.Expand(this.properties.projectBase + "/build/target/**/" +
                            launcherName + (EnvironmentHelper.IsWindows() ? ".exe" : ""));
                    const binsCount : number = replacingBins.length;
                    if (binsCount > 0) {
                        const processUpdate : any = ($binIndex : number) : void => {
                            if (ObjectValidator.IsSet(replacingBins[$binIndex])) {
                                resourcesUpdate(replacingBins[$binIndex], () : void => {
                                    $binIndex++;
                                    if ($binIndex < binsCount) {
                                        processUpdate($binIndex);
                                    } else if (connectorPath !== "") {
                                        connectorPath = this.fileSystem.NormalizePath(
                                            connectorPath.replace(this.properties.projectBase + "/build/target",
                                                this.properties.projectBase + "/build/target/" + runtimeTarget + "target"));
                                        ResourceHandler.EmbedFile(connectorPath, "ICONGROUP", "0", launcherIcon, () => {
                                            processUpdate($binIndex);
                                        });
                                    } else {
                                        $done();
                                    }
                                });
                            } else {
                                $done();
                            }
                        };
                        processUpdate(0);
                    } else {
                        $done();
                    }
                } else {
                    $done();
                }
            } else {
                $done();
            }
        }
    }
}
