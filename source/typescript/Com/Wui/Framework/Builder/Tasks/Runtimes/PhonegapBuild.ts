/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Runtimes {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import CredentialsManager = Com.Wui.Framework.Builder.Connectors.CredentialsManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class PhonegapBuild extends BaseTask {

        protected getName() : string {
            return "phonegap-build";
        }

        protected process($done : any) : void {
            const fs : Types.NodeJS.fs = require("fs");
            const phonegapBuildApi : any = require("phonegap-build-api");
            let appId : string;
            const packagePath : string = "build/" + this.properties.packageName;

            const build : any = () : void => {
                if (ObjectValidator.IsEmptyOrNull(this.builderConfig.tokens.phonegap)) {
                    LogIt.Warning("Phonegap build skipped: authentication token has to be specified by " +
                        "WUI Builder config or key-ring task");
                    $done();
                } else {
                    phonegapBuildApi.auth({token: this.builderConfig.tokens.phonegap}, ($error : string, $api : any) : void => {
                            if ($error !== null) {
                                LogIt.Error("Phonegap authentication has failed: " + $error);
                            } else {
                                const platforms : string[] = this.properties.mobilePlatforms;
                                const downloadApp : any = ($type : string, $done : () => void) : void => {
                                    Echo.Print("Downloading target for \"" + $type + "\" from build server ...");
                                    $api.get("/apps/" + appId + "/" + $type, ($error : string) : void => {
                                        if ($error !== null) {
                                            LogIt.Error("Phonegap get target has failed: " + $error);
                                        } else {
                                            LogIt.Info(" finished.");
                                            $done();
                                        }
                                    }).pipe(fs.createWriteStream(packagePath + this.properties.mobileExtensions[$type]));
                                };
                                const downloadNext : any = ($index : number) : void => {
                                    if ($index < platforms.length) {
                                        downloadApp(platforms[$index], () : void => {
                                            downloadNext($index + 1);
                                        });
                                    } else {
                                        $done();
                                    }
                                };

                                const onUpload : any = ($error : string, $data : any) : void => {
                                    if ($error !== null) {
                                        LogIt.Error("Phonegap upload has failed: " + $error);
                                    } else {
                                        LogIt.Info("... uploaded.");
                                        appId = $data.id;
                                        Echo.Print("Build with id \"" + appId + "\" is in progress ");
                                        const checkBuildStatus : any = () : void => {
                                            $api.get("/apps/" + appId, ($error : string, $data : any) : void => {
                                                if ($error !== null) {
                                                    LogIt.Error("Phonegap get build status has failed: " + $error);
                                                } else {
                                                    let readyForDownload : boolean = false;
                                                    platforms.forEach(($type : string) : void => {
                                                        if ($data.status.hasOwnProperty($type)) {
                                                            if ($data.status[$type] === "complete" || $data.status[$type] === "skip") {
                                                                readyForDownload = true;
                                                            } else if ($data.status[$type] === "pending") {
                                                                readyForDownload = false;
                                                            } else if ($data.status[$type] === "error") {
                                                                LogIt.Error(
                                                                    "Phonegap get build for \"" + $type + "\" has failed: " + $error);
                                                                $done();
                                                            }
                                                        }
                                                    });
                                                    if (readyForDownload) {
                                                        LogIt.Info("... ready for download.");
                                                        downloadNext(0);
                                                    } else {
                                                        Echo.Print(".");
                                                        setTimeout(checkBuildStatus, 500);
                                                    }
                                                }
                                            });
                                        };
                                        checkBuildStatus();
                                    }
                                };

                                $api.get("/apps", ($error : string, $data : any) : void => {
                                    if ($error !== null) {
                                        LogIt.Error("Phonegap get apps has failed: " + $error);
                                    } else {
                                        let appExists : boolean = false;
                                        $data.apps.forEach(($app : any) : void => {
                                            if ($app.package === this.project.name) {
                                                appId = $app.id;
                                                appExists = true;
                                            }
                                        });
                                        Echo.Print("Uploading source files to build server ...");
                                        const uploadOptions : any = {
                                            form: {
                                                file: this.properties.projectBase + "/" + packagePath + ".zip"
                                            }
                                        };
                                        if (appExists) {
                                            $api.put("/apps/" + appId, uploadOptions, onUpload);
                                        } else {
                                            uploadOptions.form.data = {
                                                create_method: "file",
                                                title        : this.project.target.name
                                            };
                                            $api.post("/apps/", uploadOptions, onUpload);
                                        }
                                    }
                                });
                            }
                        }
                    );
                }
            };

            if (ObjectValidator.IsEmptyOrNull(this.builderConfig.tokens.phonegap)) {
                CredentialsManager.getPhonegapToken(($value : string) : void => {
                    this.builderConfig.tokens.phonegap = $value;
                    build();
                });
            } else {
                build();
            }
        }
    }
}
