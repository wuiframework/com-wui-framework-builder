/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Runtimes {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import IBuilderWuiModules = Com.Wui.Framework.Builder.Interfaces.IBuilderWuiModules;
    import IBuilderWuiModuleConfig = Com.Wui.Framework.Builder.Interfaces.IBuilderWuiModuleConfig;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IBuilderWuiModuleLocation = Com.Wui.Framework.Builder.Interfaces.IBuilderWuiModuleLocation;
    import Resources = Com.Wui.Framework.Builder.DAO.Resources;
    import UpdatesManagerConnector = Com.Wui.Framework.Services.Connectors.UpdatesManagerConnector;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import CallbackResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.CallbackResponse;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;

    export class WuiModulesDownload extends BaseTask {
        private register : IWuiModulesRegister[];

        protected getName() : string {
            return "wui-modules-download";
        }

        protected process($done : any, $option : string) : void {
            if (!this.fileSystem.Exists(this.wuiModules)) {
                this.fileSystem.CreateDirectory(this.wuiModules);
            }

            const modulesMap : IBuilderWuiModuleConfig[] = this.getModules();
            const modulesRegisterPath : string = this.wuiModules + "/Register.json";
            this.register = <any>{};
            if (this.fileSystem.Exists(modulesRegisterPath)) {
                this.register = JSON.parse(this.fileSystem.Read(modulesRegisterPath).toString());
            }

            const getNextModule : any = ($index : number) : void => {
                if ($index < modulesMap.length) {
                    this.processModule(modulesMap[$index], $option === "force", () : void => {
                        getNextModule($index + 1);
                    });
                } else {
                    this.fileSystem.Write(modulesRegisterPath, JSON.stringify(this.register));
                    $done();
                }
            };
            getNextModule(0);
        }

        private getModules() : IBuilderWuiModuleConfig[] {
            const modulesMap : IBuilderWuiModuleConfig[] = [];
            let name : string;
            const wuiModules : IBuilderWuiModules = this.builderConfig.wuiModules;
            for (name in wuiModules) {
                if (wuiModules.hasOwnProperty(name)) {
                    const module : IBuilderWuiModuleConfig[] = wuiModules[name];
                    let moduleIndex : number;
                    for (moduleIndex = 0; moduleIndex < module.length; moduleIndex++) {
                        const config : IBuilderWuiModuleConfig = module[moduleIndex];
                        config.name = name;
                        if (config.hasOwnProperty("os")) {
                            let fullPath : string = this.wuiModules + "/" + name + "/" + config.os;
                            if (config.hasOwnProperty("arch")) {
                                fullPath += "/" + config.arch;
                            }
                            config.fullPath = this.fileSystem.NormalizePath(fullPath, true);
                            modulesMap.push(config);
                        } else {
                            LogIt.Error("Every module must have \"os\" defined. Corrupted module: " + name);
                        }
                    }
                }
            }
            return modulesMap;
        }

        private processModule($config : IBuilderWuiModuleConfig, $force : boolean, $callback : () => void) : void {
            if ($force) {
                this.fileSystem.Delete($config.fullPath);
            }
            const moduleLocation : IBuilderWuiModuleLocation = {
                path       : this.builderConfig.deployServer[<any>this.project.target.wuiModulesType],
                platform   : "",
                projectName: $config.name,
                releaseName: "",
                version    : ""
            };
            if (!ObjectValidator.IsString($config.location)) {
                if (!ObjectValidator.IsEmptyOrNull($config.version) && $config.version !== "latest") {
                    moduleLocation.version = $config.version;
                }
                if (!ObjectValidator.IsEmptyOrNull(<IBuilderWuiModuleLocation>$config.location)) {
                    Resources.Extend(moduleLocation, <IBuilderWuiModuleLocation>$config.location);
                    if (moduleLocation.version === "latest") {
                        moduleLocation.version = "";
                    }
                }
            } else {
                moduleLocation.path = <string>$config.location;
            }

            let downloadRequired : boolean = false;
            let moduleRegister : IWuiModulesRegister;
            if (this.register.hasOwnProperty($config.name) &&
                this.register[$config.name].hasOwnProperty($config.os) &&
                this.register[$config.name][$config.os].hasOwnProperty($config.arch)) {
                moduleRegister = this.register[$config.name][$config.os][$config.arch];
                if (!ObjectValidator.IsEmptyOrNull(moduleLocation.version) && !ObjectValidator.IsEmptyOrNull(moduleRegister.version) &&
                    moduleRegister.version !== moduleLocation.version) {
                    downloadRequired = true;
                }
            } else {
                downloadRequired = true;
            }

            if (!this.fileSystem.Exists($config.fullPath) ||
                StringUtils.StartsWith(moduleLocation.path, "file://") ||
                downloadRequired) {
                this.download($config, moduleLocation, $callback);
            } else {
                const connector : UpdatesManagerConnector = new UpdatesManagerConnector(5);
                connector
                    .UpdateExists(
                        moduleLocation.projectName,
                        moduleLocation.releaseName,
                        moduleLocation.platform,
                        moduleRegister.version,
                        new Date(moduleRegister.buildTime).getTime(),
                        !ObjectValidator.IsEmptyOrNull(moduleLocation.version))
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            this.download($config, moduleLocation, $callback);
                        } else {
                            $callback();
                        }
                    });
            }
        }

        private download($config : IBuilderWuiModuleConfig, $location : IBuilderWuiModuleLocation, $callback : () => void) : void {
            this.fileSystem.Delete($config.fullPath);
            LogIt.Info(">>"[ColorType.YELLOW] + " Copy of module from \"" + $location.path + "\" to \"" +
                $config.fullPath + "\".");

            const isLocalSource : boolean = StringUtils.StartsWith($location.path, "file://");
            let downloadSource : string;
            if (isLocalSource) {
                downloadSource = $location.path;
            } else {
                if (ObjectValidator.IsEmptyOrNull($location.releaseName)) {
                    $location.releaseName = "null";
                }
                if (ObjectValidator.IsEmptyOrNull($location.platform)) {
                    $location.platform = "null";
                }
                downloadSource = $location.path + "/Update/" +
                    $location.projectName + "/" + $location.releaseName + "/" + $location.platform;
                if (!ObjectValidator.IsEmptyOrNull($location.version)) {
                    downloadSource += "/" + $location.version;
                }
            }
            const responseHandler : IResponse = new CallbackResponse(($headers : string, $tempPath : string) : void => {
                $config.sourcePath = $tempPath;
                if (this.fileSystem.Exists($config.sourcePath)) {
                    const copySource : any = ($source : string, $destination : string) : void => {
                        this.fileSystem.Copy($source, $destination, ($status : boolean) : void => {
                            LogIt.Info("");
                            this.fileSystem.Delete($source);
                            if ($status) {
                                LogIt.Info("");
                                if (!isLocalSource) {
                                    if (!this.register.hasOwnProperty($config.name)) {
                                        this.register[$config.name] = {};
                                    }
                                    if (!this.register[$config.name].hasOwnProperty($config.os)) {
                                        this.register[$config.name][$config.os] = {};
                                    }
                                    this.register[$config.name][$config.os][$config.arch] = {
                                        buildTime: $headers["last-modified"],
                                        version  : $headers["content-version"]
                                    };
                                    if (this.register[$config.name][$config.os][$config.arch].version) {
                                        this.register[$config.name][$config.os][$config.arch].version = $headers["x-content-version"];
                                    }
                                } else if (this.register.hasOwnProperty($config.name)) {
                                    delete this.register[$config.name];
                                }
                                $callback();
                            } else {
                                LogIt.Error("Copy of " + $source + " to " + $destination);
                            }
                        });
                    };
                    if (this.fileSystem.IsDirectory($config.sourcePath)) {
                        copySource($config.sourcePath, $config.fullPath);
                    } else {
                        this.fileSystem.Unpack($config.sourcePath, null, ($outputPath : string) : void => {
                            LogIt.Info("");
                            this.fileSystem.Delete($config.sourcePath);
                            copySource($outputPath, $config.fullPath);
                        });
                    }
                } else {
                    LogIt.Error("Copy of " + $config.sourcePath + " has failed: path does not exist");
                }
            });
            let isFallback : boolean = false;
            responseHandler.OnError = ($message : string | Error) : void => {
                if (ObjectValidator.IsString($message)) {
                    if (!isFallback && StringUtils.Contains(<string>$message, "status code: " + HttpStatusType.NOT_FOUND)) {
                        if (!StringUtils.Contains(downloadSource, this.builderConfig.deployServer.prod)) {
                            isFallback = true;
                            LogIt.Warning("Failed to download WUI Module from: " + downloadSource + ". " +
                                "Falling back to download from: " + this.builderConfig.deployServer.prod);
                            downloadSource = StringUtils.Replace(downloadSource,
                                this.builderConfig.deployServer[<any>this.project.target.wuiModulesType],
                                this.builderConfig.deployServer.prod);
                            this.fileSystem.Download(downloadSource, responseHandler);
                        } else {
                            LogIt.Error(<string>$message);
                        }
                    } else {
                        LogIt.Error(<string>$message);
                    }
                } else {
                    LogIt.Error("Failed to download WUI Module.", <Error>$message);
                }
            };
            this.fileSystem.Download(downloadSource, responseHandler);
        }
    }

    export abstract class IWuiModulesRegister extends IBuilderWuiModuleConfig {
        public buildTime : number;
    }
}
