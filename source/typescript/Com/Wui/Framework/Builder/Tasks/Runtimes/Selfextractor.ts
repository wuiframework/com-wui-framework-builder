/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Runtimes {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ResourceHandler = Com.Wui.Framework.Builder.Utils.ResourceHandler;
    import IProjectSplashScreen = Com.Wui.Framework.Builder.Interfaces.IProjectSplashScreen;
    import IProjectSplashScreenResource = Com.Wui.Framework.Builder.Interfaces.IProjectSplashScreenResource;
    import ResourceType = Com.Wui.Framework.Builder.Enums.ResourceType;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;

    export class Selfextractor extends BaseTask {
        private configPath : string;

        protected getName() : string {
            return "selfextractor";
        }

        protected process($done : any, $option : string) : void {
            const cwd : string = this.properties.projectBase + "/build";
            this.configPath = cwd + "/compiled/selfextractor.config.json";

            let targetExtension : string = "";
            if (EnvironmentHelper.IsWindows()) {
                targetExtension = ".exe";
            }
            let packagePath : string = cwd + "/" + this.project.target.name + targetExtension;
            if (this.project.target.name === this.project.name) {
                packagePath = cwd + "/" + this.properties.packageName + targetExtension;
            }

            if (ObjectValidator.IsString(this.project.target.splashScreen)) {
                this.project.target.splashScreen = <IProjectSplashScreen>{
                    url: StringReplace.Content(<string>this.project.target.splashScreen, StringReplaceType.VARIABLES)
                };
            }

            const url : string = (<IProjectSplashScreen>this.project.target.splashScreen).url;
            if (ObjectValidator.IsEmptyOrNull(url) || require("url").parse(url).protocol === null) {
                let executable : string = "WUILauncher" + targetExtension;
                if (!this.fileSystem.Exists(this.properties.projectBase + "/build/target/" + executable)) {
                    executable = this.project.target.name + targetExtension;
                }
                if (!this.fileSystem.Exists(cwd + "/target/" + executable)) {
                    executable = this.properties.packageName + "/target/index.html";
                } else {
                    executable = this.properties.packageName + "/" + executable;
                }
                if (this.fileSystem.Exists(this.properties.projectBase + "/build/target")) {
                    if ((<IProjectSplashScreen>this.project.target.splashScreen).url === "") {
                        (<IProjectSplashScreen>this.project.target.splashScreen).executable = executable;
                    }
                    this.create(packagePath, $done);
                } else {
                    LogIt.Error("Target has to be prepared by Build task before run of this task.");
                }
            } else {
                this.create(packagePath, $done);
            }
        }

        private create($pkgPath : string, $callback : () => void) : void {
            const url : Types.NodeJS.url = require("url");
            const splash : IProjectSplashScreen = <IProjectSplashScreen>JSON.parse(JSON.stringify(this.project.target.splashScreen));

            this.fileSystem.Copy(
                this.wuiModules + "/com-wui-framework-selfextractor/win/32bit/WuiSelfExtractor.exe", $pkgPath,
                ($status : boolean) : void => {
                    if ($status) {
                        const finalize : any = () : void => {
                            this.fileSystem.Write(this.configPath, JSON.stringify(splash, null, 2));

                            let iconPath : string = this.project.target.icon;
                            if (this.fileSystem.Exists(this.properties.projectBase + "/build/target/target")) {
                                iconPath = "target/" + iconPath;
                            }
                            iconPath = this.properties.projectBase + "/build/target/" + iconPath;
                            ResourceHandler.ModifyIcon($pkgPath, iconPath, () : void => {
                                ResourceHandler.ModifyVersionInfo($pkgPath, this.project.target.name, () : void => {
                                    ResourceHandler.ModifyManifest($pkgPath, () : void => {
                                        ResourceHandler.EmbedFile($pkgPath,
                                            "CONFIG", "SELFEXTRACTOR_CONF_JSON", this.configPath, () : void => {
                                                LogIt.Info("Selfextract package has been created at: " + $pkgPath);
                                                $callback();
                                            });
                                    });
                                });
                            });
                        };

                        const prepName : any = (name : string) : string => {
                            return name.replace(/\./g, "_").replace(/\//g, "_").replace(/-/g, "_").toUpperCase();
                        };

                        const embedFile : any = ($resLoc : string, $callBack : () => void) : void => {
                            if (ObjectValidator.IsSet($resLoc) && url.parse($resLoc).protocol === null &&
                                $resLoc.substr(0, 1) !== "#" && $resLoc.substr(0, 2) !== "0x") {
                                const iResource : IProjectSplashScreenResource = <IProjectSplashScreenResource>{
                                    copyOnly: false,
                                    location: $resLoc,
                                    name    : prepName($resLoc),
                                    type    : ResourceType.RESOURCE
                                };
                                ResourceHandler.EmbedFile($pkgPath, iResource.type.toString(), iResource.name,
                                    this.properties.projectBase + "/" + $resLoc,
                                    () : void => {
                                        splash.resources.push(iResource);
                                        $callBack();
                                    });
                            } else {
                                $callBack();
                            }
                        };

                        const embedLocFile : any = ($resLoc : string, $callback : () => void) : void => {
                            if (ObjectValidator.IsSet($resLoc) && url.parse($resLoc).protocol === null) {
                                ResourceHandler.EmbedFile($pkgPath, "CONFIG", "LANG",
                                    this.properties.projectBase + "/" + $resLoc, $callback);
                            } else {
                                $callback();
                            }
                        };

                        const embedGuiFiles : any = () : void => {
                            if (ObjectValidator.IsSet(splash.background)) {
                                embedFile(splash.background, () => {
                                    if (ObjectValidator.IsSet(splash.userControls) && ObjectValidator.IsSet(splash.userControls.progress)) {
                                        embedFile(splash.userControls.progress.foreground, () : void => {
                                            embedFile(splash.userControls.progress.background, () : void => {
                                                finalize();
                                            });
                                        });
                                    } else {
                                        finalize();
                                    }
                                });
                            } else {
                                finalize();
                            }
                        };

                        const embedFiles : any = () : void => {
                            if (ObjectValidator.IsSet(splash.localization)) {
                                embedLocFile(splash.localization, embedGuiFiles);
                            } else {
                                embedGuiFiles();
                            }
                        };

                        if (ObjectValidator.IsEmptyOrNull(splash.url) || url.parse(splash.url).protocol === null) {
                            const archiveName : string = this.properties.packageName + ".zip";
                            const appArchive : string = "build/" + archiveName;
                            splash.resources.push(<IProjectSplashScreenResource>{
                                copyOnly: false,
                                location: appArchive,
                                name    : prepName(archiveName),
                                type    : ResourceType.RESOURCE
                            });
                            LogIt.Info("Offline SelfExtractor: processing resources for given configuration" +
                                " splashScreen: " + JSON.stringify(splash.resources, null, 2));
                            if (splash.resources.length > 0) {
                                let pending : number = splash.resources.length;
                                const process : any = ($callback : () => void) : void => {
                                    pending -= 1;
                                    const resLoc : string = splash.resources[pending].location;
                                    let iResource : IProjectSplashScreenResource;
                                    if (url.parse(resLoc).protocol === null) {
                                        if (splash.resources[pending].location !== appArchive) {
                                            iResource = <IProjectSplashScreenResource>{
                                                copyOnly: true,
                                                location: resLoc,
                                                name    : prepName(resLoc),
                                                type    : ResourceType.RESOURCE
                                            };
                                        } else {
                                            iResource = splash.resources[pending];
                                        }
                                        ResourceHandler.EmbedFile($pkgPath, iResource.type.toString(), iResource.name,
                                            this.properties.projectBase + "/" + resLoc,
                                            () : void => {
                                                if (iResource.location === appArchive) {
                                                    iResource.location = archiveName;
                                                }
                                                splash.resources[pending] = iResource;
                                                if (pending > 0) {
                                                    process($callback);
                                                } else {
                                                    $callback();
                                                }
                                            });
                                    } else {
                                        iResource = <IProjectSplashScreenResource>{
                                            copyOnly: true,
                                            location: resLoc,
                                            type    : ResourceType.ONLINE
                                        };
                                        splash.resources[pending] = iResource;
                                        if (pending > 0) {
                                            process($callback);
                                        } else {
                                            $callback();
                                        }
                                    }
                                };

                                process(() : void => {
                                    embedFiles();
                                });
                            } else {
                                embedFiles();
                            }
                        } else {
                            LogIt.Info("Online SelfExtractor: resources and configuration is defined externally.");
                            finalize();
                        }
                    }
                });
        }
    }
}
