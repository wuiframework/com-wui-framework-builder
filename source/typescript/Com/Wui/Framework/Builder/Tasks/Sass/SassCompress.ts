/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Sass {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;

    export class SassCompress extends BaseTask {

        protected getName() : string {
            return "cssmin";
        }

        protected process($done : any, $option : string) : void {
            const CleanCSS : any = require("clean-css");

            const input : any[] = [];
            this.fileSystem.Expand(this.properties.projectBase + "/" + this.properties.tmp + "/sass/**/*.css")
                .forEach(($file : string) : void => {
                    input.push(this.fileSystem.Read($file));
                });
            const result : any = new CleanCSS({}).minify(Buffer.concat(input).toString());
            LogIt.Debug(">>"[ColorType.YELLOW] + " compressed from {0} to {1}",
                Convert.IntegerToSize(result.stats.originalSize), Convert.IntegerToSize(result.stats.minifiedSize));
            if (!ObjectValidator.IsEmptyOrNull(result.warnings)) {
                LogIt.Warning("WARNINGS: "[ColorType.YELLOW] + JSON.stringify(result.warnings, null, 2));
            }
            if (!ObjectValidator.IsEmptyOrNull(result.errors)) {
                LogIt.Error("ERRORS: "[ColorType.RED] + JSON.stringify(result.errors, null, 2));
            }
            this.fileSystem.Write(this.properties.projectBase + "/" + this.properties.dest +
                "/resource/css/" + this.properties.packageName + ".min.css", this.properties.licenseText + result.styles);

            $done();
        }
    }
}
