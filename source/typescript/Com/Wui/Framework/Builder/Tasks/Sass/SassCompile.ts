/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Sass {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import CliTaskType = Com.Wui.Framework.Builder.Enums.CliTaskType;

    export class SassCompile extends BaseTask {

        protected getName() : string {
            return "sass-compile";
        }

        protected process($done : any, $option : string) : void {
            if (EnvironmentHelper.IsEmbedded()) {
                process.env.SASS_BINARY_PATH = Loader.getInstance().getProgramArgs().ProjectBase() +
                    "/resource/libs/node-sass/binding.node";
            }
            const sass : any = require("node-sass");
            const concatMap : any = require("concat-with-sourcemaps");
            let concat : any;
            const compilator : any = ($options : ISassConfigOptions, $expand : boolean, $cwd : string, $source : string,
                                      $dest : string, $ext : string) : void => {
                let style : string = "none";
                let sourcemap : boolean;
                if (!ObjectValidator.IsEmptyOrNull($options)) {
                    style = $options.style;
                    sourcemap = $options.sourcemap;
                }
                let files : string[];
                if ($expand === true) {
                    files = this.fileSystem.Expand($cwd + $source);
                } else {
                    files = [<string>this.fileSystem.Read($cwd + $source).toString()];
                }
                LogIt.Info("compile sass: \"" + $cwd + "\", files found count: " + files.length);

                for (const file of files) {
                    const path : string = $dest +
                        file.substr(file.indexOf($cwd) + $cwd.length, file.length).replace(".scss", $ext);
                    try {
                        const result : any = sass.renderSync({
                            file,
                            includePaths: ["resource/"],
                            outFile     : !ObjectValidator.IsEmptyOrNull(sourcemap) ? path : null,
                            outputStyle : style,
                            sourceMap   : sourcemap
                        });
                        if (ObjectValidator.IsEmptyOrNull(result.map)) {
                            concat.add(path, result.css.toString());
                        } else {
                            concat.add(path, result.css.toString(), result.map.toString());
                        }
                    } catch (ex) {
                        let status : string;
                        switch (ex.status) {
                        case 1:
                            status = "[E] Syntax Error";
                            break;
                        default:
                            status = "[W] Unknown error status " + ex.status;
                        }
                        LogIt.Error(ex.file + ":" + ex.line + " " + status + ": " + ex.message);
                    }
                }
            };

            const configs : ISassConfig[] = <any>{
                /* tslint:disable: object-literal-sort-keys */
                source      : {
                    options: {
                        style    : "expanded",
                        sourcemap: this.build.type === CliTaskType.DEV
                    },
                    files  : [
                        {
                            expand: true,
                            cwd   : "resource/sass",
                            src   : ["**/*.scss"],
                            dest  : this.properties.tmp + "/sass/Ι_" + this.project.name,
                            ext   : ".css"
                        }
                    ]
                },
                dependencies: {
                    options: {
                        style    : "expanded",
                        sourcemap: false
                    },
                    files  : [
                        {
                            expand: true,
                            cwd   : "dependencies",
                            src   : ["*/resource/sass/**/*.scss"],
                            dest  : this.properties.tmp + "/sass",
                            ext   : ".css"
                        }
                    ]
                },
                cache       : {
                    options: {
                        style    : "expanded",
                        sourcemap: true
                    },
                    files  : [
                        {
                            expand: true,
                            cwd   : "dependencies",
                            src   : ["*/resource/sass/**/*.scss"],
                            dest  : "build_cache/sass",
                            ext   : ".css"
                        }
                    ]
                }
                /* tslint:enable */
            };
            if (!ObjectValidator.IsEmptyOrNull(configs)) {
                if (configs.hasOwnProperty($option)) {
                    const config : ISassConfig = configs[$option];
                    let filesGroup : ISassConfigFiles;
                    let groupIndex : number;
                    for (groupIndex = 0; groupIndex < config.files.length; groupIndex++) {
                        filesGroup = config.files[groupIndex];

                        const cssFile : string = filesGroup.dest + "/" + $option + ".css";
                        concat = new concatMap(true, cssFile, "\n");

                        let fileIndex : number;
                        for (fileIndex = 0; fileIndex < filesGroup.src.length; fileIndex++) {
                            compilator(
                                config.options,
                                !ObjectValidator.IsEmptyOrNull(filesGroup.expand) ? filesGroup.expand : false,
                                filesGroup.cwd + "/",
                                filesGroup.src[fileIndex],
                                filesGroup.dest + "/",
                                !ObjectValidator.IsEmptyOrNull(filesGroup.ext) ? filesGroup.ext : ".css");
                        }

                        let content : string = concat.content.toString()
                            .replace(/\((.*?)\)/gm, ($match : string, $value : string) : string => {
                                let escaped : boolean = false;
                                if (StringUtils.Contains($value, "\"")) {
                                    $value = StringUtils.Remove($value, "\"");
                                    escaped = true;
                                }
                                ["graphics/", "libs/"].forEach(($path : string) : void => {
                                    if (StringUtils.Contains($value, $path)) {
                                        $value = "../" + StringUtils.Substring($value, StringUtils.IndexOf($value, $path));
                                    }
                                });
                                if (escaped) {
                                    $value = "\"" + $value + "\"";
                                }
                                return "(" + $value + ")";
                            });
                        if (config.options.sourcemap) {
                            content = content.replace(/^\/\*# sourceMappingURL=.*\.css\.map \*\/$/gm, "") +
                                "/*# sourceMappingURL=" + this.properties.packageName + ".min.css.map */\n";
                            const contentMap : any = JSON.parse(concat.sourceMap);
                            if (!ObjectValidator.IsEmptyOrNull(contentMap.sources)) {
                                let index : number;
                                for (index = 0; index < contentMap.sources.length; index++) {
                                    const source : string = contentMap.sources[index];
                                    const replaceKey : string =
                                        StringUtils.Contains(source, "dependencies/") ? "dependencies" : "resource/sass";
                                    contentMap.sources[index] = "../../../.." +
                                        StringUtils.Substring(source, StringUtils.IndexOf(source, "/" + replaceKey + "/"));
                                }
                            }
                            this.fileSystem.Write(cssFile + ".map", JSON.stringify(contentMap));
                        }
                        this.fileSystem.Write(cssFile, content);
                    }
                } else {
                    LogIt.Error("Configuration for SASS compile task " + $option + " does not exist.");
                }
            } else {
                LogIt.Error("Configuration for SASS compile task does not exist.");
            }
            $done();
        }
    }

    export abstract class ISassConfigOptions {
        public style : string;
        public sourcemap : boolean;
    }

    export abstract class ISassConfigFiles {
        public options : ISassConfigFiles;
        public expand : boolean;
        public cwd : string;
        public src : string[];
        public dest : string;
        public ext : string;
    }

    export abstract class ISassConfig {
        public options : ISassConfigOptions;
        public files : ISassConfigFiles[];
    }
}
