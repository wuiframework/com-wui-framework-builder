/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Sass {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;

    export class SassLint extends BaseTask {

        protected getName() : string {
            return "sass-lint";
        }

        protected process($done : any) : void {
            const sassLint : any = require("sass-lint");

            const patterns : string[] = [this.properties.resources + "/sass/**/*.scss"];
            if (this.programArgs.getOptions().withDependencies) {
                patterns.push(this.properties.dependenciesResources + "/sass/**/*.scss");
            }

            const files : number = this.fileSystem.Expand(patterns).length;
            if (files > 0) {
                let passed : boolean = true;
                sassLint.lintFiles(patterns.join(", "), {}, this.properties.binBase + "/resource/configs/sasslint.conf.yml")
                    .forEach(($report : any) : void => {
                        if ($report.warningCount > 0 || $report.errorCount > 0) {
                            passed = false;
                            LogIt.Info($report.filePath);
                            $report.messages.forEach(($message : any) : void => {
                                LogIt.Info("   " + $message.line + ":" + $message.column + "  " + $message.ruleId + "  " +
                                    $message.message);
                            });
                        }
                    });
                if (passed) {
                    LogIt.Info(">>"[ColorType.YELLOW] + " " + files + " files lint free.");
                    $done();
                } else {
                    LogIt.Error("SASS lint has failed");
                }
            } else {
                LogIt.Info(">>"[ColorType.RED] + " 0 files linted");
                $done();
            }
        }
    }
}
