/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Deploy {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BuildCheck = Com.Wui.Framework.Builder.Tasks.Testing.BuildCheck;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;
    import ProductType = Com.Wui.Framework.Builder.Enums.ProductType;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ReleasePlatform extends BaseTask {

        protected getName() : string {
            return "release-platform";
        }

        protected process($done : any, $option : string) : void {
            const args : BuildProductArgs = new BuildProductArgs();
            args.Parse($option);
            const buildPath : string = this.properties.projectBase + "/build";
            const targetPath : string = buildPath + "/target";
            const archiveName : string = this.properties.packageName + (
                (args.OS() === OSType.LINUX || args.OS() === OSType.IMX || args.OS() === OSType.MAC) ? ".tar.gz" : ".zip");
            const archivePath : string = buildPath + "/" + archiveName;

            let destination : string = buildPath + "/releases";
            if (!ObjectValidator.IsEmptyOrNull(this.build.releaseName)) {
                destination += "/" + this.build.releaseName;
            }
            destination += "/" + $option;

            if (args.Type() === ProductType.INSTALLER) {
                let appName : string = this.project.target.name;
                if (this.project.target.name === this.project.name) {
                    appName = this.properties.packageName;
                }
                if (args.OS() === OSType.WIN) {
                    appName += ".exe";
                }
                this.fileSystem.Copy(buildPath + "/" + appName, destination + "/" + appName, () : void => {
                    this.fileSystem.Delete(buildPath + "/" + appName);
                    this.fileSystem.Delete(archivePath);
                    $done();
                });
            } else {
                this.fileSystem.Delete(destination + "/target");
                this.fileSystem.Copy(targetPath, destination + "/target", () : void => {
                    this.fileSystem.Copy(archivePath, destination + "/" + archiveName, () : void => {
                        if (args.Toolchain() === ToolchainType.PHONEGAP) {
                            const appName : string = this.properties.packageName + this.properties.mobileExtensions[<any>args.OS()];
                            this.fileSystem.Copy(buildPath + "/" + appName, destination + "/" + appName, () : void => {
                                this.fileSystem.Delete(buildPath + "/" + appName);
                                this.fileSystem.Delete(archivePath);
                                $done();
                            });
                        } else if (args.Toolchain() === ToolchainType.NODEJS) {
                            let binaryPath : string = buildPath + "/" + this.project.target.name;
                            if (args.OS() === OSType.WIN) {
                                binaryPath += ".exe";
                            }
                            this.fileSystem.Delete(binaryPath);
                            $done();
                        } else if (args.Type() !== ProductType.PLUGIN &&
                            (args.Toolchain() === ToolchainType.ECLIPSE || args.Toolchain() === ToolchainType.IDEA)) {
                            this.fileSystem.Delete(archivePath);
                            this.fileSystem.Delete(targetPath);
                            $done();
                        } else {
                            this.fileSystem.Delete(archivePath);
                            if (args.Toolchain() !== ToolchainType.NONE) {
                                const backupPath : string = buildPath + "/webtarget";
                                let appTarget : string = targetPath;
                                let webTarget : string = targetPath + "/target";
                                if (args.Toolchain() === ToolchainType.ECLIPSE ||
                                    args.Toolchain() === ToolchainType.IDEA) {
                                    appTarget += "/target";
                                    webTarget += "/target";
                                }
                                this.fileSystem.Copy(webTarget, backupPath, () : void => {
                                    if (args.Toolchain() !== ToolchainType.ECLIPSE &&
                                        args.Toolchain() !== ToolchainType.IDEA) {
                                        this.fileSystem.Write(backupPath + "/LICENSE.txt",
                                            this.fileSystem.Read(appTarget + "/LICENSE.txt"));
                                        this.fileSystem.Write(backupPath + "/SW-Content-Register.txt",
                                            this.fileSystem.Read(appTarget + "/SW-Content-Register.txt"));
                                    }
                                    this.fileSystem.Delete(backupPath + "/resource/libs/WuiConnector");
                                    this.fileSystem.Delete(backupPath + "/wuirunner.config.jsonp");
                                    this.fileSystem.Delete(targetPath);
                                    this.fileSystem.Copy(backupPath, targetPath, () : void => {
                                        this.fileSystem.Delete(backupPath);
                                        if (args.Toolchain() === ToolchainType.ECLIPSE ||
                                            args.Toolchain() === ToolchainType.IDEA) {
                                            const packagePath = destination + "/" + this.properties.packageName;
                                            const finalPackageName = this.project.name + "-"
                                                + this.project.version.replace(/\./g, "-");
                                            const jarPath = destination + "/" + finalPackageName + ".jar";

                                            this.fileSystem.Rename(packagePath + ".zip", jarPath);
                                            this.fileSystem.Delete(destination + "/target");
                                            BuildCheck.RegisterCheck(($done : () => void) : void => {
                                                if (!this.fileSystem.Exists(destination + "/" + finalPackageName + ".jar") &&
                                                    !this.fileSystem.Exists(destination + "/" + finalPackageName + ".zip")) {
                                                    LogIt.Error("Release packaged not found.");
                                                }
                                                $done();
                                            });
                                            if (args.Toolchain() === ToolchainType.IDEA) {
                                                this.fileSystem.Copy(
                                                    this.properties.projectBase + "/build_cache/WuiIdeaRE/lib",
                                                    destination + "/" + finalPackageName + "/lib",
                                                    () : void => {
                                                        this.fileSystem.Copy(jarPath,
                                                            destination + "/" + finalPackageName + "/lib/"
                                                            + finalPackageName + ".jar",
                                                            () : void => {
                                                                this.fileSystem.Delete(jarPath);
                                                                this.fileSystem.Pack(destination + "/" + finalPackageName,
                                                                    <any>{output: destination + "/" + finalPackageName + ".zip"},
                                                                    () : void => {
                                                                        this.fileSystem.Delete(
                                                                            destination + "/" + finalPackageName);
                                                                        $done();
                                                                    });
                                                            });
                                                    });
                                            } else {
                                                $done();
                                            }
                                        } else {
                                            $done();
                                        }
                                    });
                                });
                            } else {
                                $done();
                            }
                        }
                    });
                });
            }
        }
    }
}
