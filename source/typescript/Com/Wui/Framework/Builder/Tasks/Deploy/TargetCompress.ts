/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Deploy {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class TargetCompress extends BaseTask {

        protected getName() : string {
            return "compress";
        }

        protected process($done : any, $option : string) : void {
            const configs : ICompressConfigs[] = <any>{
                /* tslint:disable: object-literal-sort-keys */
                deploy      : {
                    type: this.build.product.OS() === OSType.WIN ? "zip" : "tar.gz"
                },
                deployJava  : {
                    ignore: "build/target/target/**/*"
                },
                pluginTarget: {
                    dest : "build/target/target",
                    files: "build/target/target/**/*"
                },
                docs        : {
                    dest: "build/" + this.properties.packageName + "[docs]"
                },
                phonegap    : {}
                /* tslint:enable */
            };
            if (configs.hasOwnProperty($option)) {
                const config : ICompressConfigs = configs[$option];
                if (ObjectValidator.IsEmptyOrNull(config.dest)) {
                    config.dest = "build/" + this.properties.packageName;
                }
                if (ObjectValidator.IsEmptyOrNull(config.files)) {
                    config.files = "build/target/**/*";
                }
                if (ObjectValidator.IsEmptyOrNull(config.type)) {
                    config.type = "zip";
                }
                if (!ObjectValidator.IsEmptyOrNull(config.ignore)) {
                    this.fileSystem.Pack([
                        this.properties.projectBase + "/" + config.files,
                        "!" + this.properties.projectBase + "/" + config.ignore
                    ], {
                        cwd   : this.properties.projectBase + "/" +
                            StringUtils.Substring(config.files, 0, StringUtils.IndexOf(config.files, "/*", false)),
                        output: this.properties.projectBase + "/" + config.dest + "." + config.type,
                        type  : config.type
                    }, () : void => {
                        $done();
                    });
                } else {
                    this.fileSystem.Pack(this.properties.projectBase + "/" + config.files, {
                        output: this.properties.projectBase + "/" + config.dest + "." + config.type,
                        type  : config.type
                    }, () : void => {
                        $done();
                    });
                }
            } else {
                LogIt.Error("Target compress failed: unable to find \"" + $option + "\" configuration");
            }
        }
    }

    export abstract class ICompressConfigs {
        public files : string;
        public dest : string;
        public type : string;
        public ignore : string;
    }
}
