/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Deploy {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IProjectDeploy = Com.Wui.Framework.Builder.Interfaces.IProjectDeploy;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import UpdatesManagerConnector = Com.Wui.Framework.Services.Connectors.UpdatesManagerConnector;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;

    export class ConfigDeploy extends BaseTask {
        private client : UpdatesManagerConnector;
        private server : string;
        private maxChunkSize : number;

        protected getName() : string {
            return "config-deploy";
        }

        protected process($done : any, $option : string) : void {
            const deploy : IProjectDeploy = this.project.deploy;
            this.maxChunkSize = deploy.chunkSize;
            this.server = deploy.server.dev;
            if (!ObjectValidator.IsEmptyOrNull($option)) {
                if (deploy.server.hasOwnProperty($option)) {
                    this.server = deploy.server[$option];
                } else {
                    LogIt.Error("Unsupported config deploy option \"" + $option + "\".");
                }
            }
            if (ObjectValidator.IsEmptyOrNull(this.server)) {
                LogIt.Error("Config deploy failed: unable to find deploy server configuration");
            }
            if (ObjectValidator.IsEmptyOrNull(this.project.target.hubLocation)) {
                this.project.target.hubLocation = this.server;
            }

            this.client = new UpdatesManagerConnector(false, this.server + "/connector.config.jsonp");
            this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                LogIt.Error($eventArgs.Exception().ToString());
            });

            if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().file)) {
                this.processConfig(this.properties.projectBase + "/" + this.programArgs.getOptions().file, $done);
            } else {
                const configs : string[] = this.fileSystem.Expand(this.project.target.configs);
                const processNextConfig : any = ($index : number) : void => {
                    if ($index < configs.length) {
                        this.processConfig(this.properties.projectBase + "/" + configs[$index], () : void => {
                            processNextConfig($index + 1);
                        });
                    } else {
                        $done();
                    }
                };
                processNextConfig(0);
            }
        }

        private processConfig($path : string, $callback : () => void) : void {
            $path = this.fileSystem.NormalizePath($path);
            const info : IConfigFileInfo = this.getConfigInfo($path);
            if (!ObjectValidator.IsEmptyOrNull(info)) {
                try {
                    const processData : any = () : void => {
                        this.uploadConfig(this.project.name, $path, info.version, info.data, $callback);
                    };
                    if (info.files.length > 0) {
                        const processNextFile : any = ($index : number) : void => {
                            if ($index < info.files.length) {
                                this.uploadFile(info.files[$index], () : void => {
                                    processNextFile($index + 1);
                                });
                            } else {
                                processData();
                            }
                        };
                        processNextFile(0);
                    } else {
                        processData();
                    }
                } catch (ex) {
                    LogIt.Error(ex.message);
                }
            } else {
                LogIt.Error("Failed to process config data.");
            }
        }

        private getConfigInfo($path : string) : IConfigFileInfo {
            if (!this.fileSystem.Exists($path)) {
                LogIt.Warning("File for upload does not exists: " + $path);
                return null;
            }
            if (StringUtils.EndsWith($path, ".jsonp")) {
                const path : Types.NodeJS.path = require("path");
                const stripJsonComments : any = require("strip-json-comments");

                let version : string = "1.0.0";
                const files : IFileInfo[] = [];
                let data : string = this.fileSystem.Read($path).toString();
                data = StringReplace.Content(data, StringReplaceType.VARIABLES);
                if (data.indexOf("({") !== -1 && data.lastIndexOf("});") !== -1) {
                    const versions : any = /^\s*(version:|"version":)\s*"(\d+\.\d+\.\d+(?:[-.][a-zA-Z]+)*)"/gm.exec(data);
                    if (versions !== null && versions.length >= 2) {
                        version = versions[2];
                    }
                    if (data.match(/^\s*(\$interface:|"\$interface":)\s*"ISelfExtractorConfiguration"/gm) !== null) {
                        const jsonConfigData : string = data.substring(data.indexOf("({") + 1,
                            data.lastIndexOf("});") + 1);
                        const updatedConfigData : string = jsonConfigData.replace(
                            /(\.\/)?((test\/)?resource\/(([a-zA-Z0-9\-_\/\\\s.]+)+))/g,
                            ($substring : string) : string => {
                                let filePath = $substring;
                                if (this.fileSystem.Exists(filePath)) {
                                    const symbol : string = this.project.name + "_" +
                                        this.project.version.replace(/\./g, "-") + "_" + path.basename(filePath);
                                    files.push(<IFileInfo>{
                                        name: symbol,
                                        path: filePath
                                    });
                                    filePath = this.server + "/Download/" + symbol;
                                }
                                return filePath;
                            });

                        data = data.replace(jsonConfigData, stripJsonComments(updatedConfigData));
                    }
                }
                return {
                    data,
                    files,
                    version
                };
            } else {
                LogIt.Warning("Only files with valid JSONP format are supported");
                return null;
            }
        }

        private uploadConfig($name : string, $path : string, $version : string, $data : string, $callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            this.client
                .UploadConfiguration($name, path.basename($path), $version, $data)
                .Then(($status : boolean) : void => {
                    if ($status) {
                        LogIt.Info("Uploaded file \"" + $path + "\" to " + this.server);
                        $callback();
                    } else {
                        LogIt.Error("Failed to upload \"" + $path + "\" to " + this.server);
                    }
                });
        }

        private uploadFile($fileInfo : IFileInfo, $callback : () => void) : void {
            if (this.fileSystem.Exists($fileInfo.path)) {
                const {Transform} = require("stream");
                const fs : Types.NodeJS.fs = require("fs");

                let chunkIndex : number = 0;
                const contentSize : number = fs.statSync($fileInfo.path).size;

                const chunk : any = {
                    data : "",
                    end  : this.maxChunkSize,
                    id   : $fileInfo.name,
                    index: chunkIndex,
                    name : ObjectEncoder.Base64($fileInfo.name),
                    size : contentSize,
                    start: 0
                };

                try {
                    LogIt.Info("Uploading file \"" + $fileInfo.path + "\" to " + this.server);

                    const upload : any = () : void => {
                        let buffer : any = null;
                        const pipeTransform : typeof Transform = new Transform({
                            transform: ($data : any, $encoding : string, $callback : any) : void => {
                                if (buffer === null) {
                                    buffer = Buffer.from($data);
                                } else if (buffer.byteLength < this.maxChunkSize) {
                                    buffer = Buffer.concat([buffer, Buffer.from($data)]);
                                }
                                if (buffer.byteLength >= this.maxChunkSize) {
                                    sendChunk($callback);
                                } else {
                                    $callback();
                                }
                            }
                        });
                        const sendChunk : any = ($callback : any) : void => {
                            if (buffer !== null && buffer.byteLength !== 0) {
                                chunk.index = chunkIndex;
                                chunk.data = buffer.toString("base64");
                                chunk.end = chunk.start + this.maxChunkSize;
                                if (chunk.end > chunk.size) {
                                    chunk.end = chunk.size;
                                }

                                LogIt.Info("Sending chunk #" + chunkIndex + " (" +
                                    Convert.IntegerToSize(chunk.end) + "/" + Convert.IntegerToSize(chunk.size) + ")");
                                this.client.UploadFile(chunk)
                                    .Then(($status : boolean) : void => {
                                        if (!$status) {
                                            LogIt.Error("Chunk upload has failed");
                                        } else {
                                            chunk.start = chunk.end;
                                            chunkIndex++;
                                            buffer = null;
                                            $callback();
                                        }
                                    });
                            } else {
                                $callback();
                            }
                        };
                        const stream : any = fs.createReadStream($fileInfo.path, <any>{
                            bufferSize: this.maxChunkSize
                        });
                        stream
                            .on("end", () : void => {
                                sendChunk(() : void => {
                                    LogIt.Info("File has been uploaded successfully.");
                                    $callback();
                                });
                            })
                            .on("error", ($error : Error) : void => {
                                LogIt.Error($error.message, $error);
                            })
                            .pipe(pipeTransform);
                    };

                    upload();
                } catch (ex) {
                    LogIt.Error(ex);
                }
            } else {
                LogIt.Error("Uploading failed. Selected file \"" + $fileInfo.path + "\" not exists.");
            }
        }
    }

    export abstract class IFileInfo {
        public path : string;
        public name : string;
    }

    export abstract class IConfigFileInfo {
        public version : string;
        public files : IFileInfo[];
        public data : any;
    }
}
