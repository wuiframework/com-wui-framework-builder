/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Deploy {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IProjectDeployUpdate = Com.Wui.Framework.Builder.Interfaces.IProjectDeployUpdate;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import UpdatesManagerConnector = Com.Wui.Framework.Services.Connectors.UpdatesManagerConnector;
    import IProjectDeployUpdateApp = Com.Wui.Framework.Builder.Interfaces.IProjectDeployUpdateApp;
    import IProjectDeployUpdateAgent = Com.Wui.Framework.Builder.Interfaces.IProjectDeployUpdateAgent;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;
    import IProjectDeployUpdateLocation = Com.Wui.Framework.Builder.Interfaces.IProjectDeployUpdateLocation;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class UpdateDeploy extends BaseTask {
        private remoteFileSystem : FileSystemHandlerConnector;
        private remoteTerminal : TerminalConnector;
        private remoteRegister : any;
        private update : IProjectDeployUpdate;
        private agent : IProjectDeployUpdateAgent;

        protected getName() : string {
            return "update-deploy";
        }

        protected process($done : any, $option : string) : void {
            if (ObjectValidator.IsEmptyOrNull($option)) {
                $option = "dev";
            }

            const updates : IProjectDeployUpdate[] = [];
            const skippedUpdates : string[] = [];
            let name : string;
            for (name in this.project.deploy.updates) {
                if (this.project.deploy.updates.hasOwnProperty(name)) {
                    const update : IProjectDeployUpdate = this.project.deploy.updates[name];
                    if (update.skipped) {
                        skippedUpdates.push(name);
                    } else {
                        update.name = name;
                        updates.push(update);
                    }
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(skippedUpdates)) {
                LogIt.Warning("Some updates will be skipped: " + skippedUpdates.join(", "));
            }

            const getNextUpdate : any = ($updateIndex : number) : void => {
                if ($updateIndex < updates.length) {
                    this.update = updates[$updateIndex];
                    LogIt.Info("Started update for: " + this.update.name);

                    const platform : BuildProductArgs = new BuildProductArgs();
                    platform.Parse(this.update.location.platform);
                    if (ObjectValidator.IsEmptyOrNull(this.update.app.name)) {
                        this.update.app.name = this.project.target.name;
                    }
                    if (platform.OS() === OSType.WIN && !StringUtils.EndsWith(this.update.app.name, ".exe")) {
                        this.update.app.name += ".exe";
                    }
                    if (ObjectValidator.IsEmptyOrNull(this.update.app.path)) {
                        if (platform.OS() === OSType.WIN) {
                            this.update.app.path = "C:/wui/apps/" + this.project.name;
                        } else {
                            this.update.app.path = "/var/wui/apps/" + this.project.name;
                        }
                    }
                    if (ObjectValidator.IsEmptyOrNull(this.update.register)) {
                        if (platform.OS() === OSType.WIN) {
                            this.update.register = "C:/wui/appdata/com-wui-framework-updater/Register.json";
                        } else {
                            this.update.register = "/var/lib/com-wui-framework-updater/Register.json";
                        }
                    }
                    const getNextAgent : any = ($agentIndex : number) : void => {
                        if ($agentIndex < this.update.agents.length) {
                            this.agent = this.update.agents[$agentIndex];
                            if (ObjectValidator.IsEmptyOrNull(this.agent.hub)) {
                                this.agent.hub = this.project.deploy.agentsHub;
                                if (ObjectValidator.IsEmptyOrNull(this.agent.hub)) {
                                    this.agent.hub = this.builderConfig.deployAgentsHub;
                                }
                            }
                            if (this.agent.skipped ||
                                !ObjectValidator.IsEmptyOrNull(this.agent.profile) && this.agent.profile !== $option) {
                                LogIt.Warning("Update push skipped for: " + this.agent.name + " [" + this.agent.hub + "]");
                                getNextAgent($agentIndex + 1);
                            } else {
                                LogIt.Info("Push update to: " + this.agent.name + " [" + this.agent.hub + "]");
                                this.remoteFileSystem = new FileSystemHandlerConnector(3, this.agent.hub,
                                    WebServiceClientType.WUI_FORWARD_CLIENT);
                                this.remoteTerminal = new TerminalConnector(3, this.agent.hub,
                                    WebServiceClientType.WUI_FORWARD_CLIENT);
                                [this.remoteFileSystem, this.remoteTerminal].forEach(($connector : BaseConnector) : void => {
                                    $connector.setAgent(this.agent.name, this.agent.hub);
                                    $connector.ErrorPropagation(false);
                                    $connector.getEvents().OnError(() : void => {
                                        getNextAgent($agentIndex + 1);
                                    });
                                });
                                this.remoteRegister = {};
                                this.remoteFileSystem.Read(this.update.register).Then(($data : any) : void => {
                                    if (!ObjectValidator.IsEmptyOrNull($data)) {
                                        this.remoteRegister = $data;
                                    }
                                    const onUpdateComplete : any = () : void => {
                                        this.remoteFileSystem
                                            .Write(this.update.register, JSON.stringify(this.remoteRegister))
                                            .Then(($success : boolean) : void => {
                                                if (!$success) {
                                                    LogIt.Warning("> failed to save updater register at: " + this.update.register);
                                                }
                                                getNextAgent($agentIndex + 1);
                                            });
                                    };
                                    const updateLocation : IProjectDeployUpdateLocation = this.update.location;
                                    if (ObjectValidator.IsEmptyOrNull(updateLocation.projectName)) {
                                        updateLocation.projectName = this.project.name;
                                    }
                                    if (ObjectValidator.IsEmptyOrNull(updateLocation.path)) {
                                        updateLocation.path = this.project.deploy.server;
                                    }
                                    if (ObjectValidator.IsEmptyOrNull(updateLocation.releaseName)) {
                                        updateLocation.releaseName = "null";
                                    }
                                    if (ObjectValidator.IsEmptyOrNull(updateLocation.platform)) {
                                        updateLocation.platform = "null";
                                    }
                                    if (ObjectValidator.IsEmptyOrNull(updateLocation.version) || updateLocation.version === "latest") {
                                        updateLocation.version = "";
                                    }
                                    if (ObjectValidator.IsEmptyOrNull(this.remoteRegister[this.agent.name]) ||
                                        this.programArgs.IsForce()) {
                                        this.remoteRegister[this.agent.name] = {
                                            platform   : updateLocation.platform,
                                            projectName: updateLocation.projectName,
                                            releaseName: updateLocation.releaseName
                                        };
                                        this.downloadPackage($option, onUpdateComplete);
                                    } else {
                                        const updaterRegister : any = this.remoteRegister[this.agent.name];
                                        const updateManager : UpdatesManagerConnector = new UpdatesManagerConnector(3,
                                            updateLocation.path[$option] + "/connector.config.jsonp");
                                        updateManager.ErrorPropagation(false);
                                        updateManager.getEvents().OnError(() : void => {
                                            getNextAgent($agentIndex + 1);
                                        });
                                        updateManager
                                            .UpdateExists(
                                                updateLocation.projectName,
                                                updateLocation.releaseName,
                                                updateLocation.platform,
                                                updaterRegister.version,
                                                new Date(updaterRegister.buildTime).getTime(),
                                                !ObjectValidator.IsEmptyOrNull(updaterRegister.version)
                                            )
                                            .Then(($success : boolean) : void => {
                                                if (!$success) {
                                                    this.restartInstance(false, ($status : boolean) : void => {
                                                        if ($status) {
                                                            LogIt.Info("> application has been restarted");
                                                        } else {
                                                            LogIt.Warning("> failed to restart application");
                                                        }
                                                        getNextAgent($agentIndex + 1);
                                                    });
                                                } else {
                                                    this.downloadPackage($option, onUpdateComplete);
                                                }
                                            });
                                    }
                                });
                            }
                        } else {
                            getNextUpdate($updateIndex + 1);
                        }
                    };
                    getNextAgent(0);
                } else {
                    $done();
                }
            };
            getNextUpdate(0);
        }

        private downloadPackage($profile : string, $done : () => void) : void {
            let downloadSource : string = this.update.location.path[$profile] + "/Update/" +
                this.update.location.projectName + "/" + this.update.location.releaseName + "/" + this.update.location.platform;
            if (!ObjectValidator.IsEmptyOrNull(this.update.location.version)) {
                downloadSource += "/" + this.update.location.version;
            }
            let progressCounter : number = 0;
            this.remoteFileSystem
                .Download(downloadSource)
                .OnStart(() : void => {
                    LogIt.Info("> started package download");
                })
                .OnChange(($args : ProgressEventArgs) : void => {
                    if (progressCounter < 100) {
                        Echo.Print(".");
                    } else {
                        Echo.Println(" (" + Math.floor(100 / $args.RangeEnd() * $args.CurrentValue()) + "%)");
                        progressCounter = 0;
                    }
                    progressCounter++;
                })
                .OnComplete(($tmpPath : string, $headers : ArrayList<string>) : void => {
                    LogIt.Info("\n> started unpacking");
                    this.remoteRegister[this.agent.name].buildTime = $headers.getItem("last-modified");
                    this.remoteRegister[this.agent.name].version = $headers.getItem("content-version");
                    if (ObjectValidator.IsEmptyOrNull(this.remoteRegister[this.agent.name].version)) {
                        this.remoteRegister[this.agent.name].version = $headers.getItem("x-content-version");
                    }
                    const app : IProjectDeployUpdateApp = this.update.app;
                    this.remoteFileSystem.Unpack($tmpPath, {
                        output  : app.path + ".new",
                        override: true
                    }).Then(() : void => {
                        this.remoteTerminal.Kill(app.path + "/" + app.name).Then(($success : boolean) : void => {
                            if ($success) {
                                LogIt.Info("> application has been killed");
                            }
                            this.remoteFileSystem.Rename(app.path, app.path + ".back").Then(() : void => {
                                this.remoteFileSystem.Rename(app.path + ".new", app.path).Then(() : void => {
                                    this.restartInstance(true, ($status : boolean) : void => {
                                        if (!$status) {
                                            this.remoteFileSystem.Rename(app.path + ".back", app.path).Then(() : void => {
                                                $done();
                                            });
                                        } else {
                                            this.remoteFileSystem.Delete($tmpPath).Then(() : void => {
                                                this.remoteFileSystem.Delete(app.path + ".new").Then(() : void => {
                                                    this.remoteFileSystem.Delete(app.path + ".back").Then(() : void => {
                                                        LogIt.Info("> update has been finished");
                                                        $done();
                                                    });
                                                });
                                            });
                                        }
                                    });
                                });
                            });
                        });
                    });
                });
        }

        private restartInstance($skipKill : boolean, $done : ($status : boolean) => void) : void {
            const detachDetect : any = setTimeout(() : void => {
                if (!this.programArgs.IsDebug()) {
                    $done(true);
                }
            }, 5000);
            const app : IProjectDeployUpdateApp = this.update.app;
            if (ObjectValidator.IsEmptyOrNull(app.args)) {
                app.args = [];
            }
            const start : any = () : void => {
                let cmd : string = app.name;
                if (!StringUtils.EndsWith(cmd, ".exe")) {
                    cmd = "./" + cmd;
                }
                LogIt.Info(app.path + " > " + cmd + " " + app.args.join(" "));
                this.remoteTerminal
                    .Spawn(cmd, app.args, app.path)
                    .OnMessage(($data : string) : void => {
                        Echo.Printf($data);
                    })
                    .Then(($exitCode : number) : void => {
                        clearTimeout(detachDetect);
                        $done($exitCode === 0);
                    });
            };
            const restart : any = () : void => {
                if ($skipKill) {
                    start();
                } else {
                    this.remoteTerminal.Kill(app.path + "/" + app.name).Then(($success : boolean) : void => {
                        if ($success) {
                            LogIt.Info("> application has been killed");
                        }
                        start();
                    });
                }
            };

            if (!ObjectValidator.IsEmptyOrNull(app.config)) {
                const config : string = this.fileSystem.Read(this.properties.projectBase + "/" + app.config).toString();
                if (ObjectValidator.IsEmptyOrNull(config)) {
                    LogIt.Warning("App config upload skipped: config file has not been found or is empty");
                    restart();
                } else {
                    this.remoteFileSystem
                        .Write(app.path + "/" + StringUtils.Remove(app.name, ".exe") + ".config.jsonp", config)
                        .Then(() : void => {
                            restart();
                        });
                }
            } else {
                restart();
            }
        }
    }
}
