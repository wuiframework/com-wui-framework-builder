/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Deploy {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IProjectDeploy = Com.Wui.Framework.Builder.Interfaces.IProjectDeploy;
    import BuildExecutor = Com.Wui.Framework.Builder.Tasks.BuildProcessor.BuildExecutor;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import UpdatesManagerConnector = Com.Wui.Framework.Services.Connectors.UpdatesManagerConnector;
    import IReleaseProperties = Com.Wui.Framework.Builder.Tasks.BuildProcessor.IReleaseProperties;
    import BuildProductArgs = Com.Wui.Framework.Builder.Structures.BuildProductArgs;
    import ProductType = Com.Wui.Framework.Builder.Enums.ProductType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;

    export class TargetDeploy extends BaseTask {

        protected getName() : string {
            return "target-deploy";
        }

        protected process($done : any, $option : string) : void {
            const path : Types.NodeJS.path = require("path");
            const fs : Types.NodeJS.fs = require("fs");
            const deploy : IProjectDeploy = this.project.deploy;
            const maxChunkSize : number = deploy.chunkSize;
            let server : string = deploy.server.dev;
            if (!ObjectValidator.IsEmptyOrNull($option)) {
                if (deploy.server.hasOwnProperty($option)) {
                    server = deploy.server[$option];
                } else {
                    LogIt.Error("Unsupported target deploy option \"" + $option + "\".");
                }
            }
            if (ObjectValidator.IsEmptyOrNull(server)) {
                LogIt.Error("Target deploy failed: unable to find deploy server configuration");
            }
            let buildTime : string;

            const {Transform} = require("stream");
            const client : UpdatesManagerConnector = new UpdatesManagerConnector(false, server + "/connector.config.jsonp");
            client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                LogIt.Error($eventArgs.Exception().ToString());
            });

            const uploadPackage : any = ($releaseName : string, $platform : BuildProductArgs, $isSinglePlatform : boolean,
                                         $callback : () => void) : void => {
                const extension : string = $platform.OS() === OSType.WIN ? "zip" : "tar.gz";
                const name : string = this.properties.packageName + "." + extension;
                const platformString = $platform.Value();

                let filePath : string = this.properties.projectBase + "/build";
                if (!$isSinglePlatform && ($releaseName !== "" || platformString !== "")) {
                    filePath += "/releases";
                    if ($releaseName !== "") {
                        filePath += "/" + $releaseName;
                    }
                    if (platformString !== "") {
                        filePath += "/" + platformString;
                    }
                }
                filePath += "/" + this.properties.packageName + "." + extension;
                if (this.fileSystem.Exists(filePath)) {
                    let chunkIndex : number = 0;
                    const contentSize : number = fs.statSync(filePath).size;

                    const chunk : any = {
                        data : "",
                        end  : maxChunkSize,
                        id   : path.basename(filePath),
                        index: chunkIndex,
                        name : ObjectEncoder.Base64(path.basename(filePath)),
                        size : contentSize,
                        start: 0
                    };
                    if (name !== chunk.id) {
                        chunk.id = name;
                    }

                    try {
                        LogIt.Info("Uploading file \"" + filePath + "\" to " + server + " as " + JSON.stringify({
                            /* tslint:disable: object-literal-sort-keys */
                            name       : this.project.name,
                            releaseName: $releaseName,
                            platform   : $platform,
                            version    : this.project.version,
                            buildTime
                            /* tslint:enable */
                        }));

                        chunk.id = (new Date()).getTime() + "";

                        let buffer : any = null;
                        const pipeTransform : typeof Transform = new Transform({
                            transform($data : any, $encoding : string, $callback : any) : void {
                                if (buffer === null) {
                                    buffer = Buffer.from($data);
                                } else if (buffer.byteLength < maxChunkSize) {
                                    buffer = Buffer.concat([buffer, Buffer.from($data)]);
                                }
                                if (buffer.byteLength >= maxChunkSize) {
                                    sendChunk($callback);
                                } else {
                                    $callback();
                                }
                            }
                        });
                        const sendChunk : any = ($callback : any) : void => {
                            if (buffer !== null && buffer.byteLength !== 0) {
                                chunk.index = chunkIndex;
                                chunk.data = buffer.toString("base64");
                                chunk.end = chunk.start + maxChunkSize;
                                if (chunk.end > chunk.size) {
                                    chunk.end = chunk.size;
                                }

                                LogIt.Info("Sending chunk #" + chunkIndex +
                                    " (" + Convert.IntegerToSize(chunk.end) + "/" + Convert.IntegerToSize(chunk.size) + ")");
                                client.UploadPackage(chunk).Then(($status : boolean) : void => {
                                    if (!$status) {
                                        LogIt.Error("Chunk upload has failed");
                                    } else {
                                        chunk.start = chunk.end;
                                        chunkIndex++;
                                        buffer = null;
                                        $callback();
                                    }
                                });
                            } else {
                                $callback();
                            }
                        };

                        const stream : any = fs.createReadStream(filePath, <any>{
                            bufferSize: maxChunkSize
                        });
                        stream
                            .on("end", () : void => {
                                sendChunk(() : void => {
                                    LogIt.Info("File has been uploaded successfully.");

                                    client
                                        .RegisterPackage(
                                            this.project.name,
                                            $releaseName,
                                            platformString,
                                            this.project.version,
                                            buildTime,
                                            chunk.id,
                                            extension
                                        )
                                        .Then(($status : boolean) : void => {
                                            if ($status) {
                                                $callback();
                                            } else {
                                                LogIt.Error("Registration of the package has failed.");
                                            }
                                        });
                                });
                            })
                            .on("error", ($error : Error) : void => {
                                LogIt.Error($error.message, $error);
                            })
                            .pipe(pipeTransform);
                    } catch (ex) {
                        LogIt.Error(ex.message);
                    }
                } else {
                    LogIt.Error("File for upload does not exists: " + filePath);
                }
            };
            const logPath : string = this.properties.projectBase + "/log/build.log";
            if (this.fileSystem.Exists(logPath)) {
                const regexp : RegExp = />> Build type: (.*?), Build time: (.*?)$/igm;
                const match : RegExpExecArray = regexp.exec(this.fileSystem.Read(logPath).toString());
                if (match.length === 3) {
                    buildTime = new Date(match[2]).getTime() + "";

                    const executor : BuildExecutor = new BuildExecutor();
                    const releases : IReleaseProperties[] = executor.getReleases();
                    let platforms : BuildProductArgs[] = [];

                    const getNextPlatform : any = ($release : string, $index : number, $callback : () => void) : void => {
                        if ($index < platforms.length && platforms[$index].Type() !== ProductType.INSTALLER) {
                            if (platforms.length > 1) {
                                if ($index < platforms.length) {
                                    uploadPackage(
                                        $release,
                                        platforms[$index],
                                        false,
                                        () : void => {
                                            getNextPlatform($release, $index + 1, $callback);
                                        });
                                } else {
                                    $callback();
                                }
                            } else if (platforms.length === 1) {
                                uploadPackage(
                                    $release,
                                    platforms[$index],
                                    this.fileSystem.Expand(this.properties.projectBase + "/build/*.+(zip|tar.gz)").length === 1,
                                    () : void => {
                                        $callback();
                                    });
                            } else {
                                $callback();
                            }
                        } else {
                            $callback();
                        }
                    };
                    const getNextRelease : any = ($index : number, $callback : () => void) : void => {
                        if ($index < releases.length) {
                            platforms = executor.getTargetProducts(releases[$index].target);
                            getNextPlatform(releases[$index].name, 0, () : void => {
                                getNextRelease($index + 1, $callback);
                            });
                        } else {
                            $callback();
                        }
                    };
                    getNextRelease(0, () : void => {
                        $done();
                    });
                } else {
                    LogIt.Error("Unable to parse build time from \"" + logPath + "\".");
                }
            } else {
                LogIt.Error("Unable to locate \"" + logPath + "\" required for deploy of the package.");
            }
        }
    }
}
