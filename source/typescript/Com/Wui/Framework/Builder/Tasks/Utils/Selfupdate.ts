/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import UpdatesManagerConnector = Com.Wui.Framework.Services.Connectors.UpdatesManagerConnector;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;

    export class Selfupdate extends BaseTask {

        protected getName() : string {
            return "selfupdate";
        }

        protected process($done : any, $option : string) : void {
            const register : IPersistenceHandler = PersistenceFactory.getPersistence(this.getClassName());
            let updaterPath : string = this.fileSystem.getTempPath() + "/WuiBuilderUpdate";
            if (EnvironmentHelper.IsWindows()) {
                updaterPath += ".exe";
            }

            const validate : any = ($onUpdate? : () => void) : void => {
                const connector : UpdatesManagerConnector = new UpdatesManagerConnector(3,
                    this.builderConfig.hub.url + "/connector.config.jsonp");
                connector.ErrorPropagation(false);
                connector.getEvents().OnError(() : void => {
                    LogIt.Warning("Failed to check WUI Framework Builder updates. Continuing ...");
                    $done();
                });
                const environment : EnvironmentArgs = Loader.getInstance().getEnvironmentArgs();
                connector
                    .UpdateExists(
                        environment.getAppName(),
                        environment.getReleaseName(),
                        environment.getPlatform(),
                        environment.getProjectVersion(),
                        new Date(environment.getBuildTime()).getTime())
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            if (ObjectValidator.IsSet($onUpdate)) {
                                $onUpdate();
                            } else {
                                LogIt.Error("WUI Framework Builder should be self installed. " +
                                    "Please run 'wui update' command.");
                                $done();
                            }
                        } else {
                            register.Variable("LastSelfUpdate", new Date().getTime());
                            if (this.programArgs.IsForce() && ObjectValidator.IsSet($onUpdate)) {
                                $onUpdate();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " WUI Framework Builder is up to date.");
                                $done();
                            }
                        }
                    });
            };
            if ($option === "check") {
                if (register.Exists("LastSelfUpdate")) {
                    if ((new Date().getTime() - register.Variable("LastSelfUpdate")) > (24 * 3600000) ||
                        this.programArgs.IsForce()) {
                        this.fileSystem.Delete(updaterPath);
                        validate();
                    } else {
                        LogIt.Info("Selfupdate check skipped. Recommended period is once per day, " +
                            "to force run selfupdate:check please run task with --force option.");
                        $done();
                    }
                } else {
                    validate();
                }
            } else {
                validate(() : void => {
                    this.fileSystem.Delete(updaterPath);
                    const updaterSource : string = this.builderConfig.target.updater;
                    if (ObjectValidator.IsEmptyOrNull(updaterSource)) {
                        LogIt.Warning("Update skipped: Updater configuration is missing. " +
                            "Please validate target.updater property in package.conf.json or private.conf.json.");
                        $done();
                    } else {
                        this.fileSystem.Download(updaterSource, ($headers : string, $bodyOrPath : string) : void => {
                            this.fileSystem.Copy($bodyOrPath, updaterPath, () : void => {
                                this.fileSystem.Delete($bodyOrPath);
                                const detached : any = () : void => {
                                    register.Variable("LastSelfUpdate", new Date().getTime());
                                    LogIt.Info(">>"[ColorType.YELLOW] +
                                        " Update has been started, please wait for notification...");
                                    $done();
                                };
                                const detachDetect : any = setTimeout(() : void => {
                                    detached();
                                }, 2500);
                                this.terminal.Spawn(require("path").basename(updaterPath), [], {
                                        cwd: this.fileSystem.getTempPath()
                                    },
                                    ($exitCode : number) : void => {
                                        clearTimeout(detachDetect);
                                        if ($exitCode !== 0) {
                                            LogIt.Error("Unable to start update process.");
                                        } else {
                                            detached();
                                        }
                                    });
                            });
                        });
                    }
                });
            }
        }
    }
}
