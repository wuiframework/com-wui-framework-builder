/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import CredentialsManager = Com.Wui.Framework.Builder.Connectors.CredentialsManager;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class Selfinstall extends BaseTask {

        protected getName() : string {
            return "selfinstall";
        }

        protected process($done : any, $option : string) : void {
            const isSingleInstall : boolean = !ObjectValidator.IsEmptyOrNull($option) &&
                $option !== "all" && $option !== "cpp" && $option !== "java" && $option !== "selfextract";

            let installChain : string[] = [];
            if ($option !== "selfextract") {
                if (EnvironmentHelper.IsMac()) {
                    installChain.push(
                        "Autoconf",
                        "GCC"
                    );
                }

                if (!EnvironmentHelper.IsWindows()) {
                    installChain.push("Openssl");
                }

                installChain.push(
                    "Git",
                    "JDK",
                    "Upx",
                    "Python",
                    "CMake"
                );
                if (EnvironmentHelper.IsWindows()) {
                    installChain.push(
                        "ResourceHacker",
                        "NASM"
                    );
                }

                if (this.properties.projectHas.Cpp.Source() || $option === "cpp" || $option === "all" || isSingleInstall) {
                    if (EnvironmentHelper.IsWindows()) {
                        installChain.push("Msys2");
                    } else {
                        if (!EnvironmentHelper.IsMac()) {
                            installChain.push("GCC");
                        }
                        if (EnvironmentHelper.IsLinux()) {
                            installChain.push("ARM");
                        } else if (EnvironmentHelper.IsMac()) {
                            installChain.push("Clang");
                        }
                    }
                    installChain.push(
                        "Doxygen",
                        "Python36",
                        "Cppcheck",
                        "Gtest"
                    );
                }
                if (this.properties.projectHas.Java.Source() || $option === "java" || $option === "all" || isSingleInstall) {
                    installChain.push(
                        "Eclipse",
                        "Maven",
                        "Idea"
                    );
                }
            } else {
                installChain.push(
                    "RegisterCLI"
                );
            }
            if (isSingleInstall) {
                let singleTool : string = "";
                installChain.forEach(($tool : string) : void => {
                    if (StringUtils.ToLowerCase($tool) === StringUtils.ToLowerCase($option)) {
                        singleTool = $tool;
                    }
                });
                if (ObjectValidator.IsEmptyOrNull(singleTool)) {
                    LogIt.Error("Unable to install required external module \"" + $option + "\": module does not exist");
                } else {
                    installChain = [singleTool];
                }
            }

            const register : IPersistenceHandler = PersistenceFactory.getPersistence(this.getClassName());
            const getNextModule : any = ($index : number) : void => {
                if ($index < installChain.length) {
                    const module : string = "get" + installChain[$index];
                    if (ObjectValidator.IsFunction(this[module])) {
                        this[module](() : void => {
                            getNextModule($index + 1);
                        });
                    } else {
                        LogIt.Warning("Unable to get external module \"" + installChain[$index] + "\": module does not exist");
                        getNextModule($index + 1);
                    }
                } else {
                    register.Variable("LastSelfInstall", new Date().getTime());
                    $done();
                }
            };

            if ($option !== "selfextract" && !this.programArgs.IsForce()) {
                if (register.Exists("LastSelfInstall")) {
                    if ((new Date().getTime() - register.Variable("LastSelfInstall")) > (24 * 3600000)) {
                        getNextModule(0);
                    } else {
                        LogIt.Info("Selfinstall skipped. Recommended period is once per day, " +
                            "to force run selfinstall please run task with --force option.");
                        $done();
                    }
                } else {
                    getNextModule(0);
                }
            } else {
                getNextModule(0);
            }
        }

        private elevate($cmd : string, $cwd : string, $callback : ($exitCode : number) => void) : void {
            const spawn : Types.NodeJS.Modules.child_process.spawn = require("child_process").spawn;
            if (!this.programArgs.getOptions().noSudo && !Loader.getInstance().getAppConfiguration().noSudo) {
                CredentialsManager.getLinuxRoot(($rootPassword : string) : void => {
                    $cmd = "rootPassword=\"" + $rootPassword + "\" && " + StringUtils.Replace($cmd, "sudo ",
                        "echo $rootPassword | sudo -S ");
                    try {
                        const child : Types.NodeJS.Modules.child_process.ChildProcess =
                            spawn("eval", ["'" + $cmd + "'"], {
                                cwd        : $cwd,
                                shell      : true,
                                windowsHide: true
                            });
                        child.stdout.on("data", ($data : string) : void => {
                            Echo.Print($data + "");
                        });
                        child.stderr.on("data", ($data : string) : void => {
                            Echo.Print($data + "");
                        });
                        child.on("exit", $callback);
                        child.on("error", () : void => {
                            $callback(-1);
                        });
                    } catch (ex) {
                        $callback(-1);
                    }
                });
            } else {
                try {
                    $cmd = StringUtils.Remove($cmd, "sudo ");
                    const child : any = spawn("eval", ["'" + $cmd + "'"], {shell: true, cwd: $cwd, windowsHide: true});
                    child.stdout.on("data", ($data : string) : void => {
                        Echo.Print($data + "");
                    });
                    child.stderr.on("data", ($data : string) : void => {
                        Echo.Print($data + "");
                    });
                    child.on("exit", $callback);
                    child.on("error", () : void => {
                        $callback(-1);
                    });
                } catch (ex) {
                    $callback(-1);
                }
            }
        }

        private elevateWrite($path : string, $data : string, $callback : ($status : boolean) => void) : void {
            this.elevate("sudo tee " + $path + " >/dev/null <<EOF\n" + $data, null, ($exitCode : number) : void => {
                $callback($exitCode === 0);
            });
        }

        private getAutoconf($callback : () => void) : void {
            const version : string = "2.69";
            const path : Types.NodeJS.path = require("path");
            const installPath : string = this.externalModules + "/autoconf";
            const binPath : string = installPath + "/autoconf";

            const installMac : any = () : void => {
                this.fileSystem.CreateDirectory(installPath);
                this.terminal.Spawn(
                    "brew install autoconf@" + version + " && " +
                    "brew link --overwrite autoconf && " +
                    "ln -s $(which autoconf) " + installPath + "/autoconf",
                    null, "",
                    ($exitCode : number) : void => {
                        if ($exitCode === 0) {
                            $callback();
                        } else {
                            LogIt.Error("Unable to install autoconf. Exit code: " + $exitCode);
                        }
                    });
            };

            if (!this.fileSystem.Exists(binPath)) {
                installMac();
            } else {
                this.terminal.Spawn("./" + path.basename(binPath), ["--version"], path.dirname(binPath),
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (!StringUtils.Contains($std[0] + $std[1], version)) {
                                this.fileSystem.Delete(installPath);
                                installMac();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " autoconf is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate autoconf version.");
                        }
                    });
            }
        }

        private getGit($callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            const version : string = "2.15.0";
            let source : any = {
                unix: "https://github.com/git/git/archive/v" + version + ".tar.gz",
                win : {
                    x64: "https://github.com/git-for-windows/git/releases/download/" +
                        "v" + version + ".windows.1/PortableGit-" + version + "-64-bit.7z.exe",
                    x86: "https://github.com/git-for-windows/git/releases/download/" +
                        "v" + version + ".windows.1/PortableGit-" + version + "-32-bit.7z.exe"
                }
            };

            const curlSrc : string = "https://curl.haxx.se/download/curl-7.56.1.tar.gz";

            let installPath : string = this.externalModules + "/git";
            installPath = this.fileSystem.NormalizePath(installPath);
            let binPath : string = installPath + "/bin/git";
            if (EnvironmentHelper.IsWindows()) {
                binPath += ".exe";
                source = EnvironmentHelper.Is64bit() ? source.win.x64 : source.win.x86;
            } else {
                source = source.unix;
            }

            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        const validate : any = () : void => {
                            this.fileSystem.Delete($path);
                            if (!this.fileSystem.Exists(binPath)) {
                                LogIt.Error("Unable to install GIT.");
                            } else {
                                this.terminal.Execute("git", ["config", "--global", "core.longpaths", "true"], installPath,
                                    () : void => {
                                        $callback();
                                    });
                            }
                        };
                        if (EnvironmentHelper.IsWindows()) {
                            this.terminal.Spawn(path.basename($path), ["-y", "-gm2"], path.dirname($path),
                                ($exitCode : number) : void => {
                                    this.fileSystem.Delete($path);
                                    if ($exitCode === 0) {
                                        const extractPath : string = path.dirname($path) + "/PortableGit";
                                        this.fileSystem.Copy(extractPath, installPath, () : void => {
                                            this.fileSystem.Delete(extractPath);
                                            validate();
                                        });
                                    } else {
                                        LogIt.Error("Unable to extract GIT package.");
                                    }
                                });
                        } else {
                            this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                                this.fileSystem.Delete($path);
                                const packagePath : string = installPath + "/git-" + version;
                                this.fileSystem.Copy(packagePath, installPath, () : void => {
                                    this.fileSystem.Delete(packagePath);

                                    this.fileSystem.Download(curlSrc, ($headers : string, $curlPath : string) : void => {
                                        this.fileSystem.Unpack($curlPath, {output: installPath + "/curl"}, () : void => {
                                            this.fileSystem.Delete($curlPath);

                                            this.terminal.Spawn("chmod +x configure && chmod +x install-sh", [], installPath + "/curl",
                                                ($exitCode : number) : void => {
                                                    if ($exitCode === 0) {
                                                        this.terminal.Spawn("LIBS=\"-ldl -lpthread\" ./configure --with-ssl=" +
                                                            this.externalModules + "/openssl/build " +
                                                            "--prefix=" + installPath + "/curl/build && make -j" +
                                                            EnvironmentHelper.getCores() + " && make install", [],
                                                            installPath + "/curl",
                                                            ($exitCode : number) : void => {
                                                                if ($exitCode === 0) {
                                                                    this.terminal.Spawn(
                                                                        "chmod +x check_bindir && " +
                                                                        "make configure && chmod +x configure && " +
                                                                        "./configure --prefix=" + installPath + " --with-curl=" +
                                                                        installPath + "/curl/build --with-openssl=" +
                                                                        this.externalModules + "/openssl/build " +
                                                                        "--enable-pthreads=\"-pthread\" && " +
                                                                        "make all -j" + EnvironmentHelper.getCores() +
                                                                        " && make install",
                                                                        [], installPath,
                                                                        ($exitCode : number) : void => {
                                                                            if ($exitCode === 0) {
                                                                                validate();
                                                                            } else {
                                                                                LogIt.Error("Unable to install GIT.");
                                                                            }
                                                                        });
                                                                } else {
                                                                    LogIt.Error("Unable to install Curl dependency for GIT.");
                                                                }
                                                            });
                                                    } else {
                                                        LogIt.Error("Unable to se chmod +x to configure.");
                                                    }
                                                });
                                        });
                                    });
                                });
                            });
                        }
                    }, 250);
                });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                this.terminal.Spawn((EnvironmentHelper.IsWindows() ? "" : "./") + path.basename(binPath),
                    ["--version"], path.dirname(binPath), ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (StringUtils.VersionIsLower(($std[0] + $std[1]).trim().replace("git version ", ""), version)) {
                                this.fileSystem.Delete(installPath);
                                install();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " GIT is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate GIT version.");
                        }
                    });
            }
        }

        private getJDK($callback : () => void) : void {
            const majorVersion : string = "8";
            const minorVersion : string = EnvironmentHelper.IsMac() ? "151" : "131";
            const version : string = "1." + majorVersion + ".0_" + minorVersion;
            const path : Types.NodeJS.path = require("path");
            let source : any = {
                linux: {
                    x64: "http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/" +
                        "jdk-8u131-linux-x64.tar.gz",
                    x86: "http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/" +
                        "jdk-8u131-linux-i586.tar.gz"
                },
                mac  : {
                    x64: "https://github.com/frekele/oracle-java/releases/download/8u151-b12/" +
                        "jdk-8u151-macosx-x64.dmg"
                },
                win  : {
                    x64: "https://hub.wuiframework.com/Download/jdk-8u131-windows-x64.zip",
                    x86: "https://hub.wuiframework.com/Download/jdk-8u131-windows-i586.zip"
                }
            };

            let installPath : string = this.externalModules + "/jdk";
            installPath = this.fileSystem.NormalizePath(installPath);
            let binPath : string = installPath + "/bin/java";
            if (EnvironmentHelper.IsWindows()) {
                source = EnvironmentHelper.Is64bit() ? source.win.x64 : source.win.x86;
                binPath += ".exe";
            } else if (EnvironmentHelper.IsMac()) {
                source = source.mac.x64;
            } else {
                source = EnvironmentHelper.Is64bit() ? source.linux.x64 : source.linux.x86;
            }
            const install : any = () : void => {
                this.fileSystem.Download(<any>{
                        headers: {Cookie: "oraclelicense=accept-securebackup-cookie"},
                        url    : source
                    },
                    ($headers : string, $path : string) : void => {
                        setTimeout(() : void => {
                            if (EnvironmentHelper.IsWindows()) {
                                this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                                    this.fileSystem.Delete($path);
                                    if (!this.fileSystem.Exists(binPath)) {
                                        LogIt.Error("Unable to install JDK.");
                                    } else {
                                        $callback();
                                    }
                                });
                            } else {
                                if (EnvironmentHelper.IsMac()) {
                                    this.terminal.Execute("mkdir", ["-p", installPath], "", ($exitCode : number) : void => {
                                        if ($exitCode === 0) {
                                            const tmpFolder : string = "/tmp/com-wui-framework-builder/java";
                                            this.terminal.Spawn("brew install p7zip", null, "",
                                                ($exitCode : number, $std : string[]) : void => {
                                                    if ($exitCode === 0 ||
                                                        StringUtils.Contains($std[0] + $std[1], "already installed")) {
                                                        this.terminal.Spawn("" +
                                                            "7z x -y " + $path + " -o" + tmpFolder + " && " +
                                                            "7z x -y " + tmpFolder +
                                                            "/JDK\\ " + majorVersion + "\\ Update\\ " + minorVersion + "/" +
                                                            "JDK\\ " + majorVersion + "\\ Update\\ " + minorVersion + ".pkg " +
                                                            "-o" + tmpFolder + " && " +
                                                            "7z x -y " + tmpFolder +
                                                            "/jdk1" + majorVersion + "0" + minorVersion + ".pkg/Payload " +
                                                            "-o" + tmpFolder + " && " +
                                                            "7z x -y " + tmpFolder + "/Payload~ -o" + tmpFolder +
                                                            "",
                                                            [], installPath, () : void => {
                                                                this.fileSystem.Copy(tmpFolder + "/Contents/Home/",
                                                                    installPath, () : void => {
                                                                        if (!this.fileSystem.Exists(binPath)) {
                                                                            LogIt.Error("Unable to install JDK.");
                                                                        } else {
                                                                            this.terminal.Spawn("chmod +x " + binPath,
                                                                                [], null, () : void => {
                                                                                    this.terminal.Spawn("chmod +x " + binPath + "c",
                                                                                        [], null, () : void => {
                                                                                            this.fileSystem.Delete($path);
                                                                                            this.fileSystem.Delete(tmpFolder);
                                                                                            if (!this.fileSystem.Exists(binPath)) {
                                                                                                LogIt.Error("Unable to install JDK.");
                                                                                            } else {
                                                                                                $callback();
                                                                                            }
                                                                                        });
                                                                                });
                                                                        }
                                                                    });
                                                            });
                                                    } else {
                                                        LogIt.Error("Unable to install JDK. Exit code: " + $exitCode);
                                                    }
                                                });
                                        } else {
                                            LogIt.Error("Unable to create path for JDK.");
                                        }
                                    });
                                } else {
                                    this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                                        this.fileSystem.Copy(installPath + "/jdk" + version, installPath, () : void => {
                                            this.fileSystem.Delete(installPath + "/jdk" + version);
                                            this.terminal.Spawn("chmod +x " + binPath, [], null, () : void => {
                                                this.terminal.Spawn("chmod +x " + binPath + "c", [], null, () : void => {
                                                    this.fileSystem.Delete($path);
                                                    if (!this.fileSystem.Exists(binPath)) {
                                                        LogIt.Error("Unable to install JDK.");
                                                    } else {
                                                        $callback();
                                                    }
                                                });
                                            });
                                        });
                                    });
                                }
                            }
                        }, 250);
                    });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                this.terminal.Spawn(
                    (EnvironmentHelper.IsWindows() ? "" : "./") + path.basename(binPath), ["-version"], path.dirname(binPath),
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (!StringUtils.Contains($std[0] + $std[1], version)) {
                                this.fileSystem.Delete(installPath);
                                install();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " JDK is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate JDK version.");
                        }
                    });
            }
        }

        private getCMake($callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            const version : string = "3.11.0";
            let source : any = {
                linux: "https://cmake.org/files/v3.11/cmake-" + version + "-Linux-x86_64.tar.gz",
                mac  : "https://cmake.org/files/v3.11/cmake-" + version + "-Darwin-x86_64.tar.gz",
                win  : {
                    x64: "https://cmake.org/files/v3.11/cmake-" + version + "-win64-x64.zip",
                    x86: "https://cmake.org/files/v3.11/cmake-" + version + "-win32-x86.zip"
                }
            };

            let installPath : string = this.externalModules + "/cmake";
            installPath = this.fileSystem.NormalizePath(installPath);
            let binPath : string = installPath + "/bin/cmake";
            if (EnvironmentHelper.IsWindows()) {
                source = EnvironmentHelper.Is64bit() ? source.win.x64 : source.win.x86;
                binPath += ".exe";
            } else if (EnvironmentHelper.IsLinux()) {
                source = source.linux;
            } else if (EnvironmentHelper.IsMac()) {
                source = source.mac;
            }

            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                            const finalize : any = () : void => {
                                this.fileSystem.Delete($path);
                                if (!this.fileSystem.Exists(binPath)) {
                                    LogIt.Error("Unable to extract CMake.");
                                } else {
                                    if (EnvironmentHelper.IsWindows()) {
                                        $callback();
                                    } else {
                                        this.terminal.Spawn("chmod", ["-R", "+x", installPath + "/bin"], null,
                                            ($exitCode : number) : void => {
                                                if ($exitCode === 0) {
                                                    $callback();
                                                } else {
                                                    LogIt.Error("Unable to install CMake.");
                                                }
                                            });
                                    }
                                }
                            };
                            if (EnvironmentHelper.IsMac()) {
                                this.fileSystem.Copy(installPath + "/CMake.app/Contents", installPath, () : void => {
                                    this.fileSystem.Delete(installPath + "/CMake.app");
                                    finalize();
                                });
                            } else {
                                finalize();
                            }
                        });
                    }, 250);
                });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                this.terminal.Spawn((EnvironmentHelper.IsWindows() ? "" : "./") + path.basename(binPath),
                    ["--version"], path.dirname(binPath), ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (!StringUtils.Contains($std[0] + $std[1], version)) {
                                this.fileSystem.Delete(installPath);
                                install();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " CMake is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate CMake version.");
                        }
                    });
            }
        }

        private getDoxygen($callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            const version : string = "1.8.15";
            let source : any = {
                linux: "http://doxygen.nl/files/doxygen-" + version + ".linux.bin.tar.gz",
                mac  : "http://doxygen.nl/files/doxygen-" + version + ".src.tar.gz",
                win  : {
                    x64: "http://doxygen.nl/files/doxygen-" + version + ".windows.x64.bin.zip",
                    x86: "http://doxygen.nl/files/doxygen-" + version + ".windows.bin.zip"
                }
            };

            let installPath : string = this.externalModules + "/doxygen";
            installPath = this.fileSystem.NormalizePath(installPath);
            let binPath : string = installPath + "/doxygen";
            if (EnvironmentHelper.IsWindows()) {
                source = EnvironmentHelper.Is64bit() ? source.win.x64 : source.win.x86;
                binPath = installPath + "/doxygen.exe";
            } else if (EnvironmentHelper.IsLinux()) {
                source = source.linux;
                binPath = installPath + "/bin/doxygen";
            } else if (EnvironmentHelper.IsMac()) {
                source = source.mac;
            }

            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        this.fileSystem.Unpack($path, {
                            autoStrip: false,
                            output   : installPath
                        }, () : void => {
                            this.fileSystem.Delete($path);
                            const validate : any = () : void => {
                                if (!this.fileSystem.Exists(binPath)) {
                                    LogIt.Error("Unable to extract Doxygen.");
                                } else {
                                    $callback();
                                }
                            };
                            if (EnvironmentHelper.IsWindows()) {
                                validate();
                            } else if (EnvironmentHelper.IsMac()) {
                                const tarPackage : string = installPath + "/doxygen-" + version;
                                this.fileSystem.Copy(tarPackage, installPath, () : void => {
                                    this.fileSystem.Delete(tarPackage);
                                    this.terminal.Spawn(
                                        "cmake -Bbuild -H. -DCMAKE_BUILD_TYPE:STRING=Release -G \"Unix Makefiles\" && " +
                                        "cmake --build build --config Release -- -j " + EnvironmentHelper.getCores() + " && " +
                                        "cp build/bin/doxygen . && " +
                                        "chmod +x doxygen && " +
                                        "rm -r build", [], installPath,
                                        () : void => {
                                            validate();
                                        });
                                });
                            } else {
                                const tarPackage : string = installPath + "/doxygen-" + version;
                                this.fileSystem.Copy(tarPackage, installPath, () : void => {
                                    this.fileSystem.Delete(tarPackage);
                                    this.terminal.Spawn("chmod +x " + binPath, [], null, () : void => {
                                        validate();
                                    });
                                });
                            }
                        });
                    }, 250);
                });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                const cmd : string = EnvironmentHelper.IsWindows() ? ".\\" : "./";
                this.terminal.Spawn(cmd + path.basename(binPath), ["--version"], path.dirname(binPath),
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (!StringUtils.Contains($std[0] + $std[1], version)) {
                                this.fileSystem.Delete(installPath);
                                install();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " Doxygen is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate Doxygen version.");
                        }
                    });
            }
        }

        private getCppcheck($callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            const version : string = "1.79";
            const source : string =
                "https://netix.dl.sourceforge.net/project/cppcheck/cppcheck/" + version + "/cppcheck-" + version + ".zip";
            let installPath : string = this.externalModules + "/cppcheck";
            installPath = this.fileSystem.NormalizePath(installPath);
            let binPath : string = installPath + "/cppcheck";
            if (EnvironmentHelper.IsWindows()) {
                binPath += ".exe";
            }
            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                            this.fileSystem.Delete($path);
                            if (!this.fileSystem.Exists(installPath)) {
                                LogIt.Error("Unable to extract CppCheck.");
                            } else {
                                let cmd : string;
                                let args : string[];
                                if (EnvironmentHelper.IsWindows()) {
                                    cmd = "mingw32-make.exe";
                                    args = ["-j" + EnvironmentHelper.getCores(), "SRCDIR=build", "CFGDIR=cfg"];
                                } else {
                                    cmd = "make";
                                    args = [
                                        "-j" + EnvironmentHelper.getCores(), "SRCDIR=build",
                                        "CFGDIR=" + installPath + "/cfg"
                                    ];
                                }
                                this.terminal.Spawn(cmd, args, installPath, ($exitCode : number) : void => {
                                    if ($exitCode === 0) {
                                        $callback();
                                    } else {
                                        LogIt.Error("Unable to build CppCheck.");
                                    }
                                });
                            }
                        });
                    }, 250);
                });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                this.terminal.Spawn((EnvironmentHelper.IsWindows() ? "" : "./") + path.basename(binPath),
                    ["--version"], path.dirname(binPath), ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (!StringUtils.Contains($std[0] + $std[1], version)) {
                                this.fileSystem.Delete(installPath);
                                install();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " CppCheck is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate CppCheck version.");
                        }
                    });
            }
        }

        private getPython($callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            const version : string = "2.7.13";
            let source : any = {
                unix: "https://www.python.org/ftp/python/" + version + "/Python-" + version + ".tgz",
                win : {
                    x64: "https://netcologne.dl.sourceforge.net/project/winpython/" +
                        "WinPython_2.7/" + version + ".1/WinPython-64bit-" + version + ".1Zero.exe",
                    x86: "https://kent.dl.sourceforge.net/project/winpython/" +
                        "WinPython_2.7/" + version + ".1/WinPython-32bit-" + version + ".1Zero.exe"
                }
            };

            let installPath : string = this.externalModules + "/python";
            installPath = this.fileSystem.NormalizePath(installPath);
            let binPath : string = installPath + "/python";
            if (EnvironmentHelper.IsWindows()) {
                source = EnvironmentHelper.Is64bit() ? source.win.x64 : source.win.x86;
                binPath += ".exe";
            } else if (EnvironmentHelper.IsLinux() || EnvironmentHelper.IsMac()) {
                source = source.unix;
            }

            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        if (EnvironmentHelper.IsWindows()) {
                            this.terminal.Spawn(path.basename($path), ["/S", "/D=\"" + path.dirname($path) + "\""], path.dirname($path),
                                ($exitCode : number) : void => {
                                    if ($exitCode === 0) {
                                        this.fileSystem.Copy(
                                            StringUtils.Replace($path, ".exe", "") + "/python-" + version + ".amd64", installPath,
                                            () : void => {
                                                this.fileSystem.Delete(StringUtils.Replace($path, ".exe", ""));
                                                this.fileSystem.Delete($path);
                                                if (!this.fileSystem.Exists(binPath)) {
                                                    LogIt.Error("Unable to install Python.");
                                                } else {
                                                    this.terminal.Spawn("python.exe", ["-m", "pip", "install", "numpy"],
                                                        installPath, ($exitCode : number) : void => {
                                                            if ($exitCode === 0) {
                                                                $callback();
                                                            } else {
                                                                LogIt.Error("Unable to install numpy module for Python.");
                                                            }
                                                        });
                                                }
                                            });
                                    } else {
                                        LogIt.Error("Unable to extract Python.");
                                    }
                                });
                        } else {
                            this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                                this.fileSystem.Delete($path);
                                if (!this.fileSystem.Exists(installPath)) {
                                    LogIt.Error("Unable to extract Python.");
                                } else {
                                    const configureArguments : string = "CFLAGS=\"-I" + this.externalModules + "/openssl/include\" " +
                                        "LDFLAGS=\"-L" + this.externalModules + "/openssl\"";

                                    this.terminal.Spawn(
                                        "chmod +x configure && " +
                                        "chmod +x Parser/asdl_c.py && " +
                                        "chmod +x Parser/asdl_c.py && " +
                                        "chmod +x install-sh && " +
                                        (EnvironmentHelper.IsMac() ?
                                            "PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin && " : "") +
                                        "./configure " + configureArguments + " && " +
                                        "make -j" + EnvironmentHelper.getCores(),
                                        [], installPath,
                                        ($exitCode : number) : void => {
                                            if ($exitCode === 0) {
                                                if (!this.fileSystem.Exists(binPath)) {
                                                    LogIt.Error("Unable to install Python.");
                                                } else {
                                                    if (EnvironmentHelper.IsLinux()) {
                                                        this.elevate(
                                                            "sudo apt-get install python-pip -y && " +
                                                            "sudo pip install --upgrade pip==9.0.3 && " +
                                                            "sudo pip install --upgrade virtualenv && " +
                                                            "sudo pip install numpy",
                                                            installPath, ($exitCode : number) : void => {
                                                                if ($exitCode === 0) {
                                                                    $callback();
                                                                } else {
                                                                    LogIt.Error("Unable to install numpy module for Python.");
                                                                }
                                                            });
                                                    } else if (EnvironmentHelper.IsMac()) {
                                                        if (this.fileSystem.Rename(
                                                            installPath + "/Python",
                                                            installPath + "/_Python")) {
                                                            if (this.fileSystem.Rename(
                                                                installPath + "/python.exe",
                                                                installPath + "/python")) {
                                                                this.terminal.Spawn("./python -m ensurepip --root=" + installPath,
                                                                    [], installPath,
                                                                    ($exitCode : number) : void => {
                                                                        if ($exitCode === 0) {
                                                                            LogIt.Info("Module pip for Python installed");

                                                                            this.terminal.Spawn("./python", [
                                                                                "-m", "pip", "install",
                                                                                "--root=" + installPath,
                                                                                "numpy"
                                                                            ], {
                                                                                cwd: installPath,
                                                                                env: {
                                                                                    PATH      : "$PATH",
                                                                                    PYTHONPATH: installPath +
                                                                                        "/usr/local/lib/python2.7/site-packages"
                                                                                }
                                                                            }, ($exitCode : number) : void => {
                                                                                if ($exitCode === 0) {
                                                                                    $callback();
                                                                                } else {
                                                                                    LogIt.Error("Unable to install numpy for Python.");
                                                                                }
                                                                            });
                                                                        } else {
                                                                            LogIt.Error("Unable to install pip for Python.");
                                                                        }
                                                                    });
                                                            } else {
                                                                LogIt.Error("Unable to rename Python binary");
                                                            }
                                                        } else {
                                                            LogIt.Error("Unable to rename Python directory");
                                                        }
                                                    }
                                                }
                                            } else {
                                                LogIt.Error("Unable to install Python.");
                                            }
                                        });
                                }
                            });
                        }
                    }, 250);
                });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                let cmd : string = (EnvironmentHelper.IsWindows() ? "" : "./");
                cmd += path.basename(binPath);
                this.terminal.Spawn(cmd, ["--version"], path.dirname(binPath),
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (!StringUtils.Contains($std[0] + $std[1], "2.7")) {
                                this.fileSystem.Delete(installPath);
                                install();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " Python is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate Python version.");
                        }
                    });
            }
        }

        private getPython36($callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            const version : string = "3.6.4";
            let source : any = {
                unix: "https://www.python.org/ftp/python/" + version + "/Python-" + version + ".tgz",
                win : {
                    x64: "https://www.python.org/ftp/python/" + version + "/python-" + version + "-amd64.exe",
                    x86: "https://www.python.org/ftp/python/" + version + "/python-" + version + ".exe"
                }
            };

            let installPath : string = this.externalModules + "/python36";
            installPath = this.fileSystem.NormalizePath(installPath);
            let binPath : string = installPath + "/python";
            if (EnvironmentHelper.IsWindows()) {
                source = EnvironmentHelper.Is64bit() ? source.win.x64 : source.win.x86;
                binPath += ".exe";
            } else if (EnvironmentHelper.IsLinux() || EnvironmentHelper.IsMac()) {
                source = source.unix;
            }

            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        if (EnvironmentHelper.IsWindows()) {
                            this.terminal.Execute(path.basename($path), [
                                    "/quiet",
                                    "/repair",
                                    "TargetDir=\"" + this.fileSystem.NormalizePath(installPath, true) + "\"",
                                    "InstallAllUsers=0",
                                    "Include_debug=1",
                                    "AssociateFiles=0",
                                    "Shortcuts=0",
                                    "Include_doc=0",
                                    "Include_launcher=0",
                                    "InstallLauncherAllUsers=0"
                                ],
                                path.dirname($path), ($exitCode : number) : void => {
                                    if ($exitCode === 0) {
                                        if (!this.fileSystem.Exists(installPath + "/python.exe")) {
                                            LogIt.Error("Unable to extract Python36.");
                                        }
                                        this.fileSystem.Delete($path);
                                        this.terminal.Spawn("python.exe", ["-m", "ensurepip", "--default-pip"], installPath,
                                            ($exitCode : number) : void => {
                                                if ($exitCode === 0) {
                                                    this.terminal.Spawn("python.exe", ["-m", "pip", "install", "numpy"],
                                                        installPath, ($exitCode : number) : void => {
                                                            if ($exitCode === 0) {
                                                                $callback();
                                                            } else {
                                                                LogIt.Error("Unable to install numpy module for Python36.");
                                                            }
                                                        });
                                                } else {
                                                    LogIt.Error("Unable to prepare pip");
                                                }
                                            });

                                    } else {
                                        LogIt.Error("Unable to install Python36.");
                                    }
                                });
                        } else {
                            this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                                this.fileSystem.Delete($path);
                                if (!this.fileSystem.Exists(installPath)) {
                                    LogIt.Error("Unable to extract Python 36.");
                                } else {
                                    const buildPython : any = () : void => {
                                        const configureArguments : string = "--with-pydebug CFLAGS=\"-I" + this.externalModules +
                                            "/openssl/include\" LDFLAGS=\"-L" + this.externalModules + "/openssl\"";

                                        this.terminal.Spawn(
                                            "chmod +x configure && " +
                                            "./configure " + configureArguments + " && " +
                                            "make -j" + EnvironmentHelper.getCores(),
                                            [], installPath, ($exitCode : number) : void => {
                                                if ($exitCode === 0) {
                                                    if (!this.fileSystem.Exists(binPath)) {
                                                        LogIt.Error("Unable to install Python36.");
                                                    } else {
                                                        if (EnvironmentHelper.IsLinux()) {
                                                            this.elevate(
                                                                "sudo apt-get install python3-pip -y && " +
                                                                "sudo python3 -m pip install --upgrade pip && " +
                                                                "sudo python3 -m pip install --upgrade virtualenv && " +
                                                                "sudo python3 -m pip install numpy",
                                                                installPath, ($exitCode : number) : void => {
                                                                    if ($exitCode === 0) {
                                                                        $callback();
                                                                    } else {
                                                                        LogIt.Error("Unable to install numpy module for Python36.");
                                                                    }
                                                                });
                                                        } else if (EnvironmentHelper.IsMac()) {
                                                            if (this.fileSystem.Rename(installPath +
                                                                "/Python", installPath + "/_Python")) {
                                                                if (this.fileSystem.Rename(installPath +
                                                                    "/python.exe", installPath + "/python")) {
                                                                    $callback();
                                                                } else {
                                                                    LogIt.Error("Unable to rename Python binary");
                                                                }
                                                            } else {
                                                                LogIt.Error("Unable to rename Python directory");
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    LogIt.Error("Unable to install Python36.");
                                                }
                                            });
                                    };

                                    if (EnvironmentHelper.IsLinux()) {
                                        this.elevate(
                                            "sudo apt-get install libffi-dev",
                                            installPath, ($exitCode : number) : void => {
                                                if ($exitCode === 0) {
                                                    buildPython();
                                                } else {
                                                    LogIt.Error("Unable to install libffi-dev");
                                                }
                                            });
                                    } else {
                                        buildPython();
                                    }
                                }
                            });
                        }
                    }, 250);
                });
            };

            const externalModules : string = Loader.getInstance().getProgramArgs().AppDataPath() + "/external_modules";
            this.terminal.Spawn(path.basename(binPath), ["--version"], {
                    env: {
                        PATH: externalModules + "/python36" + (EnvironmentHelper.IsWindows() ? ";" : ":") +
                            externalModules + "/python36/Scripts"
                    }
                },
                ($exitCode : number, $std : string[]) : void => {
                    if ($exitCode === 0) {
                        if (!StringUtils.Contains($std[0] + $std[1], version)) {
                            this.fileSystem.Delete(installPath);
                            install();
                        } else {
                            LogIt.Info(">>"[ColorType.YELLOW] + " Python is up to date.");
                            $callback();
                        }
                    } else {
                        LogIt.Error("Unable to validate Python version.");
                    }
                });
        }

        private getMsys2($callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            const version : string = "7.3.0";
            const source : any = {
                x64: "http://repo.msys2.org/distrib/msys2-x86_64-latest.tar.xz",
                x86: "http://repo.msys2.org/distrib/msys2-i686-latest.tar.xz"
            };
            const pkgOption : any = {
                x64: "x86_64",
                x86: "i686"
            };
            const requiredPackages : string[] = [
                "perl", "make", "automake", "autoconf", "libtool", "texinfo", "openssh", "sshpass", "rsync"
            ];

            let installPath : string = this.externalModules + "/msys2";
            installPath = this.fileSystem.NormalizePath(installPath);
            const binPath : string = installPath + "/mingw" + (EnvironmentHelper.Is64bit() ? "64" : "32") + "/bin/gcc.exe";

            const shellExecute : any = ($cmd, $args : string[], $callback : ($exitCode : number, $stdout : string) => void,
                                        $useMsysMode? : boolean) : void => {
                let mingwMode : boolean = true;
                if (!ObjectValidator.IsEmptyOrNull($useMsysMode) && $useMsysMode) {
                    mingwMode = false;
                }
                this.terminal.Spawn(
                    installPath + "\\usr\\bin\\mintty.exe",
                    [
                        "--log", "-",
                        "-d",
                        "--nopin",
                        "--window", "hide",
                        "--hold", "never",
                        "-o", "'ConfirmExit=no'",
                        "--",
                        "/usr/bin/sh",
                        "-lc", "\"" + [$cmd].concat($args).join(" ") + "\""
                    ],
                    {
                        cwd: installPath,
                        env: {
                            CHERE_INVOKING: "1",
                            MSYSCON       : "mintty.exe",
                            MSYSTEM       : mingwMode ? (EnvironmentHelper.Is64bit() ? "MINGW64" : "MINGW32") : "MSYS"
                        }
                    }, ($exitCode : number, $std : string[]) : void => {
                        $callback($exitCode, $std[0] + $std[1]);
                    });
            };

            const updateMingw : any = ($callback : () => void) : void => {
                const updatePackages = ($callback : () => void) : void => {
                    shellExecute("pacman", [
                        "-Syu", "--noconfirm", "--needed"
                    ].concat(requiredPackages), ($exitCode : number, $stdout : string) : void => {
                        // todo check by parsing stdout, exitCode is 0 almost anyway
                        if ($exitCode === 0) {
                            $callback();
                        } else {
                            LogIt.Error("Unable to install MinGW packages.");
                        }
                    });
                };
                const removePackage : any = ($package : string, $callback : () => void) : void => {
                    shellExecute("pacman", ["-R", $package, "--noconfirm"], ($exitCode : number, $stdout : string) : void => {
                        if (StringUtils.ContainsIgnoreCase($stdout, "removing " + $package) &&
                            StringUtils.ContainsIgnoreCase($stdout, "100%")) {
                            $callback();
                        } else {
                            LogIt.Error($stdout);
                        }
                    });
                };
                const updateCore : any = ($callback : () => void, $otherOptions : string[] = []) : void => {
                    shellExecute("pacman", ["-Syuu", "--noconfirm", "--needed"].concat($otherOptions),
                        ($exitCode : number, $stdout : string) : void => {
                            if ($exitCode === 0) {
                                if (StringUtils.ContainsIgnoreCase($stdout, "error: unresolvable package conflicts detected")) {
                                    const match : any = /are\sin\sconflict.\sRemove\s(.*)\?\s\[y\/N]/gi.exec($stdout);
                                    if (!ObjectValidator.IsEmptyOrNull(match) && match.length >= 2) {
                                        removePackage(match[1], () : void => {
                                            updateCore($callback);
                                        });
                                    } else {
                                        LogIt.Error("Can not match stdout of pacman execution while updating MinGW core.");
                                    }
                                } else if (StringUtils.ContainsIgnoreCase($stdout,
                                    "error: failed to commit transaction (conflicting files)")) {
                                    const conflictListMatch : any = new RegExp(
                                        "error:\\sfailed\\sto\\scommit\\stransaction\\s" +
                                        "\\(conflicting\\sfiles\\)[\\r\\n]+((?:(?:.*):(?:.*)[\\r\\n]+)+)Errors\\soccurred",
                                        "gi").exec($stdout);
                                    if (!ObjectValidator.IsEmptyOrNull(conflictListMatch) && conflictListMatch.length >= 2) {
                                        const parsedList : string[] = conflictListMatch[1].match(/(.*):(.*)/gm);
                                        if (parsedList.length > 0) {
                                            updateCore($callback, ["--overwrite", StringUtils.Split(parsedList[0], ":")[0]]);
                                        } else {
                                            LogIt.Error("Can not identify package in conflict.");
                                        }
                                    } else {
                                        LogIt.Error("Can not match stdout of pacman commit transaction while updating MinGW core.");
                                    }
                                } else if (StringUtils.OccurrenceCount($stdout, "there is nothing to do") < 2) {
                                    updateCore($callback);
                                } else if (StringUtils.ContainsIgnoreCase($stdout, "/usr/bin/sh: pacman: command not found")) {
                                    LogIt.Error("MinGW update failed due to corrupted Msys filesystem.");
                                } else if (StringUtils.ContainsIgnoreCase($stdout, "try running pacman-db-upgrade")) {
                                    shellExecute("pacman-db-upgrade", [], () : void => {
                                        updateCore($callback);
                                    }, true);
                                } else {
                                    const option : string = EnvironmentHelper.Is64bit() ? pkgOption.x64 : pkgOption.x86;
                                    requiredPackages.unshift(
                                        "mingw-w64-" + option + "-binutils",
                                        "mingw-w64-" + option + "-gcc",
                                        "mingw-w64-" + option + "-gcc-objc",
                                        "mingw-w64-" + option + "-gdb",
                                        "mingw-w64-" + option + "-make",
                                        "mingw-w64-" + option + "-tools"
                                    );
                                    updatePackages($callback);
                                }
                            } else {
                                LogIt.Error("Can not update MinGW core.\n" + $stdout);
                            }
                        });
                };

                updateCore($callback);
            };

            const initializeMsys : any = () : void => {
                this.fileSystem.CreateDirectory(installPath + "/tmp");

                shellExecute("exit", [], ($exitCode : number, $stdout : string) : void => {
                    const match : any = $stdout.match(/#\s*This\sis\sfirst\sstart\sof\sMSYS2\.\s*#/g);
                    if ($exitCode === 0 && !ObjectValidator.IsEmptyOrNull(match)) {
                        updateMingw($callback);
                    } else {
                        this.fileSystem.Delete(installPath);
                        LogIt.Error("Can not initialize Msys.");
                    }
                }, true);
            };

            const install : any = () : void => {
                this.fileSystem.Delete(installPath);
                this.fileSystem.Download(EnvironmentHelper.Is64bit() ? source.x64 : source.x86,
                    ($headers : string, $path : string) : void => {
                        setTimeout(() : void => {
                            this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                                this.fileSystem.Delete($path);
                                if (!this.fileSystem.Exists(installPath + "/msys2_shell.cmd")) {
                                    LogIt.Error("Unable to build Msys2.");
                                } else {
                                    initializeMsys();
                                }
                            });
                        }, 250);
                    });
            };

            if (!this.fileSystem.Exists(binPath)) {
                if (this.fileSystem.Exists(installPath)) {
                    initializeMsys();
                } else {
                    install();
                }
            } else {
                this.terminal.Spawn(path.basename(binPath), ["--version"], path.dirname(binPath),
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (StringUtils.VersionIsLower(StringUtils.Remove($std[0],
                                "gcc.exe (Rev1, Built by MSYS2 project) ", " "), version)) {
                                install();
                            } else {
                                updateMingw($callback);
                            }
                        } else {
                            LogIt.Error("Unable to validate Msys2 (gcc) version.");
                        }
                    });
            }
        }

        private getGCC($callback : () => void) : void {
            const version : string = "8.2.0";
            const path : Types.NodeJS.path = require("path");
            const installPath : string = this.externalModules + "/gcc";
            const binPath : string = installPath + "/gcc";

            const installLinux : any = () : void => {
                this.fileSystem.CreateDirectory(installPath);
                this.elevate(
                    "sudo apt-get update && " +
                    "sudo apt-get install build-essential software-properties-common -y && " +
                    "sudo add-apt-repository ppa:ubuntu-toolchain-r/test -y && " +
                    "sudo apt-get update && " +
                    "sudo apt-get install gcc-snapshot -y && " +
                    "sudo apt-get update && " +
                    "sudo apt-get install gcc-8 g++-8 texinfo patchelf gdb -y && " +
                    "ln -s /usr/bin/gcc-8 " + installPath + "/cc && " +
                    "ln -s /usr/bin/gcc-8 " + installPath + "/gcc && " +
                    "ln -s /usr/bin/g++-8 " + installPath + "/c++ && " +
                    "ln -s /usr/bin/g++-8 " + installPath + "/g++",
                    null,
                    ($exitCode : number) : void => {
                        if ($exitCode === 0) {
                            $callback();
                        } else {
                            LogIt.Error("Unable to install gcc. Exit code: " + $exitCode);
                        }
                    });
            };

            const installMac : any = () : void => {
                this.fileSystem.CreateDirectory(installPath);
                this.terminal.Spawn("brew install gcc@8",
                    null, "",
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0 ||
                            StringUtils.Contains($std[0] + $std[1], "already installed")) {
                            this.terminal.Spawn(
                                "brew link --overwrite gcc@8 && " +
                                "ln -s $(which gcc-8) " + installPath + "/cc && " +
                                "ln -s $(which gcc-8) " + installPath + "/gcc && " +
                                "ln -s $(which g++-8) " + installPath + "/c++ && " +
                                "ln -s $(which g++-8) " + installPath + "/g++",
                                null, "", ($exitCode : number) : void => {
                                    if ($exitCode === 0) {
                                        $callback();
                                    } else {
                                        LogIt.Error("Unable to install gcc. Exit code: " + $exitCode);
                                    }
                                });
                        } else {
                            LogIt.Error("Unable to install gcc. Exit code: " + $exitCode);
                        }
                    });
            };

            const install : any = () : void => {
                if (EnvironmentHelper.IsMac()) {
                    installMac();
                } else {
                    installLinux();
                }
            };

            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                this.terminal.Spawn("./" + path.basename(binPath), ["--version"], path.dirname(binPath),
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (!StringUtils.Contains($std[0] + $std[1], version)) {
                                this.fileSystem.Delete(installPath);
                                install();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " gcc is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate gcc version.");
                        }
                    });
            }
        }

        private getARM($callback : () => void) : void {
            const version : string = "8.2.0";

            const install : any = ($callback : () => void) : void => {
                this.elevate(
                    "sudo add-apt-repository \"deb http://mirrors.kernel.org/ubuntu artful main\" && " +
                    "sudo apt-get update && " +
                    "sudo apt-get install gcc-8-aarch64-linux-gnu g++-8-aarch64-linux-gnu -y",
                    null,
                    ($exitCode : number) : void => {
                        if ($exitCode === 0) {
                            $callback();
                        } else {
                            LogIt.Error("Unable to install ARM gcc. Exit code: " + $exitCode);
                        }
                    });
            };

            this.terminal.Spawn("aarch64-linux-gnu-gcc-8", ["--version"], null,
                ($exitCode : number, $std : string[]) : void => {
                    if ($exitCode !== 0 || StringUtils.Contains($std[0], version)) {
                        install($callback);
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " ARM gcc is up to date.");
                        $callback();
                    }
                });
        }

        private getClang($callback : () => void) : void {
            const version : string = "4.2.1";
            const installPath : string = this.externalModules + "/clang";

            const installMac : any = () : void => {
                this.fileSystem.CreateDirectory(installPath);

                // Homebrew installation requires clang (and it automatically installs it), thus there's no need to install it manually
                this.terminal.Spawn(
                    "ln -s $(which clang) " + installPath + "/cc && " +
                    "ln -s $(which clang) " + installPath + "/clang && " +
                    "ln -s $(which clang++) " + installPath + "/c++ && " +
                    "ln -s $(which clang++) " + installPath + "/clang++",
                    null, null,
                    ($exitCode : number) : void => {
                        if ($exitCode === 0) {
                            $callback();
                        } else {
                            LogIt.Error("Unable to install clang. Exit code: " + $exitCode);
                        }
                    });
            };

            if (!this.fileSystem.Exists(installPath)) {
                installMac();
            } else {
                this.terminal.Spawn("clang", ["-dumpversion"], null,
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            const installedVersion : string = $std[0].trim();
                            if (StringUtils.VersionIsLower(installedVersion, version)) {
                                LogIt.Info(">>"[ColorType.YELLOW] + " current version " + installedVersion + " is lower than " + version);

                                this.fileSystem.Delete(installPath);
                                installMac();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " clang is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate clang version.");
                        }
                    });
            }
        }

        private getGtest($callback : () => void) : void {
            const tag : string = "release-1.8.1";
            const source : string = "https://github.com/google/googletest.git";
            const installPath : string = this.externalModules + "/gtest";

            class Toolset {
                private name : string;
                private cmakeArguments : string[];
                private productsPaths : string[];
                private isMandatory : boolean = true;
                private buildPath : string;
                private terminal : Com.Wui.Framework.Builder.Connectors.Terminal;
                private fileSystem : Com.Wui.Framework.Builder.Connectors.FileSystemHandler;

                constructor($name : string, $cmakeArguments : string[], $productsPaths : string[], $isMandatory : boolean) {
                    this.name = $name;
                    this.cmakeArguments = $cmakeArguments;
                    this.productsPaths = $productsPaths;
                    this.isMandatory = $isMandatory;
                    this.buildPath = installPath + "/build_" + this.name;

                    this.terminal = Loader.getInstance().getTerminal();
                    this.fileSystem = Loader.getInstance().getFileSystemHandler();
                }

                public validate($onSuccess : () => void, $onError : () => void) : void {
                    let allMandatoryProductsExist : boolean = true;

                    for (const productPath of this.productsPaths) {
                        const fullPathToProduct : string = this.buildPath + "/" + productPath;

                        if (!this.fileSystem.Exists(fullPathToProduct)) {
                            if (this.isMandatory) {
                                LogIt.Warning("Mandatory product at path " + fullPathToProduct + " doesn't exist");

                                allMandatoryProductsExist = false;

                                if ($onError) {
                                    $onError();
                                }
                            } else {
                                LogIt.Warning("Non-mandatory product at path " + fullPathToProduct + " doesn't exist");
                            }
                        }
                    }

                    if (allMandatoryProductsExist) {
                        if ($onSuccess) {
                            $onSuccess();
                        }
                    }
                }

                public buildAndValidate($onSuccess : () => void, $onError : () => void) : void {
                    this.build(() : void => {
                        this.validate($onSuccess, $onError);
                    });
                }

                private build($nextAction : () => void) : void {
                    const cmakeArguments : string[] = ["-B" + this.buildPath, "-H."].concat(this.cmakeArguments);

                    this.terminal.Spawn("cmake", cmakeArguments, installPath, ($exitCode : number) : void => {
                        if ($exitCode === 0) {
                            LogIt.Info("Generation phase successful");

                            this.terminal.Spawn(
                                "cmake",
                                ["--build", this.buildPath, "--config Release --", "--jobs=" + EnvironmentHelper.getCores()],
                                null, ($exitCode : number) : void => {
                                    if ($exitCode === 0) {
                                        LogIt.Info("Build phase successful");

                                        $nextAction();
                                    } else {
                                        LogIt.Error("Build phase failed for toolset " + this.name);
                                    }
                                });
                        } else {
                            LogIt.Error("Generation phase failed for toolset " + this.name);
                        }
                    });
                }
            }

            class Toolsets {
                private toolsets : Toolset[] = [];

                public build($onError : () => void) : void {
                    LogIt.Info("Will build totally " + this.toolsets.length + " toolsets");

                    this.buildAndValidateToolset(0, null, $onError);
                }

                public validate($onError : () => void) : void {
                    LogIt.Info("Will validate totally " + this.toolsets.length + " toolsets");

                    this.validateToolset(0, null, $onError);
                }

                public push($toolset : Toolset) : void {
                    this.toolsets.push($toolset);
                }

                private buildAndValidateToolset($index : number, $onSuccess : () => void, $onError : () => void) : void {
                    if ($index < this.toolsets.length) {
                        const processNext : any = () : void => {
                            this.toolsets[$index].buildAndValidate(() : void => {
                                this.buildAndValidateToolset($index + 1, $onSuccess, $onError);
                            }, $onError);
                        };

                        const processLastOne : any = () : void => {
                            this.toolsets[$index].buildAndValidate($callback, $onError);
                        };

                        if ($index + 1 === this.toolsets.length) {
                            processLastOne();
                        } else {
                            processNext();
                        }
                    }
                }

                private validateToolset($index : number, $onSuccess : () => void, $onError : () => void) : void {
                    if ($index < this.toolsets.length) {
                        const processNext : any = () : void => {
                            this.toolsets[$index].validate(() : void => {
                                this.validateToolset($index + 1, $onSuccess, $onError);
                            }, $onError);
                        };

                        const processLastOne : any = () : void => {
                            this.toolsets[$index].validate($callback, $onError);
                        };

                        if ($index + 1 === this.toolsets.length) {
                            processLastOne();
                        } else {
                            processNext();
                        }
                    }
                }
            }

            const toolsets = new Toolsets();

            if (EnvironmentHelper.IsWindows()) {
                toolsets.push(new Toolset(
                    "gcc",
                    [
                        "-DCMAKE_BUILD_TYPE:STRING=Release",
                        "-DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=ON",
                        "-G", "\"MinGW Makefiles\""
                    ],
                    [
                        "googlemock/libgmock.a",
                        "googlemock/gtest/libgtest.a"
                    ],
                    true)
                );
            } else {
                toolsets.push(new Toolset(
                    "gcc",
                    [
                        "-DCMAKE_BUILD_TYPE:STRING=Release",
                        "-DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=ON"
                    ],
                    [
                        "googlemock/libgmock.a",
                        "googlemock/gtest/libgtest.a"
                    ],
                    true)
                );

                if (EnvironmentHelper.IsLinux()) {
                    toolsets.push(new Toolset(
                        "arm",
                        [
                            "-DCMAKE_BUILD_TYPE:STRING=Release",
                            "-DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=ON",
                            "-DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc-8",
                            "-DCMAKE_CXX_COMPILER=aarch64-linux-gnu-g++-8"
                        ],
                        [
                            "googlemock/libgmock.a",
                            "googlemock/gtest/libgtest.a"
                        ],
                        true)
                    );
                } else if (EnvironmentHelper.IsMac()) {
                    toolsets.push(new Toolset(
                        "clang",
                        [
                            "-DCMAKE_BUILD_TYPE:STRING=Release",
                            "-DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=ON",
                            "-DCMAKE_C_COMPILER:PATH=" + this.externalModules + "/clang/cc",
                            "-DCMAKE_CXX_COMPILER:PATH=" + this.externalModules + "/clang/c++"
                        ],
                        [
                            "googlemock/libgmock.a",
                            "googlemock/gtest/libgtest.a"
                        ],
                        true)
                    );
                }
            }

            const download : any = ($onSuccess : () => void) : any => {
                this.terminal.Spawn("git", ["clone", "--branch", tag, "--depth", "1", source, installPath],
                    null, ($exitCode : number) : void => {
                        if ($exitCode === 0) {
                            $onSuccess();
                        } else {
                            LogIt.Error("Unable to clone gtest at tag " + tag);
                        }
                    });
            };

            const install : any = () : void => {
                download(() : void => {
                    const onError : any = () : void => {
                        LogIt.Error("Some of products were not successfully built");
                    };

                    toolsets.build(onError);
                });
            };

            const reinstall : any = () : void => {
                if (this.fileSystem.Exists(installPath)) {
                    this.fileSystem.Delete(installPath);
                }

                install();
            };

            const checkVersion : any = ($nextAction : () => void) : void => {
                this.terminal.Spawn("git", ["describe", "--tags"], installPath,
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            const installedVersion : string = $std[0].trim();
                            if (installedVersion !== tag) {
                                LogIt.Info(">>"[ColorType.YELLOW] + " current version " + installedVersion + " is different than " + tag);

                                reinstall();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " gtest is up to date.");

                                toolsets.validate(reinstall);
                            }
                        } else {
                            LogIt.Error("Unable to validate gtest version.");
                        }
                    });
            };

            if (!this.fileSystem.Exists(installPath)) {
                install();
            } else {
                checkVersion();
            }
        }

        private getResourceHacker($callback : () => void) : void {
            const source : string = "https://hub.wuiframework.com/Download/" +
                Buffer.from(("http://www.angusj.com/resourcehacker/resource_hacker.zip")).toString("base64");

            let installPath : string = this.externalModules + "/resourcehacker";
            installPath = this.fileSystem.NormalizePath(installPath);
            const binPath : string = installPath + "/ResourceHacker.exe";

            if (!this.fileSystem.Exists(binPath)) {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                        this.fileSystem.Delete($path);
                        if (!this.fileSystem.Exists(binPath)) {
                            LogIt.Error("Unable to extract ResourceHacker.");
                        } else {
                            $callback();
                        }
                    });
                });
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " ResourceHacker is up to date.");
                $callback();
            }
        }

        private getUpx($callback : () => void) : void {
            const path : Types.NodeJS.path = require("path");
            const version : string = "3.94";
            let source : any = {
                linux: {
                    x64: "https://github.com/upx/upx/releases/download/v" + version + "/upx-" + version + "-amd64_linux.tar.xz",
                    x86: "https://github.com/upx/upx/releases/download/v" + version + "/upx-" + version + "-i386_linux.tar.xz"
                },
                mac  : {
                    ucl: "http://www.oberhumer.com/opensource/ucl/download/ucl-1.03.tar.gz",
                    upx: "https://github.com/upx/upx/releases/download/v" + version + "/upx-" + version + "-src.tar.xz"
                },
                win  : {
                    x64: "https://github.com/upx/upx/releases/download/v" + version + "/upx" +
                        StringUtils.Replace(version, ".", "") + "w.zip",
                    x86: "https://github.com/upx/upx/releases/download/v" + version + "/upx" +
                        StringUtils.Replace(version, ".", "") + "w.zip"
                }
            };

            let installPath : string = this.externalModules + "/upx";
            installPath = this.fileSystem.NormalizePath(installPath);
            let binPath : string = installPath + "/upx";
            if (EnvironmentHelper.IsWindows()) {
                source = EnvironmentHelper.Is64bit() ? source.win.x64 : source.win.x86;
                binPath += ".exe";
            } else if (EnvironmentHelper.IsLinux()) {
                source = EnvironmentHelper.Is64bit() ? source.linux.x64 : source.linux.x86;
            }

            const macInstallUpx : any = ($uclPath : string) : void => {
                this.fileSystem.Download(source.mac.upx, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        this.fileSystem.Unpack($path, null, ($path : string) : void => {
                            LogIt.Info("upx unpacked at: " + $path);
                            this.terminal.Spawn(
                                "export UPX_UCLDIR=" + $uclPath + " && " +
                                "make CC=$(which clang) CXX=$(which clang++) -j4 all",
                                [],
                                $path, ($exitcode : number) : void => {
                                    if ($exitcode === 0) {
                                        this.fileSystem.Copy($path, installPath, () : void => {
                                            this.fileSystem.Copy($path + "/src/upx.out",
                                                binPath, () : void => {
                                                    this.terminal.Spawn("chmod +x " + binPath, [], $path, () : void => {
                                                        $callback();
                                                    });
                                                });
                                        });
                                    } else {
                                        LogIt.Error("Failed to compile UPX");
                                    }
                                });
                        });
                    }, 250);
                });
            };

            const install : any = () : void => {
                if (EnvironmentHelper.IsMac()) {
                    this.fileSystem.Download(source.mac.ucl, ($headers : string, $path : string) : void => {
                        setTimeout(() : void => {
                            this.fileSystem.Unpack($path, null, ($uclPath : string) : void => {
                                this.terminal.Spawn("export CC=\"llvm-gcc\" && chmod +x configure && ./configure && make -j4", [], $uclPath,
                                    ($exitcode : number) : void => {
                                        if ($exitcode === 0) {
                                            macInstallUpx($uclPath);
                                        } else {
                                            LogIt.Error("Failed to compile UCL");
                                        }
                                    });
                            });
                        }, 250);
                    });
                } else {
                    this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                        setTimeout(() : void => {
                            this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                                this.fileSystem.Delete($path);
                                if (!this.fileSystem.Exists(binPath)) {
                                    LogIt.Error("Unable to extract Upx.");
                                } else {
                                    if (EnvironmentHelper.IsWindows()) {
                                        $callback();
                                    } else {
                                        this.terminal.Spawn("chmod", ["-R", "+x", binPath], null,
                                            ($exitCode : number) : void => {
                                                if ($exitCode === 0) {
                                                    $callback();
                                                } else {
                                                    LogIt.Error("Unable to install Upx.");
                                                }
                                            });
                                    }
                                }
                            });
                        }, 250);
                    });
                }
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                this.terminal.Spawn((EnvironmentHelper.IsWindows() ? "" : "./") + path.basename(binPath),
                    ["--version"], path.dirname(binPath), ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (!StringUtils.Contains($std[0] + $std[1], version)) {
                                this.fileSystem.Delete(installPath);
                                install();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " Upx is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate Upx version.");
                        }
                    });
            }
        }

        private getMaven($callback : ($path? : string) => void) : void {
            const path : Types.NodeJS.path = require("path");
            const version : string = "3.5.4";
            const source : string = "http://www-us.apache.org/dist/maven/maven-3/" +
                version + "/binaries/apache-maven-" + version + "-bin.tar.gz";
            const installPath : string = this.externalModules + "/maven";
            const binPath : string = installPath + "/bin/mvn";

            const setLocalRepository : () => void = () => {
                const settingsPath : string = installPath + "/conf/settings.xml";
                this.fileSystem.Write(settingsPath,
                    this.fileSystem.Read(settingsPath).toString()
                        .replace("</settings>", "<localRepository>${maven.home}/repository/</localRepository>\n</settings>")
                );
            };

            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                            this.fileSystem.Delete($path);
                            if (!this.fileSystem.Exists(binPath)) {
                                LogIt.Error("Unable to install Maven.");
                            } else {
                                setLocalRepository();
                                $callback($path);
                            }
                        });
                    }, 250);
                });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                this.terminal.Spawn((EnvironmentHelper.IsWindows() ? "" : "./") + path.basename(binPath),
                    ["--version"], path.dirname(binPath), ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (!StringUtils.Contains($std[0] + $std[1], version)) {
                                this.fileSystem.Delete(installPath);
                                install();
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " Maven is up to date.");
                                $callback();
                            }
                        } else {
                            LogIt.Error("Unable to validate Maven version.");
                        }
                    });
            }
        }

        private getIdea($callback : ($path? : string) => void) : void {
            const version : string = "172.4574.19";
            const source : string = "https://download.jetbrains.com/idea/ideaIC-2017.2.7.win.zip";
            const installPath : string = this.externalModules + "/idea";
            const binPath : string = installPath + "/idea.exe";

            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                            this.fileSystem.Delete($path);
                            if (!this.fileSystem.Exists(binPath)) {
                                LogIt.Error("Unable to install Idea.");
                            } else {
                                $callback($path);
                            }
                        });
                    }, 250);
                });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else if (!StringUtils.Contains(this.fileSystem.Read(installPath + "/build.txt").toString(), version)) {
                this.fileSystem.Delete(installPath);
                install();
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Idea is up to date.");
                $callback();
            }
        }

        private getEclipse($callback : ($path? : string) => void) : void {
            const type : string = "oxygen";
            const release : string = "1a";
            const version : string = "4.7.1";
            const source : string = "http://mirror.dkm.cz/eclipse/technology/epp/downloads/release/" + type + "/" + release +
                "/eclipse-java-" + type + "-" + release + "-win32.zip";
            const installPath : string = this.externalModules + "/eclipse";
            const binPath : string = installPath + "/eclipse.exe";

            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                            this.fileSystem.Delete($path);
                            if (!this.fileSystem.Exists(binPath)) {
                                LogIt.Error("Unable to install Eclipse.");
                            } else {
                                $callback($path);
                            }
                        });
                    }, 250);
                });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else if (!StringUtils.Contains(this.fileSystem.Read(installPath + "/.eclipseproduct").toString(), version)) {
                this.fileSystem.Delete(installPath);
                install();
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Eclipse is up to date.");
                $callback();
            }
        }

        private getOpenssl($callback : () => void) : void {
            const version : string = "1.1.0f";
            let source : any = {
                linux: "https://www.openssl.org/source/openssl-" + version + ".tar.gz",
                mac  : "https://www.openssl.org/source/openssl-" + version + ".tar.gz",
                win  : "https://www.openssl.org/source/openssl-" + version + ".tar.gz"
            };

            let installPath : string = this.externalModules + "/openssl";
            installPath = this.fileSystem.NormalizePath(installPath);
            const binPath : string = installPath + "/build/lib/libssl.a";
            if (EnvironmentHelper.IsWindows()) {
                source = source.win;
            } else if (EnvironmentHelper.IsLinux()) {
                source = source.linux;
            } else if (EnvironmentHelper.IsMac()) {
                source = source.mac;
            }

            const checkVersion : any = () : boolean => {
                let isActual : boolean = false;

                if (this.fileSystem.Exists(installPath + "/README")) {
                    const data : string = this.fileSystem.Read(installPath + "/README").toString();
                    const regex : any = new RegExp("^[.\\r\\n\\s]+OpenSSL (" + version.replace(/\./g, "\\.") + ")\\s");
                    const match : RegExpMatchArray = (data + "").match(regex);
                    if (match !== null && match.length === 2) {
                        if (match[1] === version) {
                            isActual = true;
                        } else {
                            LogIt.Warning("Already installed openssl has different version: " + match[1]);
                        }
                    }
                }

                return isActual;
            };

            const install : any = () : void => {
                const validate : any = () : void => {
                    if (!this.fileSystem.Exists(binPath) || !checkVersion()) {
                        LogIt.Error("Unable to install openssl.");
                    } else {
                        $callback();
                    }
                };

                this.fileSystem.Download(source,
                    ($headers : string, $path : string) : void => {
                        this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                            this.fileSystem.Delete($path);

                            if (EnvironmentHelper.IsWindows()) {
                                validate();
                            } else {
                                this.terminal.Spawn(
                                    "chmod +x config && " +
                                    "chmod +x util/point.sh && " +
                                    "./config shared --prefix=" + installPath + "/build " +
                                    "--openssldir=" + installPath + "/build -fPIC && " +
                                    "make -j" + EnvironmentHelper.getCores() + " && make install",
                                    [], installPath,
                                    ($exitCode : number) : void => {
                                        if ($exitCode === 0) {
                                            validate();
                                        } else {
                                            LogIt.Error("Unable to install OpenSSL.");
                                        }
                                    });
                            }
                        });
                    });
            };

            if (!this.fileSystem.Exists(binPath) || !checkVersion()) {
                if (EnvironmentHelper.IsWindows()) {
                    install();
                } else {
                    this.terminal.Spawn("rm", ["-rf", installPath], null, () : void => {
                        install();
                    });
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " openssl is up to date.");
                $callback();
            }
        }

        private getRegisterCLI($callback : () => void) : void {
            const registerPath : string = StringUtils.Replace(this.programArgs.ProjectBase() + "/cmd;", "/", "\\");
            if (StringUtils.Contains(process.env.PATH, registerPath)) {
                LogIt.Info(">>"[ColorType.YELLOW] + " WUI Builder CLI registration is up to date.");
                $callback();
            } else {
                EnvironmentHelper.IsElevated(($status : boolean) : void => {
                    if (EnvironmentHelper.IsWindows()) {
                        const getPathValue : any = ($done : ($value : string) => void) : void => {
                            let key : string = "HKCU\\Environment";
                            let name : string = "PATH";
                            if ($status) {
                                key = "HKLM\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment";
                                name = "Path";
                            }
                            const args : string[] = ["query", "\"" + key + "\"", "/v", "\"" + name + "\""];
                            if (EnvironmentHelper.Is64bit()) {
                                args.push("/reg:64");
                            }
                            this.terminal.Spawn("reg", args, null, ($exitCode : number, $std : string[]) : void => {
                                if (!ObjectValidator.IsEmptyOrNull($exitCode)) {
                                    if ($exitCode === 0) {
                                        if (StringUtils.Contains($std[0], " REG_SZ ")) {
                                            $done(StringUtils.Substring($std[0], StringUtils.IndexOf($std[0], " REG_SZ ") + 8));
                                        } else if (StringUtils.Contains($std[0], " REG_EXPAND_SZ ")) {
                                            $done(StringUtils.Substring($std[0], StringUtils.IndexOf($std[0], " REG_EXPAND_SZ ") + 14));
                                        } else if (StringUtils.ContainsIgnoreCase($std[0] + $std[1], "ERROR:")) {
                                            $done("");
                                        } else {
                                            $done($std[0]);
                                        }
                                    } else {
                                        $done("");
                                    }
                                }
                            });
                        };
                        getPathValue(($envPath : string) : void => {
                            if (!StringUtils.Contains($envPath, registerPath)) {
                                $envPath = $envPath.trim();
                                if (!StringUtils.EndsWith($envPath, ";")) {
                                    $envPath += ";";
                                }
                                const args : string[] = [];
                                if ($status) {
                                    args.push("/M");
                                }
                                args.push("PATH");
                                args.push("\"" + $envPath + registerPath + "\"");
                                this.terminal.Spawn("SETX", args, null, ($exitCode : number) : void => {
                                    if (!ObjectValidator.IsEmptyOrNull($exitCode)) {
                                        if ($exitCode === 0) {
                                            $callback();
                                        } else {
                                            LogIt.Error("Unable to register WUI Builder global CLI.");
                                        }
                                    }
                                });
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " WUI Builder CLI registration is up to date.");
                                $callback();
                            }
                        });
                    } else {
                        LogIt.Warning("Unable to register WUI Builder global CLI: unsupported OS");
                        $callback();
                    }
                });
            }
        }

        private getNASM($callback : ($path? : string) => void) : void {
            const version : string = "2.13.03";
            const type : string = "win" + (EnvironmentHelper.Is64bit() ? "64" : "32");
            const source : string = "https://www.nasm.us/pub/nasm/releasebuilds/" +
                version + "/" + type + "/nasm-" + version + "-" + type + ".zip";
            const installPath : string = this.externalModules + "/nasm";
            const binPath : string = installPath + "/nasm.exe";

            const install : any = () : void => {
                this.fileSystem.Download(source, ($headers : string, $path : string) : void => {
                    setTimeout(() : void => {
                        this.fileSystem.Unpack($path, {output: installPath}, () : void => {
                            this.fileSystem.Delete($path);
                            if (!this.fileSystem.Exists(binPath)) {
                                LogIt.Error("Unable to install NASM.");
                            } else {
                                $callback($path);
                            }
                        });
                    }, 250);
                });
            };
            if (!this.fileSystem.Exists(binPath)) {
                install();
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " NASM is up to date.");
                $callback();
            }
        }
    }
}
