/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import CredentialsManager = Com.Wui.Framework.Builder.Connectors.CredentialsManager;
    import IServerConfigurationJira = Com.Wui.Framework.Builder.Interfaces.IServerConfigurationJira;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import JIRAConnector = Com.Wui.Framework.Builder.Connectors.JIRAConnector;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IBuilderServerCredentials = Com.Wui.Framework.Builder.Interfaces.IBuilderServerCredentials;
    import StdinManager = Com.Wui.Framework.Localhost.Utils.StdinManager;
    import StdinQuestion = Com.Wui.Framework.Localhost.Utils.StdinQuestion;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class JIRASync extends BaseTask {
        private validate : boolean;
        private ignoreKeys : string[] = [];
        private cachePath : string;
        private maxUpdates : number;
        private projects : string[];
        private source : any;
        private target : any;

        protected getName() : string {
            return "jira-sync";
        }

        protected process($done : any, $option : string) : void {
            const config : IServerConfigurationJira = this.builderConfig.jira;
            if (ObjectValidator.IsEmptyOrNull(config.cachePath)) {
                config.cachePath = this.programArgs.AppDataPath() + "/resource/data/" +
                    StringUtils.Replace(this.getClassName(), ".", "/") + "/Cache.json";
            } else {
                if (!StringUtils.StartsWith(config.cachePath, "/") &&
                    !StringUtils.Contains(config.cachePath, ":/")) {
                    config.cachePath = this.programArgs.ProjectBase() + "/" + config.cachePath;
                }
            }
            this.cachePath = config.cachePath;
            this.maxUpdates = 50;
            if (ObjectValidator.IsEmptyOrNull(config.projects)) {
                this.projects = ["WUI", "GPUSDK"];
            } else {
                this.projects = config.projects;
            }
            this.ignoreKeys = this.ignoreKeys.concat(config.ignoreKeys);
            this.validate = $option === "dry-run";
            if ($option === "users") {
                StdinManager.Process([
                    <StdinQuestion>{
                        callback($password : string, $callback : ($status : boolean) => void) : void {
                            CredentialsManager.setJIRAPass(config.source.user, $password, $callback);
                        },
                        hidden: true,
                        prefix: "JIRA password for " + config.source.user
                    },
                    <StdinQuestion>{
                        callback($password : string, $callback : ($status : boolean) => void) : void {
                            CredentialsManager.setJIRAPass(config.target.user, $password, $callback);
                        },
                        hidden: true,
                        prefix: "JIRA password for " + config.target.user
                    }
                ], () : void => {
                    $done();
                });
            } else {
                const getCredentials : any = () : void => {
                    CredentialsManager.getJIRAPass(config.source.user, ($pass : string) : void => {
                        config.source.pass = $pass;
                        CredentialsManager.getJIRAPass(config.target.user, ($pass : string) : void => {
                            config.target.pass = $pass;
                            this.source = {
                                connector  : new JIRAConnector("WUI", config.source),
                                data       : new ArrayList<any>(),
                                missing    : [],
                                transitions: [],
                                update     : []
                            };
                            this.target = {
                                connector  : new JIRAConnector("NXP", config.target),
                                data       : new ArrayList<any>(),
                                missing    : [],
                                transitions: [],
                                update     : []
                            };
                            this.getCache($option === "reindex" || this.programArgs.IsForce(), () : void => {
                                if ($option !== "reindex") {
                                    this.synchronize($done);
                                } else {
                                    $done();
                                }
                            });
                        });
                    });
                };
                const passCredentials : any = ($config : IBuilderServerCredentials, $callback : () => void) : void => {
                    CredentialsManager.getJIRAPass($config.user, ($pass : string) : void => {
                        if (ObjectValidator.IsEmptyOrNull($pass) || $pass !== $config.pass) {
                            LogIt.Warning("Force store pass for " + $config.user);
                            CredentialsManager.setJIRAPass($config.user, $config.pass,
                                ($status : boolean) : void => {
                                    if (!$status) {
                                        LogIt.Warning("Unable to store pass for " + $config.user);
                                    }
                                    $callback();
                                });
                        } else {
                            $callback();
                        }
                    });
                };
                if (!ObjectValidator.IsEmptyOrNull(config.source.pass)) {
                    passCredentials(config.source, () : void => {
                        if (!ObjectValidator.IsEmptyOrNull(config.target.pass)) {
                            passCredentials(config.target, getCredentials);
                        } else {
                            getCredentials();
                        }
                    });
                } else if (!ObjectValidator.IsEmptyOrNull(config.target.pass)) {
                    passCredentials(config.target, getCredentials);
                } else {
                    getCredentials();
                }
            }
        }

        private getCache($force : boolean = false, $done : () => void) : void {
            LogIt.Info("> create or update JIRA sync cache");
            let cache : any = {
                source: [],
                target: []
            };
            if ($force) {
                this.fileSystem.Delete(this.cachePath);
            }
            if (this.fileSystem.Exists(this.cachePath)) {
                cache = JSON.parse(this.fileSystem.Read(this.cachePath).toString());
            }
            let projects : string = "";
            this.projects.forEach(($project : string) : void => {
                if (!ObjectValidator.IsEmptyOrNull(projects)) {
                    projects += " OR ";
                }
                projects += "project=" + $project;
            });
            const jql : string = "(" + projects + ") ORDER BY id ASC";
            this.source.connector.Search(jql, ($data : any) : void => {
                if ($data.length > 0) {
                    cache.source = cache.source.concat($data);
                }
                this.target.connector.Search(jql, ($data : any) : void => {
                    if ($data.length > 0) {
                        cache.target = cache.target.concat($data);
                    }
                    this.fileSystem.Write(this.cachePath, JSON.stringify(cache));

                    cache.source.forEach(($issue : any) : void => {
                        this.source.data.Add($issue, $issue.key);
                    });
                    cache.target.forEach(($issue : any) : void => {
                        this.target.data.Add($issue, $issue.key);
                    });
                    if (!$force) {
                        this.source.connector.Search(jql, ($data : any) : void => {
                            $data.forEach(($issue : any) : void => {
                                this.source.data.Add($issue, $issue.key);
                            });
                            this.target.connector.Search(jql, ($data : any) : void => {
                                $data.forEach(($issue : any) : void => {
                                    this.target.data.Add($issue, $issue.key);
                                });
                                $done();
                            }, cache.source.length - this.maxUpdates);
                        }, cache.source.length - this.maxUpdates);
                    } else {
                        $done();
                    }
                }, cache.target.length);
            }, cache.source.length);
        }

        private synchronize($done : () => void) : void {
            const transMap : any = {
                DONE       : ["Done", "Release Cancelled", "Release Delivered", "Resolved", "Closed"],
                IN_PROGRESS: ["In Progress", "CCB"],
                TODO       : ["To Do", "Open"]
            };

            const source : any = this.source;
            const target : any = this.target;

            const normalizeText : any = ($issue : any) : void => {
                $issue.fields.description = StringUtils.Remove($issue.fields.description, "\r");
                $issue.fields.description = StringUtils.Remove($issue.fields.description, "\n", "\r\n");
            };
            const normalizeTransitions : any = ($issue : any) : string => {
                let name : string = ObjectValidator.IsString($issue) ? $issue : $issue.fields.status.name;
                if (transMap.TODO.Contains(name)) {
                    name = "TODO";
                } else if (transMap.IN_PROGRESS.Contains(name)) {
                    name = "IN_PROGRESS";
                } else if (transMap.DONE.Contains(name)) {
                    name = "DONE";
                } else {
                    LogIt.Warning("Unknown transition: " + name);
                }
                return name;
            };
            const duplicateError : any = ($key : string, $connector : string) : void => {
                LogIt.Error("Looks like sync issue for " + $key + " at " + $connector + " JIRA. " +
                    "Protecting creation of duplicates. Fix sync manually or add issue key to ignore list.");
            };
            source.data.foreach(($issue : any, $key : string) : void => {
                if (this.ignoreKeys.indexOf($key) === -1) {
                    if (!target.data.KeyExists($key)) {
                        if ($issue.fields.reporter.name === this.builderConfig.jira.source.user) {
                            duplicateError($key, target.connector.getName());
                        }
                        target.missing.push($key);
                        if (normalizeTransitions($issue) !== "TODO") {
                            target.transitions.push($key);
                        }
                    } else {
                        const targetIssue : any = target.data.getItem($key);
                        normalizeText(targetIssue);
                        normalizeText($issue);
                        if (
                            targetIssue.fields.summary !== $issue.fields.summary ||
                            targetIssue.fields.description !== $issue.fields.description
                        ) {
                            if (new Date(targetIssue.fields.updated).getTime() < new Date($issue.fields.updated).getTime()) {
                                target.update.push($key);
                            }
                        }
                        if (normalizeTransitions(targetIssue) !== normalizeTransitions($issue)) {
                            target.transitions.push($key);
                        }
                    }
                } else {
                    LogIt.Debug("IGNORE: [{0}] at {1} JIRA", $key, target.connector.getName());
                }
            });
            target.data.foreach(($issue : any, $key : string) : void => {
                if (this.ignoreKeys.indexOf($key) === -1) {
                    if (!source.data.KeyExists($key)) {
                        if ($issue.fields.reporter.name === this.builderConfig.jira.target.user) {
                            duplicateError($key, source.connector.getName());
                        }
                        source.missing.push($key);
                        if (normalizeTransitions($issue) !== "TODO") {
                            source.transitions.push($key);
                        }
                    } else {
                        const sourceIssue : any = source.data.getItem($key);
                        if (target.update.indexOf($key) === -1) {
                            normalizeText(sourceIssue);
                            normalizeText($issue);
                            if (
                                sourceIssue.fields.summary !== $issue.fields.summary ||
                                sourceIssue.fields.description !== $issue.fields.description
                            ) {
                                source.update.push($key);
                            }
                        }
                        if (target.transitions.indexOf($key) === -1) {
                            if (normalizeTransitions(sourceIssue) !== normalizeTransitions($issue)) {
                                source.transitions.push($key);
                            }
                        }
                    }
                } else {
                    LogIt.Debug("IGNORE: [{0}] at {1} JIRA", $key, source.connector.getName());
                }
            });

            const create : any = ($key : string, $atSource : boolean, $callback : () => void) : void => {
                const data : any = ($atSource ? source : target).data.getItem($key).fields;
                if (data.issuetype.name === "Epic" ||
                    data.issuetype.name === "Requirement" ||
                    data.issuetype.name === "SW Release Request") {
                    LogIt.Debug("Converting issue type {0} to Task for [{1}]", data.issuetype.name, $key);
                    data.issuetype.name = "Task";
                }

                const fields : any = {
                    issuetype: {
                        name: data.issuetype.name
                    },
                    project  : {
                        key: data.project.key
                    }
                };
                if (data.issuetype.name === "Sub-task") {
                    fields.parent = {key: data.parent.key};
                }
                if (!ObjectValidator.IsEmptyOrNull(data.priority)) {
                    fields.priority = {
                        id: data.priority.id
                    };
                }
                if (!ObjectValidator.IsEmptyOrNull(data.summary)) {
                    fields.summary = data.summary;
                }
                if (!ObjectValidator.IsEmptyOrNull(data.description)) {
                    fields.description = data.description;
                }
                if (!ObjectValidator.IsEmptyOrNull(data.assignee.name)) {
                    fields.assignee = {
                        name: data.assignee.name
                    };
                }
                if (!ObjectValidator.IsEmptyOrNull(data.components)) {
                    fields.components = [];
                    data.components.forEach(($component : any) : void => {
                        fields.components.push({
                            name: $component.name
                        });
                    });
                }
                const connector : JIRAConnector = (!$atSource ? source : target).connector;
                if (!this.validate) {
                    LogIt.Info(StringUtils.Format("Creating [{0}] at {1} JIRA\n{2}",
                        $key, connector.getName(), JSON.stringify(fields, null, 2)));
                    connector.CreateIssue(fields, $callback);
                } else {
                    LogIt.Debug("Create attempt [{0}]\nSOURCE:\n{1}\nTARGET:\n{2}", $key,
                        JSON.stringify(source.data.getItem($key), null, 2),
                        JSON.stringify(target.data.getItem($key), null, 2));
                    $callback();
                }
            };
            const getNextCreate : any = ($index : number, $toSource : boolean, $callback : ($updated : boolean) => void) : void => {
                const keys : string[] = ($toSource ? target : source).missing;
                if ($index < keys.length) {
                    create(keys[$index], $toSource, () : void => {
                        getNextCreate($index + 1, $toSource, $callback);
                    });
                } else {
                    LogIt.Info(StringUtils.Format("{0} JIRA {1}",
                        ($toSource ? target : source).connector.getName(),
                        keys.length === 0 ?
                            "is up-to-date: nothing to create" : "has been synced: all missing issues has been created"));
                    $callback(keys.length > 0);
                }
            };

            const update : any = ($key : string, $atSource : boolean, $callback : () => void) : void => {
                const data : any = ($atSource ? source : target).data.getItem($key).fields;
                const connector : JIRAConnector = (!$atSource ? source : target).connector;
                const fields : any = {};
                if (!ObjectValidator.IsEmptyOrNull(data.summary)) {
                    fields.summary = data.summary;
                }
                if (!ObjectValidator.IsEmptyOrNull(data.description)) {
                    fields.description = data.description;
                }
                if (!this.validate) {
                    LogIt.Info(StringUtils.Format("Updating [{0}] at {1} JIRA\n{2}",
                        $key, connector.getName(), JSON.stringify(data, null, 2)));
                    connector.UpdateIssue($key, {fields}, $callback);
                } else {
                    LogIt.Debug("Update attempt [{0}]\nSOURCE:\n{1}\nTARGET:\n{2}", $key,
                        JSON.stringify(source.data.getItem($key), null, 2),
                        JSON.stringify(target.data.getItem($key), null, 2));
                    $callback();
                }
            };
            const getNextUpdate : any = ($index : number, $toSource : boolean, $callback : ($updated : boolean) => void) : void => {
                const keys : string[] = ($toSource ? target : source).update;
                if ($index < keys.length) {
                    update(keys[$index], $toSource, () : void => {
                        getNextUpdate($index + 1, $toSource, $callback);
                    });
                } else {
                    LogIt.Info(StringUtils.Format("{0} JIRA {1}",
                        ($toSource ? target : source).connector.getName(),
                        keys.length === 0 ?
                            "is up-to-date: nothing to update" : "has been synced: all data has been processed"));
                    $callback(keys.length > 0);
                }
            };
            const transitions : any = ($key : string, $atSource : boolean, $callback : () => void) : void => {
                const data : any = ($atSource ? source : target).data.getItem($key);
                const connector : JIRAConnector = (!$atSource ? source : target).connector;

                connector.getTransitions($key, ($data : any[]) : void => {
                    const required : string = normalizeTransitions(data);
                    let id : string = "";
                    const possible : any[] = [];
                    $data.forEach(($transition : any) : void => {
                        if (normalizeTransitions($transition.name) === required) {
                            possible.push($transition);
                        }
                    });
                    if (possible.length === 1) {
                        id = possible[0].id;
                    } else {
                        possible.forEach(($transition : any) : void => {
                            if (ObjectValidator.IsEmptyOrNull(id)) {
                                if ($transition.text === "Open" ||
                                    $transition.text === "Resolved" ||
                                    $transition.text === "Done" ||
                                    $transition.text === "In Progress") {
                                    id = $transition.id;
                                }
                            }
                        });
                    }
                    if (ObjectValidator.IsEmptyOrNull(id)) {
                        LogIt.Warning("Unknown transition: " + required + " for: " + $key + " at: " + connector.getName());
                        LogIt.Debug($data);
                        $callback();
                    } else {
                        LogIt.Info(StringUtils.Format("Transition {0}[{1}] for {2} at {3} JIRA",
                            required, id, $key, connector.getName()));
                        if (!this.validate) {
                            connector.DoIssueTransition($key, id, ($data : any) : void => {
                                if (!ObjectValidator.IsEmptyOrNull($data)) {
                                    LogIt.Warning($data);
                                }
                                $callback();
                            });
                        } else {
                            $callback();
                        }
                    }
                });
            };
            const getNextTransitions : any = ($index : number, $toSource : boolean, $callback : ($updated : boolean) => void) : void => {
                const keys : string[] = ($toSource ? target : source).transitions;
                if ($index < keys.length) {
                    transitions(keys[$index], $toSource, () : void => {
                        getNextTransitions($index + 1, $toSource, $callback);
                    });
                } else {
                    LogIt.Info(StringUtils.Format("{0} JIRA {1}",
                        ($toSource ? target : source).connector.getName(),
                        keys.length === 0 ?
                            "is up-to-date: nothing to update" : "has been synced: all data has been processed"));
                    $callback(keys.length > 0);
                }
            };

            const updates : boolean[] = [];
            LogIt.Info("> Process issues create");
            getNextCreate(0, true, ($updated : boolean) : void => {
                updates.push($updated);
                getNextCreate(0, false, ($updated : boolean) : void => {
                    updates.push($updated);
                    LogIt.Info("> Process issues update");
                    getNextUpdate(0, true, ($updated : boolean) : void => {
                        updates.push($updated);
                        getNextUpdate(0, false, ($updated : boolean) : void => {
                            updates.push($updated);
                            LogIt.Info("> Process transitions update");
                            getNextTransitions(0, true, ($updated : boolean) : void => {
                                updates.push($updated);
                                getNextTransitions(0, false, ($updated : boolean) : void => {
                                    updates.push($updated);
                                    let syncCache : boolean = false;
                                    updates.forEach(($status : boolean) : void => {
                                        if ($status) {
                                            syncCache = true;
                                        }
                                    });
                                    if (syncCache && !this.validate) {
                                        this.getCache(true, $done);
                                    } else {
                                        $done();
                                    }
                                });
                            });
                        });
                    });
                });
            });
        }
    }
}
