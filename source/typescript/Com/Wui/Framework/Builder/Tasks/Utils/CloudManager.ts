/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import OwnCloudClient = Com.Wui.Framework.Builder.WebServiceApi.Clients.OwnCloudClient;
    import OwnCloudFile = Com.Wui.Framework.Builder.Primitives.OwnCloudFile;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;

    export class CloudManager extends BaseTask {
        private static contentNormalizationEnabled : boolean = false;
        private static isCppProfile : boolean = false;

        public static getModifiedFiles($callback? : (($files : string[]) => void) | IResponse) : void {
            const fs : Types.NodeJS.fs = require("fs");
            const projectBase : string = Loader.getInstance().getAppProperties().projectBase;
            const cloudFolder : string = "/" + Loader.getInstance().getProjectConfig().name;
            const localDatabasePath : string = projectBase + "/package-map.json";
            let database : ICloudDatabaseItem[] = <any>{};
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (fileSystem.Exists(localDatabasePath)) {
                database = JSON.parse(fileSystem.Read(localDatabasePath).toString());
            }
            const files : string[] = fileSystem.Expand(projectBase + "/**/*");
            const output : string[] = [];
            const lookupFiles : any = ($index : number) : void => {
                if ($index < files.length) {
                    const localFile : string = projectBase + "/" + files[$index];
                    const cloudFile : string = cloudFolder + "/" + files[$index];
                    if (fileSystem.IsFile(localFile) && !CloudManager.isIgnored("/" + files[$index], true)) {
                        if (database.hasOwnProperty(cloudFile)) {
                            const stats : any = fs.lstatSync(localFile);
                            if (CloudManager.getSize(database[cloudFile]) !== CloudManager.getSize(stats)) {
                                CloudManager.getHash(localFile, ($hash : string) : void => {
                                    if (database[cloudFile].hash !== $hash) {
                                        output.push(localFile);
                                    }
                                    lookupFiles($index + 1);
                                });
                            } else {
                                lookupFiles($index + 1);
                            }
                        } else {
                            output.push(localFile);
                            lookupFiles($index + 1);
                        }
                    } else {
                        lookupFiles($index + 1);
                    }
                } else {
                    ResponseFactory.getResponse($callback).Send(output);
                }
            };
            lookupFiles(0);
        }

        private static getExclude($push : boolean) : string[] {
            const exclude : string[] = [
                "/.idea",
                "/.git",
                "/__cleanup",
                "/build_cache",
                "/cmake-build",
                "/log",
                "/package-map.json"
            ];
            const pushExclude : string[] = [].concat(exclude);
            const pullExclude : string[] = [].concat(exclude);

            if (Loader.getInstance().getProgramArgs().IsAgentTask()) {
                pullExclude.push("/bin");
                pullExclude.push("/build");
                pushExclude.push("/build/compiled");
                pushExclude.push("/" + Loader.getInstance().getAppProperties().packageName + ".zip");
                if (CloudManager.isCppProfile) {
                    pushExclude.push("/bin");
                    pushExclude.push("/build");
                }
            } else {
                const target : IProject = Loader.getInstance().getProjectConfig();
                const packageName : string = target.name + "-" + StringUtils.Replace(target.version, ".", "-") + ".zip";
                pullExclude.push("/" + packageName);
                pushExclude.push("/build");
            }

            CloudManager.getExclude = ($push : boolean) : string[] => {
                return $push ? pushExclude : pullExclude;
            };
            return $push ? pushExclude : pullExclude;
        }

        private static isIgnored($filepath : string, $push : boolean) : boolean {
            let ignore : boolean = false;
            CloudManager.getExclude($push).some(($pattern : string) : boolean => {
                if (StringUtils.Contains($filepath, $pattern)) {
                    ignore = true;
                }
                return ignore;
            });
            return ignore;
        }

        private static getHash($path : string, $callback : ($value : string) => void) : void {
            if (Loader.getInstance().getFileSystemHandler().IsFile($path)) {
                const fs : Types.NodeJS.fs = require("fs");
                const crypto : any = require("crypto");

                const getfileSha1 : any = ($path : string) : Promise<string> => {
                    return new Promise(($resolve, $reject) : void => {
                        const hash : any = crypto.createHash("sha1");
                        const stream : any = fs.createReadStream($path);
                        stream.on("error", $reject);
                        stream.on("data", ($chunk : string) : void => {
                            hash.update($chunk);
                        });
                        stream.on("end", () : void => {
                            $resolve(hash.digest("hex"));
                        });
                    });
                };
                getfileSha1($path).then(($hash : string) : void => {
                    $callback($hash);
                }).catch(($ex : Error) : void => {
                    LogIt.Error("Unable to calculate hash for: " + $path, $ex);
                });
            } else {
                $callback(null);
            }
        }

        private static getSize($value : any) : number {
            return ObjectValidator.IsEmptyOrNull($value.size) || isNaN($value.size) ? 0 : $value.size;
        }

        protected getName() : string {
            return "cloud-manager";
        }

        protected process($done : any, $option : string) : void {
            const fs : Types.NodeJS.fs = require("fs");
            const istextorbinary : any = require("istextorbinary");
            const os : Types.NodeJS.os = require("os");
            const client : OwnCloudClient = new OwnCloudClient(Loader.getInstance().getAppConfiguration().ownCloud);

            CloudManager.isCppProfile = this.properties.projectHas.Cpp.Source();

            client.IsAuthorized(($status : boolean) : void => {
                if ($status) {
                    let cloudFolder : string = "/" + this.project.name;
                    const localDatabasePath : string = this.properties.projectBase + "/package-map.json";
                    let database : ICloudDatabaseItem[] = <any>{};

                    const addDatabaseRecord : any = ($data : OwnCloudFile, $path : string, $callback : () => void) : void => {
                        const stats : any = fs.lstatSync($path);
                        database[$data.getPath()] = <ICloudDatabaseItem>{
                            etag: $data.getETag(),
                            hash: null,
                            size: CloudManager.getSize(stats)
                        };
                        CloudManager.getHash($path, ($hash : string) : void => {
                            database[$data.getPath()].hash = $hash;
                            $callback();
                        });
                    };
                    const readDatabase : any = ($path? : string) : void => {
                        if (ObjectValidator.IsEmptyOrNull($path)) {
                            $path = localDatabasePath;
                        }
                        if (this.fileSystem.Exists($path)) {
                            database = JSON.parse(this.fileSystem.Read($path).toString());
                        } else {
                            LogIt.Debug("Database read skipped: database does not exist at {0}", $path);
                        }
                    };
                    const saveDatabase : any = ($path? : string) : void => {
                        if (ObjectValidator.IsEmptyOrNull($path)) {
                            $path = localDatabasePath;
                        }
                        this.fileSystem.Write($path, JSON.stringify(database, null, 2));
                    };

                    if ($option === "push" || $option === "push-force") {
                        const files : string[] = this.fileSystem.Expand(this.properties.projectBase + "/**/*");
                        const remoteDatabase : ICloudDatabaseItem[] = <any>{};
                        let updateDatabase : boolean = false;
                        const addNextFile : any = ($index : number) : void => {
                            if ($index < files.length) {
                                files[$index] = StringUtils.Remove(files[$index], this.properties.projectBase + "/");
                                const file : string = this.properties.projectBase + "/" + files[$index];
                                const cloudFile : string = cloudFolder + "/" + files[$index];
                                if (this.fileSystem.IsFile(file) && !CloudManager.isIgnored("/" + files[$index], true)) {
                                    if (remoteDatabase.hasOwnProperty(cloudFile)) {
                                        if (database.hasOwnProperty(cloudFile)) {
                                            const stats : any = fs.lstatSync(file);
                                            if (remoteDatabase[cloudFile].etag !== database[cloudFile].etag ||
                                                CloudManager.getSize(database[cloudFile]) !== CloudManager.getSize(stats)) {
                                                CloudManager.getHash(file, ($hash : string) : void => {
                                                    if (database[cloudFile].hash !== $hash) {
                                                        updateDatabase = true;
                                                        LogIt.Info("Update file: " + cloudFile +
                                                            " (" + Convert.IntegerToSize(stats.size) + ")");
                                                        client.PutFile(cloudFile, file, ($status : boolean) : void => {
                                                            if ($status) {
                                                                addNextFile($index + 1);
                                                            } else {
                                                                LogIt.Error("Unable to push file: " + file);
                                                            }
                                                        });
                                                    } else {
                                                        addNextFile($index + 1);
                                                    }
                                                });
                                            } else {
                                                addNextFile($index + 1);
                                            }
                                        } else {
                                            addNextFile($index + 1);
                                        }
                                    } else {
                                        updateDatabase = true;
                                        LogIt.Info("Upload file: " + cloudFile +
                                            " (" + Convert.IntegerToSize(fs.lstatSync(file).size) + ")");
                                        client.PutFile(cloudFile, file, ($status : boolean) : void => {
                                            if ($status) {
                                                addNextFile($index + 1);
                                            } else {
                                                LogIt.Error("Unable to push file: " + file);
                                            }
                                        });
                                    }
                                } else if (this.fileSystem.IsDirectory(file) && !CloudManager.isIgnored("/" + files[$index], true) &&
                                    !remoteDatabase.hasOwnProperty(cloudFile + "/")) {
                                    updateDatabase = true;
                                    LogIt.Info("Create folder: " + cloudFile);
                                    client.CreateFolder(cloudFile, ($status : boolean) : void => {
                                        if ($status) {
                                            addNextFile($index + 1);
                                        } else {
                                            LogIt.Error("Unable to push folder: " + file);
                                        }
                                    });
                                } else {
                                    addNextFile($index + 1);
                                }
                            } else {
                                const syncDatabase : any = () : void => {
                                    LogIt.Info("Fetching synchronization table.");
                                    client.List(cloudFolder + "/", "infinity", ($items : OwnCloudFile[]) : void => {
                                        database = <any>{};
                                        $items.forEach(($info : OwnCloudFile) : void => {
                                            database[$info.getPath()] = <ICloudDatabaseItem>{
                                                etag: $info.getETag(),
                                                hash: null,
                                                size: $info.getSize()
                                            };
                                        });
                                        const registerNextFile : any = ($index : number) : void => {
                                            if ($index < files.length) {
                                                let $file : string = files[$index];
                                                if (this.fileSystem.IsDirectory(cloudFolder + "/" + $file)) {
                                                    $file += "/";
                                                }
                                                if (!CloudManager.isIgnored("/" + $file, true) &&
                                                    database.hasOwnProperty(cloudFolder + "/" + $file)) {
                                                    const stats : any = fs.lstatSync(this.properties.projectBase + "/" + $file);
                                                    database[cloudFolder + "/" + $file].size = CloudManager.getSize(stats);
                                                    CloudManager.getHash(this.properties.projectBase + "/" + $file,
                                                        ($hash : string) : void => {
                                                            database[cloudFolder + "/" + $file].hash = $hash;
                                                            registerNextFile($index + 1);
                                                        });
                                                } else {
                                                    registerNextFile($index + 1);
                                                }
                                            } else {
                                                saveDatabase();
                                                LogIt.Info(">>"[ColorType.YELLOW] + " Push successfully finished.");
                                                $done();
                                            }
                                        };
                                        registerNextFile(0);
                                    });
                                };
                                let localItem : string;
                                for (localItem in database) {
                                    if (database.hasOwnProperty(localItem) && !this.fileSystem.Exists(
                                        this.properties.projectBase + "/" + StringUtils.Remove(localItem, this.project.name + "/"))) {
                                        delete database[localItem];
                                    }
                                }
                                const removeItems : string[] = [];
                                let remoteItem : string;
                                for (remoteItem in remoteDatabase) {
                                    if (!CloudManager.isIgnored(remoteItem, true) &&
                                        remoteDatabase.hasOwnProperty(remoteItem) && !database.hasOwnProperty(remoteItem) &&
                                        removeItems.indexOf(remoteItem) === -1) {
                                        let exists : boolean = false;
                                        removeItems.forEach(($item : string) : void => {
                                            if (StringUtils.StartsWith(remoteItem, $item)) {
                                                exists = true;
                                            }
                                        });
                                        if (!exists) {
                                            removeItems.push(remoteItem);
                                        }
                                    }
                                }
                                if (removeItems.length !== 0) {
                                    const removeNextItem : any = ($index : number) : void => {
                                        if ($index < removeItems.length) {
                                            const item : string = removeItems[$index];
                                            LogIt.Debug("Remove: " + item);
                                            client.Delete(item, () : void => {
                                                removeNextItem($index + 1);
                                            });
                                        } else {
                                            syncDatabase();
                                        }
                                    };
                                    removeNextItem(0);
                                } else if (updateDatabase) {
                                    syncDatabase();
                                } else {
                                    LogIt.Info(">>"[ColorType.YELLOW] + " Cloud is up to data.");
                                    $done();
                                }
                            }
                        };
                        const syncFolder : any = () : void => {
                            client.CreateFolder(cloudFolder, ($status : boolean) : void => {
                                if ($status) {
                                    database = <any>{};
                                    addNextFile(0);
                                } else {
                                    LogIt.Error("Unable to create sync folder: " + cloudFolder);
                                }
                            });
                        };
                        client.Exists(cloudFolder, ($status : boolean) : void => {
                            if ($status) {
                                if ($option === "push-force" || this.programArgs.IsForce()) {
                                    client.Delete(cloudFolder, ($status : boolean) : void => {
                                        if ($status) {
                                            syncFolder();
                                        } else {
                                            LogIt.Error("Unable to clean up sync folder: " + cloudFolder);
                                        }
                                    });
                                } else {
                                    LogIt.Info("Fetching synchronization table.");
                                    readDatabase();
                                    client.List(cloudFolder + "/", "infinity", ($items : OwnCloudFile[]) : void => {
                                        if ($status) {
                                            $items.forEach(($info : OwnCloudFile) : void => {
                                                remoteDatabase[$info.getPath()] = <ICloudDatabaseItem>{
                                                    etag: $info.getETag(),
                                                    hash: null,
                                                    size: $info.getSize()
                                                };
                                            });
                                            addNextFile(0);
                                        } else {
                                            LogIt.Error("Unable to fetch database records.");
                                        }
                                    });
                                }
                            } else {
                                syncFolder();
                            }
                        });
                    } else if ($option === "pull" || $option === "pull-force") {
                        let projectName : string = this.project.name;
                        if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().project)) {
                            projectName = this.programArgs.getOptions().project;
                        }
                        let syncFolder : string = this.properties.projectBase;
                        if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().to)) {
                            syncFolder = this.programArgs.getOptions().to;
                            syncFolder = StringUtils.Replace(syncFolder, "\\", "/");
                            syncFolder = StringUtils.Replace(syncFolder, "//", "/");
                            cloudFolder = "/" + projectName;
                            if (!StringUtils.EndsWith(syncFolder, cloudFolder)) {
                                syncFolder += cloudFolder;
                            }
                            if ($option === "pull-force") {
                                this.fileSystem.Delete(syncFolder);
                            }
                        } else if ($option === "pull-force") {
                            LogIt.Warning("Force pull is support only with specified option --to.");
                        }
                        const syncDatabasePath : string = syncFolder + "/package-map.json";
                        readDatabase(syncDatabasePath);
                        const createFile : any = ($file : OwnCloudFile, $path : string, $callback : () => void) : void => {
                            $path = syncFolder + StringUtils.Remove($path, projectName + "/");
                            LogIt.Info((!this.fileSystem.Exists($path) ? "Download" : "Update") + " file: " + $path +
                                " (" + Convert.IntegerToSize($file.getSize()) + ")");
                            client.getFile($file.getPath(), $path, ($status : boolean) : void => {
                                if ($status) {
                                    let maxIterations : number = 5;
                                    const check : any = ($callback : ($status : boolean) => void) : void => {
                                        if (fs.existsSync($path)) {
                                            $callback(true);
                                        } else {
                                            if (maxIterations-- > 0) {
                                                setTimeout(() : void => {
                                                    LogIt.Debug("Pending delayed checks ({0})", maxIterations);
                                                    check($callback);
                                                }, 100);
                                            } else {
                                                $callback(false);
                                            }
                                        }
                                    };

                                    check(($status : boolean) : void => {
                                        if ($status) {
                                            if (CloudManager.contentNormalizationEnabled &&
                                                istextorbinary.isTextSync($path, fs.createReadStream($path))) {
                                                fs.writeFileSync($path,
                                                    fs.readFileSync($path).toString("utf8").replace(/(?:\r\n?|\n)/g, os.EOL));
                                            }
                                            addDatabaseRecord($file, $path, $callback);
                                        } else {
                                            LogIt.Error("Something wrong happen in file download.");
                                        }
                                    });
                                } else {
                                    LogIt.Error("Unable to pull file: " + $file.getPath());
                                }
                            });
                        };
                        const createDir : any = ($file : OwnCloudFile, $path : string, $callback : () => void) : void => {
                            try {
                                const targetPath = syncFolder + StringUtils.Remove($path, projectName + "/");
                                if (!this.fileSystem.Exists(targetPath)) {
                                    LogIt.Info("Create folder: " + targetPath);
                                    this.fileSystem.CreateDirectory(targetPath);
                                }
                                addDatabaseRecord($file, targetPath, $callback);
                            } catch (ex) {
                                LogIt.Error("Unable to create folder: " + $path, ex);
                            }
                        };
                        const getDirectory : any = ($path : string, $callback : () => void) : void => {
                            client.List($path, "1", ($files : OwnCloudFile[]) : void => {
                                const getNextFile : any = ($index : number) : void => {
                                    if ($index < $files.length) {
                                        const file : OwnCloudFile = $files[$index];
                                        const path : string = file.getPath();
                                        if (path !== $path && !CloudManager.isIgnored(path, false)) {
                                            if (!file.IsDir()) {
                                                if (database.hasOwnProperty(path)) {
                                                    if (database[path].etag !== file.getETag()) {
                                                        createFile(file, path, () : void => {
                                                            getNextFile($index + 1);
                                                        });
                                                    } else {
                                                        getNextFile($index + 1);
                                                    }
                                                } else {
                                                    createFile(file, path, () : void => {
                                                        getNextFile($index + 1);
                                                    });
                                                }
                                            } else {
                                                if (database.hasOwnProperty(path)) {
                                                    if (database[path].etag !== file.getETag()) {
                                                        createDir(file, path, () : void => {
                                                            getDirectory(path, () : void => {
                                                                getNextFile($index + 1);
                                                            });
                                                        });
                                                    } else {
                                                        getNextFile($index + 1);
                                                    }
                                                } else {
                                                    createDir(file, path, () : void => {
                                                        getDirectory(path, () : void => {
                                                            getNextFile($index + 1);
                                                        });
                                                    });
                                                }
                                            }
                                        } else {
                                            getNextFile($index + 1);
                                        }
                                    } else {
                                        $callback();
                                    }
                                };
                                getNextFile(0);
                            });
                        };
                        getDirectory(cloudFolder, () : void => {
                            LogIt.Info("Fetching synchronization table.");
                            client.List(cloudFolder + "/", "infinity", ($items : OwnCloudFile[]) : void => {
                                try {
                                    const remoteDatabase : ICloudDatabaseItem[] = <any>{};
                                    $items.forEach(($info : OwnCloudFile) : void => {
                                        remoteDatabase[$info.getPath()] = <ICloudDatabaseItem>{
                                            etag: $info.getETag(),
                                            hash: null,
                                            size: $info.getSize()
                                        };
                                    });
                                    const removeItems : string[] = [];
                                    let localItem : string;
                                    for (localItem in database) {
                                        if (!CloudManager.isIgnored(localItem, false) &&
                                            database.hasOwnProperty(localItem) && !remoteDatabase.hasOwnProperty(localItem)) {
                                            delete database[localItem];
                                            if (StringUtils.EndsWith(localItem, "/")) {
                                                localItem = StringUtils.Substring(localItem, 0, StringUtils.Length(localItem) - 1);
                                            }
                                            localItem = StringUtils.Remove(localItem, cloudFolder + "/");
                                            if (removeItems.indexOf(localItem) === -1) {
                                                removeItems.push(localItem);
                                            }
                                        }
                                    }
                                    const files : string[] = this.fileSystem.Expand(syncFolder + "/**/*");
                                    files.forEach(($file : string) : void => {
                                        $file = StringUtils.Remove($file, syncFolder + "/");
                                        let item : string = $file;
                                        if (this.fileSystem.IsDirectory(syncFolder + "/" + $file)) {
                                            item += "/";
                                        }
                                        if (!CloudManager.isIgnored("/" + $file, false) &&
                                            !database.hasOwnProperty(cloudFolder + "/" + item) &&
                                            removeItems.indexOf($file) === -1) {
                                            removeItems.push($file);
                                        }
                                    });
                                    if (removeItems.length !== 0) {
                                        removeItems.forEach(($item : string) : void => {
                                            this.fileSystem.Delete(syncFolder + "/" + $item);
                                        });
                                        LogIt.Info(">>"[ColorType.YELLOW] + " Pull successfully finished.");
                                    } else {
                                        LogIt.Info(">>"[ColorType.YELLOW] + " Project is up-to-date.");
                                    }

                                    const syncDatabaseRecords : any = ($index : number, $callback : () => void) : void => {
                                        if ($index < $items.length) {
                                            const path : string = $items[$index].getPath();
                                            const localPath : string = syncFolder + StringUtils.Remove(path, projectName + "/");
                                            if (ObjectValidator.IsEmptyOrNull(database[path])) {
                                                database[path] = {};
                                            }
                                            const localItem : ICloudDatabaseItem = database[path];
                                            const remoteItem : ICloudDatabaseItem = remoteDatabase[path];
                                            let stats : any = {
                                                size: 0
                                            };
                                            if (!$items[$index].IsDir() && fs.existsSync(localPath)) {
                                                stats = fs.lstatSync(localPath);
                                            }

                                            localItem.etag = remoteItem.etag;
                                            if (ObjectValidator.IsEmptyOrNull(localItem.size) || localItem.size !== remoteItem.size) {
                                                if (!ObjectValidator.IsEmptyOrNull(remoteItem.size)) {
                                                    localItem.size = remoteItem.size;
                                                } else {
                                                    localItem.size = CloudManager.getSize(stats);
                                                }
                                            }
                                            if (ObjectValidator.IsEmptyOrNull(localItem.hash)) {
                                                CloudManager.getHash(localPath, ($hash : string) : void => {
                                                    localItem.hash = $hash;
                                                    syncDatabaseRecords($index + 1, $callback);
                                                });
                                            } else {
                                                syncDatabaseRecords($index + 1, $callback);
                                            }
                                        } else {
                                            $callback();
                                        }
                                    };
                                    syncDatabaseRecords(0, () : void => {
                                        saveDatabase(syncDatabasePath);
                                        $done();
                                    });
                                } catch (ex) {
                                    LogIt.Error(this.getClassName(), ex);
                                }
                            });
                        });
                    } else {
                        LogIt.Error("Unsupported task option \"" + $option + "\".");
                    }
                } else {
                    LogIt.Error("Unable to login to the WUI Builder cloud");
                }
            });
        }
    }

    export class ICloudDatabaseItem {
        public etag : string;
        public size : number;
        public hash : string;
    }
}
