/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IDockerRegistryConfig = Com.Wui.Framework.Builder.Interfaces.IDockerRegistryConfig;
    import Resources = Com.Wui.Framework.Builder.DAO.Resources;

    export class Docker extends BaseTask {
        private configsCache : any;
        private dockerContextPath : string;

        public BuildImage($options? : BuildOptions) : IDockerProgressPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                onMessage($message : string) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            let options : BuildOptions = {
                advanced: {},
                name    : "",
                useEnv  : true
            };
            if (ObjectValidator.IsEmptyOrNull($options)) {
                options = Resources.Extend(options, this.getDockerOptions().build);
                if (!ObjectValidator.IsEmptyOrNull(options.name) && !StringUtils.Contains(options.name, ":")) {
                    options.name = options.name + ":" + this.getDockerOptions().version;
                }
            } else {
                options = Resources.Extend(options, $options);
            }

            const buildArgs : any = {};
            if (options.useEnv && this.fileSystem.Exists(this.dockerContextPath + "/.env")) {
                const envString : string = StringUtils.Remove(<string>this.fileSystem.Read(this.dockerContextPath + "/.env"), "\r");
                if (!ObjectValidator.IsEmptyOrNull(envString)) {
                    StringUtils.Split(envString, "\n").forEach(($line : string) : void => {
                        if (!StringUtils.StartsWith($line, "#")) {
                            const result : any = /([a-zA-Z0-9_]+)=(.*)/.exec($line);
                            if (!ObjectValidator.IsEmptyOrNull(result)) {
                                if (!buildArgs.hasOwnProperty(result[1])) {
                                    buildArgs[result[1]] = result[2];
                                }
                            }
                        } else {
                            LogIt.Warning("Commented environment variable in \"" + this.dockerContextPath + "/.env file is skipped.");
                        }
                    });
                }
            }

            const buildOptions : any = options.advanced;
            if (!ObjectValidator.IsEmptyOrNull(options.name)) {
                buildOptions.t = options.name;
            }
            if (!ObjectValidator.IsEmptyOrNull(buildArgs)) {
                buildOptions.buildargs = buildArgs;
            }
            if (this.programArgs.IsForce()) {
                buildOptions.nocache = true;
            }

            docker.buildImage({context: this.dockerContextPath}, buildOptions)
                .then(($response : any) : void => {
                    const parser : any = require("JSONStream").parse();
                    $response.pipe(parser);
                    parser.on("data", ($data : any) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($data.stream)) {
                            callbacks.onMessage($data.stream);
                        }
                    });
                    parser.on("end", () : void => {
                        callbacks.then("Image build.");
                    });
                })
                .catch(($error : any) : void => {
                    callbacks.onError($error);
                });
            return {
                OnMessage: ($callback : ($message : string) => void) : IDockerPromise => {
                    callbacks.onMessage = $callback;
                    return {
                        OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                            callbacks.onError = $callback;
                            return {
                                Then: ($callback : ($response : any) => void) : void => {
                                    callbacks.then = $callback;
                                }
                            };
                        }
                    };
                }
            };
        }

        public PullImage($image : string, $authOptions : IDockerRegistryConfig) : IDockerProgressPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                onMessage($message : string) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            const authconfig : any = {
                email        : $authOptions.email,
                password     : $authOptions.pass,
                serveraddress: $authOptions.url,
                username     : $authOptions.user
            };
            docker
                .pull($image, {authconfig})
                .then(($response : any) : void => {
                    const parser : any = require("JSONStream").parse();
                    $response.pipe(parser);
                    parser.on("data", ($data : any) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($data.status)) {
                            if (!ObjectValidator.IsEmptyOrNull($data.progress)) {
                                callbacks.onMessage($data.status + " [" + $data.id + "]... " + $data.progress);
                            } else if (!ObjectValidator.IsEmptyOrNull($data.id)) {
                                callbacks.onMessage($data.status + " [" + $data.id + "]");
                            } else {
                                callbacks.onMessage($data.status);
                            }
                        }
                    });
                    parser.on("end", () : void => {
                        callbacks.then("Image downloaded.");
                    });
                })
                .catch(($error : any) : void => {
                    callbacks.onError($error);
                });

            return {
                OnMessage: ($callback : ($message : string) => void) : IDockerPromise => {
                    callbacks.onMessage = $callback;
                    return {
                        OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                            callbacks.onError = $callback;
                            return {
                                Then: ($callback : ($response : any) => void) : void => {
                                    callbacks.then = $callback;
                                }
                            };
                        }
                    };
                }
            };
        }

        public PushImage($image : string, $tag : string, $authOptions : IDockerRegistryConfig) : IDockerProgressPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                onMessage($message : string) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            const authconfig : any = {
                email        : $authOptions.email,
                password     : $authOptions.pass,
                serveraddress: $authOptions.url,
                username     : $authOptions.user
            };
            docker.getImage($image)
                .push({name: $tag, authconfig})
                .then(($response : any) : void => {
                    const parser : any = require("JSONStream").parse();
                    $response.pipe(parser);
                    parser.on("data", ($data : any) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($data.status)) {
                            if (!ObjectValidator.IsEmptyOrNull($data.progress)) {
                                callbacks.onMessage($data.status + " [" + $data.id + "]... " + $data.progress);
                            } else if (!ObjectValidator.IsEmptyOrNull($data.id)) {
                                callbacks.onMessage($data.status + " [" + $data.id + "]");
                            } else {
                                callbacks.onMessage($data.status);
                            }
                        }
                    });
                    parser.on("end", () : void => {
                        callbacks.then("Image downloaded.");
                    });
                })
                .catch(($error : any) : void => {
                    callbacks.onError($error);
                });

            return {
                OnMessage: ($callback : ($message : string) => void) : IDockerPromise => {
                    callbacks.onMessage = $callback;
                    return {
                        OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                            callbacks.onError = $callback;
                            return {
                                Then: ($callback : ($response : any) => void) : void => {
                                    callbacks.then = $callback;
                                }
                            };
                        }
                    };
                }
            };
        }

        public TagImage($image : string, $tag : string) : IDockerPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            const repo : string = StringUtils.Substring($tag, 0, StringUtils.IndexOf($tag, ":"));
            const tag : string = StringUtils.Remove($tag, repo, ":");
            docker.getImage($image)
                .tag({repo, tag})
                .then(($data : any) : void => {
                    callbacks.then("Image \"" + $data.name + "\" tagged with \"" + repo + ":" + tag + "\" successfully.");
                })
                .catch(($error : any) : void => {
                    callbacks.onError($error);
                });

            return {
                OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                    callbacks.onError = $callback;
                    return {
                        Then: ($callback : ($response : any) => void) : void => {
                            callbacks.then = $callback;
                        }
                    };

                }
            };
        }

        public RemoveImage($image : string) : IDockerPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            const image : any = docker.getImage($image);
            image.remove({})
                .then(() : void => {
                    callbacks.then("Image \"" + $image + "\" removed.");
                })
                .catch(($error : any) : void => {
                    callbacks.onError($error);
                });

            return {
                OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                    callbacks.onError = $callback;
                    return {
                        Then: ($callback : ($response : any) => void) : void => {
                            callbacks.then = $callback;
                        }
                    };

                }
            };
        }

        public CreateContainer($image : string, $options? : ContainerOptions) : IDockerPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            let options : ContainerOptions = {
                advanced: {},
                name    : ""
            };
            if (ObjectValidator.IsEmptyOrNull($options)) {
                options = this.getDockerOptions().container;
            } else {
                options = <ContainerOptions>$options;
            }

            let createOptions : any = {
                AttachStderr: true,
                AttachStdin : false,
                AttachStdout: true,
                OpenStdin   : false,
                StdinOnce   : false,
                Tty         : true
            };

            if (!ObjectValidator.IsEmptyOrNull(options.advanced)) {
                createOptions = options.advanced;
            }

            if (!ObjectValidator.IsEmptyOrNull(options.name)) {
                createOptions.name = options.name;
            }
            if (!ObjectValidator.IsEmptyOrNull($image)) {
                createOptions.Image = $image;
            }

            if (!ObjectValidator.IsEmptyOrNull(options.cmd)) {
                createOptions.Cmd = options.cmd;
            }
            if (!ObjectValidator.IsEmptyOrNull(options.bindPorts)) {
                options.bindPorts.forEach(($value : BindPortOptions) : void => {
                    const key : string = $value.containerPort + "/" +
                        (ObjectValidator.IsEmptyOrNull($value.protocol) ? "tcp" : $value.protocol);

                    if (ObjectValidator.IsEmptyOrNull(createOptions.ExposedPorts)) {
                        createOptions.ExposedPorts = {};
                    }
                    if (ObjectValidator.IsEmptyOrNull(createOptions.HostConfig)) {
                        createOptions.HostConfig = {};
                        createOptions.HostConfig.PortBindings = {};
                    }
                    createOptions.ExposedPorts[key] = {};
                    createOptions.HostConfig.PortBindings[key] = [
                        {
                            HostIp  : $value.hostIp,
                            HostPort: $value.hostPort.toString()
                        }
                    ];
                });
            }

            docker.createContainer(createOptions)
                .then(($container : any) : void => {
                    callbacks.then($container);
                })
                .catch(($error : any) : void => {
                    callbacks.onError($error);
                });

            return {
                OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                    callbacks.onError = $callback;
                    return {
                        Then: ($callback : ($response : any) => void) : void => {
                            callbacks.then = $callback;
                        }
                    };

                }
            };
        }

        public StartContainer($nameOrId : string) : IDockerPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            const container : any = docker.getContainer($nameOrId);
            container.start()
                .then(() : void => {
                    callbacks.then($nameOrId);
                })
                .catch(($error : any) : void => {
                    if ($error.statusCode === 304) {
                        callbacks.then($nameOrId);
                    } else {
                        callbacks.onError($nameOrId);
                    }
                });

            return {
                OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                    callbacks.onError = $callback;
                    return {
                        Then: ($callback : ($response : any) => void) : void => {
                            callbacks.then = $callback;
                        }
                    };

                }
            };
        }

        public StopContainer($nameOrId : string) : IDockerPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            const container : any = docker.getContainer($nameOrId);
            container.stop()
                .then(() : void => {
                    callbacks.then($nameOrId);
                })
                .catch(($error : any) : void => {
                    if ($error.statusCode === 304) {
                        callbacks.then($nameOrId);
                    } else {
                        callbacks.onError($nameOrId);
                    }
                });

            return {
                OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                    callbacks.onError = $callback;
                    return {
                        Then: ($callback : ($response : any) => void) : void => {
                            callbacks.then = $callback;
                        }
                    };

                }
            };
        }

        public RemoveContainer($nameOrId : string) : IDockerPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            const container : any = docker.getContainer($nameOrId);
            container.remove()
                .then(() : void => {
                    callbacks.then($nameOrId);
                })
                .catch(($error : any) : void => {
                    if ($error.statusCode === 304) {
                        callbacks.then($nameOrId);
                    } else {
                        callbacks.onError($nameOrId);
                    }
                });

            return {
                OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                    callbacks.onError = $callback;
                    return {
                        Then: ($callback : ($response : any) => void) : void => {
                            callbacks.then = $callback;
                        }
                    };

                }
            };
        }

        public CheckAuth($authOptions : IDockerRegistryConfig) : IDockerPromise {
            const dockerode : any = require("dockerode");
            const docker : any = new dockerode();

            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            docker
                .checkAuth({
                    email        : $authOptions.email,
                    password     : $authOptions.pass,
                    serveraddress: $authOptions.url,
                    username     : $authOptions.user
                })
                .then(($response : any) : void => {
                    callbacks.then($response);
                })
                .catch(($error : any) : void => {
                    callbacks.onError($error);
                });

            return {
                OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                    callbacks.onError = $callback;
                    return {
                        Then: ($callback : ($response : any) => void) : void => {
                            callbacks.then = $callback;
                        }
                    };

                }
            };
        }

        public ComposeUp() : IDockerPromise {
            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            this.terminal.Spawn("docker-compose", ["up", "-d"],
                {
                    advanced: {
                        noTerminalLog: true
                    },
                    cwd     : this.dockerContextPath
                },
                ($exitCode : number, $std : string[]) : void => {
                    if ($exitCode === 0) {
                        callbacks.then("Compose-up succeed");
                    } else {
                        callbacks.onError($std.join(""));
                    }
                });

            return {
                OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                    callbacks.onError = $callback;
                    return {
                        Then: ($callback : ($response : any) => void) : void => {
                            callbacks.then = $callback;
                        }
                    };

                }
            };
        }

        public ComposeDown() : IDockerPromise {
            const callbacks : any = {
                onError($error : any) : any {
                    // declare default callback
                },
                then($response : any) : any {
                    // declare default callback
                }
            };

            this.terminal.Spawn("docker-compose", ["down"],
                {
                    advanced: {
                        noTerminalLog: true
                    },
                    cwd     : this.dockerContextPath
                },
                ($exitCode : number, $std : string[]) : void => {
                    if ($exitCode === 0) {
                        callbacks.then("Compose-down succeed");
                    } else {
                        callbacks.onError($std.join(""));
                    }
                });

            return {
                OnError: ($callback : ($error : any) => void) : IDockerBasePromise => {
                    callbacks.onError = $callback;
                    return {
                        Then: ($callback : ($response : any) => void) : void => {
                            callbacks.then = $callback;
                        }
                    };

                }
            };
        }

        protected getDockerOptions() : DockerOptions {
            let options : DockerOptions = <DockerOptions>{
                build    : {
                    name: ""
                },
                container: {
                    name: ""
                },
                version  : "latest"
            };
            const cfgFile : string = this.dockerContextPath + "/" + "docker.config.json";
            if (ObjectValidator.IsEmptyOrNull(this.configsCache)) {
                if (this.fileSystem.Exists(cfgFile)) {
                    try {
                        options = <DockerOptions>JSON.parse(<string>this.fileSystem.Read(cfgFile));
                        this.configsCache = options;
                    } catch (e) {
                        LogIt.Warning("Docker can not parse " + cfgFile + ".");
                    }
                }
            } else {
                options = this.configsCache;
            }
            return options;
        }

        protected getName() : string {
            return "docker";
        }

        protected process($done : any, $option : string) : void {
            let dockerBuildAllowed : boolean = false;
            let dockerComposeAllowed : boolean = false;

            this.dockerContextPath = this.fileSystem.NormalizePath(this.programArgs.getOptions().file);

            if (this.fileSystem.IsFile(this.dockerContextPath)) {
                this.dockerContextPath = StringUtils.Substring(this.dockerContextPath, 0,
                    StringUtils.IndexOf(this.dockerContextPath, "/", false));
            }

            if (this.fileSystem.Exists(this.dockerContextPath + "/Dockerfile")) {
                dockerBuildAllowed = true;
            }
            if (this.fileSystem.Exists(this.dockerContextPath + "/docker-compose.yml")) {
                dockerComposeAllowed = true;
            }

            if ($option === "build" && dockerBuildAllowed) {
                this.BuildImage()
                    .OnMessage(($message : string) : void => {
                        Echo.Printf($message);
                    })
                    .OnError(($error : string) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : any) : void => {
                        LogIt.Info($response);
                        $done();
                    });
            } else if ($option === "create-container") {
                this.CreateContainer("wuiframework/wui-builder-base:latest")
                    .OnError(($error : any) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : any) : void => {
                        LogIt.Info("Container \"" + $response.id + "\" created");
                        $done();
                    });

            } else if ($option === "remove-container") {
                this.RemoveContainer("builder-base")
                    .OnError(($error : any) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : any) : void => {
                        LogIt.Info("Container \"" + $response + "\" removed.");
                        $done();
                    });
            } else if ($option === "start-container") {
                this.StartContainer("builder-base")
                    .OnError(($error : any) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : any) : void => {
                        LogIt.Info("Container \"" + $response + "\" started.");
                        $done();
                    });
            } else if ($option === "stop-container") {
                this.StopContainer("builder-base")
                    .OnError(($error : any) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : any) : void => {
                        LogIt.Info("Container \"" + $response + "\" stopped.");
                        $done();
                    });
            } else if ($option === "auth") {
                this.CheckAuth(this.builderConfig.dockerRegistry)
                    .OnError(($error : any) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : any) : void => {
                        LogIt.Info("Authorization status " + $response);
                        $done();
                    });
            } else if ($option === "pull") {
                this.PullImage("wuiframework/wui-hub:2018.2.0-alpha", this.builderConfig.dockerRegistry)
                    .OnMessage(($message : string) : void => {
                        Echo.Println($message);
                    })
                    .OnError(($error : any) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : string) : void => {
                        LogIt.Info($response);
                        $done();
                    });
            } else if ($option === "push") {
                this.PushImage(
                    "wuiframework/wui-builder-base:latest",
                    "wuiframework/wui-builder-base:latest",
                    this.builderConfig.dockerRegistry)
                    .OnMessage(($message : string) : void => {
                        Echo.Println($message);
                    })
                    .OnError(($error : any) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : string) : void => {
                        LogIt.Info($response);
                        $done();
                    });
            } else if ($option === "tag") {
                this.TagImage("ubuntu:18.04", "some-project/some-repo:some-tag")
                    .OnError(($error : any) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : any) : void => {
                        LogIt.Info($response);
                        $done();
                    });
            } else if ($option === "remove") {
                this.RemoveImage("image-name:version")
                    .OnError(($error : string) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : any) : void => {
                        LogIt.Info($response);
                        $done();
                    });
            } else if ($option === "compose-up" && dockerComposeAllowed) {
                this.ComposeUp()
                    .OnError(($error : string) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : string) : void => {
                        LogIt.Info($response);
                        $done();
                    });
            } else if ($option === "compose-down" && dockerComposeAllowed) {
                this.ComposeDown()
                    .OnError(($error : string) : void => {
                        LogIt.Error($error);
                    })
                    .Then(($response : string) : void => {
                        LogIt.Info($response);
                        $done();
                    });
            } else {
                LogIt.Error("Unsupported task option: " + $option + " in path: " + this.dockerContextPath);
            }
        }
    }

    export class BuildOptions {
        public name : string;
        public useEnv? : boolean;
        public advanced? : any;
    }

    export class BindPortOptions {
        public containerPort : number;
        public hostPort : number;
        public protocol? : string;
        public hostIp? : string;
    }

    export class ContainerOptions {
        public name : string;
        public bindPorts? : BindPortOptions[];
        public cmd? : string[];
        public advanced? : any;
    }

    export class DockerOptions {
        public version : string;
        public build : BuildOptions;
        public container : ContainerOptions;
    }

    export interface IDockerBasePromise {
        Then($callback : ($response : any) => void) : void;
    }

    export interface IDockerPromise {
        OnError($callback : ($error : any) => void) : IDockerBasePromise;
    }

    export interface IDockerProgressPromise {
        OnMessage($callback : ($message : string) => void) : IDockerPromise;
    }
}
