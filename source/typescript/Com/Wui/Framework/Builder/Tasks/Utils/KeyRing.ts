/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import CredentialsManager = Com.Wui.Framework.Builder.Connectors.CredentialsManager;
    import StdinManager = Com.Wui.Framework.Localhost.Utils.StdinManager;
    import StdinQuestion = Com.Wui.Framework.Localhost.Utils.StdinQuestion;

    export class KeyRing extends BaseTask {

        protected getName() : string {
            return "key-ring";
        }

        protected process($done : any) : void {
            StdinManager.Process([
                <StdinQuestion>{
                    callback($password : string, $callback : ($status : boolean) => void) : void {
                        CredentialsManager.setLinuxRoot($password, $callback);
                    },
                    hidden: true,
                    prefix: "Linux root password"
                },
                <StdinQuestion>{
                    callback($password : string, $callback : ($status : boolean) => void) : void {
                        CredentialsManager.setPhonegapToken($password, $callback);
                    },
                    hidden: true,
                    prefix: "Phonegap token"
                }
            ], () : void => {
                $done();
            });
        }
    }
}
