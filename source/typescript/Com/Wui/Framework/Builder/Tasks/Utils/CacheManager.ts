/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ITypeScriptConfigs = Com.Wui.Framework.Builder.Interfaces.ITypeScriptConfigs;
    import ArchType = Com.Wui.Framework.Builder.Enums.ArchType;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;
    import TypeScriptCompile = Com.Wui.Framework.Builder.Tasks.TypeScript.TypeScriptCompile;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;

    export class CacheManager extends BaseTask {

        public static ReferenceRollback() : void {
            const properties : IProperties = Loader.getInstance().getAppProperties();
            const conf : ITypeScriptConfigs = TypeScriptCompile.getConfig();
            if (conf.hasOwnProperty("source") && conf.source.hasOwnProperty("reference")) {
                const referenceFile : string = properties.projectBase + "/" + conf.source.reference;
                const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                if (fileSystem.Exists(referenceFile + ".orig")) {
                    LogIt.Info("Rollback of " + referenceFile + " file");
                    fileSystem.Write(referenceFile, fileSystem.Read(referenceFile + ".orig"));
                    fileSystem.Delete(referenceFile + ".orig");
                }
            }
        }

        protected getName() : string {
            return "cache-manager";
        }

        protected process($done : any, $option : string) : void {
            const os : Types.NodeJS.os = require("os");
            const concatMap : any = require("concat-with-sourcemaps");
            let concat : any;
            const conf : ITypeScriptConfigs = TypeScriptCompile.getConfig();
            let referenceFile : string;
            let data : string;

            if (conf.hasOwnProperty("cache")) {
                let moduleBase : string;
                if (conf.cache.hasOwnProperty("dest")) {
                    const copyRelease : any = ($source : string, $destFolder : string) : void => {
                        const sourcePathLength : number = $source.length;
                        const files : string[] = this.fileSystem.Expand($source + "/**/*.*");
                        if (files.length > 0) {
                            for (const file of files) {
                                if (file.indexOf(".pdb") === -1 &&
                                    file.indexOf(".exe.config") === -1 &&
                                    file.indexOf("README.md") === -1) {
                                    this.fileSystem.Write(this.properties.projectBase + "/build_cache/" +
                                        $destFolder + file.substr(sourcePathLength, file.length), this.fileSystem.Read(file));
                                }
                            }
                        }
                    };
                    const prepareConnector : any = () : void => {
                        moduleBase = this.wuiModules + "/com-wui-framework-connector/win/" +
                            (this.build.product.Arch() === ArchType.X86 ? "32bit" : "64bit");
                        if (this.fileSystem.Exists(moduleBase)) {
                            copyRelease(moduleBase, "WuiConnector");
                        }
                        moduleBase = this.wuiModules + "/com-wui-framework-launcher/win/32bit";
                        if (this.fileSystem.Exists(moduleBase)) {
                            copyRelease(moduleBase, "WuiLauncher");
                        }
                    };
                    switch ($option) {
                    case "generate-ts":
                        CacheManager.ReferenceRollback();
                        if (!this.fileSystem.Exists(this.properties.projectBase + "/" + conf.cache.dest)) {
                            if (this.properties.projectHas.WuiContent("dependencies/*/source/typescript/**/*.ts")) {
                                this.runTask($done, "typescript:cache");
                            } else {
                                LogIt.Info("Generation of TypeScript cache has been skipped: no dependencies have been found");
                                $done();
                            }
                        } else {
                            LogIt.Info("Generation of TypeScript cache has been skipped: cache already exists");
                            $done();
                        }
                        break;
                    case "prepare-ts":
                        if (this.fileSystem.Exists(this.properties.projectBase + "/" + conf.cache.dest)) {
                            const dependenciesReferenceFile : string = conf.cache.dest.replace(".js", ".d.ts");
                            LogIt.Info("Preparation of TypeScript cache: clean up of \"" + dependenciesReferenceFile + "\"");
                            this.fileSystem.Write(dependenciesReferenceFile, this.fileSystem.Read(dependenciesReferenceFile).toString()
                                .replace(/\/\/\/ <reference path="(.*)" \/>/ig, ""));
                            if (conf.hasOwnProperty("source") && conf.source.hasOwnProperty("reference")) {
                                referenceFile = conf.source.reference;
                                if (this.fileSystem.Exists(referenceFile)) {
                                    data = this.fileSystem.Read(referenceFile).toString();
                                    LogIt.Info("Preparation of TypeScript cache: creation of backup for \"" + referenceFile + "\"");
                                    this.fileSystem.Write(referenceFile + ".orig", data);
                                    let referenceContent : string =
                                        "/// <reference path=\"../../" + dependenciesReferenceFile + "\" />" + os.EOL + os.EOL;
                                    const lines : string[] = data.split(os.EOL);
                                    let lineIndex : number;
                                    for (lineIndex = 0; lineIndex < lines.length; lineIndex++) {
                                        if (lines[lineIndex].indexOf("/dependencies/") === -1 &&
                                            lines[lineIndex] !== os.EOL) {
                                            referenceContent += lines[lineIndex] + os.EOL;
                                        }
                                    }
                                    LogIt.Info("Preparation of TypeScript cache: creation of \"" + referenceFile + "\" suitable for cache");
                                    this.fileSystem.Write(referenceFile, referenceContent);
                                }
                            }
                            TypeScriptCompile.getConfig().source.src = [this.properties.sources + "/*.ts"];
                        } else {
                            LogIt.Info("Preparation of TypeScript cache has been skipped: cache does not exist");
                        }
                        $done();
                        break;
                    case "finalize-ts":
                        CacheManager.ReferenceRollback();
                        data = "";
                        const minJsFile : string = this.properties.projectBase + "/" + this.properties.dest +
                            "/resource/javascript/" + this.properties.packageName + ".min.js";
                        concat = new concatMap(true, minJsFile, "\n");
                        concat.add(null, this.properties.licenseText);
                        const tsCacheDest : string = this.properties.projectBase + "/" + conf.cache.dest;
                        if (this.fileSystem.Exists(tsCacheDest)) {
                            concat.add(tsCacheDest,
                                this.fileSystem.Read(tsCacheDest).toString(), this.fileSystem.Read(tsCacheDest + ".map").toString());
                        }
                        const tsSourceDest : string = this.properties.projectBase + "/" + conf.source.dest;
                        if (this.fileSystem.Exists(tsSourceDest)) {
                            LogIt.Info("Generating package JS file: " + minJsFile);
                            concat.add(tsSourceDest,
                                this.fileSystem.Read(tsSourceDest).toString(), this.fileSystem.Read(tsSourceDest + ".map").toString());
                        }
                        this.fileSystem.Write(minJsFile, concat.content.toString()
                            .replace(/^\/\/# sourceMappingURL=dependencies\.js\.map$/gm, "")
                            .replace(
                                /^\/\/# sourceMappingURL=source\.js\.map$/gm,
                                "//# sourceMappingURL=" + this.properties.packageName + ".min.js.map"));
                        this.fileSystem.Write(minJsFile + ".map", concat.sourceMap.toString()
                            .replace(/\.\.\/dependencies\//gim, "../../../../dependencies/"));
                        $done();
                        break;

                    case "generate-sass":
                        if (!this.fileSystem.Exists(this.properties.projectBase + "/build_cache/dependencies.css")) {
                            if (this.properties.projectHas.WuiContent("dependencies/*/resource/sass/**/*.scss")) {
                                this.runTask($done, "sass-compile:cache");
                            } else {
                                LogIt.Info("Generation of SASS cache has been skipped: no dependencies have been found");
                                $done();
                            }
                        } else {
                            LogIt.Info("Generation of SASS cache has been skipped: cache already exists");
                            $done();
                        }
                        break;
                    case "finalize-sass":
                        const sassCacheDest : string = this.properties.projectBase + "/build_cache/sass/cache.css";
                        const sassDependencies : string = this.properties.projectBase + "/build_cache/dependencies.css";
                        if (this.fileSystem.Exists(sassCacheDest)) {
                            this.fileSystem.Write(sassDependencies, this.fileSystem.Read(sassCacheDest));
                            this.fileSystem.Write(sassDependencies + ".map", this.fileSystem.Read(sassCacheDest + ".map"));
                        }
                        if (this.fileSystem.Exists(this.properties.projectBase + "/build_cache/sass")) {
                            this.fileSystem.Delete(this.properties.projectBase + "/build_cache/sass");
                        }
                        const minCssFile : string = this.properties.projectBase + "/" + this.properties.dest +
                            "/resource/css/" + this.properties.packageName + ".min.css";
                        LogIt.Info("Generating package CSS file: " + minCssFile);
                        concat = new concatMap(true, minCssFile, "\n");
                        concat.add("", this.properties.licenseText, null);
                        if (this.fileSystem.Exists(sassDependencies)) {
                            if (this.fileSystem.Exists(sassDependencies + ".map")) {
                                concat.add("dependencies.css",
                                    this.fileSystem.Read(sassDependencies).toString(),
                                    this.fileSystem.Read(sassDependencies + ".map").toString());
                            } else {
                                concat.add("dependencies.css", this.fileSystem.Read(sassDependencies).toString(), null);
                                this.fileSystem.Delete(sassDependencies);
                                LogIt.Warning("Invalidating dependencies SASS cache due to missing map file. " +
                                    "Current build may have malfunction in CSS mapping.");
                            }
                        }
                        this.fileSystem.Expand(this.properties.projectBase + "/build/compiled/source/sass/**/*.css")
                            .forEach(($file : string) : void => {
                                concat.add($file, this.fileSystem.Read($file).toString(), this.fileSystem.Read($file + ".map").toString());
                            });
                        this.fileSystem.Write(minCssFile, concat.content.toString());
                        this.fileSystem.Write(minCssFile + ".map", concat.sourceMap.toString());

                        $done();
                        break;

                    case "finalize-chromiumre":
                        prepareConnector();
                        moduleBase = this.wuiModules + "/com-wui-framework-chromiumre/win/" +
                            (this.build.product.Arch() === ArchType.X86 ? "32bit" : "64bit");
                        if (this.fileSystem.Exists(moduleBase)) {
                            copyRelease(moduleBase, "WuiChromiumRE");
                        }
                        $done();
                        break;

                    case "finalize-eclipse":
                        prepareConnector();
                        moduleBase = this.wuiModules + "/com-wui-framework-idejre-eclipse/win/32bit";
                        const jarFile : string[] = this.fileSystem.Expand(moduleBase + "/com-wui-framework-idejre-*.jar");
                        if (this.fileSystem.Exists(moduleBase) && jarFile.length === 1) {
                            this.fileSystem.Unpack(jarFile[0], <any>{output: this.properties.projectBase + "/build_cache/WuiEclipseRe"},
                                () : void => {
                                    $done();
                                });
                        } else {
                            LogIt.Error("Unable to locate Eclipse source module.");
                        }
                        break;
                    case "finalize-idea":
                        prepareConnector();
                        moduleBase = this.wuiModules + "/com-wui-framework-idejre-idea/win/32bit";
                        const zipFile : string[] = this.fileSystem.Expand(moduleBase + "/com-wui-framework-idejre-*.zip");
                        if (this.fileSystem.Exists(moduleBase) && zipFile.length === 1) {
                            this.fileSystem.Unpack(zipFile[0], <any>{output: this.properties.projectBase + "/build_cache/WuiIdeaRe"},
                                () : void => {
                                    const appPath : string = this.properties.projectBase + "/build_cache/WuiIdeaRe/app";
                                    const libPath : string = this.properties.projectBase + "/build_cache/WuiIdeaRe/lib";
                                    this.fileSystem.Unpack(libPath + "/com-wui-framework-idejre.jar", <any>{output: appPath},
                                        () : void => {
                                            this.fileSystem.Delete(libPath + "/com-wui-framework-idejre.jar");
                                            $done();
                                        });
                                });
                        } else {
                            LogIt.Error("Unable to locate Idea source module.");
                        }
                        break;

                    default:
                        $done();
                        break;
                    }
                }
            }
        }
    }
}
