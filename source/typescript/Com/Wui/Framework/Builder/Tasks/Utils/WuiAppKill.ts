/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;

    export class WuiAppKill extends BaseTask {

        protected getName() : string {
            return "wui-app-kill";
        }

        protected process($done : any) : void {
            let extension : string = "";
            if (EnvironmentHelper.IsWindows()) {
                extension = ".exe";
            }
            const appName : string = this.project.target.name;
            let cwd : string;
            const stopConnector : any = () : void => {
                const files : string[] = this.fileSystem.Expand(
                    this.properties.projectBase + "/build/**/WuiConnector/*Connector" + extension);
                if (files.length > 0) {
                    let stopSuccess : boolean = true;
                    const nextStop : any = ($index : number) : void => {
                        if ($index < files.length) {
                            if (!this.fileSystem.IsDirectory(files[$index])) {
                                this.terminal.Kill(files[$index], ($status : boolean) : void => {
                                    if (!$status) {
                                        stopSuccess = false;
                                    }
                                    nextStop($index + 1);
                                });
                            }
                        } else {
                            if (!stopSuccess) {
                                LogIt.Error("WUI app kill: unable to stop all process");
                            } else {
                                LogIt.Info("WUI app kill: all process stopped successfully");
                                $done();
                            }
                        }
                    };
                    nextStop(0);
                } else {
                    LogIt.Info("WUI app kill: skipped, because application or WUIConnector has not been found");
                    $done();
                }
            };
            cwd = this.properties.projectBase + "/build/target";
            if (this.fileSystem.Exists(cwd + "/build/target/" + appName + extension)) {
                if (this.fileSystem.Exists(cwd + "/wuichromiumre/" + appName + extension)) {
                    this.terminal.Kill(cwd + "/" + appName + extension, ($status : boolean) : void => {
                        if (!$status) {
                            LogIt.Error("WUI app kill: unable to stop ChromiumRE process");
                        } else {
                            stopConnector();
                        }
                    });
                } else {
                    stopConnector();
                }
            } else {
                stopConnector();
            }
        }
    }
}
