/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;

    export class GitManager extends BaseTask {

        public static Execute($args : string[], $callback : ($stdout : string) => void, $onError? : () => void,
                              $repoPath? : string) : void {
            const path : Types.NodeJS.path = require("path");
            if (ObjectValidator.IsEmptyOrNull($repoPath)) {
                $repoPath = Loader.getInstance().getAppProperties().projectBase;
            }
            const gitBin : string = EnvironmentHelper.IsWindows() ? "/cmd" : "/bin";
            Loader.getInstance().getTerminal().Spawn("git", $args, {
                    cwd: $repoPath,
                    env: {
                        PATH: StringUtils.Replace(Loader.getInstance().getProgramArgs().AppDataPath() + "/external_modules" +
                            "/git" + gitBin, "/", path.sep)
                    }
                },
                ($exitCode : number, $std : string[]) : void => {
                    if ($exitCode === 0) {
                        $callback($std[0]);
                    } else if (ObjectValidator.IsSet($onError)) {
                        $onError();
                    } else {
                        LogIt.Error("GIT execution has failed");
                    }
                });
        }

        public static StashRestore() : void {
            const path : Types.NodeJS.path = require("path");
            const backupFilePath : any =
                Loader.getInstance().getAppProperties().projectBase.replace(/\\/g, "/") + "/build_cache/unstaged.diff";
            const spawnSync : Types.NodeJS.Modules.child_process.spawnSync = require("child_process").spawnSync;
            if (!Loader.getInstance().getFileSystemHandler().IsEmpty(backupFilePath)) {
                const retVal : Types.NodeJS.Modules.child_process.SpawnSyncReturns<Buffer> =
                    spawnSync("git apply --whitespace=fix \"" + backupFilePath.replace(/[/\\]/g, path.sep) + "\"", {
                        cwd        : Loader.getInstance().getAppProperties().projectBase,
                        env        : process.env,
                        shell      : true,
                        windowsHide: true
                    });

                if (retVal.status === 0) {
                    LogIt.Info("Git stash rollback done.");
                    if (Loader.getInstance().getFileSystemHandler().Delete(backupFilePath)) {
                        LogIt.Info(">>"[ColorType.YELLOW] + " Git backup file cleaned.");
                    } else {
                        LogIt.Warning("Git backup file cleanup failed.");
                    }
                } else {
                    LogIt.Debug(retVal.stdout.toString() + retVal.stderr.toString());
                    LogIt.Warning("Git stash rollback failed. " +
                        "Use 'git apply \"" + backupFilePath + "\"' to rollback manually.");
                }
            } else {
                LogIt.Info("Git stash rollback not necessary.");
            }
        }

        public static UpdateTracked($callback : () => void) : void {
            GitManager.Execute(["ls-files", "--ignored", "--exclude-standard"], ($stdout : string) : void => {
                const removeList : string[] = StringUtils.Split(StringUtils.Replace($stdout, "\r", ""), "\n");
                if (removeList.length > 0) {
                    GitManager.Execute(["rm", "--cached"].concat(removeList), () : void => {
                        $callback();
                    });
                } else {
                    $callback();
                }
            });
        }

        protected getName() : string {
            return "git-manager";
        }

        protected process($done : any, $option : string) : void {
            const path : Types.NodeJS.path = require("path");
            const backupFilePath : any = this.properties.projectBase.replace(/\\/g, "/") + "/build_cache/unstaged.diff";

            switch ($option) {
            case "backup":
                this.fileSystem.CreateDirectory(path.dirname(backupFilePath));
                this.terminal.Execute("git diff > \"" + backupFilePath + "\"", [], this.properties.projectBase,
                    ($exitCode : number, $std : string[]) : void => {
                        if ($exitCode === 0) {
                            if (this.fileSystem.Read(backupFilePath).length > 0) {
                                GitManager.Execute(["apply", "-R", "--whitespace=fix", backupFilePath],
                                    () : void => {
                                        LogIt.Info(">>"[ColorType.YELLOW] + " Unstaged files have been moved to backup.");
                                        $done();
                                    },
                                    () : void => {
                                        LogIt.Warning("Can not remove unstaged files.");
                                        $done();
                                    });
                            } else {
                                LogIt.Info(">>"[ColorType.YELLOW] + " GIT unstage skipped: nothing to unstage");
                                $done();
                            }
                        } else {
                            LogIt.Warning("Can not process git diff: " + $std[1]);
                            $done();
                        }
                    });
                break;
            case "restore":
                GitManager.StashRestore();
                $done();
                break;
            case "push":
                let name : string = "origin";
                const push : any = () : void => {
                    GitManager.Execute(["push", name], () : void => {
                        GitManager.Execute(["push", name, "--tags"], $done);
                    });
                };
                if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().to)) {
                    const remote : string[] = StringUtils.Split(this.programArgs.getOptions().to, ";");
                    if (remote.length === 2) {
                        name = remote[0];
                        const source : string = remote[1];
                        GitManager.Execute(["remote", "-v"], ($stdout : string) : void => {
                            if (StringUtils.Contains($stdout, name + source)) {
                                push();
                            } else {
                                GitManager.Execute(["remote", "add", name, source], push);
                            }
                        });
                    } else {
                        LogIt.Error("Argument in incorrect format: Expected is --to=\"<remote_name>;<remote_url>\"");
                    }
                } else {
                    push();
                }
                break;
            case "porcelaincheck":
                const onError : any = () : void => {
                    LogIt.Error("Uncommitted changes has been found. Commit or stash them.");
                };
                GitManager.Execute(["update-index", "-q", "--ignore-submodules", "--refresh"], () : void => {
                    GitManager.Execute(["diff-files", "--quiet", "--ignore-submodules"], () : void => {
                        GitManager.Execute(["diff-index", "--cached", "--quiet", "HEAD", "--ignore-submodules"], $done, onError);
                    }, onError);
                });
                break;
            case "hooks":
                const hookPath : string = this.properties.projectBase + "/.git/hooks/pre-commit";
                if (this.programArgs.IsForce()) {
                    this.fileSystem.Delete(hookPath);
                }
                if (!this.fileSystem.Exists(hookPath)) {
                    this.fileSystem.Copy(this.properties.binBase + "/resource/scripts/pre-commit", hookPath, $done);
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " GIT hooks install skipped: hooks already installed. " +
                        "Use option --force for reinstall.");
                    $done();
                }
                break;
            default:
                LogIt.Error("Unknown option \"" + $option + "\" for git-manager.");
                break;
            }
        }
    }
}
