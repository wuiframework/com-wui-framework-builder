/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;

    export class StringReplace extends BaseTask {

        public static File($path : string, $type : StringReplaceType, $dest? : string) : void {
            if (ObjectValidator.IsEmptyOrNull($dest)) {
                $dest = $path;
            }
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (fileSystem.IsFile($path)) {
                let content : string = fileSystem.Read($path).toString();
                content = StringReplace.Content(content, $type);
                fileSystem.Write($dest, content);
            }
        }

        public static Content($input : string, $type : StringReplaceType, $varFormatter? : ($value : string) => string) : string {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const os : Types.NodeJS.os = require("os");

            const templates : any = {
                cmake   : {
                    commonDependency: "" +
                        "set(<? @var this.name ?>_CONFIG_SCRIPT_PATH <? @var this.configure-script.path ?>/" +
                        "<? @var this.configure-script.name ?>)" + os.EOL +
                        "if (NOT IS_ABSOLUTE ${<? @var this.name ?>_CONFIG_SCRIPT_PATH})" + os.EOL +
                        "    set(<? @var this.name ?>_CONFIG_SCRIPT_PATH ${CMAKE_SOURCE_DIR}/${<? @var this.name ?>_CONFIG_SCRIPT_PATH})" +
                        os.EOL +
                        "endif ()" + os.EOL +
                        "include(${<? @var this.name ?>_CONFIG_SCRIPT_PATH})" + os.EOL +
                        "<? @var this.configure-script.macro-name ?>(<? @var project.target.name ?> internal-unit-test-runner " +
                        "${TEST_TARGET_NAME}" +
                        os.EOL +
                        "        \"<? @var this.location.path ?>\"" + os.EOL +
                        "        \"<? @var this.version ?>\"" + os.EOL +
                        "        \"<? @var this.configure-script.attributes ?>\")" + os.EOL,

                    wuiDependency: "" +
                        "set(DEPENDENCIES_FILES \"\")" + os.EOL +
                        "set(DEPENDENCIES_HEADERS \"\")" + os.EOL +
                        "set(DEPENDENCIES_LOOK_FOLDERS <? @var this.name ?>)" + os.EOL +
                        "foreach (item ${DEPENDENCIES_LOOK_FOLDERS})" + os.EOL +
                        "    FILE(GLOB_RECURSE _FILES ${CMAKE_SOURCE_DIR}/dependencies/${item}/source/cpp/*.cpp)" + os.EOL +
                        "    FILE(GLOB_RECURSE _HEADERS ${CMAKE_SOURCE_DIR}/dependencies/${item}/source/cpp/reference.hpp)" + os.EOL +
                        "    set(DEPENDENCIES_FILES ${DEPENDENCIES_FILES} ${_FILES})" + os.EOL +
                        "    set(DEPENDENCIES_HEADERS ${DEPENDENCIES_HEADERS} ${_HEADERS})" + os.EOL +
                        "endforeach ()" + os.EOL +
                        "list(LENGTH DEPENDENCIES_FILES DEPENDENCIES_COUNT)" + os.EOL +
                        "if (NOT DEPENDENCIES_COUNT EQUAL 0)" + os.EOL +
                        "    add_library(${PROJECT_NAME}_wui_dependencies.lib ${DEPENDENCIES_HEADERS} ${DEPENDENCIES_FILES})" + os.EOL +
                        "    target_include_directories(${PROJECT_NAME}_wui_dependencies.lib PUBLIC <? @var properties.cppIncludes ?>)" +
                        os.EOL +
                        "    target_link_libraries(${PROJECT_NAME}_wui_dependencies.lib <? @var properties.cppLibraries ?>)" + os.EOL +
                        "    target_link_libraries(<? @var project.target.name ?> ${PROJECT_NAME}_wui_dependencies.lib)" + os.EOL +
                        "    target_link_libraries(internal-unit-test-runner ${PROJECT_NAME}_wui_dependencies.lib)" + os.EOL +
                        "endif ()" + os.EOL
                },
                maven   : {
                    artifact  : os.EOL +
                        "        <dependency>" + os.EOL +
                        "            <groupId><? @var this.groupId ?></groupId>" + os.EOL +
                        "            <artifactId><? @var this.artifactId ?></artifactId>" + os.EOL +
                        "            <version><? @var this.version ?></version>" + os.EOL +
                        "        </dependency>",
                    repository: os.EOL +
                        "        <repository>" + os.EOL +
                        "            <id><? @var this.id ?></id>" + os.EOL +
                        "            <url><? @var this.url ?></url>" + os.EOL +
                        "        </repository>",
                    resource  : os.EOL +
                        "            <resource>" + os.EOL +
                        "                <directory><? @var this.value ?></directory>" + os.EOL +
                        "                <targetPath>${project.build.outputDirectory}/resource</targetPath>" + os.EOL +
                        "                <filtering>true</filtering>" + os.EOL +
                        "                <excludes>" + os.EOL +
                        "                    <exclude>*.jar</exclude>" + os.EOL +
                        "                    <exclude>**/*.jar</exclude>" + os.EOL +
                        "                </excludes>" + os.EOL +
                        "            </resource>",
                    source    : os.EOL +
                        "                            <source>${project.basedir}/dependencies/<? @var this.name ?>/source/java/</source>"
                },
                phonegap: {
                    platform: "    <platform name=\"<? @var this.value ?>\" />"
                }
            };

            const env : EnvironmentArgs = Loader.getInstance().getEnvironmentArgs();
            env.UpdateProperties();
            const getProperty = ($name : string, $from? : any) : any => {
                if (ObjectValidator.IsEmptyOrNull($from)) {
                    $from = {
                        build       : env.getBuildArgs(),
                        clientConfig: env.getAppProperties().clientConfig,
                        project     : env.getTargetConfig(),
                        properties  : env.getAppProperties(),
                        serverConfig: env.getAppProperties().serverConfig,
                        templates,
                        testConfig  : env.getTestConfig()
                    };
                }
                const split : string[] = $name.split(".");
                let fromCopy : any = JSON.parse(JSON.stringify($from));
                let index : number;
                let found : boolean = false;
                for (index = 0; index < split.length; index++) {
                    if (fromCopy.hasOwnProperty(split[index])) {
                        fromCopy = fromCopy[split[index]];
                        found = true;
                    } else {
                        found = false;
                    }
                }
                if (found) {
                    return fromCopy;
                }
                return undefined;
            };
            const forEachReplacement = ($match : string, $what : string, $where : string) : string => {
                try {
                    const whatValue : any = getProperty($what);
                    const whereValue : any = getProperty($where);

                    let output : string = "";
                    whereValue.forEach(($target : string) : void => {
                        if (output !== "") {
                            output += os.EOL;
                        }
                        if (typeof $target === "string") {
                            output += whatValue.replace(/<\? @var this\.value \?>/gi, $target);
                        } else {
                            output += whatValue.replace(/<\? @var this\.(.*?) \?>/gi, ($match : string, $value : string) : string => {
                                try {
                                    const replacement : string = getProperty($value, $target);
                                    if (ObjectValidator.IsSet(replacement)) {
                                        return replacement;
                                    }
                                } catch (ex) {
                                    LogIt.Debug(ex);
                                }
                                LogIt.Warning(
                                    "string-replace: property \"" + $value + "\" was not found, so replace was skipped.");
                                return "<? @var this." + $value + " ?>";
                            });
                        }
                    });
                    return output;
                } catch (ex) {
                    LogIt.Warning(ex);
                }
                LogIt.Warning("string-replace: forEach replacement for \"" + $where + "\" has failed, so replace was skipped.");
                return $match;
            };
            const importsFinishReplacement = ($match : string, $path : string) : string => {
                let content : string = fileSystem.Read($path).toString();
                content = StringReplace.Content(content, StringReplaceType.FOR_EACH);
                content = StringReplace.Content(content, StringReplaceType.VARIABLES);
                return content;
            };
            const importsPrepareReplacement = ($match : string, $value : string) : string => {
                let path : string = $value;
                if ($value.toString().indexOf(".html") !== -1) {
                    path = "source/html/" + $value;
                } else if ($value.toString().indexOf(".js") !== -1) {
                    path = "resource/javascript/" + $value;
                }
                const index : number = $value.toString().indexOf("dependencies");
                const isDependency : boolean = index !== -1;
                if (isDependency) {
                    path = $value.substr(index, $value.length);
                }
                if (fileSystem.Exists(path)) {
                    const dest : string = env.getAppProperties().projectBase + "/build/compiled/" + path;
                    if (isDependency) {
                        fileSystem.Write(dest, fileSystem.Read(path));
                    }
                    return "<!-- @import " + dest + " -->";
                } else {
                    return "<!-- @import " + $value + " -->";
                }
            };
            const varReplacement = ($match : string, $value : string) : string => {
                try {
                    let replacement : string = getProperty($value);
                    if (ObjectValidator.IsSet(replacement)) {
                        if (!ObjectValidator.IsEmptyOrNull($varFormatter)) {
                            replacement = $varFormatter(replacement);
                        }
                        if (ObjectValidator.IsObject(replacement)) {
                            replacement = JSON.stringify(JSON.stringify(replacement));
                            replacement = replacement.substring(1, replacement.length - 1);
                        }
                        return replacement;
                    }
                } catch (ex) {
                    // ignore unresolved replacement
                }
                LogIt.Warning("string-replace: property \"" + $value + "\" was not found, so replace was skipped.");
                return $match;
            };

            let replacements : any[] = [];
            switch ($type) {
            case StringReplaceType.IMPORTS_PREPARE:
                replacements = [
                    {
                        pattern    : /<\? @import (.*?) \?>/ig,
                        replacement: importsPrepareReplacement
                    },
                    {
                        pattern    : /<!-- @import (.*?) -->/ig,
                        replacement: importsPrepareReplacement
                    },
                    {
                        pattern    : /#?\/\* @import (.*?) \*\//ig,
                        replacement: importsPrepareReplacement
                    },
                    {
                        pattern    : /\/\*\*\s*<!-- @import (.*?) -->\s*\*\//ig,
                        replacement: importsPrepareReplacement
                    }
                ];
                break;
            case StringReplaceType.IMPORTS_FINISH:
                replacements = [
                    {
                        pattern    : /<\? @import (.*?) \?>/ig,
                        replacement: importsFinishReplacement
                    },
                    {
                        pattern    : /<!-- @import (.*?) -->/ig,
                        replacement: importsFinishReplacement
                    },
                    {
                        pattern    : /\/\* @import (.*?) \*\//ig,
                        replacement: importsFinishReplacement
                    },
                    {
                        pattern    : /\/\*\*\s*<!-- @import (.*?) -->\s*\*\//ig,
                        replacement: importsPrepareReplacement
                    }
                ];
                break;
            case StringReplaceType.FOR_EACH:
                replacements = [
                    {
                        pattern    : /<\? @forEach (.*?) in (.*?) \?>/ig,
                        replacement: forEachReplacement
                    },
                    {
                        pattern    : /<!-- @forEach (.*?) in (.*?) -->/ig,
                        replacement: forEachReplacement
                    }
                ];
                break;
            case StringReplaceType.VARIABLES:
                replacements = [
                    {
                        pattern    : /<\? @var (.*?) \?>/ig,
                        replacement: varReplacement
                    },
                    {
                        pattern    : /<!-- @var (.*?) -->/ig,
                        replacement: varReplacement
                    }
                ];
                break;
            case StringReplaceType.VARIABLES_PLUGIN:
                replacements = [
                    {
                        pattern    : /<\? @var (.*?) \?>/ig,
                        replacement: varReplacement
                    },
                    {
                        pattern    : /<!-- @var (.*?) -->/ig,
                        replacement: varReplacement
                    }
                ];
                break;
            case StringReplaceType.DEPENDENCIES:
                replacements = [
                    {
                        pattern    : /..\/dependencies\//ig,
                        replacement: "../../"
                    },
                    {
                        pattern    : /..\/bin\//ig,
                        replacement: "../../../bin/"
                    }
                ];
                break;
            case StringReplaceType.XCPP:
                replacements = [
                    {
                        pattern    : /\/\*\*.*\r?\n( \* WARNING:.*)(.*\r?\n)+(.*\*\/?\r?\n\r?\n)/,
                        replacement: ""
                    },
                    {
                        pattern    : /<\? @var (.*?) \?>/ig,
                        replacement: varReplacement
                    },
                    {
                        pattern    : /"reference\.hpp"/ig,
                        replacement: "\"../../source/cpp/reference.hpp\""
                    },
                    {
                        pattern    : /\r?\n\/\/\s*NOLINT\s*\(legal\/copyright\)\s*\r?\n/ig,
                        replacement: ""
                    }
                ];
                break;

            default:
                LogIt.Warning("Unsupported StringReplace type: " + $type);
                break;
            }

            replacements.forEach(($replacement : any) : void => {
                $input = $input.replace($replacement.pattern, $replacement.replacement);
            });
            return $input;
        }

        protected getName() : string {
            return "string-replace";
        }

        protected process($done : any, $option : string) : void {
            const processAll : any = ($sources : string[], $dest : string, $type : string) : void => {
                for (let index : number = 0; index < $sources.length; index++) {
                    $sources[index] = this.properties.projectBase + "/" + $sources[index];
                }
                $dest = this.properties.projectBase + "/" + $dest;
                this.fileSystem.Expand($sources).forEach(($file : string) : void => {
                    let destination : string = $file;
                    destination = StringUtils.Remove(destination, $dest);
                    destination = StringUtils.Remove(destination, this.properties.projectBase + "/");
                    StringReplace.File($file, $type, $dest + "/" + destination);
                });
            };
            switch ($option) {
            case "imports-prepare":
                processAll([
                    this.properties.sources + "/html/**/*.html",
                    this.properties.dependenciesResources + "/javascript/**/*.js",
                    this.properties.resources + "/javascript/**/*.js",
                    this.properties.resources + "/configs/wuirunner.config.jsonp",
                    this.properties.sources + "/cpp/ApplicationMain.cpp"
                ], this.properties.compiled, StringReplaceType.IMPORTS_PREPARE);
                break;
            case "imports-finish":
                processAll([
                    this.properties.compiled + "/**/html/**/*.html",
                    this.properties.compiled + "/**/javascript/**/*.js",
                    this.properties.compiled + "/**/wuirunner.config.jsonp",
                    this.properties.tmp + "/cpp/ApplicationMain.cpp"
                ], this.properties.compiled, StringReplaceType.IMPORTS_FINISH);
                break;
            case "variables":
                processAll([
                    this.properties.compiled + "/**/html/**/*.html",
                    this.properties.compiled + "/**/javascript/**/*.js",
                    this.properties.compiled + "/**/wuirunner.config.jsonp"
                ], this.properties.compiled, StringReplaceType.VARIABLES);
                break;
            case "variables-plugin":
                processAll([
                    this.properties.dest + "/**/plugin.xml",
                    this.properties.dest + "/**/**/plugin.xml"
                ], this.properties.dest, StringReplaceType.VARIABLES_PLUGIN);
                break;
            case "dependencies":
                processAll([
                    "dependencies/*/source/typescript/**/*.d.ts",
                    "dependencies/*/test/**/*.d.ts",
                    "dependencies/*/resource/sass/**/*.scss"
                ], "dependencies", StringReplaceType.DEPENDENCIES);
                break;
            case "xcpp":
                processAll([
                    this.properties.tmp + "/cpp/ApplicationMain.cpp"
                ], this.properties.tmp + "/cpp", "xcpp");
                processAll([
                    "bin/resource/scripts/UnitTestLoader.cpp",
                    "bin/resource/scripts/UnitTestRunner.cpp"
                ], "bin/resource/scripts/", StringReplaceType.XCPP);
                break;

            default:
                LogIt.Warning("Unsupported string-replace option: " + $option);
                break;
            }
            $done();
        }
    }
}
