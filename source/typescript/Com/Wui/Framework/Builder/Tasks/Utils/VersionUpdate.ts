/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IProject = Com.Wui.Framework.Builder.Interfaces.IProject;
    import StdinManager = Com.Wui.Framework.Localhost.Utils.StdinManager;
    import StdinQuestion = Com.Wui.Framework.Localhost.Utils.StdinQuestion;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class VersionUpdate extends BaseTask {

        protected getName() : string {
            return "version-update";
        }

        protected process($done : any) : void {
            const os : Types.NodeJS.os = require("os");
            let version : string = this.programArgs.getOptions().newVersion;
            let description : string = this.programArgs.getOptions().description;

            const update : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(version)) {
                    if (!ObjectValidator.IsEmptyOrNull(description)) {
                        const packageConfPath : string = this.properties.projectBase + "/package.conf.json";
                        const scrPath : string = this.properties.projectBase + "/SW-Content-Register.txt";
                        const readmePath : string = this.properties.projectBase + "/README.md";

                        let packageConfOrig : string;
                        let scrOrig : string;
                        let readmeOrig : string;
                        try {
                            if (this.fileSystem.Exists(packageConfPath)) {
                                packageConfOrig = this.fileSystem.Read(packageConfPath).toString();
                                const conf : IProject = JSON.parse(this.fileSystem.Read(packageConfPath).toString());
                                const packageConf : string =
                                    packageConfOrig.replace(
                                        "\"version\": \"" + conf.version + "\"", "\"version\": \"" + version + "\"");
                                this.fileSystem.Write(packageConfPath, packageConf);

                                if (this.fileSystem.Exists(scrPath)) {
                                    scrOrig = this.fileSystem.Read(scrPath).toString();
                                    const scr : string = scrOrig.replace(
                                        "Release Name: " + conf.name + " v" + conf.version, "Release Name: " + conf.name + " v" + version);
                                    this.fileSystem.Write(scrPath, scr.replace(
                                        "Release Name: " + conf.name + " v" + conf.version, "Release Name: " + conf.name + " v" + version));
                                }

                                if (this.fileSystem.Exists(readmePath)) {
                                    readmeOrig = this.fileSystem.Read(readmePath).toString();
                                    let readme : string = readmeOrig.replace(
                                        "# " + conf.name + " v" + conf.version, "# " + conf.name + " v" + version);
                                    readme = readme.replace("## Licence", "## License");
                                    const regex : RegExp = new RegExp("## History((?:.*[\r\n])+)[\r\n][\r\n]## License", "gmi");
                                    const history : RegExpMatchArray = regex.exec(readme);
                                    if (history !== null && history.length === 2) {
                                        if (StringUtils.StartsWith(history[1], os.EOL)) {
                                            history[1] = history[1].substring(os.EOL.length);
                                        }
                                        readme = readme.replace(history[1], os.EOL + "### v" + version + os.EOL + description + history[1]);
                                        this.fileSystem.Write(readmePath, readme);
                                    }
                                    $done();
                                } else {
                                    LogIt.Error("README.md file has not been found.");
                                }
                            } else {
                                LogIt.Error("package.conf.json file has not been found.");
                            }
                        } catch (ex) {
                            if (!ObjectValidator.IsEmptyOrNull(packageConfOrig)) {
                                this.fileSystem.Write(packageConfPath, packageConfOrig);
                            }
                            if (!ObjectValidator.IsEmptyOrNull(scrOrig)) {
                                this.fileSystem.Write(scrPath, scrOrig);
                            }
                            if (!ObjectValidator.IsEmptyOrNull(readmeOrig)) {
                                this.fileSystem.Write(readmePath, readmeOrig);
                            }
                            LogIt.Error(ex);
                        }
                    } else {
                        LogIt.Error("Argument \"description\" must be specified.");
                    }
                } else {
                    LogIt.Error("Argument \"new-version\" must be specified.");
                }
            };

            if (!ObjectValidator.IsEmptyOrNull(version) && !ObjectValidator.IsEmptyOrNull(description)) {
                update();
            } else {
                StdinManager.Process([
                    <StdinQuestion>{
                        callback($value : string, $callback : ($status : boolean) => void) : void {
                            version = $value;
                            $callback(true);
                        },
                        prefix: "new version"
                    },
                    <StdinQuestion>{
                        callback($value : string, $callback : ($status : boolean) => void) : void {
                            description = $value;
                            $callback(true);
                        },
                        prefix: "description"
                    }
                ], update);
            }
        }
    }
}
