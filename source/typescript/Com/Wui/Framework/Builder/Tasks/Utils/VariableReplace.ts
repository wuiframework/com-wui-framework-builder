/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import GeneralTaskType = Com.Wui.Framework.Builder.Enums.GeneralTaskType;

    export class VariableReplace extends BaseTask {

        protected getName() : string {
            return GeneralTaskType.VAR_REPLACE;
        }

        protected process($done : any) : void {
            this.runTask($done, "string-replace:imports-prepare", "string-replace:variables", "string-replace:imports-finish");
        }
    }
}
