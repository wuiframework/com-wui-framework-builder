/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Utils {
    "use strict";
    import BaseTask = Com.Wui.Framework.Builder.Primitives.BaseTask;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class InstallIdeaTools extends BaseTask {

        protected getName() : string {
            return "install-idea-tools";
        }

        protected process($done : any) : void {
            const configBase : string = this.programArgs.BinBase() + "/resource/configs/ideaTools";
            let ideaBase : string = this.programArgs.getOptions().file;
            if (ObjectValidator.IsEmptyOrNull(ideaBase)) {
                LogIt.Error("Install of IDEA tools has failed: config base has not been specified by --file option");
            }
            if (!this.fileSystem.Exists(ideaBase)) {
                LogIt.Error("Install of IDEA tools has failed: path to config base has not been found");
            }
            ideaBase += "/config/tools";
            ideaBase = this.fileSystem.NormalizePath(ideaBase);
            const files : string[] = this.fileSystem.Expand(configBase + "/*.xml");
            const copyNext : any = ($index : number) : void => {
                if ($index < files.length) {
                    this.fileSystem.Copy(files[$index], ideaBase + StringUtils.Remove(files[$index], configBase),
                        () : void => {
                            copyNext($index + 1);
                        });
                } else {
                    $done();
                }
            };
            copyNext(0);
        }
    }
}
