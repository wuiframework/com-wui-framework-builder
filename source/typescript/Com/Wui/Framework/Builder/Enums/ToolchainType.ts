/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class ToolchainType extends BaseEnum {
        public static readonly NONE : string = "none";
        public static readonly CHROMIUM_RE : string = "chromiumre";
        public static readonly GCC : string = "gcc";
        public static readonly ARM : string = "arm";
        public static readonly MSVC : string = "msvc";
        public static readonly CLANG : string = "clang";
        public static readonly NODEJS : string = "nodejs";
        public static readonly APT_GET : string = "aptget";
        public static readonly STORE : string = "store";
        public static readonly ECLIPSE : string = "eclipse";
        public static readonly IDEA : string = "idea";
        public static readonly VISUAL_STUDIO : string = "vs";
        public static readonly PHONEGAP : string = "phonegap";
        public static readonly JDK : string = "jdk";
        public static readonly GYP : string = "gyp";
    }
}
