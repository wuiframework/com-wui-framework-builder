/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class ProductType extends BaseEnum {
        public static readonly APP : string = "app";
        public static readonly PLUGIN : string = "plugin";
        public static readonly STATIC : string = "static";
        public static readonly SHARED : string = "shared";
        public static readonly INSTALLER : string = "installer";
        public static readonly REPOSITORY : string = "repository";
        public static readonly PACKAGE : string = "package";
    }
}
