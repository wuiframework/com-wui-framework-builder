/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class OSType extends BaseEnum {
        public static readonly WIN : string = "win";
        public static readonly LINUX : string = "linux";
        public static readonly MAC : string = "mac";
        public static readonly ANDROIND : string = "android";
        public static readonly WINPHONE : string = "winphone";
        public static readonly IOS : string = "ios";
        public static readonly IMX : string = "imx";
        public static readonly OTHER : string = "other";
    }
}
