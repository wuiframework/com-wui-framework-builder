/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class CliTaskType extends BaseEnum {
        public static readonly INSTALL : string = "install";
        public static readonly EAP : string = "eap";
        public static readonly ALPHA : string = "alpha";
        public static readonly BETA : string = "beta";
        public static readonly PROD : string = "prod";
        public static readonly DEV : string = "dev";
        public static readonly DEV_SKIP_TEST : string = "dev-skip-test";
        public static readonly REBUILD : string = "rebuild";
        public static readonly REBUILD_DEV : string = "rebuild-dev";
        public static readonly REBUILD_DEV_SKIP_TEST : string = "rebuild-dev-skip-test";
    }
}
