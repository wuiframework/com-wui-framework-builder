/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class StringReplaceType extends BaseEnum {
        public static readonly IMPORTS_PREPARE : string = "importsPrepare";
        public static readonly IMPORTS_FINISH : string = "importsFinish";
        public static readonly FOR_EACH : string = "forEach";
        public static readonly VARIABLES : string = "variables";
        public static readonly VARIABLES_PLUGIN : string = "variablesPlugin";
        public static readonly DEPENDENCIES : string = "dependencies";
        public static readonly XCPP : string = "xcpp";
    }
}
