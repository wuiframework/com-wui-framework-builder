/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class WindowsServiceStatus extends BaseEnum {
        public static readonly UNDEFINED : string = "undefined";
        public static readonly DOES_NOT_EXIST : string = "service does not exist";
        public static readonly RUNNING : string = "RUNNING";
        public static readonly NOT_STARTED : string = "service is not started";
        public static readonly STOPPED : string = "STOPPED";
        public static readonly START_PENDING : string = "START_PENDING";
        public static readonly STOP_PENDING : string = "STOP_PENDING";
    }
}
