/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class ResourceType extends BaseEnum {
        public static readonly CONFIG : string = "CONFIG";
        public static readonly RESOURCE : string = "RESOURCES";
        public static readonly ONLINE : string = "ONLINE";
    }
}
