/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Builder.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;

    export class Snippets extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testAutoconect() : void {
            const connector : FileSystemHandlerConnector = new FileSystemHandlerConnector(false,
                this.getRequest().getHostUrl() + "connector.config.jsonp", WebServiceClientType.WUI_DESKTOP_CONNECTOR);
            connector.getTempPath().Then(($path : string) : void => {
                Echo.Printf($path);
            });
        }
    }
}
/* dev:end */
