# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(TEST_CONFIGURE app_target test_target test_lib_target path version attributes)

endmacro()
