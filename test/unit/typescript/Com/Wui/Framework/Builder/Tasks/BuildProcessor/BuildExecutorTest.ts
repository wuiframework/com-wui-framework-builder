/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.BuildProcessor {
    "use strict";
    import TaskEnvironment = Com.Wui.Framework.Builder.Structures.TaskEnvironment;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class BuildExecutorTest extends UnitTestRunner {
        private executor : BuildExecutor;
        private env : TaskEnvironment;

        constructor() {
            super();
            // this.setMethodFilter("testgetDependenciesTreeDefault");
        }

        public testDefault() : void {
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "compress:deploy",
                "build-check:dev-skip-test",
                "builder-service:start"
            ]);
        }

        public testNodejs() : void {
            this.env.Project().target.toolchain = "nodejs";
            this.env.Project().target.platforms = "win64";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "copy-local-scripts:schema",
                "nodejs-packaging",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-win-nodejs",
                "build-check:dev-skip-test"
            ]);
        }

        public testInstaller() : void {
            this.env.Project().target.platforms = "installer";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",
                "product-settings:0",
                "copy-local-scripts:target",
                "selfextractor",
                "upx",
                "code-sign",
                "build-check:dev-skip-test"
            ]);

            this.env.Project().target.toolchain = "nodejs";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",
                "product-settings:0",
                "copy-local-scripts:target",
                "selfextractor",
                "upx",
                "code-sign",
                "build-check:dev-skip-test"
            ]);
        }

        public testLib() : void {
            this.env.Properties().projectHas.Cpp.Source = () : boolean => {
                return true;
            };
            this.env.Project().target.platforms = "libwin64-shared";
            this.env.Project().target.toolchain = "gcc";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), (EnvironmentHelper.IsWindows() ? [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "upx",
                "compress:deploy",
                "release-platform:shared-win-gcc",
                "build-check:dev-skip-test"
            ] : []));

            this.env.Project().target.platforms = "liblinux32-shared";
            this.env.Project().target.toolchain = "arm";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), (EnvironmentHelper.IsLinux() ? [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "upx",
                "compress:deploy",
                "build-check:dev-skip-test"
            ] : [
                "builder-service:stop",
                "project-cleanup:init",
                "build-check:dev-skip-test"
            ]));
        }

        public testApp() : void {
            this.env.Project().target.platforms = "win32";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "compress:deploy",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-chromiumre",
                "wui-modules-release:chromiumre",
                "upx",
                "code-sign",
                "compress:deploy",
                "build-check:dev-skip-test"
            ]);
            assert.deepEqual(this.executor.getDependenciesTree("alpha"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:eap",
                "compress:deploy",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-chromiumre",
                "wui-modules-release:chromiumre",
                "upx",
                "code-sign",
                "project-cleanup:prod",
                "compress:deploy",
                "build-check:eap"
            ]);
            assert.deepEqual(this.executor.getDependenciesTree("prod"), [
                "builder-service:stop",
                "wui-modules-download:force",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:prod",
                "compress:deploy",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-chromiumre",
                "wui-modules-release:chromiumre",
                "upx",
                "code-sign",
                "project-cleanup:prod",
                "compress:deploy",
                "build-check:prod"
            ]);

            this.env.Project().target.toolchain = "gcc";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-win-gcc",
                "build-check:dev-skip-test"
            ]);

            this.env.Properties().projectHas.Cpp.Source = () : boolean => {
                return true;
            };
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-win-gcc",
                "build-check:dev-skip-test"
            ]);
        }

        public testPlugin() : void {
            this.env.Project().target.platforms = "eclipse";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "compress:deploy",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-eclipse",
                "wui-modules-release:eclipse",
                "string-replace:variables-plugin",
                "compress:pluginTarget",
                "compress:deployJava",
                "build-check:dev-skip-test"
            ]);

            this.env.Project().target.platforms = "idea";
            assert.deepEqual(this.executor.getDependenciesTree("alpha"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:eap",
                "compress:deploy",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-idea",
                "wui-modules-release:idea",
                "string-replace:variables-plugin",
                "project-cleanup:prod",
                "compress:pluginTarget",
                "compress:deployJava",
                "build-check:eap"
            ]);
        }

        public testMobile() : void {
            this.env.Project().target.platforms = "android";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "compress:deploy",
                "var-replace",
                "copy-local-scripts:target",
                "copy-scripts:phonegap",
                "copy-local-scripts:phonegap",
                "compress:phonegap",
                "phonegap-build",
                "compress:deploy",
                "release-platform:app-android-phonegap",
                "build-check:dev-skip-test"
            ]);

            this.env.Project().target.platforms = "winphone";
            assert.deepEqual(this.executor.getDependenciesTree("alpha"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:eap",
                "compress:deploy",
                "var-replace",
                "copy-local-scripts:target",
                "project-cleanup:prod",
                "copy-scripts:phonegap",
                "copy-local-scripts:phonegap",
                "compress:phonegap",
                "phonegap-build",
                "compress:deploy",
                "release-platform:app-winphone-phonegap",
                "build-check:eap"
            ]);
        }

        public testJava() : void {
            this.env.Properties().projectHas.Java.Source = () : boolean => {
                return true;
            };
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "java-packaging",
                "compress:deploy",
                "build-check:dev-skip-test"
            ]);

            this.env.Project().target.platforms = "eclipse";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "java-packaging:eclipse",
                "compress:deploy",
                "build-check:dev-skip-test"
            ]);

            this.env.Project().target.platforms = "idea";
            assert.deepEqual(this.executor.getDependenciesTree("alpha"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:eap",
                "java-packaging:idea",
                "compress:deploy",
                "build-check:eap"
            ]);
        }

        public testMultiPlatform() : void {
            this.env.Project().target.platforms = ["web", "win32"];
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",

                "product-settings:0",
                "build-machine:dev-skip-test",
                "compress:deploy",
                "release-platform:static-win-none",

                "product-settings:1",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-chromiumre",
                "wui-modules-release:chromiumre",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-win-chromiumre",

                "build-check:dev-skip-test",
                "builder-service:start"
            ]);

            this.env.Project().target.toolchain = "nodejs";
            this.env.Project().target.platforms = ["win64", "installer"];
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",

                "product-settings:0",
                "build-machine:dev-skip-test",
                "copy-local-scripts:schema",
                "nodejs-packaging",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-win-nodejs",

                "product-settings:1",
                "copy-local-scripts:target",
                "selfextractor",
                "upx",
                "code-sign",
                "release-platform:installer-win-nodejs",

                "build-check:dev-skip-test"
            ]);

            this.env.Properties().projectHas.Java.Source = () : boolean => {
                return true;
            };
            this.env.Project().target.toolchain = "";
            this.env.Project().target.platforms = ["idea", "eclipse"];
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",

                "product-settings:0",
                "build-machine:dev-skip-test",
                "java-packaging:idea",
                "compress:deploy",
                "release-platform:shared-win-idea",

                "product-settings:1",
                "plugin-clean-compile",
                "build-machine:product-dev-skip-test",
                "java-packaging:eclipse",
                "compress:deploy",
                "release-platform:shared-win-eclipse",

                "build-check:dev-skip-test"
            ]);
        }

        public testReleases() : void {
            this.env.Project().target.platforms = ["win32", "installer"];
            this.env.Project().releases = <any>{
                internal: {
                    skipped: false
                },
                public  : {
                    skipped: false
                }
            };
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",

                "product-settings:0",
                "build-machine:dev-skip-test",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-chromiumre",
                "wui-modules-release:chromiumre",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-win-chromiumre",

                "product-settings:1",
                "copy-local-scripts:target",
                "selfextractor",
                "upx",
                "code-sign",
                "release-platform:installer-win-none",

                "product-settings:2",
                "build-machine:dev-skip-test",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-chromiumre",
                "wui-modules-release:chromiumre",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-win-chromiumre",

                "product-settings:3",
                "copy-local-scripts:target",
                "selfextractor",
                "upx",
                "code-sign",
                "release-platform:installer-win-none",

                "build-check:dev-skip-test"
            ]);
        }

        public testReleasesSinglePlatform() : void {
            this.env.Project().target.platforms = ["win32"];
            this.env.Project().releases = <any>{
                internal: {
                    skipped: false
                },
                public  : {
                    skipped: false
                }
            };
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "wui-modules-download",
                "project-cleanup:init",

                "product-settings:0",
                "build-machine:dev-skip-test",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-chromiumre",
                "wui-modules-release:chromiumre",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-win-chromiumre",

                "product-settings:1",
                "build-machine:dev-skip-test",
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-chromiumre",
                "wui-modules-release:chromiumre",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-win-chromiumre",

                "build-check:dev-skip-test"
            ]);
        }

        public testGyp() : void {
            this.env.Project().target.platforms = "app-win-gyp";
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "compress:deploy",
                "release-platform:app-win-gyp",
                "build-check:dev-skip-test"
            ]);
        }

        public testGypRelease() : void {
            this.env.Project().releases = <any>{
                imx  : {
                    platforms: "app-imx-gyp",
                    skipped  : false
                },
                linux: {
                    platforms: "app-linux-gyp",
                    skipped  : false
                }
            };
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",
                "product-settings:0",
                "build-machine:dev-skip-test",
                "compress:deploy",
                "release-platform:app-imx-gyp",

                "product-settings:1",
                "build-machine:dev-skip-test",
                "compress:deploy",
                "release-platform:app-linux-gyp",

                "build-check:dev-skip-test"
            ]);
        }

        public testCppRelease() : void {
            this.env.Project().releases = <any>{
                imx  : {
                    platforms: "static-imx-gcc",
                    skipped  : false
                },
                linux: {
                    platforms: "app-linux-gcc",
                    skipped  : false
                }
            };
            assert.deepEqual(this.executor.getDependenciesTree("dev-skip-test"), [
                "builder-service:stop",
                "project-cleanup:init",

                "product-settings:0",
                "build-machine:dev-skip-test",
                "upx",
                "compress:deploy",
                "release-platform:static-imx-gcc",

                "product-settings:1",
                "build-machine:dev-skip-test",
                "upx",
                "code-sign",
                "compress:deploy",
                "release-platform:app-linux-gcc",

                "build-check:dev-skip-test"
            ]);
        }

        public testgetTargetProducts() : void {
            this.env.Project(<any>{
                releases: {
                    Linux : {
                        platforms: ["app-linux-gyp", "static-linux-arm"]
                    },
                    Linux2: {
                        platforms: ["app-linux-gcc", "static-linux-docker"],
                        skipped  : true
                    },
                    Win   : {
                        platforms: ["app-win-gyp", "static-win-arm"]
                    },
                    iMX   : {
                        platforms: ["static-imx-gyp", "app-imx-gyp"]
                    }
                },
                target  : {
                    name     : "ExecutorTest",
                    platforms: []
                }
            });
            this.executor.setEnvironment(this.env);

            assert.equal(JSON.stringify(this.executor.getTargetProducts()),
                JSON.stringify([
                    {type: "app", os: "linux", arch: "x64", toolchain: "gyp"},
                    {type: "static", os: "linux", arch: "x64", toolchain: "arm"},
                    {type: "app", os: "win", arch: "x64", toolchain: "gyp"},
                    {type: "static", os: "win", arch: "x64", toolchain: "arm"},
                    {type: "static", os: "imx", arch: "x64", toolchain: "gyp"},
                    {type: "app", os: "imx", arch: "x64", toolchain: "gyp"}
                ])
            );
        }

        protected setUp() : void {
            this.env = new TaskEnvironment();
            this.env.Project(<any>{
                releases: {},
                target  : {
                    name     : "ExecutorTest",
                    platforms: ["web"]
                }
            });
            this.env.Properties(<any>{
                projectHas: {
                    Cpp : {
                        Source() : boolean {
                            return false;
                        }
                    },
                    Java: {
                        Source() : boolean {
                            return false;
                        }
                    }
                }
            });
            this.env.ProgramArgs(new ProgramArgs());
            (<any>Loader.getInstance()).getEnvironmentArgs().project = this.env.Project();
            this.executor = new BuildExecutor();
            this.executor.setEnvironment(this.env);
        }
    }
}
