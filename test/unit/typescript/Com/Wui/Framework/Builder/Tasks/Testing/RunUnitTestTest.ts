/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.Testing {
    "use strict";
    import TaskEnvironment = Com.Wui.Framework.Builder.Structures.TaskEnvironment;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;
    import TypeScriptCompile = Com.Wui.Framework.Builder.Tasks.TypeScript.TypeScriptCompile;
    import XCppTest = Com.Wui.Framework.Builder.Tasks.XCpp.XCppTest;
    import MavenEnv = Com.Wui.Framework.Builder.Tasks.Java.MavenEnv;

    export class RunUnitTestTest extends UnitTestRunner {
        private executor : RunUnitTest;
        private env : TaskEnvironment;
        private type : string;

        constructor() {
            super();
            // this.setMethodFilter("testDefault");
        }

        public testDefault() : void {
            assert.deepEqual(this.executor.getDependenciesTree(), [
                "product-settings:0",
                "install:dev",
                "mock-server",
                "builder-server:start",
                "var-replace",
                "copy-staticfiles:targetresource",
                "copy-staticfiles:resource",
                "typescript-interfaces:test",
                "typescript-reference:test",
                "typescript:unit",
                "typescript-test:" + this.type
            ]);
            assert.deepEqual(TypeScriptCompile.getConfig().unit.src, [
                this.env.Properties().sources + "/*.ts", this.env.Properties().dependencies + "/*.ts",
                "test/unit/**/*.ts",
                "bin/resource/scripts/UnitTestRunner.ts",
                "!**/*.d.ts"
            ]);

            assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
                "install:dev",
                "mock-server",
                "builder-server:start",
                "typescript-test:" + this.type
            ]);
            assert.deepEqual(TypeScriptCompile.getConfig().unit.src, [
                this.env.Properties().sources + "/*.ts", this.env.Properties().dependencies + "/*.ts",
                "test/unit/**/*.ts",
                "bin/resource/scripts/UnitTestRunner.ts",
                "!**/*.d.ts"
            ]);
        }

        public testTypeScriptWithDependencies() : void {
            this.env.ProgramArgs().getOptions().withDependencies = true;
            assert.deepEqual(this.executor.getDependenciesTree(), [
                "product-settings:0",
                "install:dev",
                "mock-server",
                "builder-server:start",
                "var-replace",
                "copy-staticfiles:targetresource",
                "copy-staticfiles:resource",
                "typescript-interfaces:test",
                "typescript-reference:test",
                "typescript:unit",
                "typescript-test:" + this.type
            ]);
            assert.deepEqual(TypeScriptCompile.getConfig().unit.src, [
                this.env.Properties().sources + "/*.ts", this.env.Properties().dependencies + "/*.ts",
                "test/unit/**/*.ts", "dependencies/*/test/unit/**/*.ts",
                "bin/resource/scripts/UnitTestRunner.ts",
                "!**/*.d.ts"
            ]);
        }

        public testCpp() : void {
            this.env.Properties().projectHas.TypeScript.Tests = () : boolean => {
                return false;
            };
            this.env.Properties().projectHas.Cpp.Source = () : boolean => {
                return true;
            };
            this.env.Properties().projectHas.Cpp.Tests = () : boolean => {
                return true;
            };
            assert.deepEqual(this.executor.getDependenciesTree(), [
                "product-settings:0",
                "install:dev",
                "mock-server",
                "builder-server:start",
                "var-replace",
                "string-replace:xcpp",
                "copy-staticfiles:targetresource",
                "copy-staticfiles:resource",
                "xcpp-interfaces:source",
                "xcpp-reference:source",
                "xcpp-compile:" + this.type,
                "copy-staticfiles:targetresource-postbuild",
                "test:xcpp" + this.type
            ]);
            assert.equal(XCppTest.getConfig().filter, "*");

            assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
                "install:dev",
                "mock-server",
                "builder-server:start",
                "test:xcpp" + this.type
            ]);
            assert.equal(XCppTest.getConfig().filter, "*");
        }

        public testJava() : void {
            this.env.Properties().projectHas.TypeScript.Tests = () : boolean => {
                return false;
            };
            this.env.Properties().projectHas.Java.Source = () : boolean => {
                return true;
            };
            this.env.Properties().projectHas.Java.Tests = () : boolean => {
                return true;
            };
            assert.deepEqual(this.executor.getDependenciesTree(), [
                "product-settings:0",
                "install:dev",
                "mock-server",
                "builder-server:start",
                "var-replace",
                "copy-staticfiles:targetresource",
                "copy-staticfiles:resource",
                "maven-env:init",
                "maven-build:" + this.type,
                "maven-env:clean"
            ]);
            assert.deepEqual(MavenEnv.getConfig().unit.src, []);

            assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
                "install:dev",
                "mock-server",
                "builder-server:start",
                "maven-env:init",
                "maven-build:" + this.type,
                "maven-env:clean"
            ]);
            assert.deepEqual(MavenEnv.getConfig().unit.src, []);
        }

        public testSingleTypeScript() : void {
            this.env.ProgramArgs().getOptions().file = "/unit/someTest.ts";
            assert.deepEqual(this.executor.getDependenciesTree(), [
                "product-settings:0",
                "install:dev",
                "mock-server",
                "builder-server:start",
                "var-replace",
                "copy-staticfiles:targetresource",
                "copy-staticfiles:resource",
                "typescript-interfaces:test",
                "typescript-reference:test",
                "typescript:unit",
                "typescript-test:" + this.type
            ]);
            assert.deepEqual(TypeScriptCompile.getConfig().unit.src, [
                this.env.Properties().sources + "/*.ts", this.env.Properties().dependencies + "/*.ts",
                "test/unit/someTest.ts",
                "bin/resource/scripts/UnitTestRunner.ts",
                "!**/*.d.ts"
            ]);

            assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
                "install:dev",
                "mock-server",
                "builder-server:start",
                "typescript-test:" + this.type
            ]);
            assert.deepEqual(TypeScriptCompile.getConfig().unit.src, [
                this.env.Properties().sources + "/*.ts", this.env.Properties().dependencies + "/*.ts",
                "test/unit/someTest.ts",
                "bin/resource/scripts/UnitTestRunner.ts",
                "!**/*.d.ts"
            ]);
        }

        public testSingleCpp() : void {
            this.env.ProgramArgs().getOptions().file = "/unit/someTest.cpp";
            this.env.Properties().projectHas.TypeScript.Tests = () : boolean => {
                return false;
            };
            this.env.Properties().projectHas.Cpp.Source = () : boolean => {
                return true;
            };
            this.env.Properties().projectHas.Cpp.Tests = () : boolean => {
                return true;
            };
            assert.deepEqual(this.executor.getDependenciesTree(), [
                "product-settings:0",
                "install:dev",
                "mock-server",
                "builder-server:start",
                "var-replace",
                "string-replace:xcpp",
                "copy-staticfiles:targetresource",
                "copy-staticfiles:resource",
                "xcpp-interfaces:source",
                "xcpp-reference:source",
                "xcpp-compile:" + this.type,
                "copy-staticfiles:targetresource-postbuild",
                "test:xcpp" + this.type
            ]);
            assert.equal(XCppTest.getConfig().filter, "unit/someTest.*");

            assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
                "install:dev",
                "mock-server",
                "builder-server:start",
                "test:xcpp" + this.type
            ]);
            assert.equal(XCppTest.getConfig().filter, "unit/someTest.*");
        }

        public testSingleJava() : void {
            this.env.ProgramArgs().getOptions().file = "/unit/someTest.java";
            this.env.Properties().projectHas.TypeScript.Tests = () : boolean => {
                return false;
            };
            this.env.Properties().projectHas.Java.Source = () : boolean => {
                return true;
            };
            this.env.Properties().projectHas.Java.Tests = () : boolean => {
                return true;
            };
            assert.deepEqual(this.executor.getDependenciesTree(), [
                "product-settings:0",
                "install:dev",
                "mock-server",
                "builder-server:start",
                "var-replace",
                "copy-staticfiles:targetresource",
                "copy-staticfiles:resource",
                "maven-env:init",
                "maven-build:" + this.type,
                "maven-env:clean"
            ]);
            assert.deepEqual(MavenEnv.getConfig().unit.src, ["unit.someTest"]);

            assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
                "install:dev",
                "mock-server",
                "builder-server:start",
                "maven-env:init",
                "maven-build:" + this.type,
                "maven-env:clean"
            ]);
            assert.deepEqual(MavenEnv.getConfig().unit.src, ["unit.someTest"]);
        }

        protected setUp() : void {
            this.type = "unit";
            this.env = new TaskEnvironment();
            this.env.Project(<any>{
                releases: {},
                target  : {
                    name     : "ExecutorTest",
                    platforms: ["web"]
                }
            });
            this.env.Properties(<any>{
                dependencies: "dependencies/*/source/**",
                projectHas  : {
                    Cpp       : {
                        Source() : boolean {
                            return false;
                        },
                        Tests() : boolean {
                            return false;
                        }
                    },
                    Java      : {
                        Source() : boolean {
                            return false;
                        },
                        Tests() : boolean {
                            return false;
                        }
                    },
                    TypeScript: {
                        Tests() : boolean {
                            return true;
                        }
                    }
                },
                sources     : "source/**"
            });
            this.env.ProgramArgs(new ProgramArgs());
            (<any>Loader.getInstance()).getEnvironmentArgs().properties = this.env.Properties();
            (<any>Loader.getInstance()).getEnvironmentArgs().project = this.env.Project();
            (<any>TypeScriptCompile).config = null;
            (<any>XCppTest).config = null;
            (<any>MavenEnv).config = null;

            this.executor = new RunUnitTest();
            this.executor.setEnvironment(this.env);
        }
    }
}
