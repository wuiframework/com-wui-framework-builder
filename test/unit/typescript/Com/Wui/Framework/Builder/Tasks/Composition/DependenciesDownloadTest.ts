/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.BuildProcessor {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import DependenciesDownload = Com.Wui.Framework.Builder.Tasks.Composition.DependenciesDownload;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;
    import TaskEnvironment = Com.Wui.Framework.Builder.Structures.TaskEnvironment;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IProjectDependency = Com.Wui.Framework.Builder.Interfaces.IProjectDependency;

    export class MockDependenciesDownload extends DependenciesDownload {
        public processSingleSource($dependencyName : string, $source : Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocation,
                                   $dependency : Com.Wui.Framework.Builder.Interfaces.IProjectDependency, $targetPath : string,
                                   $callback : () => void) {
            super.processSingleSource($dependencyName, $source, $dependency, $targetPath, $callback);
        }

        public ResolveDependencyPath($path : string | Com.Wui.Framework.Builder.Interfaces.IProjectDependencyLocationPath) : string[] {
            return super.ResolveDependencyPath($path);
        }

        public isPlatformMatch($platform : string) : boolean {
            return super.isPlatformMatch($platform);
        }

        public download($location : string | Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions, $targetPath : string,
                        $callback : () => void) : void {
            super.download($location, $targetPath, $callback);
        }

        public processDependency($dependencyName : string,
                                 $dependency : Com.Wui.Framework.Builder.Interfaces.IProjectDependency | string,
                                 $callback : () => void) : void {
            super.processDependency($dependencyName, $dependency, $callback);
        }

        public collectDependencies($dependencies : Com.Wui.Framework.Builder.Interfaces.IProjectDependency[]) : string[] {
            return super.collectDependencies($dependencies);
        }

        public processDependencies($dependencies : Com.Wui.Framework.Builder.Interfaces.IProjectDependency[],
                                   $callback : () => void) : void {
            super.processDependencies($dependencies, $callback);
        }
    }

    export class DependenciesDownloadTest extends UnitTestRunner {
        private dependenciesDownload : MockDependenciesDownload;
        private fileSystem : FileSystemHandler;
        private readonly env : TaskEnvironment;
        private dependencies : any;
        private readonly dependencyNames : string[];

        constructor() {
            super();
            // this.setMethodFilter("testCollectDependencies");
            this.env = new TaskEnvironment();
            this.dependencyNames = [];
        }

        public testParseLocation() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                for (const item in this.dependencies) {
                    if (this.dependencies.hasOwnProperty(item) && !ObjectValidator.IsEmptyOrNull(this.dependencyNames) &&
                        (this.dependencyNames.indexOf(item) !== -1 || this.dependencyNames.length === 0)) {
                        if (this.dependencies[item].hasOwnProperty("releases")) {
                            this.env.Project().releases = this.dependencies[item].releases;
                        } else {
                            this.env.Project().releases = [];
                        }
                        (<any>this.dependenciesDownload).buildExecutor.setEnvironment(this.env);
                        assert.equal(
                            JSON.stringify(this.dependenciesDownload.ParseLocation(this.dependencies[item].input)),
                            JSON.stringify(this.dependencies[item].output), "Failed case: " + item);
                    }
                }
                $done();
            };
        }

        // todo to much hacks, not sure how to test it
        public testgetSource() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const testItems : string[] = [];
                for (const item in this.dependencies) {
                    if (this.dependencies.hasOwnProperty(item) && !ObjectValidator.IsEmptyOrNull(this.dependencyNames) &&
                        (this.dependencyNames.indexOf(item) !== -1 || this.dependencyNames.length === 0)) {
                        testItems.push(item);
                    }
                }

                const hackClone : any = ($repoSource : string, $branch : string, $targetPath : string,
                                         $callback : ($repoSource : string, $branch : string, $targetPath : string) => void) : void => {
                    $callback($repoSource, $branch, $targetPath);
                };
                const hackDownload : any = ($location : string | any, $targetPath : string, $callback : () => void) : void => {
                    $callback();
                };
                (<any>this.dependenciesDownload).clone = hackClone;
                (<any>this.dependenciesDownload).download = hackDownload;

                let index : number = 0;
                const processNext : any = () : void => {
                    if (index < testItems.length) {
                        const itemName : string = testItems[index];
                        const testItem : any = this.dependencies[itemName];
                        if (testItem.hasOwnProperty("releases")) {
                            this.env.Project().releases = testItem.releases;
                        } else {
                            this.env.Project().releases = [];
                        }
                        (<any>this.dependenciesDownload).buildExecutor.setEnvironment(this.env);

                        this.dependenciesDownload.getSource(itemName, testItem.input, testItem, "<path>", () : void => {
                            // assert.equal(
                            //     JSON.stringify(this.dependenciesDownload.ParseLocation(this.dependencies[item].input)),
                            //     JSON.stringify(this.dependencies[item].output), "Failed case: " + item);
                            index++;
                            processNext();
                        });
                    } else {
                        $done();
                    }
                };

                processNext();
            };
        }

        public testCollectDependencies() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const dependenciesDownload : MockDependenciesDownload = new MockDependenciesDownload();
                const deps : any = {
                    dep1: "git+https://some.org/project#branch",
                    dep2: "file://some.org/project",
                    dep3: "git+https://some.org/project#branch"
                };

                const backup : any = LogIt.Error;
                let errorCalled : boolean = false;
                LogIt.Error = () : void => {
                    errorCalled = true;
                };
                assert.deepEqual(dependenciesDownload.collectDependencies(deps), ["dep1", "dep2", "dep3"]);
                deps.dep3 = "git+https://some.org/project#branch1";
                assert.deepEqual(dependenciesDownload.collectDependencies(deps), []);
                assert.ok(errorCalled);
                deps.dep3 = "!git+https://some.org/project#branch1";
                assert.deepEqual(dependenciesDownload.collectDependencies(deps), []);
                assert.equal(deps.dep3, "git+https://some.org/project#branch1");
                LogIt.Error = backup;
                $done();
            };
        }

        protected setUp() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.env.Project(<any>{
                    releases: {},
                    target  : {
                        name     : "DependenciesDownloadTest",
                        platforms: []
                    }
                });
                this.env.Properties(<any>{
                    projectHas: {
                        Cpp : {
                            Source() : boolean {
                                return false;
                            }
                        },
                        Java: {
                            Source() : boolean {
                                return false;
                            }
                        },
                        PHP : {
                            Source() : boolean {
                                return false;
                            }
                        }
                    }
                });
                (<any>Loader.getInstance()).getEnvironmentArgs().project = this.env.Project();
                this.dependenciesDownload = new MockDependenciesDownload();
                $done();
            };
        }

        protected before() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem = Loader.getInstance().getFileSystemHandler();
                this.env.ProgramArgs(new ProgramArgs());

                const stripJsonComments : any = require("strip-json-comments");
                this.dependencies = JSON.parse(stripJsonComments(this.fileSystem.Read(this.getAbsoluteRoot() +
                    "/test/resource/data/Com/Wui/Framework/Builder/Tasks/Composition/dependenciesDownloadData.json").toString()));
                $done();
            };
        }
    }
}
