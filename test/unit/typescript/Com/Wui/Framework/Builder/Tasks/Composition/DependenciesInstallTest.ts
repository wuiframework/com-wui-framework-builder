/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Tasks.BuildProcessor {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import FileSystemHandler = Com.Wui.Framework.Builder.Connectors.FileSystemHandler;
    import IProjectDependency = Com.Wui.Framework.Builder.Interfaces.IProjectDependency;
    import DependenciesInstall = Com.Wui.Framework.Builder.Tasks.Composition.DependenciesInstall;
    import TaskEnvironment = Com.Wui.Framework.Builder.Structures.TaskEnvironment;
    import IProjectDependencyScriptConfig = Com.Wui.Framework.Builder.Interfaces.IProjectDependencyScriptConfig;
    import IProperties = Com.Wui.Framework.Builder.Interfaces.IProperties;
    import ProgramArgs = Com.Wui.Framework.Builder.Structures.ProgramArgs;

    export class MockDependenciesInstall extends DependenciesInstall {
        public findScript($basePath : string, $scriptName : string) : string {
            return super.findScript($basePath, $scriptName);
        }

        public configDataLookup($dependencyName : string, $scriptType : string,
                                $data : IProjectDependencyScriptConfig) : IProjectDependencyScriptConfig {
            return super.configDataLookup($dependencyName, $scriptType, $data);
        }

        public configure($dependencyName : string, $dependency : IProjectDependency) : boolean {
            return super.configure($dependencyName, $dependency);
        }

        public install($dependencyName : string, $dependency : IProjectDependency, $callback : () => void) : void {
            super.install($dependencyName, $dependency, $callback);
        }

        public processDependency($configureOnly : boolean, $dependencyName : string, $dependency : IProjectDependency | string,
                                 $callback : () => void) : void {
            super.processDependency($configureOnly, $dependencyName, $dependency, $callback);
        }

        public processDependencies($configureOnly : boolean, $callback : () => void) : void {
            super.processDependencies($configureOnly, $callback);
        }
    }

    export class DependenciesInstallTest extends UnitTestRunner {
        private env : TaskEnvironment;
        private dependenciesInstall : MockDependenciesInstall;
        private fileSystem : FileSystemHandler;
        private testRoot : string;

        constructor() {
            super();
            this.setMethodFilter("testInstall");
        }

        protected testFindScript() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const findRoot : string = this.testRoot + "/install/findScript";

                assert.equal(this.dependenciesInstall.findScript(findRoot, "case1Configure.cmake"),
                    findRoot + "/bin/resource/scripts/case1Configure.cmake");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case2Configure.cmake"),
                    findRoot + "/bin/xcpp_scripts/case2Configure.cmake");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case3Configure.cmake"),
                    findRoot + "/resource/scripts/case3Configure.cmake");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case4Configure.cmake"),
                    findRoot + "/xcpp_scripts/case4Configure.cmake");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case5Configure.cmake"),
                    findRoot + "/dependencies/custom-dependency/resource/scripts/case5Configure.cmake");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case6Configure.cmake"),
                    findRoot + "/dependencies/custom-dependency/xcpp_scripts/case6Configure.cmake");

                assert.equal(this.dependenciesInstall.findScript(findRoot, "case1Install.js"),
                    findRoot + "/bin/resource/scripts/case1Install.js");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case2Install.js"),
                    findRoot + "/bin/xcpp_scripts/case2Install.js");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case3Install.js"),
                    findRoot + "/resource/scripts/case3Install.js");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case4Install.js"),
                    findRoot + "/xcpp_scripts/case4Install.js");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case5Install.js"),
                    findRoot + "/dependencies/custom-dependency/resource/scripts/case5Install.js");
                assert.equal(this.dependenciesInstall.findScript(findRoot, "case6Install.js"),
                    findRoot + "/dependencies/custom-dependency/xcpp_scripts/case6Install.js");

                $done();
            };
        }

        protected testConfigDataLookup() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                let config : IProjectDependencyScriptConfig = <IProjectDependencyScriptConfig>{
                    "attributes"               : ["attrA", "attrB"],
                    "ignore-parent-attribute": false,
                    "name"                     : "some_install.js"
                };

                this.env.Properties(<IProperties>{projectBase: this.testRoot});
                this.dependenciesInstall.setEnvironment(this.env);

                assert.deepEqual(this.dependenciesInstall.configDataLookup("test-dependency", "configure-script", null), null);
                assert.deepEqual(this.dependenciesInstall.configDataLookup("test-dependency", "configure-script", config), {
                    "attributes"             : [
                        "attrA", "attrB", "attr1", "attr2"
                    ],
                    "ignore-parent-attribute": false,
                    "name"                   : "some_install.js"
                });
                config = <IProjectDependencyScriptConfig>{
                    "attributes"               : ["attrA", "attrB"],
                    "ignore-parent-attribute": true,
                    "name"                     : "some_install.js"
                };
                assert.deepEqual(this.dependenciesInstall.configDataLookup("test-dependency", "configure-script", config), {
                    "attributes"             : [
                        "attrA", "attrB"
                    ],
                    "ignore-parent-attribute": true,
                    "name"                   : "some_install.js"
                });

                $done();
            };
        }

        protected testConfigure() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.env.Properties(<IProperties>{projectBase: this.testRoot});
                this.dependenciesInstall.setEnvironment(this.env);
                const dependency : IProjectDependency = <IProjectDependency>{
                    "configure-script": {
                        attributes: ["muhehe"],
                        name      : "test_configure.cmake"
                    }
                };
                (<any>this.dependenciesInstall).configured = 0;

                assert.ok(this.dependenciesInstall.configure("test", dependency));
                assert.equal(dependency["configure-script"]["macro-name"], "TEST_CONFIGURE");
                assert.ok(this.fileSystem.Exists(dependency["configure-script"].path + "/test_configure.cmake"));
                assert.equal(this.dependenciesInstall.configure("test", null), false);
                assert.equal((<any>this.dependenciesInstall).configured, 1);

                $done();
            };
        }

        protected testInstall() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.env.Properties(<IProperties>{projectBase: this.testRoot});
                this.dependenciesInstall.setEnvironment(this.env);
                const dependency : IProjectDependency = <IProjectDependency>{
                    "install-script": {
                        attributes: ["muhehe"],
                        name      : "test_install.js"
                    },
                    "location"        : {
                        path: this.testRoot
                    }
                };

                (<any>this.dependenciesInstall).installed = 0;
                this.dependenciesInstall.install("test", dependency, () : void => {
                    assert.equal((<any>this.dependenciesInstall).installed, 1);
                    assert.equal(this.fileSystem.Exists(this.testRoot + "/install.log"), true);
                    this.fileSystem.Delete(this.testRoot + "/install.log");
                    $done();
                });
            };
        }

        protected setUp() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.env = new TaskEnvironment();
                this.env.Project(<any>{
                    releases: {},
                    target  : {
                        name     : "DependenciesInstallTest",
                        platforms: ["web"]
                    }
                });
                this.env.ProgramArgs(new ProgramArgs());
                this.env.Properties(<any>{
                    dependencies: "dependencies/*/source/**",
                    projectBase: this.testRoot + "/install/findScript",
                    projectHas  : {
                        Cpp       : {
                            Source() : boolean {
                                return false;
                            },
                            Tests() : boolean {
                                return false;
                            }
                        },
                        Java      : {
                            Source() : boolean {
                                return false;
                            },
                            Tests() : boolean {
                                return false;
                            }
                        },
                        TypeScript: {
                            Tests() : boolean {
                                return true;
                            }
                        }
                    },
                    sources     : "source/**"
                });
                (<any>Loader.getInstance()).getEnvironmentArgs().properties = this.env.Properties();
                (<any>Loader.getInstance()).getEnvironmentArgs().project = this.env.Project();

                this.dependenciesInstall = new MockDependenciesInstall();
                this.dependenciesInstall.setEnvironment(this.env);
                $done();
            };
        }

        protected before() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem = Loader.getInstance().getFileSystemHandler();
                this.testRoot = this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Builder/Tasks/Composition";

                const config : any = {
                    case1: {
                        dir    : this.testRoot + "/install/findScript/bin/resource/scripts",
                        scripts: [
                            "case1Configure.cmake",
                            "case1Install.js"
                        ]
                    },
                    case2: {
                        dir    : this.testRoot + "/install/findScript/bin/xcpp_scripts",
                        scripts: [
                            "case2Configure.cmake",
                            "case2Install.js"
                        ]
                    },
                    case3: {
                        dir    : this.testRoot + "/install/findScript/resource/scripts",
                        scripts: [
                            "case3Configure.cmake",
                            "case3Install.js"
                        ]
                    },
                    case4: {
                        dir    : this.testRoot + "/install/findScript/xcpp_scripts",
                        scripts: [
                            "case4Configure.cmake",
                            "case4Install.js"
                        ]
                    },
                    case5: {
                        dir    : this.testRoot + "/install/findScript/dependencies/custom-dependency/resource/scripts",
                        scripts: [
                            "case5Configure.cmake",
                            "case5Install.js"
                        ]
                    },
                    case6: {
                        dir    : this.testRoot + "/install/findScript/dependencies/custom-dependency/xcpp_scripts",
                        scripts: [
                            "case6Configure.cmake",
                            "case6Install.js"
                        ]
                    }
                };

                for (const item in config) {
                    if (config.hasOwnProperty(item)) {
                        this.fileSystem.CreateDirectory(config[item].dir);
                        for (const script of config[item].scripts) {
                            this.fileSystem.Write(config[item].dir + "/" + script, "data");
                        }
                    }
                }

                $done();
            };
        }

        protected after() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Delete(this.testRoot + "/install");
                $done();
            };
        }
    }
}
