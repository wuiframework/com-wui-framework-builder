/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Builder.Structures {
    "use strict";
    import ProductType = Com.Wui.Framework.Builder.Enums.ProductType;
    import OSType = Com.Wui.Framework.Builder.Enums.OSType;
    import ArchType = Com.Wui.Framework.Builder.Enums.ArchType;
    import ToolchainType = Com.Wui.Framework.Builder.Enums.ToolchainType;

    export class BuildProductArgsTest extends UnitTestRunner {
        private args : BuildProductArgs;

        constructor() {
            super();
            // this.setMethodFilter("testParseDefault");
        }

        public testParseDefault() : void {
            assert.equal(this.args.Value(), "static-win-none");
        }

        public testParseWeb() : void {
            this.args.Parse("web");
            assert.equal(this.args.Value(), "static-win-none");
            assert.equal(this.args.Type(), ProductType.STATIC);
            assert.equal(this.args.OS(), OSType.WIN);
            assert.equal(this.args.Arch(), ArchType.X64);
            assert.equal(this.args.Toolchain(), ToolchainType.NONE);

            this.setUp();
            this.args.Parse("web", "jdk");
            assert.equal(this.args.Value(), "shared-win-jdk");
        }

        public testParseWin() : void {
            this.args.Parse("win32");
            assert.equal(this.args.Value(), "app-win-chromiumre");

            this.setUp();
            this.args.Parse("win32", "gcc");
            assert.equal(this.args.Value(), "app-win-gcc");

            this.setUp();
            this.args.Parse("win32", "nodejs");
            assert.equal(this.args.Value(), "app-win-nodejs");

            this.setUp();
            this.args.Parse("win64");
            assert.equal(this.args.Value(), "app-win-chromiumre");

            this.setUp();
            this.args.Parse("win");
            assert.equal(this.args.Value(), "app-win-chromiumre");

            this.setUp();
            this.args.Parse("libwin32-static", "msvc");
            assert.equal(this.args.Value(), "static-win-msvc");
        }

        public testParseLinux() : void {
            this.args.Parse("linux32");
            assert.equal(this.args.Value(), "app-linux-gcc");

            this.setUp();
            this.args.Parse("linux64");
            assert.equal(this.args.Value(), "app-linux-gcc");

            this.setUp();
            this.args.Parse("linux");
            assert.equal(this.args.Value(), "app-linux-gcc");
        }

        public testParseMac() : void {
            this.args.Parse("mac32");
            assert.equal(this.args.Value(), "app-mac-none");

            this.setUp();
            this.args.Parse("mac64");
            assert.equal(this.args.Value(), "app-mac-none");

            this.setUp();
            this.args.Parse("mac");
            assert.equal(this.args.Value(), "app-mac-none");

            this.setUp();
            this.args.Parse("mac", "gcc");
            assert.equal(this.args.Value(), "app-mac-gcc");

            this.setUp();
            this.args.Parse("mac", "clang");
            assert.equal(this.args.Value(), "app-mac-clang");

            this.setUp();
            this.args.Parse({
                arch     : "x64",
                os       : "mac",
                toolchain: "clang",
                type     : "app"
            });
            assert.equal(this.args.Value(), "app-mac-clang");
        }

        public testParseMobile() : void {
            this.args.Parse("android");
            assert.equal(this.args.Value(), "app-android-phonegap");

            this.setUp();
            this.args.Parse("winphone");
            assert.equal(this.args.Value(), "app-winphone-phonegap");
        }

        public testParsePlugin() : void {
            this.args.Parse("eclipse");
            assert.equal(this.args.Value(), "plugin-win-eclipse");

            this.setUp();
            this.args.Parse("idea");
            assert.equal(this.args.Value(), "plugin-win-idea");

            this.setUp();
            this.args.Parse("eclipse", "jdk");
            assert.equal(this.args.Value(), "shared-win-eclipse");

            this.setUp();
            this.args.Parse("idea", "jdk");
            assert.equal(this.args.Value(), "shared-win-idea");
        }

        public testParseNodejs() : void {
            this.args.Parse("nodejs32");
            assert.equal(this.args.Value(), "shared-win-nodejs");

            this.setUp();
            this.args.Parse("app-win-nodejs");
            assert.equal(this.args.Value(), "app-win-nodejs");
            assert.equal(this.args.Type(), ProductType.APP);
            assert.equal(this.args.OS(), OSType.WIN);
            assert.equal(this.args.Arch(), ArchType.X64);
            assert.equal(this.args.Toolchain(), ToolchainType.NODEJS);
        }

        public testParseInstaller() : void {
            this.args.Parse("installer");
            assert.equal(this.args.Value(), "installer-win-none");
        }

        public testParseLib() : void {
            this.args.Parse("lib");
            assert.equal(this.args.Value(), "static-win-gcc");

            this.setUp();
            this.args.Parse("libwin64-static");
            assert.equal(this.args.Value(), "static-win-gcc");

            this.setUp();
            this.args.Parse("liblinux64-static");
            assert.equal(this.args.Value(), "static-linux-gcc");

            this.setUp();
            this.args.Parse("libwin64-shared");
            assert.equal(this.args.Value(), "shared-win-gcc");
        }

        public testParseArm() : void {
            this.args.Parse("arm");
            assert.equal(this.args.Value(), "static-linux-arm");

            this.setUp();
            this.args.Parse("arm", "nodejs");
            assert.equal(this.args.Value(), "app-imx-nodejs");

            this.setUp();
            this.args.Parse("liblinux64-shared", "arm");
            assert.equal(this.args.Value(), "shared-linux-arm");
        }

        /* tslint:disable: object-literal-sort-keys */
        public testSerializeDefault() : void {
            assert.equal(this.args.Value(), "static-win-none");
        }

        public testSerializeApp() : void {
            this.args.Parse({
                type     : "app",
                os       : "win",
                toolchain: "chromiumre"
            });
            assert.equal(this.args.Value(), "app-win-chromiumre");

            this.setUp();
            this.args.Parse({
                type     : "app",
                os       : "win",
                arch     : "x64",
                toolchain: "chromiumre"
            });
            assert.equal(this.args.Value(), "app-win-chromiumre");

            this.args.Parse({
                type     : "app",
                os       : "win",
                arch     : "x86",
                toolchain: "chromiumre"
            });
            assert.equal(this.args.Value(), "app-win32-chromiumre");

            this.args.Parse({
                toolchain: "chromiumre"
            });
            assert.equal(this.args.Value(), "app-win-chromiumre");

            this.setUp();
            this.args.Parse({
                type     : "app",
                os       : "win",
                arch     : "x64",
                toolchain: "gcc"
            });
            assert.equal(this.args.Value(), "app-win-gcc");
        }

        public testSerializeNodejs() : void {
            this.args.Parse({
                type     : "static",
                os       : "win",
                toolchain: "nodejs"
            });
            assert.equal(this.args.Value(), "static-win-nodejs");

            this.setUp();
            this.args.Parse({
                type     : "static",
                os       : "win",
                arch     : "x64",
                toolchain: "nodejs"
            });
            assert.equal(this.args.Value(), "static-win-nodejs");

            this.setUp();
            this.args.Parse({
                type     : "shared",
                os       : "win",
                arch     : "x64",
                toolchain: "nodejs"
            });
            assert.equal(this.args.Value(), "shared-win-nodejs");

            this.setUp();
            this.args.Parse({
                type     : "app",
                os       : "win",
                arch     : "x64",
                toolchain: "nodejs"
            });
            assert.equal(this.args.Value(), "app-win-nodejs");

            this.setUp();
            this.args.Parse({
                type     : "app",
                os       : "linux",
                toolchain: "nodejs"
            });
            assert.equal(this.args.Value(), "app-linux-nodejs");

            this.setUp();
            this.args.Parse({
                type     : "app",
                os       : "android",
                toolchain: "nodejs"
            });
            assert.equal(this.args.Value(), "app-android-nodejs");

            this.setUp();
            this.args.Parse({
                type     : "app",
                os       : "imx",
                toolchain: "nodejs"
            });
            assert.equal(this.args.Value(), "app-imx-nodejs");
        }

        public testSerializeInstaller() : void {
            this.args.Parse({
                type: "installer"
            });
            assert.equal(this.args.Value(), "installer-win-none");
        }

        public testSerializeLib() : void {
            this.args.Parse({
                type     : "shared",
                os       : "win",
                arch     : "x64",
                toolchain: "gcc"
            });
            assert.equal(this.args.Value(), "shared-win-gcc");

            this.setUp();
            this.args.Parse({
                type     : "shared",
                os       : "win",
                arch     : "x64",
                toolchain: "msvc"
            });
            assert.equal(this.args.Value(), "shared-win-msvc");

            this.setUp();
            this.args.Parse({
                type     : "shared",
                os       : "linux",
                toolchain: "gcc"
            });
            assert.equal(this.args.Value(), "shared-linux-gcc");

            this.setUp();
            this.args.Parse({
                type     : "shared",
                toolchain: "arm"
            });
            assert.equal(this.args.Value(), "shared-linux-arm");

            this.setUp();
            this.args.Parse({
                type     : "shared",
                os       : "linux",
                toolchain: "arm"
            });
            assert.equal(this.args.Value(), "shared-linux-arm");
        }

        public testSerializePackage() : void {
            this.args.Parse({
                type     : "package",
                os       : "android",
                toolchain: "store"
            });
            assert.equal(this.args.Value(), "package-android-store");

            this.setUp();
            this.args.Parse({
                type     : "package",
                toolchain: "apt-get"
            });
            assert.equal(this.args.Value(), "package-linux-aptget");

            this.setUp();
            this.args.Parse({
                type     : "package",
                os       : "linux",
                toolchain: "apt-get"
            });
            assert.equal(this.args.Value(), "package-linux-aptget");

            this.setUp();
            this.args.Parse({
                type     : "package",
                toolchain: "aptget"
            });
            assert.equal(this.args.Value(), "package-linux-aptget");
        }

        /* tslint:enable */

        protected setUp() : void {
            this.args = new BuildProductArgs();
        }
    }
}
